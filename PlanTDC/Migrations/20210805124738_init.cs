﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PlanTDC.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Plans",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LogDateTime = table.Column<string>(type: "TEXT", nullable: true),
                    PlanOne = table.Column<string>(type: "TEXT", nullable: true),
                    PlanTwo = table.Column<string>(type: "TEXT", nullable: true),
                    PlanThree = table.Column<string>(type: "TEXT", nullable: true),
                    PlanFour = table.Column<string>(type: "TEXT", nullable: true),
                    PlanFive = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Plans");
        }
    }
}
