﻿using Microsoft.AspNetCore.Mvc;
using PlanTDC.EF;
using PlanTDC.Models;
using System;
using System.Diagnostics;

namespace PlanTDC.Controllers
{
    public class HomeController : Controller
    {
        //然后，PlanContext 可以通过构造函数注入在 ASP.NET Core 控制器或其他服务中使用
        private readonly PlanContext _context;

        public HomeController(PlanContext context)
        {
            _context = context;
        }

        //private readonly IDbContextFactory<PlanContext> _contextFactory;

        ////可以通过构造函数注入在其他服务中使用 DbContextFactory 工厂
        //public HomeController(IDbContextFactory<PlanContext> contextFactory)
        //{
        //    _contextFactory = contextFactory;
        //}

        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Plan(Plan plan)
        {
            plan.LogDateTime = string.Format("{0:G}", plan.LogDateTime);

            //可以使用注入的工厂在服务代码中构造 DbContext 实例
            _context.Plans.Add(plan);

            if (_context.SaveChanges() > -1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                throw new Exception();
            }



        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
