﻿using Microsoft.EntityFrameworkCore;
using PlanTDC.Models;

namespace PlanTDC.EF
{
    public class PlanContext : DbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context PlanContext -OutputDir Migrations[name] Add-Migration -Context Tool -OutputDir Context\Migrations_Tool[name]

        //回滚迁移
        //Remove-Migration  -Context Tool

        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Tool

        //删库
        //Drop-Database  -Context Tool

        //生成SQL脚本，来手动迁移         
        //Script-Migration -Context                    

        //获取帮助
        //get-help entityframework

        //资料来源
        //https://docs.microsoft.com/zh-cn/ef/core/dbcontext-configuration/

        public PlanContext(DbContextOptions<PlanContext> options) : base(options)
        {

        }

        public DbSet<Plan> Plans { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}