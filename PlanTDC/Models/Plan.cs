﻿namespace PlanTDC.Models
{
    public class Plan
    {
        public int ID { get; set; }

        //日志日期
        public string LogDateTime { get; set; }

        //计划一
        public string PlanOne { get; set; }

        //计划二
        public string PlanTwo { get; set; }

        //计划三
        public string PlanThree { get; set; }

        //计划四
        public string PlanFour { get; set; }

        //计划五
        public string PlanFive { get; set; }
    }
}
