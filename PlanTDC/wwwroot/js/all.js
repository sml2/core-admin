$(function() {

	// $('#datepicker,input.date').focus(function() {
	// 	laydate.render({
	// 		elem: '#datepicker,input.date' //指定元素
	// 	});
	// })

	// layui日期选择器
	//执行一个laydate实例
	laydate.render({
		elem: '#datepicker,input.date' //指定元素
	});

	// layui日期选择器
	// layui.use('laydate', function() {
	// 	var laydate = layui.laydate;

	// 	//执行一个laydate实例
	// 	laydate.render({
	// 		elem: 'input.date' //指定元素
	// 	});
	// });

	// $("#datepicker").datepicker({
	// 	// 格式日期
	// 	dateFormat: "yy-mm-dd",
	// 	// 是否显示done
	// 	showButtonPanel: true,
	// 	//日期面板显示动画
	// 	showAnim: "blind"
	// });

	// $("input.date").datepicker({
	// 	// 格式日期
	// 	dateFormat: "yy-mm-dd",
	// 	// 是否显示done
	// 	showButtonPanel: true,
	// 	//日期面板显示动画
	// 	showAnim: "blind"
	// });

	//实现layui富文本编辑
	// layui.use('layedit', function() {
	// 	var layedit = layui.layedit;
	// 	layedit.set({
	// 		uploadImage: {
	// 			url: '/upload/' //接口url
	// 				,
	// 			type: 'post' //默认post
	// 		}
	// 	});

	// 	//注意：layedit.set 一定要放在 build 前面，否则配置全局接口将无效。
	// 	//构建一个默认的编辑器 自定义工具Bar
	// 	var index = layedit.build('demo', {
	// 		tool: [
	// 			'strong' //加粗
	// 			, 'italic' //斜体
	// 			, 'underline' //下划线
	// 			, 'del' //删除线
	// 			, '|' //分割线
	// 			, 'left' //左对齐
	// 			, 'center' //居中对齐
	// 			, 'right' //右对齐
	// 			, 'link' //超链接
	// 			, 'unlink' //清除链接
	// 			, 'face' //表情
	// 			, 'image' //插入图片
	// 			, 'help' //帮助
	// 		]
	// 	});

	// 	//编辑器外部操作
	// 	var active = {
	// 		content: function() {
	// 			alert(layedit.getContent(index)); //获取编辑器内容
	// 		},
	// 		text: function() {
	// 			alert(layedit.getText(index)); //获取编辑器纯文本内容
	// 		},
	// 		selection: function() {
	// 			alert(layedit.getSelection(index));
	// 		}
	// 	};

	// 	$('.site-demo-layedit').on('click', function() {
	// 		var type = $(this).data('type');
	// 		active[type] ? active[type].call(this) : '';
	// 	});
	// });

	//点击文本框包含块 文本框获取焦点
	$('#content div.inputParent').click(function() {
		$(this).children().focus();
	})

	//获取文本框的父元素的宽度
	var parentWidth = 0;
	//获取文本框的初始宽度
	var inputTextWidth = $('#content div.inputParent input[type="text"]').outerWidth();
	//文本框获得焦点
	$('#content div.inputParent input[type="text"]').focus(function() {
		//获取父元素的宽度
		parentWidth = $(this).parent().outerWidth();
		//修改样式
		$(this).parent().css({
			'padding': '0px',
			'background-color': '#f2f2f2',
		});
		$(this).css({
			'padding': '8px 12px 6px',
			'width': `${ parentWidth }px`,
			'border': '1px solid #1E88E5',
		}).addClass('test');
	})

	//当文本框一行塞不下文本的时候 文本框换行 //笑死居然想的出来让文本框的高度变大实现换行 hahhahahahaha
	// $('#content div.inputParent input[type="text"]').keyup(function() {
	// 	$(this)[0].style.height *= Math.ceil(textWidth($(this).val()) / parentWidth - 32);
	// 	$(this).parent()[0].style.height *= Math.ceil(textWidth($(this).val()) / parentWidth - 32);
	// })

	//文本框失去焦点
	$('#content div.inputParent input[type="text"]').blur(function() {
		//获取失去焦点的文本框
		var inputText = $(this);
		//还原样式
		inputText.parent().css({
			'padding': '8px 12px 6px',
			'background-color': '#f7f7f7',
			'border': '1px solid transparent'
		});
		inputText.css({
			'padding': '0px',
			'border': '1px solid transparent'
		}).removeClass('test');

		//当文本框失去焦点之后 判断用户是否向文本框输入值
		if (inputText.val().length === 0) {
			inputText.outerWidth(inputTextWidth); //当用户不输入值的时候
		} else {
			inputText.width(textWidth(inputText.val())); //当用户输入值的时候 注意设置的是文本框的宽度 并不是外宽
		}
	})

	//获取文本宽度
	var textWidth = function(text) {
		var sensor = $('<pre>' + text + '</pre>').css({
			'display': 'none'
		});

		$('body').append(sensor);

		//获取文本框的字符间距
		// var letterSpacing = $('#content div.inputParent input[type="text"]').attr('letterSpacing');

		// console.log(letterSpacing);

		var width = sensor.width(); //需要加上字符间距

		sensor.remove();

		// return width;

		return width >= parentWidth - 26 ? parentWidth - 26 : width;
	};

	//悬浮文本框父元素 文本框也一起变色
	$('#content div.inputParent').hover(
		function() {
			$(this).css(
				'background-color', '#f2f2f2'
			);
			$(this).children().css(
				'background-color', '#f2f2f2'
			)
		},
		function() {
			$(this).css(
				'background-color', '#f7f7f7'
			);
			$(this).children().css(
				'background-color', '#f7f7f7'
			)
		}
	)

	//实现悬浮感叹号tips
	$('span.iconfont').mouseover(function(e) {
		//获取当前被点击感叹号元素
		var element = $(e.target);

		//测试方法对不对
		// console.log(element.attr("class").split(' ')[0]);

		//呈现出tip层
		layer.tips(`${element.attr('title')}`, '#' + `${element.attr("id")}`, {
			//tips位置：1上；2右；3下；4左
			tips: [1, '#383838'] //还可配置颜色
		});

	});

	//实现Rate评分
	var rating = $('div.rating');

	var leftRating = rating.offset().left,
		width = rating.width(),
		clickWidth = 0;

	rating.mouseover(function(e) {
		var overWidth = parseInt(((e.pageX - leftRating) / width) * 100, 10);
		$(this).find('a').css({
			'width': (overWidth + '%')
		});
	});
	rating.mouseout(function() {
		$(this).find('a').css({
			'width': (clickWidth) + '%'
		});
	});
	rating.mousemove(function(e) {
		var hoverWidth = parseInt(((e.pageX - leftRating) / width) * 100, 10);
		$(this).find('a').css({
			'width': (hoverWidth + '%')
		});
	});
	rating.click(function(e) {
		clickWidth = parseInt(((e.pageX - leftRating) / width) * 100, 10);
		$(this).find('a').css({
			'width': (clickWidth + '%')
		});
		//获取当前点击的元素 然后定位到要修改数值的元素
		$(e.target).next('div.score').find('em').text(clickWidth);
	});

	//点击放大按钮
	// $('#container div.fangda').click(function() {
	// 	$('#container').css({
	// 		"width": "0px"
	// 	})
	// })
});
