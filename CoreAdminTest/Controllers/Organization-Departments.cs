﻿using CoreAdminTestADY.Models.Organization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreAdminTest.Controllers
{
    partial class Organization
    {
        /// <summary>
        /// 获取部门信息列表
        /// </summary>
        /// <returns></returns>
        //SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/list#c98c3220
        [Route("[action]")]
        public async Task<string> Departments()
        {
            if (DBContext.Departmentsitem.Any())
            {
                return "Already Has Data";
            }
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments?department_id_type=department_id&fetch_child=true&page_size=50&parent_department_id=0&user_id_type=user_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Get, url);                          //定义请求参数
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    var json = JObject.Parse(str);
                    //if (json["code"].ToString() == "0")
                    if (json["code"].ToObject<int>() == 0)
                    {
                        var data = json["data"].ToObject<DepartmentsData>();
                        var ds = data.items;

                        var Us = new HashSet<UsersItem>();
                        foreach (var d in ds)
                        {
                            var us = await Users(d);
                            if (us != null && us.Length > 0)
                            {
                                foreach (var u in us)
                                {
                                    Us.Add(u);
                                }
                            }
                        }
                        DBContext.AddRange(Us.ToArray());
                        DBContext.AddRange(ds);
                        DBContext.SaveChanges();
                    }
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {
                return e.Message;
            }
        }
        #region 暂时用不到
        /// <summary>
        /// 创建部门
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/create
        [Route("[action]")]
        public async Task<string> DepartmentCreate()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Post, url);                          //定义请求参数
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        /// <summary>
        /// 获取父部门的部门信息
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/parent
        [Route("[action]")]
        public async Task<string> ParentDepartment()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments/parent?department_id=od-b1f6b6dcd3add49f5dc5be284244b99c";            //定义URL      必须要有ID
            var request = new HttpRequestMessage(HttpMethod.Get, url);                                                                              //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                                                                           //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求

                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    var json = JObject.Parse(str);
                    //if (json["code"].ToString() == "0")
                    if (json["code"].ToObject<int>() == 0)
                    {
                        var data = json["data"].ToObject<DepartmentsData>();
                    }
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        /// <summary>
        /// 获取单个部门的部门信息
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/get
        [Route("[action]")]
        public async Task<string> SingleDepartment()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments?fetch_child=true&parent_department_id=0";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Get, url);                                                                   //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                                                                //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                                                        //发送请求
                if (response.IsSuccessStatusCode)                                                                                      //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    var json = JObject.Parse(str);
                    //if (json["code"].ToString() == "0")
                    if (json["code"].ToObject<int>() == 0)
                    {
                        var data = json["data"].ToObject<DepartmentsData>();
                    }
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        /// <summary>
        /// 搜索部门
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/search
        [Route("[action]")]
        public async Task<string> SearchDepartment()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments/search";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Post, url);                          //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }

        /// <summary>
        /// 修改部门
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/patch
        [Route("[action]")]
        public async Task<string> ModifyDepartment()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments/:department_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Patch, url);                                       //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                                          //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                                  //发送请求
                if (response.IsSuccessStatusCode)                                                                //判断响应是否成功
                {
                    //response.Headers;                                                                         
                    //Response.Headers;                                                                         
                    //using var responseStream = await response.Content.ReadAsStreamAsync();                     //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                                           //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        /// <summary>
        /// 更新部门所有信息
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/update
        [Route("[action]")]
        public async Task<string> UpdateDepartment()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments/:department_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Put, url);                          //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        /// <summary>
        /// 删除部门
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/delete
        [Route("[action]")]
        public async Task<string> DeleteDepartment()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/departments/od-871d27d44759ae800eef3b9ac952d72d";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Delete, url);                                   //定义请求参数
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        #endregion
    }
}
