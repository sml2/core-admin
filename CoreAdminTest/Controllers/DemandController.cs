﻿using CoreAdminTest;
using CoreAdminTest.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoreAdminTestADY.Controllers
{
     [Route("[controller]")]
    public class DemandController : Controller
    {      
          private readonly Demands DBContext;
          public DemandController(Demands Demand)
          {
             DBContext=Demand;
          }
       [Route("{action}")]
        public IActionResult Index()
        {
            if (Request.Method.ToUpper().Equals("POST"))
            {
                Demand Demandl = new Demand();
                Demandl.Title = Request.Form["Title"];
                Demandl.Content = Request.Form["Content"];
                Demandl.Feedback = Request.Form["Feedback"];
                Demandl.IsCkecked = int.Parse(Request.Form["IsCkecked"]);
                DBContext.DemandsNeed.Add(Demandl);
                if (DBContext.SaveChanges() > 1)
                {
                    return View("Ok");
                }
                else
                {
                    return View("No");
                }
            }
            else {
                return View();
            }
        }
    }
}
