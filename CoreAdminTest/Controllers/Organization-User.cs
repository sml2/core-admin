﻿using CoreAdminTestADY.Models.Organization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

using System.Net.Http;
using System.Threading.Tasks;

namespace CoreAdminTest.Controllers
{
    partial class Organization
    {
        /// <summary>
        /// 获取用户列表
        /// </summary><
        /// <returns></returns>
        /// SDK: https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/list
        [Route("[action]")]
        public async Task<UsersItem[]> Users(DepartmentsItem departmentsItem)
        {
            //var url = "https://open.feishu.cn/open-apis/contact/v3/users";                 //定义URL
            var url = $"https://open.feishu.cn/open-apis/contact/v3/users?department_id={departmentsItem.department_id}&department_id_type=department_id&user_id_type=user_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Get, url);                          //定义请求参数
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            var response = await client.SendAsync(request);                                 //发送请求
            if (response.IsSuccessStatusCode)                                               //判断响应是否成功
            {
                //response.Headers;
                //Response.Headers;
                //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                var str = await response.Content.ReadAsStringAsync();
                var json = JObject.Parse(str);
                //if (json["code"].ToString() == "0")
                if (json["code"].ToObject<int>() == 0)
                {
                    var data = json["data"].ToObject<UsersData>();
                    return data.items;
                }
                return null;
            }
            else
            {
                return null;
            }

        }
        #region "暂时用不到" 
        /// <summary>
        /// 获取单个用户信息
        /// </summary>
        /// <returns></returns>
        /// SDK: https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/get
        [Route("[action]")]
        public async Task<string> SingleUser()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/users/";                 //定义URL     调用open_id
            var request = new HttpRequestMessage(HttpMethod.Get, url);                      //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    var json = JObject.Parse(str);
                    //if (json["code"].ToString() == "0")
                    if (json["code"].ToObject<int>() == 0)
                    {
                        var data = json["data"].ToObject<UsersData>();
                    }
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }

        [Route("[action]")]
        /// <summary>
        /// 创建用户(没权限)
        /// </summary>
        /// <returns></returns>
        /// SDK: https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/create
        public async Task<string> CreateUsers()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/users";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Post, url);                     //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }

        /// <summary>
        /// 修改用户部分信息
        /// </summary>
        /// <returns></returns>
        /// SDK: https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/patch
        [Route("[action]")]
        public async Task<string> PatchUser()
        {
            var url = "	https://open.feishu.cn/open-apis/contact/v3/users/:user_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Patch, url);                          //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }

        /// <summary>
        /// 更新用户部分信息
        /// </summary>
        /// <returns></returns>
        /// SDK: https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/update
        [Route("[action]")]
        public async Task<string> UpdateUser()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/users/:user_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Put, url);                          //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }

        /// <summary>
        /// 删除用户部分信息(没权限)
        /// </summary>
        /// <returns></returns>
        /// SDK: https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/delete
        [Route("[action]")]
        public async Task<string> DeleteUser()
        {
            var url = "https://open.feishu.cn/open-apis/contact/v3/users/:user_id";                 //定义URL
            var request = new HttpRequestMessage(HttpMethod.Delete, url);                          //定义请求参数
            //定义请求的Header
            if (string.IsNullOrEmpty(access_token))
            {
                return "fail:access_token IsNullOrEmpty";
            }
            var Authorization = $"Bearer {access_token}";
            request.Headers.Add("Authorization", Authorization);

            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.SendAsync(request);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    //response.Headers;
                    //Response.Headers;
                    //using var responseStream = await response.Content.ReadAsStreamAsync();    //从响应内容中获取流
                    //await responseStream.CopyToAsync(Response.Body);                          //重定向流做返回值(失败，原因是Header头没有复制过去)
                    var str = await response.Content.ReadAsStringAsync();
                    return "ok:" + str;
                }
                else
                {
                    return "fail";
                }
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
        }
        #endregion
    }
}
