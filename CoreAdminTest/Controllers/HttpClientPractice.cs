﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using CoreAdmin.Mvc.Actions;

namespace CoreAdminTestADY.Controllers
{
    [Route("[controller]")]
    public class HttpClientPractice : Controller
    {
        private readonly IHttpClientFactory _ClientFactory;
        //IHttpClientFactory
       public HttpClientPractice(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }
        [Route("[action]")]
        public async Task<string> Index()
        {
            var url = "https://www.baidu.com/";

            var request = new HttpRequestMessage(HttpMethod.Get, url);

            var client = _ClientFactory.CreateClient();
            var respons = await client.SendAsync(request);
            if (respons.IsSuccessStatusCode)
{
                using var responseStream = await respons.Content.ReadAsStreamAsync();
            
                //if (ContentLength!="")
                //{

                //    return "1";
                //}
                return "2";
            }
            else
            {

                return "0";
            }
            return "返回";
        }
    }
}
