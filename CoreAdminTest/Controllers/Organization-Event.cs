﻿using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using CoreAdminTestADY.Models.Organization;
using System.Linq;

namespace CoreAdminTest.Controllers
{
     partial class Organization
    {
        /// <summary>
        /// 创建部门
        /// </summary>
        /// <returns></returns>
        /// SDK:https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/create
        [Route("[action]")]
        public async Task<string> Event(JsonRequest jsonRequest)
        {
            try
            {
                const string Version = "2.0";
                //获取http/https请求的请求体的内容
                if (jsonRequest.Schema!= null && jsonRequest.Schema.Equals(Version))
                {
                    if (jsonRequest.Header != null)
                    {
                        var event_type = jsonRequest.Header.event_type;
                        switch (event_type)
                        {
                            //部门创建
                            case "contact.department.created_v3":
                                var cd = jsonRequest.Event["data..xxx"].ToObject<DepartmentsItem>();
                                DBContext.Add(cd);
                                DBContext.SaveChanges();
                                break;
                            //部门被删除
                            case "contact.department.deleted_v3":
                                var dd = jsonRequest.Event["data..xxx"].ToObject<DepartmentsItem>();
                                var ddid = dd.department_id;
                                var dent = DBContext.Departmentsitem.Where(m => m.department_id == ddid).First();
                                if (dent == null)
                                {
                                    throw new NotSupportedException();
                                }
                                else {
                                    dent.is_deleted = true;
                                    DBContext.SaveChanges();
                                }
                                break;
                            //修改部门
                            case "contact.department.updated_v3":
                                var ud = jsonRequest.Event["data..xxx"].ToObject<DepartmentsItem>();
                                var udid = ud.department_id;
                                var uent = DBContext.Departmentsitem.Where(m => m.department_id == udid).First();
                                //uent.setData(ud);
                                DBContext.SaveChanges();
                                break;
                            //员工入职
                            case "contact.user.created_v3":
                                break;
                            //员工离职
                            case "contact.user.deleted_v3":
                                break;
                            //员工变更
                            case "contact.user.updated_v3":
                                break;
                            case "xxxxx":
                                break;
                            default: throw new ArgumentOutOfRangeException(nameof(jsonRequest.Header.event_type),$"can not deal with [{event_type}]");
                        }
                    }
                    else 
                    {
                        throw new ArgumentNullException(nameof(jsonRequest.Header),$"Header Must Has Value");
                    }
                }
                else
                {
                    throw new ArgumentNullException(nameof(jsonRequest.Schema),$"Schema Must Be {Version}");
                }
            }
            catch (Exception)
            {
                //此处应当写日志记录Request信息，并修复接收逻辑，最后用PostMan模拟飞书服务器重新发送
            }
            return "ok";
        }
        public class JsonRequest
        {
            public string Schema { get; set; }
            public HeaderData Header { get; set; }
            public JObject Event { get; set; }
            public class HeaderData
            {
                public string event_id { get; set; }
                public string event_type { get; set; }
                public string create_time { get; set; }
                public string token { get; set; }
                public string app_id { get; set; }
                public string tenant_key { get; set; }
            }
        }
    }
}
