﻿using CoreAdminTestADY;
using CoreAdminTestADY.Models.Organization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoreAdminTest.Controllers
{
    [Route("[controller]")]
    public partial class Organization : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly Organizations DBContext;
        private string _access_token;
        private string access_token
        {
            get
            {
                if (_access_token == null)
                {
                    string app_id = "cli_a064312eee38d00d";
                    string app_secret = "XOOTSC1ZEAlyHLMGLkURccWevTgt1dcs";
                    _access_token = getAccessToken(app_id, app_secret).Result;
                }
                return _access_token;
            }
        }
        public Organization(IHttpClientFactory clientFactory, Organizations organizations)
        {
            _clientFactory = clientFactory;
            DBContext = organizations;
        }
        #region "AccessToken"

        class AccessToken
        {
            public int code { get; set; }                   //错误码，非 0 表示失败
            public string msg { get; set; }                 //错误描述

            public string app_access_token { get; set; } //访问 token

            public string tenant_access_token { get; set; } //访问 token
            public int expire { get; set; }                 //tenant_access_token 过期时间 ，单位为秒
        }
        //Desc https://open.feishu.cn/document/ukTMukTMukTM/ukDNz4SO0MjL5QzM/g#top_anchor
        //SDK https://open.feishu.cn/document/ukTMukTMukTM/uIjNz4iM2MjLyYzM
        //var url = "https://open.feishu.cn/open-apis/auth/v3/tenant_access_token/internal/";                 //定义URL
        private async Task<string> getAccessToken(string app_id, string app_secret)
        {
            var url = "https://open.feishu.cn/open-apis/auth/v3/app_access_token/internal/";                 //定义URL
            var data = new { app_id = app_id, app_secret = app_secret };
            var json = JsonSerializer.Serialize(data);
            var reqdata = new StringContent(json, Encoding.UTF8, "application/json");
            //request.Headers.Add("Content-Type", "application/json; charset=utf-8");//?StringContent指定了，此处是否可以省略
            var client = _clientFactory.CreateClient();                                         //创建Client对象
            try
            {
                var response = await client.PostAsync(url, reqdata);                                 //发送请求
                if (response.IsSuccessStatusCode)                                               //判断响应是否成功
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var AccessToken = await JsonSerializer.DeserializeAsync<AccessToken>(responseStream);
                    if (AccessToken != null)
                    {
                        return AccessToken.app_access_token ?? AccessToken.tenant_access_token;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("AccessToken Is Null");
                        return null;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("response Fail");
                    return null;
                }
            }
            catch (System.Exception e)
            {
                System.Diagnostics.Debug.WriteLine($"Error {e.Message}");
                return null;
            }
        }
        #endregion
    }
}
