﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdminTestADY.Models.Organization
{
    public class Rootobject
    {
        public int code { get; set; }
        public DepartmentsData data { get; set; }
        public string msg { get; set; }
    }
}
