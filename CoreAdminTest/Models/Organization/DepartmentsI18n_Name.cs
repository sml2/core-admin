﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdminTestADY.Models.Organization
{
    public class DepartmentsI18n_Name
    {
        public string en_us { get; set; }
        public string ja_jp { get; set; }
        public string zh_cn { get; set; }
    }
}
