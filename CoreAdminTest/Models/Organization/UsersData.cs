﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdminTestADY.Models.Organization
{
    public class UsersData
    {
        public bool has_more { get; set; }
        public UsersItem[] items { get; set; }
    }
}
