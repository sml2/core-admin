﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdminTestADY.Models.Organization
{
    public class UsersItem: IEqualityComparer<UsersItem>
    {
        public int id { get; set; }
        [NotMapped]
        public UsersAvatar avatar { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string city { get; set; }
        /// <summary>
        /// 国家
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// 用户所属部门的ID列表
        /// </summary>
        [NotMapped]
        public string[] department_ids { get; set; }
        public string description { get; set; }
        public string email { get; set; }
        /// <summary>
        /// 工号
        /// </summary>
        public string employee_no { get; set; }
        /// <summary>
        /// 头像文件key
        /// </summary>
        public string avatar_key { get; set; }
        /// <summary>
        /// 用户的直接主管的用户ID
        /// </summary>
        public string leader_user_id { get; set; }  
        /// <summary>
        /// 员工类型  <code>示例值：1</code>
        /// </summary>
        public int employee_type { get; set; }
        /// <summary>
        /// 英文名字
        /// </summary>
        public string en_name { get; set; }
        public int gender { get; set; }
        public bool is_tenant_manager { get; set; }
        public string job_title { get; set; }
        /// <summary>
        /// 入职时间 <code>示例值：2147483647</code>
        /// </summary>
        public int join_time { get; set; }
        public string mobile { get; set; }
        /// <summary>
        ///  ?? 0 保密  1：男  2：女
        /// </summary>
        public bool mobile_visible { get; set; }
        public string name { get; set; }
        public string open_id { get; set; }
        [NotMapped]
        public Order[] orders { get; set; }
        [NotMapped]
        public Status status { get; set; }
        public string union_id { get; set; }
        public string user_id { get; set; }
        /// <summary>
        /// 工位
        /// </summary>
        public string work_station { get; set; }

        public bool Equals(UsersItem x, UsersItem y)
        {
            return x.Equals(y);
        }

        public int GetHashCode([DisallowNull] UsersItem obj)
        {
            return obj.GetHashCode();
        }
        public override int GetHashCode()
        {
            return user_id.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj is UsersItem other && user_id != null)
            {
                return user_id.Equals(other.user_id);
            }
            return false;
        }
        public class Status
        {
            public bool is_activated { get; set; }
            public bool is_frozen { get; set; }
            public bool is_resigned { get; set; }
        }
        public class UsersAvatar
        {
            /// <summary>
            /// 72*72像素头像链接
            /// </summary>
            public string avatar_72 { get; set; }  
            /// <summary>
            /// 240*240像素头像链接
            /// </summary>
            public string avatar_240 { get; set; } 
            /// <summary>
            /// 640*640像素头像链接
            /// </summary>
            public string avatar_640 { get; set; } 
            /// <summary>
            /// 原始头像链接
            /// </summary>
            public string avatar_origin { get; set; } 
        }
        /// <summary>
        /// 用户归属信息
        /// </summary>
        public class Order
        {
            /// <summary>
            /// 排序信息对应的部门ID  <code>示例值："od-4e6ac4d14bcd5071a37a39de902c7141"</code>
            /// </summary>
            public string department_id { get; set; } 
            /// <summary>
            /// 用户在其直属部门内的排序，数值越大，排序越靠前 示例值：100
            /// </summary>
            public int user_order { get; set; }
            /// <summary>
            /// 用户所属的多个部门间的排序，数值越大，排序越靠前  示例值：100
            /// </summary>
            public int department_order { get; set; }  

        }
    }
    
}
