﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdminTestADY.Models.Organization
{
    public class DepartmentsItem
    {
        public int id { get; set; }
        public string chat_id { get; set; }
        public string department_id { get; set; }
        [NotMapped]
        public DepartmentsI18n_Name i18n_name { get; set; }
        public string leader_user_id { get; set; }
        public int member_count { get; set; }
        public string name { get; set; }
        public string open_department_id { get; set; }
        public string order { get; set; }
        public string parent_department_id { get; set; }
        [NotMapped]
        public Status status { get; set; }
        //仅数据库使用
        public bool is_deleted { get=> status.is_deleted; set=> status.is_deleted =value; }
        public override string ToString() => $"{name}({status})";
        public class Status
        {
            public bool is_deleted { get; set; }
            public override string ToString() =>$"{(is_deleted ? "已" : "未")}删除";
        }
    }

}
