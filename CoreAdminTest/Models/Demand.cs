﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdminTest.Models
{
    public class Demand
    {
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 需求
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 反馈建议
        /// </summary>
        public string Feedback { get; set; }
        /// <summary>
        /// 单选 1待评估 2待开发 3待回复 4开发中 5待测试(已完成) 6待上线(已测试) 7已完成 8已延期 9已搁置
        /// </summary>
        public int IsCkecked { get; set; }
    }
}
