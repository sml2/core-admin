﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace CoreAdminTest.Models
{
    public class Config : BaseModel, IHasTimestamps
    {
        public  Config(){
            System.Diagnostics.Debug.WriteLine($"{typeof(Config).Name} Created");
        }
        [Display(Name = "编号")]
        public int ID { get; set; }
        [Display(Name ="名称")]
        public string Name { get; set; }
        [Display(Name = "路径")]
        public string Path { get; set; }
        [Display(Name = "启用")]
        public bool Open { get; set; }
        [Display(Name = "安卓地址")]
        public string Android_Address { get; set; }
        [Display(Name = "安卓访问量")]
        public int Android_Count { get; set; }
        [Display(Name = "苹果地址")]
        public string IOS_Address { get; set; }
        [Display(Name = "苹果访问量")]
        public int IOS_Count { get; set; }
        [Display(Name = "无效访问量")]
        public int Invalid_Count { get; set; }
        [Display(Name = "备注说明")]
        public string Description { get; set; }
        [Display(Name = "创建时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Display(Name = "更新时间")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
