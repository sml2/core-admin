﻿using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc;
using CoreAdmin.EF;
using CoreAdminTestADY.Models.Organization;

namespace CoreAdminTestADY
{
    public class Organizations : BaseDbContext, IAdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Organizations -OutputDir Context\Migrations_Organizations [name]
        //回滚迁移
        //Remove-Migration  -Context Organizations
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Organizations
        //删库
        //Drop-Database  -Context Organizations
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Tool
        //获取帮助
        //get-help entityframework
        //20210720034538_DepartmentsData
        public DbSet<DepartmentsItem> Departmentsitem { get; set; }
        public DbSet<UsersItem> userItem { get; set; }


        public Organizations(DbContextOptions<Organizations> options) : base(options)
        {
        }
    }
}
