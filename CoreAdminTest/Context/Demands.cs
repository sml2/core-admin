﻿using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc;
using CoreAdmin.EF;
using CoreAdminTest.Models;
using System;

namespace CoreAdminTest
{
    public class Demands : BaseDbContext, IAdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Demands -OutputDir Context\Migrations_Demands [name]
        //回滚迁移
        //Remove-Migration  -Context Demands
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Demands
        //删库
        //Drop-Database  -Context Demands
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Demands
        //获取帮助
        //get-help entityframework
        //20210720034538_DepartmentsData
        public DbSet<Demand> DemandsNeed { get; set; }

        public Demands(DbContextOptions<Demands> options) : base(options)
        {
        }

    }
}
