﻿
using System;
using CoreAdmin.EF;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using CoreAdminTest.Models;

namespace CoreAdminTest.EF
{
    public class Admin : AdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Admin -OutputDir Context\Migrations_Admin [name]
        //回滚迁移
        //Remove-Migration
        //执行迁移 没有数据库会自动创建
        //Update-Database
        //删库
        //Drop-Database
        //生成SQL脚本，来手动迁移
        //Script-Migration
        //获取帮助
        //get-help entityframework


        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source=CoreAdmin.db");

        public DbSet<Config> Configs { get; set; }
        public Admin(DbContextOptions<Admin> options) : base(options)
        {
            Initial();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MenuModel>().HasData(
                    new MenuModel { Id = 9, ParentID = 0, Title = "Test", Uri = "/config", Icon = "fa-500px", Order = 8 }
            );
        }
    }
}
