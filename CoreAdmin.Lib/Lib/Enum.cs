﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Lib
{
    public static class Enum
    {
        /// <summary>
        /// 获取枚举中的所有值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// TODO: 添加动态编译，优化开销
        public static IEnumerable<T> GetItems<T>() where T : System.Enum
        {
            return typeof(T).GetEnumValues().Cast<T>();
        }

        /// <summary>
        /// 将枚举转换为字典
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static Dictionary<int, string> ToDictionary<TEnum>() where TEnum : struct, System.Enum
        {
            return System.Enum.GetValues(typeof(TEnum)).Cast<int>().ToDictionary(x => (int)x, x => System.Enum.GetName(typeof(TEnum), x));
        }

        [Obsolete("必须指明3个类型，太麻烦")]
        internal static Dictionary<TKey, TValue> ToDictionary<TEnum, TKey, TValue>(Func<TEnum, TKey> func1, Func<TEnum, TValue> func2) where TEnum : struct,System.Enum
        {
            return GetItems<TEnum>().ToDictionary(func1, func2);
        }
    }
}