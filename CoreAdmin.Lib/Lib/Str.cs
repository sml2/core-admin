﻿using System;

namespace CoreAdmin.Lib
{
    
    
    public static class Str
    {
       /// <summary>
       /// 截取不能为空的标识符的方法
       /// </summary>
        public static string Uniqid(string prefix = null, bool moreEntropy = true)
        {
            if (string.IsNullOrEmpty(prefix))
                prefix = string.Empty;

            return !moreEntropy ? (prefix + Guid.NewGuid()).Substring(0, 13) : (prefix + Guid.NewGuid() + Guid.NewGuid()).Substring(0, 23);
        }
        
    }
}
