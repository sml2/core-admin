﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CoreAdmin.Lib
{

    /// <summary>
    /// <see cref="Property{T}"/>
    /// </summary>
    [Obsolete("性能差于泛型")]
    public static class Property
    {
        private static readonly Dictionary<string, Delegate> LambdaCache = new();
        /// <summary>
        /// 获取对象属性值
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static object Get<T>(T obj, string propertyName)
        {
            var propertyDelegate = (Func<T, object>)GetPropertyFunc(obj.GetType(), propertyName);

            return propertyDelegate?.Invoke(obj);
        }

        /// <summary>
        /// 获取 获取对象属性值的表达式
        /// </summary>
        /// <returns></returns>
        private static Delegate GetPropertyFunc(Type type, string propertyName)
        {
            var key = $"{type.FullName}.{propertyName}";
            if (LambdaCache.TryGetValue(key, out var func)) return func;

            var parameter = Expression.Parameter(type, "m");
            var property = type.GetProperty(propertyName);
            if (property == null) return LambdaCache[key] = null;
            var expressProp = Expression.Property(parameter, property.Name);
            var propertyDelegateExpression = Expression.Lambda(Expression.Convert(expressProp, typeof(object)), parameter);

            return LambdaCache[key] = propertyDelegateExpression.Compile();
        }
    }
    
    /// <summary>
    /// 高性能获取属性值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class Property<T>
    {
        private static readonly Dictionary<string, Delegate> LambdaCache = new();
        
        /// <summary>
        /// 获取对象属性值
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static object Get(T obj, string propertyName)
        {
            var propertyDelegate = GetPropertyFunc(obj.GetType(), propertyName);

            return propertyDelegate?.DynamicInvoke(obj);
        }
        
        /// <summary>
        /// 获取 获取对象属性值的Lambda表达式
        /// </summary>
        /// <returns></returns>
        private static Delegate GetPropertyFunc(Type type, string propertyName)
        {
            // TODO: 拼接字符串会占用一半的性能
            var key = $"{type.Name}.{propertyName}";

            if (LambdaCache.TryGetValue(key, out var func)) return func;

            var parameter = Expression.Parameter(type, "m");
            var property = type.GetProperty(propertyName);
            if (property == null) return LambdaCache[key] = null;

            var expressProp = Expression.Property(parameter, property.Name);
            var propertyDelegateExpression = Expression.Lambda(Expression.Convert(expressProp, typeof(object)), parameter);

            return LambdaCache[key] = propertyDelegateExpression.Compile();
        }
    }
}
