﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace CoreAdmin.Lib
{
    public static class Temp
    {
       /// <summary>
       /// 判断返回数据      方法用于调用
       /// </summary>
        public static ViewDataDictionary AddToViewData(Dictionary<string, dynamic> dic, ViewDataDictionary viewData)
        {
            if (dic == null)
            {
                return viewData;
            }
            var data = viewData;
            foreach (var (key, value) in dic)
            {
                data[key] = value;
            }
            return data;
        }
    }
}
