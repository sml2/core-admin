﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreAdmin.Extensions
{
    public static class StringExtension
    {
        private static Dictionary<string, string> _cacheStudly;
        /// <summary>
        /// 判断是否为url格式
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool IsValidUrl(this string url)
        {
            return url != null && (url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("mailto:") || url.StartsWith("tel:") || url.StartsWith("sms:"));
        }

        /// <summary>
        /// 首字母大写
        /// </summary>
        /// <param name="old"></param>
        /// <returns></returns>
        public static string UpperCaseFirst(this string old)
        {
            return old.First().ToString().ToUpper() + old[1..];
        }

        /// <summary>
        /// 将以横线分割的字符串合并首字母大写
        /// 如: abc_abc -> AbcAbc
        /// </summary>
        /// <param name="old"></param>
        /// <returns></returns>
        public static string Studly(this string old)
        {
            if (null == _cacheStudly) _cacheStudly = new ();
            if (_cacheStudly.ContainsKey(old))
            {
                return _cacheStudly[old];
            }
            string[] delemiter = { "-", "_" };
            return string.Join("", old.Replace(delemiter, " ").Split(" ").Select(s => s.UpperCaseFirst()));
        }

        /// <summary>
        /// 重复字符串n次
        /// </summary>
        /// <param name="old"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string Repeat(this string old, int num)
        {
            string str = "";
            for(var i = 0; i < num; i++)
            {
                str += old;
            }
            return str;
        }

        /// <summary>
        /// 将输入的字符串数组替换为新的字符串
        /// </summary>
        /// <param name="old"></param>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public static string Replace(this string old, string[] oldValue, string newValue)
        {
            foreach (var value in oldValue)
            {
                old = old.Replace(value, newValue);
            }
            return old;
        }

        private static Dictionary<string, string> _snakeCache;
        /// <summary>
        /// 将字符串变为蛇形
        /// 如: AbcAbc -> abc_abc
        /// </summary>
        /// <param name="old"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string Snake(this string old, char delimiter = '_')
        {
            var key = old + delimiter;
            if (_snakeCache == null) _snakeCache = new();
            if (_snakeCache.ContainsKey(key))
            {
                return _snakeCache[key];
            }
            StringBuilder builder = new();
            var index = 0;
            foreach (char ch in old)
            {
                if (index != 0 && ch >= 'A' && ch <= 'Z')
                {
                    builder.Append(delimiter);
                }

                builder.Append(char.ToLower(ch));
                index++;
            }
            return _snakeCache[key] = builder.ToString();
        }
    }
}
