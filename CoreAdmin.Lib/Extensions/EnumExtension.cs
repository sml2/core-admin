﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace CoreAdmin.Extensions
{
    public static class EnumExtension
    {
        /// <summary>
        /// 获取enum的备注
        /// </summary>
        /// <param name="entry">枚举</param>
        /// <returns>备注</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static string GetDescription(this Enum entry)
        {
            if (entry == null) throw new ArgumentNullException(nameof(entry));
            
            var type = entry.GetType();
            var member = type.GetMember(entry.ToString()).FirstOrDefault(m => m.DeclaringType == type);
            var descProp = member?.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
            return descProp?.Description;
        }
        
        /// <summary>
        /// 获取枚举的备注特性
        /// </summary>
        /// <param name="entry"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static T GetDescriptionAttribute<T>(this Enum entry) where T : Attribute
        {
            if (entry == null) throw new ArgumentNullException(nameof(entry));
            
            var type = entry.GetType();
            var member = type.GetMember(entry.ToString()).FirstOrDefault(m => m.DeclaringType == type);
            var descProp = member?.GetCustomAttribute(typeof(T)) as T;
            return descProp;
        }
        
    }

}