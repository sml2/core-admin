﻿using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;

namespace CoreAdmin.Extensions
{
    public static class LoggerExtension
    {
        /// <summary>
        /// 警告log,展示调用方信息
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="name"></param>
        /// <param name="line"></param>
        /// <param name="path"></param>
        public static void LogWarningInfo(this ILogger logger, string message, [CallerMemberName] string name = "", [CallerLineNumber] int line = 0, [CallerFilePath] string path = "")
        {
            logger.LogWarning($@"
FilePath  : {path}
LineNum   : {line}
CallerFunc: {name} 
Message   : {message}");
        }
    }
}
