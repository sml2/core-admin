﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Extensions
{
    public static class EnumerableExtension
    {
        /// <summary>
        /// 获取数组中与参照物最近的数
        /// </summary>
        /// <param name="array">数组</param>
        /// <param name="refer">参照物</param>
        /// <returns>最接近的数</returns>
        public static int Closest(this IEnumerable<int> array, int refer)
        {
            var diff = refer;
            int closer = default;
            foreach (var el in array)
            {
                var d = Math.Abs(refer - el);
                if (d >= diff) continue;
                closer = el;
                diff = d;
            }

            return closer;
        }
    }
}