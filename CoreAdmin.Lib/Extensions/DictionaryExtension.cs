﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace CoreAdmin.Extensions
{
    public static class DictionaryExtension
    {
        /// <summary>
        /// 合并字典，覆盖前面的数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic1"></param>
        /// <param name="dic2"></param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> Merge<TKey, TValue>(this Dictionary<TKey, TValue> dic1,
            Dictionary<TKey, TValue> dic2)
        {
            if (dic2 == null) return dic1;

            foreach (var (key, value) in dic2)
            {
                dic1[key] = value;
            }

            return dic1;
        }

        /// <summary>
        /// 合并字典，保留前面的数据
        /// </summary>
        /// <param name="dic1"></param>
        /// <param name="dic2"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> MergeSaveBefore<TKey, TValue>(this Dictionary<TKey, TValue> dic1,
            Dictionary<TKey, TValue> dic2)
        {
            foreach (var (key, value) in dic1)
            {
                dic2[key] = value;
            }

            return dic2;
        }

        /// <summary>
        /// 合并字典，保留前面的数据
        /// </summary>
        /// <param name="dic1"></param>
        /// <param name="dic2"></param>
        /// <returns></returns>
        public static OrderedDictionary MergeSaveBefore(this OrderedDictionary dic1, OrderedDictionary dic2)
        {
            foreach (DictionaryEntry pair in dic2)
            {
                dic1.Add(pair.Key, pair.Value);
            }

            return dic1;
        }

        /// <summary>
        /// 去除空值
        /// </summary>
        /// <param name="dic"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> RemoveEmpty<TKey, TValue>(this Dictionary<TKey, TValue> dic)
        {
            foreach (var (key, value) in dic)
            {
                if (value == null
                    || (value is string valueStr && string.IsNullOrEmpty(valueStr))
                    || (value is IEnumerable<dynamic> valueEnum && !valueEnum.Any())
                )
                {
                    dic.Remove(key);
                }
            }

            return dic;
        }
    }
}