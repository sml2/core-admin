﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace CoreAdmin.Extensions
{
    public static class TypeExtension
    {

#nullable enable
        private static string? GetDisplayName(this object[]? DisplayAttributes)
        {
            return (DisplayAttributes?.FirstOrDefault() is DisplayAttribute attr) ? attr.Name : string.Empty;
        }
        private static string? GetDisplayName(this FieldInfo? type)
        {
            return type?.GetCustomAttributes(typeof(DisplayAttribute), false).GetDisplayName();
        }
        private static string? GetDisplayName(this PropertyInfo? type)
        {
            return type?.GetCustomAttributes(typeof(DisplayAttribute), false).GetDisplayName();
        }
#nullable restore
        public static string GetDisplayName(this Type EnumType,Enum value)
        {
            return EnumType.GetField(value.ToString()).GetDisplayName(); ;
        }
        public static string GetDisplayName(this Type type, string name)
        {
            return type.GetProperty(name).GetDisplayName();
        }
        public static string GetDisplayName(this object type, string name)
        {
            return GetDisplayName(type.GetType(), name);
        }


    }
}