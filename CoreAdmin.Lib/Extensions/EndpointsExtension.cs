﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using E = Microsoft.AspNetCore.Http.Endpoint;
using IR = System.Collections.Generic.IReadOnlyList<Microsoft.AspNetCore.Http.Endpoint>;
using IE = System.Collections.Generic.IEnumerable<Microsoft.AspNetCore.Http.Endpoint>;

namespace CoreAdmin.Extensions
{
    public static class EndpointsExtension
    {
        #region "Test"
        //public static void Test(le Endpoints)
        //{
        //    //三个方法应对的需求其实不完全相同
        //    //================方法一二效率测试================
        //    var count = 10000;
        //    Stopwatch stopwatch = new();
        //    stopwatch.Start();
        //    for (int i = 0; i < count; i++)
        //    {
        //        _ = endPoints_1_0(Endpoints).Count();
        //    }
        //    stopwatch.Stop();
        //    Console.WriteLine($"1_0 Count:{endPoints_1_0(Endpoints).Count()},Elapsed:{stopwatch.ElapsedMilliseconds}");
        //    stopwatch.Restart();
        //    for (int i = 0; i < count; i++)
        //    {
        //        _ = endPoints_2_0(Endpoints).Count();
        //    }
        //    stopwatch.Stop();
        //    Console.WriteLine($"2_0 Count:{endPoints_2_0(Endpoints).Count()},Elapsed:{stopwatch.ElapsedMilliseconds}");
        //    stopwatch.Restart();
        //    for (int i = 0; i < count; i++)
        //    {
        //        _ = endPoints_2_1(Endpoints).Count();
        //    }
        //    stopwatch.Stop();
        //    Console.WriteLine($"2_1 Count:{endPoints_2_1(Endpoints).Count()},Elapsed:{stopwatch.ElapsedMilliseconds}");
        //    stopwatch.Restart();
        //    for (int i = 0; i < count; i++)
        //    {
        //        _ = endPoints_2_2(Endpoints).Count();
        //    }
        //    stopwatch.Stop();
        //    Console.WriteLine($"2_2 Count:{endPoints_2_2(Endpoints).Count()},Elapsed:{stopwatch.ElapsedMilliseconds}");
        //    stopwatch.Restart();
        //    for (int i = 0; i < count; i++)
        //    {
        //        _ = endPoints_3_0(Endpoints).Count();
        //    }
        //    stopwatch.Stop();
        //    Console.WriteLine($"3_0 Count:{endPoints_3_0(Endpoints).Count()},Elapsed:{stopwatch.ElapsedMilliseconds}");
        //    stopwatch.Restart();
        //    for (int i = 0; i < count; i++)
        //    {
        //        _ = endPoints_3_1(Endpoints).Count();
        //    }
        //    stopwatch.Stop();
        //    Console.WriteLine($"3_1 Count:{endPoints_3_1(Endpoints).Count()},Elapsed:{stopwatch.ElapsedMilliseconds}");
        //}
        /////*  三次测试结果
        ////   1_0 Count:49,Elapsed:406 411 468
        ////   2_0 Count:50,Elapsed:391 387 392
        ////   2_1 Count:50,Elapsed:376 388 390
        ////   2_2 Count:50,Elapsed:380 374 376
        ////   3_0 Count:90,Elapsed:421 411 431
        ////   3_1 Count:90,Elapsed:227 229 222
        ////*/
        #endregion
        /// <summary>
        /// 只读委托静态获取HttpGetAttribute方法
        /// </summary>
        //================方法一================HttpGetAttribute
        static readonly Func<IR, IE>  endPoints_1_0 = (Endpoints) => Endpoints.Where(e => e.Metadata.Any(m => m.GetType().Name.Equals(nameof(HttpGetAttribute))));
        //================方法二================HttpGetAttribute,endpoints.MapGet
        static readonly Func<IR, IE> endPoints_2_0 = (Endpoints) => Endpoints.Where(e => e.Metadata.Any(m =>
        {
            if (m is IHttpMethodMetadata ihttps)
            {
                return ihttps.HttpMethods.Contains("GET");
            }
            return false;
        }));
        // ||||||||||||||||||||||||
        static readonly Func<IR, IE> endPoints_2_1 = (Endpoints) => Endpoints.Where(e => e.Metadata.Any(m => (m is IHttpMethodMetadata i) ? i.HttpMethods.Contains("GET") : false));
        // ||||||||||||||||||||||||
        static readonly Func<IR, IE> endPoints_2_2 = (Endpoints) => Endpoints.Where(e => e.Metadata.Any(m => m is IHttpMethodMetadata i && i.HttpMethods.Contains("GET")));
        //================方法三================HttpGetAttribute,endpoints.MapGet,Any
        static readonly Func<IR, IE> endPoints_3_0 = (Endpoints) => Endpoints.Where(e =>
        {
            var None = true;
            return e.Metadata.Any(m =>
            {
                if (m is IHttpMethodMetadata ihttps)
                {
                    None = false;
                    return ihttps.HttpMethods.Contains("GET");
                }
                return false;
            }
            ) || None;
        });
        // ||||||||||||||||||||||||
        static readonly Func<IR, IE> endPoints_3_1 = (Endpoints) => Endpoints.Where(e => {
            var None = true;
            foreach (var item in e.Metadata)
            {
                if (item is IHttpMethodMetadata ihttps)
                {
                    None = false;
                    return ihttps.HttpMethods.Contains("GET");
                }
            }
            return None;
        });
        public static IE Filter<T>(this IReadOnlyList<T> Endpoints, Leave leave = Leave.ALL) where T : E
        {
            return (leave switch{Leave.HttpGetAttribute=> endPoints_1_0,Leave.MapGetMethod=> endPoints_2_2,_=> endPoints_3_1 })(Endpoints);
        }
        public enum Leave {
            HttpGetAttribute,MapGetMethod,ALL
        }
    }

}