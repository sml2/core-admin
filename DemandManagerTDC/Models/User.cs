﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemandManagerTDC.Models
{
    public class User
    {
        public int ID { get; set; }

        //账号
        public string Login { get; set; }

        //密码
        public string Password { get; set; }

        //角色
        public int Role { get; set; }//0管理员 1技术部 2市场部
    }
}
