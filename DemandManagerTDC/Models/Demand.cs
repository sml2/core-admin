﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemandManagerTDC.Models
{
    public class Demand
    {
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Column("标题")]
        public string Title { get; set; }

        /// <summary>
        /// 需求
        /// </summary>
        [Display(Name = "需求")]
        [Comment("名称")]
        public string Content { get; set; }

        /// <summary>
        /// 反馈建议
        /// </summary>
        [Display(Name = "反馈建议")]
        [Comment("反馈建议")]
        public string Feedback { get; set; }

        /// <summary>
        /// 单选 
        /// </summary>
        [Display(Name = "进度")]
        [Comment("选择")]
        public Processes Process { get; set; }

        /// <summary>
        /// 详细进度
        /// </summary>
        public enum Processes
        {
            [Description("待评估")]
            [Display(Name = "待评估")]
            /// <summary>
            /// 待评估
            /// </summary>
            WAIT_EVALUATION,

            [Description("待开发")]
            [Display(Name = "待开发")]
            /// <summary>
            /// 待开发
            /// </summary>
            WAIT_DEVELOP,

            [Description("待回复")]
            [Display(Name = "待回复")]
            /// <summary>
            /// 待回复
            /// </summary>
            WAIT_REPLY,

            [Description("开发中")]
            [Display(Name = "开发中")]
            /// <summary>
            /// 开发中
            /// </summary>
            DEVELOPING,

            [Description("待测试")]
            [Display(Name = "待测试")]
            /// <summary>
            /// 待测试
            /// </summary>
            WAIT_TEST,

            [Description("开发完成")]
            [Display(Name = "开发完成")]
            /// <summary>
            /// 开发完成
            /// </summary>
            DEVELOPED = WAIT_TEST,

            [Description("待上线")]
            [Display(Name = "待上线")]
            /// <summary>
            /// 待上线
            /// </summary>
            WAIT_PUBLISH,

            [Description("测试完成")]
            [Display(Name = "测试完成")]
            /// <summary>
            /// 测试完成
            /// </summary>
            TESTED = WAIT_PUBLISH,

            [Description("已发布")]
            [Display(Name = "已发布")]
            /// <summary>
            /// 已发布
            /// </summary>
            PUBLISHED,

            [Description("已完成")]
            [Display(Name = "已完成")]
            /// <summary>
            /// 已完成
            /// </summary>
            COMPLETED = PUBLISHED,

            [Description("暂时延期")]
            [Display(Name = "暂时延期")]
            /// <summary>
            ///  暂时延期
            /// </summary>
            DELAY,

            [Description("已搁置")]
            [Display(Name = "已搁置")]
            /// <summary>
            /// 已搁置
            /// </summary>
            SHELVED
        }

    }
}
