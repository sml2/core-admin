﻿using DemandManagerTDC.EF;
using DemandManagerTDC.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace DemandManagerTDC.Controllers
{
    public class HomeController : Controller
    {
        //然后，TeamContext 可以通过构造函数注入在 ASP.NET Core 控制器或其他服务中使用
        private readonly TeamContext db;

        public HomeController(TeamContext context)
        {
            db = context;
        }

        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            //获取Cookie 判断之前是否有用户勾选"记住我"
            string Login = null;

            string Password = null;

            //获取Cookie的值
            Request.Cookies.TryGetValue("Login", out Login);

            Request.Cookies.TryGetValue("Password", out Password);

            //判断获取的Login和Password是否为空
            if (!string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(Password))
            {
                //不为空 说明之前有用户勾选了"记住我" 免去登录直接跳转到指定页面
                return View("DemandManager");
            }
            else
            {
                return View();
            }
        }

        public IActionResult DemandManager(User user, string check)
        {
            //判断账号和密码是否输入正确
            if (db.Users.SingleOrDefault(u => u.Login == user.Login && u.Password == user.Password) != null)
            {
                //判断用户是否勾选"记住我"
                if (check == "true")
                {
                    //勾选
                    Response.Cookies.Append("Login", user.Login);

                    Response.Cookies.Append("Password", user.Password);

                    return View("DemandManager");
                }
                else
                {
                    //未勾选 直接返回视图
                    return View("DemandManager");
                }
            }
            else
            {
                return View("Index");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
