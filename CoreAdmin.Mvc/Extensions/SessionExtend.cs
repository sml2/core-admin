﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Text;

namespace CoreAdmin.Mvc.Extensions
{
    public static class  SessionExtend
    {
        public static void SetObj<T>(this ISession session, string key, T value)
        {
            var a = JsonSerializer.CreateDefault();
            string jsonstr = JsonConvert.SerializeObject(value);
            byte[] byteArray = Encoding.Default.GetBytes(jsonstr);
            session.Set(key, byteArray);
        }

        public static T Get<T>(this ISession session, string key)
        {
            byte[] byteArray;
            bool isvalue = session.TryGetValue(key, out byteArray);
            if (isvalue)
            {
                string str = Encoding.Default.GetString(byteArray);
                T val = JsonConvert.DeserializeObject<T>(str);
                return val;
            }
            else
            {
                return default(T);
            }
        }
    }
}
