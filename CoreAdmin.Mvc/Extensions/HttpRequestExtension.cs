﻿using CoreAdmin.Mvc.Middleware;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Extensions
{
    public static class HttpRequestExtension
    {
        /// <summary>
        /// 判断是否为Pjax请求
        /// </summary>
        /// <param name="request">the <see cref="HttpRequest"/> object for this request</param>
        /// <returns></returns>
        public static bool IsPjax(this HttpRequest request) => request.Headers.ContainsKey(PjaxMiddleware.PjaxSign) && request.Headers.ContainsKey(PjaxMiddleware.PjaxContainer);


        //return request.Headers.ContainsKey("x-requested-with") && request.Headers["x-requested-with"] == "XMLHttpRequest";
        //request.Headers[] never be null        
        /// <summary>
        /// 判断是否为Ajax请求
        /// </summary>
        /// <param name="request">the <see cref="HttpRequest"/> object for this request</param>
        /// <returns></returns>
        public static bool IsAjax(this HttpRequest request) => request.Headers["x-requested-with"].Equals("XMLHttpRequest");
        //return "XMLHttpRequest".Equals(request.Headers["x-requested-with"]);

        /// <summary>
        /// 判断request是否存在key
        /// </summary>
        /// <param name="request">the <see cref="HttpRequest"/> object for this request</param>
        /// <param name="key">The key to locate in the <see cref="IQueryCollection"/> Or <see cref="IFormCollection"/>.</param>
        /// <returns></returns>
        public static bool Has(this HttpRequest request, string key)
        {
            return request.Query.ContainsKey(key) || request.Form.ContainsKey(key);
        }

        public static string Old(this HttpRequest request, string key, string _default = "")
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (request.Method == "POST" && request.Form.ContainsKey(key))
            {
                return request.Form[key];
            }
            return _default;
        }

        private static List<string> RemoveKeys = new()
        {
            "_pjax"
        };
        /// <summary>
        /// 添加query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FullUrlWithQuery(this HttpRequest request, string key, object value)
        {
            var query = request.Query.Where(x => !RemoveKeys.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
            if (value is Dictionary<string, string> dic)
            {
                foreach (var (dicKey, dicVal) in dic)
                {
                    query[$"{key}[{dicKey}]"] = new Microsoft.Extensions.Primitives.StringValues(dicVal);
                }
            }
            else
            {
                query[key] = new Microsoft.Extensions.Primitives.StringValues(value?.ToString());
            }
            return request.Path + "?" + string.Join('&', query.Select(x => $"{x.Key}={x.Value}"));
        }

        /// <summary>
        /// 添加query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string FullUrlWithQuery(this HttpRequest request, Dictionary<string, object> param)
        {
            var query = request.Query.Where(x => !RemoveKeys.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
            foreach (var (key, value) in param)
            {
                query[key] = new Microsoft.Extensions.Primitives.StringValues(value.ToString());
            }
            return request.Path + "?" + string.Join('&', query.Select(x => $"{x.Key}={x.Value}"));
        }

        /// <summary>
        /// 删除query key
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string FullUrlWithoutQuery(this HttpRequest request, string key)
        {
            var queries = request.Query.Where(x => !RemoveKeys.Contains(x.Key)).Where(k => k.Key != key);
            var parameters = string.Join('&', queries.Select(pair => $"{pair.Key}={pair.Value}"));
            return request.Path + '?' + parameters;
        }

        public static string FullUrlWithoutQuery(this HttpRequest request, List<string> keys)
        {
            var queries = request.Query.Where(x => !RemoveKeys.Contains(x.Key)).Where(k => !keys.Contains(k.Key));
            var parameters = string.Join('&', queries.Select(pair => $"{pair.Key}={pair.Value}"));
            return request.Path + "?" + parameters;
        }

        /// <summary>
        /// 获取request中的值
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string Get(this HttpRequest request, string key, string defaultValue = default)
        {
            if (request.Query.TryGetValue(key, out var str))
            {
                return str;
            }

            if (request.HasFormContentType && request.Form.TryGetValue(key, out str))
            {
                return str;
            }

            return defaultValue;
        }

        public static Dictionary<string, string> GetDic(this HttpRequest request, string key, Dictionary<string, string> dic = default)
        {
            // 尝试寻找query里的键值对
            var keys = request.Query.Keys.Where(x => x.StartsWith($"{key}[")).ToList();
            if (keys.Any())
            {
                var keyLen = key.Length + 1;
                return keys.Select(x => KeyValuePair.Create(x[keyLen..^1], request.Query[x]))
                    .ToDictionary(x => x.Key, x => x.Value.ToString());
            }

            return dic;
        }

        /// <summary>
        /// 获取ipv4
        /// </summary>
        /// <param name="HttpContext"></param>
        /// <returns></returns>
        public static string GetIPv4(this HttpContext HttpContext)
        {
            return HttpContext.Request.GetIPv4();
        }
        public static string GetIPv4(this HttpRequest request)
        {
            string[] MaybeHeaders = {
                "CLIENT_IP",//常用CND加速节点
                "CLIENT-IP",//常用CND加速节点
                "ALI_CDN_REAL_IP",//阿里云加速
                "Ali-CDN-Real-IP",//阿里云加速
                "X_FORWARDED_FOR",//标准代理头，最易被仿照
                "X-FORWARDED-FOR",//标准代理头，最易被仿照
            };

            var Header = request.Headers;
            foreach (var MaybeHeader in MaybeHeaders)
            {
                string temp;
                if (Header.ContainsKey(MaybeHeader) && (temp = Header[MaybeHeader]).Contains("."))
                {
                    return temp;
                }
            }
            var ip = request.HttpContext.Connection.RemoteIpAddress?.MapToIPv4();
            return null != ip ? ip.ToString() : "0.0.0.0";
        }
        public static string[] Segments(this HttpRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            return request.Path.Value?.Split("/", StringSplitOptions.RemoveEmptyEntries).ToArray();
        }
    }
}
