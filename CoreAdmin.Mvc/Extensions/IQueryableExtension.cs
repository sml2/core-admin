﻿using System.Linq;

namespace CoreAdmin.Mvc.Extensions
{
    public static class IQueryableExtension
    {
        public static string GetKeyName<T>(this IQueryable<T> ds) where T : class
        {
            // var entityType = (IEntityType)ds.GetType().GetProperty(nameof(DbSet<T>.EntityType))?.GetValue(ds);
            // if (null != entityType)
            // {
            //     return entityType.FindPrimaryKey().Properties.Select(x => x.Name).Single();
            // }
            //
            // throw new ArgumentException($"{ds.GetType().FullName} 不存在属性 [EntityType]");
            return BaseModelExtension.GetKeyName(ds.GetType().GetGenericArguments()[0]).Name;
        }

    }
}
