﻿using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Extensions
{
    public static class DbSetExtension
    {
        /// <summary>
        /// 获取树形options字符串，供选择框使用
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ds"></param>
        /// <param name="rootText"></param>
        /// <returns></returns>
        public static async Task<Dictionary<string, string> > SelectOptions<T>(this DbSet<T> ds, string rootText = "ROOT") where T : ModelTree<T>
        {
            var options = await ds.BuildSelectOptions();
            options.Insert(0, "0", rootText);
            Dictionary<string, string> dic = new();
            foreach (DictionaryEntry entry in options)
            {
                dic.Add((string)entry.Key, (string)entry.Value);
            }
            return dic;
        }

        private static async Task<OrderedDictionary> BuildSelectOptions<T>(this DbSet<T> ds, List<T> nodes = null, int parentId = 0, string prefix = "", string space = "&nbsp;") where T : ModelTree<T>
        {
            prefix ??= "┝"+space;

            OrderedDictionary options = new ();

            if (nodes == null || nodes.Count == 0) {
                nodes = await ds.AllNodes();
            }
            Type type = typeof(T);
            T tree = (T)Activator.CreateInstance(type);
            PropertyInfo parentColumnProInfo = type.GetProperty(tree.ParentColumn);
            PropertyInfo titleColumnProInfo = type.GetProperty(ModelTree<T>.TitleColumn);

            PropertyInfo keyNameProInfo = type.GetProperty(ds.GetKeyName());
            foreach (var node in nodes) {
                if ((int)parentColumnProInfo.GetValue(node) == parentId) {
                    var keyname = (int)keyNameProInfo.GetValue(node);
                    var titleColumn = titleColumnProInfo.GetValue(node);
                    
                    var childrenPrefix = prefix.Replace("┝", space.Repeat(6))+"┝"+ prefix.Replace(new string[] { "┝", space}, "");

                    var children = await ds.BuildSelectOptions(nodes, keyname, childrenPrefix);

                    options[keyname.ToString()] = prefix + space + titleColumn;

                    if (children != null && children.Count > 0) {
                        options = options.MergeSaveBefore(children);
                    }
                }
            }

            return options;
        }

        public static Task<List<T>> AllNodes<T>(this IQueryable<T> ds) where T:ModelTree<T>
        {
            //var type = typeof(T);
            //var orderColumn = (string)type.GetProperty("Order")?.GetValue(null);
            //return Permission.Menu().Cast<T>().ToList();
            return ds.OrderBy(m => m.Order).ToListAsync();
        }
        /// <summary>
        /// 获取树形子父级模型列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static Task<List<T>> ToTree<T>(this IQueryable<T> ds) where T : ModelTree<T>
        {
            return ds.BuildNestedArray();
        }

        private static async Task<List<T>> BuildNestedArray<T>(this IQueryable<T> ds, List<T> nodes = null, int parentId = 0) where T : ModelTree<T>
        {
            var branch = new List<T>();

            if (nodes == null || nodes.Count == 0)
            {
                nodes = await ds.AllNodes();
            }
            Type type = ds.GetType().GetGenericArguments()[0];
            T tree = (T)Activator.CreateInstance(type);
            PropertyInfo parentColumn = type.GetProperty(tree.ParentColumn);
            PropertyInfo keyName = type.GetProperty(ds.GetKeyName());

            foreach (var node in nodes)
            {
                if ((int)parentColumn.GetValue(node) == parentId)
                {
                    var children = await ds.BuildNestedArray(nodes, (int)keyName.GetValue(node));

                    if (children.Count > 0)
                    {
                        node.Children = children;
                    }

                    branch.Add(node);
                }
            }

            return branch;
        }
    }
}
