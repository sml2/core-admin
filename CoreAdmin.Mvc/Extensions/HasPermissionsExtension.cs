﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Extensions
{
    public static class HasPermissions
    {
        /// <summary>
        ///查询  用于角色登录的调用
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static List<Permissions> allPermissions(this UserModel User)
        {
            var RolesPermissionsList = User.Roles.Pluck<Roles, Permissions>(nameof(Roles.Permissions));
            var PermissionsList = User.Permissions;

            return RolesPermissionsList.Concat(PermissionsList).Where(P => P != null).ToList();
        }
        /// <summary>
        /// 是否包含角色
        /// </summary>
        /// <param name="User"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsRole(this UserModel User, string role)
        {
            return User.Roles.Pluck(nameof(Roles.Slug)).Contains(role);
        }


        public static bool IsRole(this UserModel User, List<string> role)
        {
            var collect = User.Roles.Pluck(nameof(Roles.Slug)).Intersect(role);
            return collect != null && collect.Count() > 0;
        }
        /// <summary>
        /// 调用上面是否含有角色的方法
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static bool IsAdministrator(this UserModel User)
        {
            return User.IsRole("administrator");
        }

        public static bool Can(this UserModel User, string abilty)
        {
            if (string.IsNullOrEmpty(abilty))
            {
                return true;
            }
            if (User.IsAdministrator())
            {
                return true;
            }

            if (User.Permissions.Pluck(nameof(Roles.Slug)).Contains(abilty))
            {
                return true;
            }
            return User.Roles.Pluck<Roles, Permissions>(nameof(Roles.Permissions)).Pluck(nameof(Permissions.Slug)).Contains(abilty);
        }

        public static bool Cannot(this UserModel User, string abilty)
        {
            return !User.Can(abilty);
        }

    }
}
