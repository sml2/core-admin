﻿using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using CoreAdmin.Lib;

namespace CoreAdmin.Mvc.Extensions
{
    public static class BaseModelExtension
    {
        /// <summary>
        /// 获取模型中的值
        /// </summary>
        /// <param name="model"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get<T>(this T model, string key) where T : IIdexerModel
        {
            #region 索引器
            var value = model[key];
            if (null != value || key.StartsWith("__"))
            {
                return value;
            }
            #endregion

            #region 属性

            if (!key.Contains('.'))
            {
                try
                {
                    return Property<T>.Get(model, key);
                }
                catch (Exception e)
                {
                    Admin.LoggerFactory.CreateLogger<BaseModel>().LogWarningInfo(e.Message);
                    // ignored
                    return null;
                }
            }
            
            #endregion

            #region 关联模型
            var keyArr = key.Split('.');
            var relation = keyArr[0];
            var name = keyArr[1];
            var relationModel = model.GetType().GetProperty(relation);
            if (null != relationModel)
            {
                var relationValue = relationModel.GetValue(model);
                if (null != relationValue)
                {
                    if (relationValue is IIdexerModel)
                    {
                        var relationProperty = relationValue.GetType().GetProperty(name);
                        return relationProperty.GetValue(relationValue);
                    }

                    if (relationValue is IEnumerable<IIdexerModel> enumerable)
                    {
                        var relationProperty = enumerable.GetType().GetGenericArguments()[0].GetProperty(name);
                        return enumerable.Select(model => relationProperty.GetValue(model)).ToList();
                    }
                }

            }
            #endregion
            return null;
        }

        /// <summary>
        /// 向模型赋值
        /// TODO: 暂不支持关联模型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Set(this IIdexerModel model, string key, object value)
        {
            #region 属性
            var property = model.GetType().GetProperty(key);
            if (null != property)
            {
                property.SetValue(model, value);
            }
            #endregion

            #region 索引器
            model[key] = value;
            #endregion
        }

        /// <summary>
        /// 获取模型的主键值
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int GetKey<T>(this T model) where T : IIdexerModel
        {
            var key = model.GetKeyName();
            return (int)Property<T>.Get(model, key);
        }

        /// <summary>
        /// 获取模型的主键名
        /// TODO: 可以使用缓存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string GetKeyName(this IIdexerModel model)
        {
            var type = model.GetType();
            return GetKeyName(type).Name;
        }

        public static PropertyInfo GetKeyNameProperty(this IIdexerModel model)
        {
            var type = model.GetType();
            return GetKeyName(type);
        }

        /// <summary>
        /// 手动寻找主键
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static PropertyInfo GetKeyName(Type type)
        {
            foreach (var property in type.GetProperties())
            {
                if (property.Name.Replace(type.Name,"").ToUpper().Equals("ID") || property.GetCustomAttribute<KeyAttribute>() != null)
                {
                    return property;
                }
            }

            throw new Exception("无法自动找到keyName");
        }
    }
}
