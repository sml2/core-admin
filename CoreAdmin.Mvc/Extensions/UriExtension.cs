﻿
namespace System
{
    public static class UriExtension
    {
        /// <summary>
        /// 返回URL
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static bool IsValidUrl(this Uri uri)
        {
            var url = uri.ToString();
            return url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("mailto:") || url.StartsWith("tel:") || url.StartsWith("sms:");
        }
    }
}
