﻿using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Extensions
{
    public static class ListExtension
    {
        /// <summary>
        /// 获取模型的某一列数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="column">列名</param>
        /// <returns></returns>
        public static IEnumerable<string> Pluck<T>(this IEnumerable<T> list, string column) where T:IIdexerModel
        {
            if (string.IsNullOrEmpty(column))
            {
                throw new ArgumentException("参数不能为空");
            }

            var property = typeof(T).GetProperty(column);
            if (property == null)
            {
                throw new ArgumentException($"[{typeof(T).FullName}]中不存在属性[{column}]!");
            }

            List<string> newList = new();
            foreach (var model in  list)
            {
                newList.Add((string)property.GetValue(model));
            }
            return newList;
        }

        public static IEnumerable<TResult> Pluck<T, TResult>(this IEnumerable<T> list, string column) where T : IIdexerModel
        {
            if (string.IsNullOrEmpty(column))
            {
                throw new ArgumentException("参数不能为空");
            }

            var property = typeof(T).GetProperty(column);
            if (property == null)
            {
                throw new ArgumentException($"[{typeof(T).FullName}]中不存在属性[{column}]!");
            }

            List<TResult> newList = new();
            foreach (var model in list)
            {
                newList.Add((TResult)property.GetValue(model));
            }
            return newList;
        }

        /// <summary>
        /// 获取模型的某一列数据，主键作为id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="column"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Dictionary<string, string> Pluck<T>(this List<T> list, string column, string id) where T : IIdexerModel
        {
            if (string.IsNullOrEmpty(column))
            {
                throw new ArgumentException("参数不能为空");
            }

            var property = typeof(T).GetProperty(column);
            if (property == null)
            {
                throw new ArgumentException($"[{typeof(T).FullName}]中不存在属性[{column}]!");
            }
            var idProperty = typeof(T).GetProperty(id);
            if (idProperty == null)
            {
                throw new ArgumentException($"[{typeof(T).FullName}]中不存在属性[{id}]!");
            }

            Dictionary<string, string> newList = new();
            list.ForEach(model =>
            {
                newList.Add(idProperty.GetValue(model).ToString(), property.GetValue(model).ToString());
            });
            return newList;
        }

        /// <summary>
        /// 取两个集合的差集
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="another"></param>
        /// <returns></returns>
        public static List<T> Diff<T>(this List<T> list, List<T> another)
        {
            return list.Where(name => !another.Contains(name)).ToList();
        }

        /// <summary>
        /// 取两个集合的交集
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="another"></param>
        /// <returns></returns>
        public static List<T> InterSection<T>(this List<T> list, List<T> another)
        {
            return list.Where(name => another.Contains(name)).ToList();
        }
    }
}
