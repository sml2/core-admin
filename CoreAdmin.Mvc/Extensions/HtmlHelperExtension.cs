﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;

namespace CoreAdmin.Mvc.Extensions
{
    public static class HtmlHelperExtension
    {
        /// <summary>
        /// 待优化 
        /// 会根据传来的类型实例化
        /// TODO
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="helper"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string DisplayNameFor<TModel>(this IHtmlHelper<TModel> helper, Type type, string name)
        {
            if (name.StartsWith("__")) return string.Empty;

            if (name.Contains('.'))
            {
                var nameArr = name.Split('.');
                var relation = nameArr[0];
                name = nameArr[1];

                type = type.GetProperty(relation).PropertyType;

                if (type.IsGenericType)
                {
                    type = type.GetGenericArguments()[0];
                }

            }
            var modelExplorer = helper.MetadataProvider.GetModelExplorerForType(type, Activator.CreateInstance(type));
            modelExplorer = modelExplorer.GetExplorerForProperty(name);
            if (modelExplorer == null) return name;
            return GenerateDisplayName(modelExplorer, name);
        }

        public static string DisplayNameFor<TModel>(this IHtmlHelper<TModel> helper, object model, string name)
        {
            if (name.StartsWith("__")) return string.Empty;

            var modelExplorer = helper.MetadataProvider.GetModelExplorerForType(model.GetType(), model);
            modelExplorer = modelExplorer.GetExplorerForProperty(name);
            if (modelExplorer == null) return name;
            return GenerateDisplayName(modelExplorer, name);
        }

        /// <summary>
        /// 复制 Mvc\Mvc.ViewFeatures\src\HtmlHelper.cs
        /// </summary>
        /// <param name="modelExplorer"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static string GenerateDisplayName(ModelExplorer modelExplorer, string expression)
        {
            if (modelExplorer == null)
            {
                throw new ArgumentNullException(nameof(modelExplorer));
            }

            // We don't call ModelMetadata.GetDisplayName here because
            // we want to fall back to the field name rather than the ModelType.
            // This is similar to how the GenerateLabel get the text of a label.
            var resolvedDisplayName = modelExplorer.Metadata.DisplayName ?? modelExplorer.Metadata.PropertyName;
            if (resolvedDisplayName == null && expression != null)
            {
                var index = expression.LastIndexOf('.');
                if (index == -1)
                {
                    // Expression does not contain a dot separator.
                    resolvedDisplayName = expression;
                }
                else
                {
                    resolvedDisplayName = expression.Substring(index + 1);
                }
            }

            return resolvedDisplayName ?? string.Empty;
        }
    }
}
