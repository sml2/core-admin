﻿using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CoreAdmin.EF
{
    public abstract class AdminDbContext : BaseDbContext
    {
        public static AdminDbContext Instance { get; protected set; }

        protected void Initial()
        {
            Instance = this;
        }

        public AdminDbContext()
        {
            Initial();
        }

        protected AdminDbContext(DbContextOptions options) : base(options)
        {
            Initial();
        }
        //AAdd-Migration -Context AdminDbContext -OutputDir Context\Migrations_Admin intoPermissions
        public override DbSet<UserModel> Users { get; set; }
        public DbSet<MenuModel> Menu { get; set; }
        public DbSet<Permissions> Permissions { get; set; }
        public override DbSet<Roles> Roles { get; set; }
        public DbSet<OperationLog> OperationLog { get; set; }

        // Add-Migration -Context AdminDbContext -OutputDir Context\Migrations_DbContextOperationLog intoContentLength
        // protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source=CoreAdmin.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MenuModel>().HasData(
                new MenuModel
                    {Id = 1, ParentID = 0, Title = "Dashboard", Uri = "Home/Index", Icon = "fa-bar-chart", Order = 1},
                new MenuModel {Id = 2, ParentID = 0, Title = "Admin", Uri = "", Icon = "fa-tasks", Order = 2},
                new MenuModel
                {
                    Id = 3, ParentID = 2, Title = "Users",
                    Uri = nameof(Mvc.Controllers.UsersController).ControllerNameToRoutePath(), Icon = "fa-users",
                    Order = 3
                },
                new MenuModel
                {
                    Id = 4, ParentID = 2, Title = "Roles",
                    Uri = nameof(Mvc.Controllers.RolesController).ControllerNameToRoutePath(), Icon = "fa-user",
                    Order = 4
                },
                new MenuModel
                {
                    Id = 5, ParentID = 2, Title = "Permission",
                    Uri = nameof(Mvc.Controllers.PermissionsController).ControllerNameToRoutePath(), Icon = "fa-ban",
                    Order = 5
                },
                new MenuModel
                {
                    Id = 6, ParentID = 2, Title = "Menu",
                    Uri = nameof(Mvc.Controllers.MenuController).ControllerNameToRoutePath(), Icon = "fa-bars",
                    Order = 6
                },
                new MenuModel
                {
                    Id = 7, ParentID = 2, Title = "Operation Log",
                    Uri = nameof(Mvc.Controllers.OperationLog).ControllerNameToRoutePath(), Icon = "fa-history",
                    Order = 7
                }
            );
            modelBuilder.Entity<Roles>().HasData(
                new Roles {Id = 1, Name = "Admin", Slug = "Admin", NormalizedName = "ADMIN"}
            );
            modelBuilder.Entity<Permissions>().HasData(
                new Permissions
                {
                    ID = 1, Name = "All permission", Slug = "Dashboard", HttpPath = "/*", HttpMethod = "ANY",
                    CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now,
                },
                new Permissions
                {
                    ID = 2, Name = "Dashboard", Slug = "Admin", HttpPath = "/", HttpMethod = "GET",
                    CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now,
                },
                new Permissions
                {
                    ID = 3, Name = "Login", Slug = "Users", HttpPath = "/auth/login,/manage/auth/logout",
                    HttpMethod = "", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now,
                },
                new Permissions
                {
                    ID = 4, Name = "User setting", Slug = "Roles", HttpPath = "/auth/setting", HttpMethod = "GET,PUT",
                    CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now,
                },
                new Permissions
                {
                    ID = 5, Name = "Auth management", Slug = "Permission",
                    HttpPath = "/auth/roles,/auth/permissions,/auth/menu,/auth/logs", HttpMethod = "",
                    CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now,
                }
            );

            modelBuilder.Entity<UserModel>().HasData(
                new UserModel
                {
                    Id = 1, UserName = "admin", Name = "Administrator",
                    Email = "admin@ca.com", EmailConfirmed = true
                }
            );
        }
    }

    static class StringExt
    {
        public static string ControllerNameToRoutePath(this string s)
        {
            return s.ToLower().Replace("controller", string.Empty);
        }
    }
}