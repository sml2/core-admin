﻿using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Configure;
using CoreAdmin.Mvc.Grid.Displayers;
using CoreAdmin.Mvc.Layout;
//using CoreAdmin.Layout;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class CoreAdminServiceCollectionExtensions
    {
        public static IServiceCollection UseCoreAdmin(
             this IServiceCollection services, IConfiguration config)
        {
            #region 配置项
            services.Configure<CoreAdminConfigure>(config.GetSection(CoreAdminConfigure.Section));
            #endregion
            #region Session
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.Cookie.Name = ".AdventureWorks.Session";
                //options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.IsEssential = true;
            });
            #endregion

            JsonSerializerSettings setting = new JsonSerializerSettings();
            JsonConvert.DefaultSettings = () =>
            {
                //日期类型默认格式化处理
                setting.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
                setting.DateFormatString = "yyyy-MM-dd HH:mm:ss";

                //空值处理
                setting.NullValueHandling = NullValueHandling.Ignore;
                setting.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                //高级用法九中的Bool类型转换 设置
                //setting.Converters.Add(new BoolConvert("是,否"));

                return setting;
            };

            #region 国际化
            services.AddLocalization();
            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization(options => {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                        factory.Create(typeof(SharedResource));
                });
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[] { "zh-CN", "en" };
                options.SetDefaultCulture(supportedCultures[0])
                    .AddSupportedCultures(supportedCultures)
                    .AddSupportedUICultures(supportedCultures);
            });
            #endregion

            #region Singleton 单例
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<Admin>();
            services.AddScoped<Content>();
            #endregion

            #region displayers组件
            services.UseDisplayers();
            #endregion

            //services.TryAddTransient(typeof(BGrid<>), typeof(BGrid<>));
            return services;
        }
    }
}
