﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Layout
{
    public class Column : IBuildable
    {
        protected Dictionary<string, int> width = new();

        public List<HtmlContent> Contents { get; protected set; } = new();

        public string Class { get => string.Join(" ", width.Select(pair => $"col-{pair.Key}-{pair.Value}")); }

        public Column(Action<Column> content, int width = 12)
        {
            content.Invoke(this);
            InitWidth(width);
        }
        public Column(HtmlContent content, int width = 12)
        {
            Append(content);
            InitWidth(width);
        }
        protected void InitWidth(int width)
        {
            this.width["md"] = 0 >= width ? 12 : width;
        }

        public Column Append(HtmlContent content)
        {
            Contents.Add(content);
            return this;
        }

        /**
         * Add a row for column.
         *
         * @param $content
         *
         * @return Column
         */
        //public function row($content)
        //{
        //    if (!$content instanceof \Closure) {
        //    $row = new Row($content);
        //    } else
        //    {
        //    $row = new Row();

        //        call_user_func($content, $row);
        //    }

        //    ob_start();

        //$row->build();

        //$contents = ob_get_contents();

        //    ob_end_clean();

        //    return $this->append($contents);
        //}
        [Obsolete]
        public List<HtmlContent> Build()
        {
            var rs = new List<HtmlContent> { StartColumn() };

            foreach (var content in Contents)
            {
                if (content is IRenderable || content is BGrid)
                {
                    //echo content->render();
                }
                else
                {
                    rs.Add(content);
                }
            }

            rs.Add(EndColumn());
            return rs;
        }

        protected HtmlContent StartColumn()
        {
            // get class name using width array
            var className = string.Join(" ", width.Select(pair => $"col-{pair.Key}-{pair.Value}"));

            return new() { Value = $"<div class=\"{className}\">" };
        }

        protected HtmlContent EndColumn()
        {
            return new() { Value = "</div>" };
        }
    }
}
