﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Layout
{
    public interface IBuildable
    {
        public List<HtmlContent> Build();
    }
}
