﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Layout
{
    public class Row : IBuildable, IRenderable
    {
        public List<Column> Columns { get; private set; } = new();

        protected List<string> _class = new();

        public string Class
        {
            get
            {
                var className = _class;
                className.Add("row");
                return string.Join(" ", className);
            }
        }

        public Row(HtmlContent content)
        {
            Column(12, content);
        }

        public void Column(int width, Action<Column> content)
        {
            width = width < 1 ? 12 * width : width;
            var column = new Column(new HtmlContent(), width);
            content.Invoke(column);
            Column(column);
        }

        public void Column(int width, HtmlContent content)
        {
            width = width < 1 ? 12 * width : width;

            var column = new Column(content, width);

            Column(column);
        }
        public void Column(Column column)
        {
            AddColumn(column);
        }

        public Row InitClass(string className) => InitClass(new List<string> { className });
        public Row InitClass(List<string> className)
        {
            _class = className;
            return this;
        }

        protected void AddColumn(Column column)
        {
            Columns.Add(column);
        }

        [Obsolete]
        public List<HtmlContent> Build()
        {
            var contents = new List<HtmlContent>
            {
                StartRow()
            };

            foreach (var column in Columns)
            {
                contents.AddRange(column.Build());
            }
            contents.Add(EndRow());
            return contents;
        }

        protected HtmlContent StartRow()
        {
            var _class = this._class;
            _class.Add("row");
            return new() { Value = $"<div class=\"{string.Join(" ", _class)}\">" };
        }

        protected HtmlContent EndRow()
        {
            return new() { Value = "</div>" };
        }

        public string Render()
        {
            Build();
            //return $contents;
            return "";
        }
    }
}
