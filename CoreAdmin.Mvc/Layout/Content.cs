﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CoreAdmin.Mvc.Layout
{
    public class Content
    {
        private readonly IViewComponentHelper _componentHelper;
        private readonly UserManager<UserModel> _userManager;
        private string _title;

        private string _description;

        private bool _fullScreenSwitch;

        protected List<BreadcrumbViewModel> breadcrumb = new();

        public List<Row> Rows { get; protected set; } = new();

        protected Dictionary<string, string> view;
        private Controller _controller;

        public Content(IViewComponentHelper componentHelper, UserManager<UserModel> userManager)
        {
            _componentHelper = componentHelper;
            _userManager = userManager;
        }

        public Content(Action<Content> callback) => callback.Invoke(this);

        public Content Header(string header) => Title(header);

        public Content Title(string title)
        {
            _title = title;
            return this;
        }


        public Content FullScreenSwitch(bool fullScreenSwitch=true)
        {
            _fullScreenSwitch = fullScreenSwitch;
            return this;
        }

        public Content WithController(Controller controller)
        {
            _controller = controller;
            return this;
        }

        public Content Description(string description)
        {
            _description = description;
            return this;
        }

        public Content Breadcrumb(List<BreadcrumbViewModel> breadcrumb)
        {
            ValidateBreadcrumb(breadcrumb);

            this.breadcrumb = breadcrumb;

            return this;
        }

        protected bool ValidateBreadcrumb(List<BreadcrumbViewModel> breadcrumb)
        {
            foreach (var item in breadcrumb)
            {
                if (!string.IsNullOrEmpty(item.Text))
                {
                    throw new ApplicationException("Breadcrumb format error!");
                }
            }

            return true;
        }

        public async Task<Content> Body(BGrid grid) => Body(await grid.Render());
        public Content Body(BForm form) => Body(form.Render(true));
        public Content Body(BShow show) => Body(show.Render());

        public Content Body(HtmlContent content)
        {
            return Row(content);
        }

        public Content Row(BForm form) => Row(form.Render(true));
        public Content Row(Action<Row> content)
        {
            var row = new Row(new() { });
            content.Invoke(row);
            return Row(row);
        }
        public Content Row(HtmlContent content)
        {
            var row = new Row(content);
            return Row(row);
        }
        public Content Row(Row row)
        {
            AddRow(row);
            return this;
        }

        public Content View(string view, List<string> data)
        {
            this.view.Add("view", view);
            // TODO: data
            this.view.Add("data", view);
            return this;
        }

        public Content Component(string view, Dictionary<string, dynamic> data)
        {
            return Body(Admin.Component(view, data));
        }

        public Content Dump(string var)
        {
            //return Row(admin_dump(...func_get_args()));
            Debug.Assert(false);
            return this;
        }

        protected void AddRow(Row row)
        {
            Rows.Add(row);
        }

        // public List<HtmlContent> Build()
        [Obsolete]
        public Task<string> Build()
        {
            var mvcOptions = _controller.HttpContext.RequestServices.GetService<IOptions<MvcViewOptions>>();
            var ViewOptions = mvcOptions.Value;
            var contents = new List<HtmlContent>();
            foreach (var row in Rows)
            {
                contents.AddRange(row.Build());
            }
            return Task.Run(() => "contents");
        }

        public Content WithSuccess(string title, string message)
        {
            Admin.Success(title, message);
            return this;
        }
        public Content WithError(string title, string message)
        {
            Admin.Error(title, message);
            return this;
        }
        public Content WithWarning(string title, string message)
        {
            Admin.Warning(title, message);
            return this;
        }
        public Content WithInfo(string title, string message)
        {
            Admin.Info(title, message);
            return this;
        }

        protected Task<UserModel> GetUserData()
        {
            return _userManager.GetUserAsync(_controller.User);
            //return Arr::only($user->toArray(), ['id', 'username', 'email', 'name', 'avatar']);
        }

        public async Task<IActionResult> Render()
        {
            var model = new ContentViewModel
            {
                Header = _title,
                Description = _description,
                Breadcrumb = breadcrumb,
                FullScreenSwitch = _fullScreenSwitch,
                Content = this,
                View = view,
                User = await GetUserData(),
            };
            var viewDataDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(),
                               new ModelStateDictionary());
            viewDataDictionary.Model = model;
            return new ViewResult()
            {
                ViewName = "~/Views/Content.cshtml",
                ViewData = viewDataDictionary
            };
        }
    }
}
