﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace CoreAdmin.Mvc.Interface
{
    public interface IForm
    {
        public Type ModelType { get; set; }
        public IQueryable<IIdexerModel> Model();

        public HttpRequest Request { get; }
    }

    public interface IForm<out TModel>
    {
        public IQueryable<TModel> Model();
        public HttpRequest Request { get; }
    }
}
