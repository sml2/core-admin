﻿namespace CoreAdmin.Mvc.Interface
{
    public interface IHtmlable
    {
        public string ToHtml();
    }
}
