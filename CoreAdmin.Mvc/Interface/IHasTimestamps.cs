﻿using System;

namespace CoreAdmin.Mvc.Interface
{
    public interface IHasTimestamps
    {
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}