﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Interface
{
    public interface IRenderable
    {
        public HtmlContent Render();
    }
}
