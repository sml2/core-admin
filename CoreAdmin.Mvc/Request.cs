﻿using Microsoft.AspNetCore.Http;
using System;

namespace CoreAdmin.Mvc
{
    [Obsolete("使用依赖注入")]
    public class Request
    {
        private static Request _instance;
        private static HttpContext _httpContext;


        private Request(HttpContext httpContext)
        {
            _httpContext = httpContext;
            _instance = this;
        }

        public class Method
        {
        }
    }
}
