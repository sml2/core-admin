﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CoreAdmin.Mvc.Models
{
    [Table("Users")]
    [Index(new string[] { nameof(UserName) }, IsUnique = true)]
    public class UserModel : IdentityUser<int>, IIdexerModel
    {
        private Dictionary<string, object> dic = new();
        public object this[string col] { get => dic.ContainsKey(col) ? dic[col] : null; set => dic[col] = value; }

        [Column("id")]
        public override int Id { get; set; }

        [StringLength(20, MinimumLength = 3), Display(Name = "Username")]
        [Column("username"), Required]
        public override string UserName { get; set; }

        [StringLength(255), Column("name"), Display(Name = "nick")]
        public string Name { get; set; }

        [Column("avatar"), Display(Name = "avatar")]
        public string Avatar { get; set; } = "/_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg";
        [Column("remember_token")]
        public string RememberToken { get; set; }
        [Display(Name = "password")]
        public override string PasswordHash { get; set; }

        [Column("created_at"), Display(Name = "created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public bool Visible(MenuModel menu) => true;
        public bool Can(MenuModel menu) => true;


        [NotMapped, Display(Name = "roles")]
        public List<string> RolesList { get; set; }

        //多对多映射
        public ICollection<Roles> Roles { get; set; }

        public List<UserRole> UserRoles { get; set; }


        [NotMapped, Display(Name = "Permissions")]
        public List<string> PermissionsList { get; set; }
        //多对多映射
        public ICollection<Permissions> Permissions { get; set; }

    }
}
