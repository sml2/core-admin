﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Models
{
    /// <summary>
    /// 是否有值返回
    /// </summary>
    public abstract class BaseModel : IIdexerModel
    {
        private Dictionary<string, object> dic = new();
        public object this[string col] { get => dic.ContainsKey(col) ? dic[col] : null; set => dic[col] = value; }
     
    }
}
