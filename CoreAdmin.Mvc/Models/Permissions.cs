﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreAdmin.Mvc.Models
{
    [Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class Permissions:BaseModel
    {

        [Column("id")]
        public int ID { get; set; }

        [StringLength(50),Column("name"), Required, Display(Name = "权限")]
        public string Name { get; set; }
        [StringLength(50),Column("slug"), Required]
        public string Slug { get; set; }
        [StringLength(255), Column("http_method")]
        public string HttpMethod { get; set; }

        [NotMapped]
        public List<string> HttpMethodList { 
            get => !string.IsNullOrEmpty(HttpMethod) ? HttpMethod.Split(",").ToList() : new();
            set {
                HttpMethod = string.Join(",", value).Trim(',');
            } }

        [StringLength(255),Column("http_path")]
        public string HttpPath { get; set; }

        [NotMapped]
        public List<string> HttpPathList
        {
            get => !string.IsNullOrEmpty(HttpPath) ? HttpPath.Split(",").ToList() : new();
            set
            {
                HttpPath = string.Join("\n", value);
            }
        }



        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }

        //多对多映射
        public ICollection<Roles> Roles { get; set; }

        //多对多映射
        public ICollection<UserModel> UserModel { get; set; }

        public bool ShouldPassThrough(string RPath,string RMethod)
        {
            if(string.IsNullOrEmpty(this.HttpMethod) && string.IsNullOrEmpty(this.HttpPath))
            {
                return true;
            }
            var Method = this.HttpMethod;
            var HttpPath = this.HttpPathList;

            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
            foreach (var Path in HttpPath)
            {

                Dictionary<string, string> item = new Dictionary<string, string>() {
                    { "Method",Method},
                    { "Path",Path}
                };
                items.Add(item);
            }  
            foreach(var i in items)
            {
                if (this.matchRequest(i, RPath, RMethod)) {
                    return true;
                }
            }           
            return false;
        }

        public bool matchRequest(Dictionary<string,string> match, string RPath,string RMethod)
        {
            var Path = "";

            if(match.ContainsKey("Path") && match["Path"] == "/")
            {
                Path = "/";
            }
            else
            {
                Path = match["Path"].Trim('/');
            }

            if(Path == "*")
            {
                return true;  
            }

            if (!Path.ToLower().StartsWith(RPath.ToLower().Trim('/')))
            {
                return false;
            }

            var Method = "";
            if (match.ContainsKey("Method"))
            {
                Method = match["Method"].ToUpper();
            }

            return string.IsNullOrEmpty(Method) || Method.ToUpper().Contains(RMethod);
        }



    }
}
