﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreAdmin.Mvc.Models
{
    public abstract class ModelTree<T> : BaseModel where T:ModelTree<T>
    {
        [NotMapped]
        public virtual string ParentColumn { get; set; } = "ParentID";
        [NotMapped]
        public static string TitleColumn { get; set; } = "Title";
        [NotMapped]
        public static string OrderColumn { get; set; } = "Order";

        protected Func<T, T> queryCallback;
        [NotMapped]
        public List<T> Children { get; set; }
        public bool HasChildren => Children != null && Children.Count > 0;

        [Column("order"), Display(Name = "order")]
        public int Order { get; set; }

    }

}
