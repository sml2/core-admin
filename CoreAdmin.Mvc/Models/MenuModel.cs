﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreAdmin.Mvc.Models
{
    public class MenuModel : ModelTree<MenuModel>
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("parent_id"), Display(Name = "parent_id")]
        public int ParentID { get; set; }

        [StringLength(50), Column("title"), Required, Display(Name = "title")]
        public string Title { get; set; }

        [StringLength(50), Column("icon"), Required, Display(Name = "icon")]
        public string Icon { get; set; }

        [StringLength(50), Column("uri"), Display(Name = "uri")]
        public string Uri { get; set; }

        [NotMapped, Display(Name="permission")]
        public List<string> UiPermission { get; set; }
        [StringLength(255), Column("permission")]
        public string Permission { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        [NotMapped, Display(Name="roles")]
        public List<string> UiRoles { get; set; }
        [Display(Name = "title")]
        public ICollection<Roles> Roles { get; set; } = new HashSet<Roles>();


        //public List<MenuModel> Children { get; set; }

    }
}
