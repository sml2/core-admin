﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace CoreAdmin.Mvc.Models
{
    [Table("Roles")]
    [Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class Roles : IdentityRole<int>, IIdexerModel
    {
        private Dictionary<string, object> dic = new();
        public object this[string col] { get => dic.ContainsKey(col) ? dic[col] : null; set => dic[col] = value; }

        [Column("id")]
        public override int Id { get; set; }
        [StringLength(50), Required, Column("name")]
        public override string Name { get; set; }

        [StringLength(50), Required, Column("slug")]
        public string Slug { get; set; }
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }

        private List<string> _permissionsList;
        [NotMapped]
        public List<string> PermissionsList {
            get => Permissions != null ? Permissions.Select(p => p.ID.ToString()).ToList() : _permissionsList;
            set => _permissionsList = value;
        }

        //多对多映射
        public ICollection<Permissions> Permissions { get; set; }

        [JsonIgnore]
        //多对多映射
        public ICollection<UserModel> Users { get; set; }

        public List<UserRole> UserRoles { get; set; }
        //多对多映射
        public ICollection<MenuModel> Menu { get; set; }

    }
}
