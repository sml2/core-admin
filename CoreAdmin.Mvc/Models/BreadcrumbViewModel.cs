﻿namespace CoreAdmin.Mvc.Models
{
    public class BreadcrumbViewModel
    {
        public string Icon { get; set; }
        public bool HasIcon { get => !string.IsNullOrEmpty(Icon); }
        public string Text { get; set; }
        public string Url { get; set; }
        public bool HasUrl { get => !string.IsNullOrEmpty(Url); }

    }
}
