using System.ComponentModel.DataAnnotations;

namespace CoreAdmin.Mvc.Models
{
    public class LoginViewModel
    {
        [StringLength(20, MinimumLength = 2), Display(Name = "username")]
        [Required]
        public string Username { get; set; }

        [StringLength(20, MinimumLength = 6), Display(Name = "password")]
        [Required]
        public string Password { get; set; }

        [Display(Name = "remember_me")]
        public bool RememberMe { get; set; }
    }
}
