﻿using CoreAdmin.Mvc.Interface;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Models
{
    public class HtmlContent
    {
        public HtmlContentType Name { get; set; } = HtmlContentType.Str;
        public string Value { get; set; }
        public ViewDataDictionary ViewData { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public IViewData Form { get; set; }

    }

    public enum HtmlContentType
    {
        Str,
        Partial,
        Component
    }
}
