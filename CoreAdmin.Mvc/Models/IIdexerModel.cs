﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Models
{

    public interface IIdexerModel
    {
        public object this[string col] { get; set; }
    }
}
