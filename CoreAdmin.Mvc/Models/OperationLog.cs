﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreAdmin.Mvc.Models
{
    [Table("OperationLog")]
    [Index(new string[] { nameof(UserId) })]
    public class OperationLog : BaseModel
    {

        [Column("id")]
        public int ID { get; set; }
        [Required, Column("user_id")]
        public int UserId { get; set; }
        [Required, Column("username")]
        public string Username { get; set; }


        [StringLength(50), Required, Column("path")]
        public string Path { get; set; }
        [StringLength(50), Required, Column("method")]
        public string Method { get; set; }
        [StringLength(50), Required, Column("ip")]
        public string IP { get; set; }
        [StringLength(50), Required, Column("host")]
        public string Host { get; set; }
        [Column("query")]
        public string Query { get; set; }
        [Column("body"), Comment("可为空,表示没有请求体")]
        public string Body { get; set; }
        [Display(Name = "响应码[长度]")]
        public string RequestInfoUI { get => $"{ResponseCode}[{ContentLength}]"; }
        [Comment("响应码"), Display(Name = "响应码")]
        public int ResponseCode { get; set; }
        [Comment("响应内容长度"), Display(Name = "响应内容长度")]
        public long ContentLength { get; set; }
        [Column("created_at")]
        [Display(Name = "请求时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
