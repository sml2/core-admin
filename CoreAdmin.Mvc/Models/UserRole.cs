﻿using Microsoft.AspNetCore.Identity;

namespace CoreAdmin.Mvc.Models
{
    public class UserRole : IdentityUserRole<int>
    {
        public virtual UserModel User { get; set; }

        public virtual Roles Role { get; set; }
    }
}