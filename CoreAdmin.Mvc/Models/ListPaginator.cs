﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.Models
{
    public class ListPaginator<TSource> : List<TSource>
    {
        public const string PageKeyName = "page";
        
        public int CurrentPage { get; protected set; }
        public bool OnFirstPage => CurrentPage <= 1;
        public int LastPage => Math.Max((int)Math.Ceiling((double)Total / PageSize), 1);
        public int SkipNum => (int)(PageSize * (CurrentPage - 1));
        public bool HasMorePages => CurrentPage < LastPage;
        public uint PageSize { get; protected set; }
        public uint Total { get; protected set; }
        public int OnEachSide { get; set; } = 3;
        public bool HasPages => CurrentPage != 1 || HasMorePages;

        public uint FirstItem => (uint)(Count > 0 ? (CurrentPage - 1) * PageSize + 1 : 0);
        public uint LastItem => (uint)(Count > 0 ? FirstItem + Count - 1 : 0);


        public string Path => _context.Request.Path;
        private HttpContext _context;

        public ListPaginator(uint count, int page, int pageSize, HttpContext gridContext)
        {
            Total = count;
            PageSize = (uint)pageSize;
            CurrentPage = Math.Min(page,LastPage);
            _context = gridContext;
        }

        public string PreviousPageUrl()
        {
            return CurrentPage > 1 ? Url(CurrentPage - 1) : null;
        }
        public string NextPageUrl()
        {
            return HasMorePages ? Url(CurrentPage + 1) : null;
        }


        public string Url(int page)
        {
            if (page == 0)
            {
                page = 1;
            }
            
            return _context.Request.FullUrlWithQuery(PageKeyName, page);
        }

        public Dictionary<int, string> GetUrlRange(int start, int end)
        {
            return Enumerable.Range(start, end - start + 1).Select((page) => KeyValuePair.Create(page, Url(page))).ToDictionary(p => p.Key, p => p.Value);
        }

        public List<dynamic> Elements()
        {
            var window = UrlWindow<TSource>.Make(this);

            return new List<dynamic>()
            {
                window["first"],
                window["slider"] is Dictionary<int, string> ? "..." : null,
                window["slider"],
                window["last"] is Dictionary<int, string> ? "..." : null,
                window["last"],
            }.Where(value => value != null).ToList();
        }


    }
    public static class ListPaginator
    {
        public static async Task<ListPaginator<TModel>> CreateAsync<TModel>(IQueryable<TModel> sources, int page,
            int pageSize, HttpContext gridContext)
        {
            var count = (uint) await sources.CountAsync();
            ListPaginator<TModel> lp = new (count, page, pageSize, gridContext);
            var item = await sources.Skip(lp.SkipNum).Take(pageSize).ToListAsync();
            lp.AddRange(item);
            return lp;
        }
    }
}
