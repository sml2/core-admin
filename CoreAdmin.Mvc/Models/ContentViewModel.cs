﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Models
{
    public class ContentViewModel
    {
        public bool HasBreadcrumb { get => null != Breadcrumb && Breadcrumb.Count > 0; }

        //public bool HasFullScreen { get => FullScreenSwitch; }

        public bool FullScreenSwitch { get; set; }
        public List<BreadcrumbViewModel> Breadcrumb { get; set; }

        public Uri Uri { get => new("http://a.x.com/aa"); }

        /// <summary>
        /// @include($_view_['view'], $_view_['data'])
        /// </summary>
        public Dictionary<string, string> View { get; set; }
        public Layout.Content Content { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }

        public UserModel User { get; set; }
    }
}
