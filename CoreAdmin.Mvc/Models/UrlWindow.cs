﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Models
{
    public class UrlWindow<TSource>
    {

        protected ListPaginator<TSource> Paginator;

        public UrlWindow(ListPaginator<TSource> paginator)
        {
            Paginator = paginator;
        }

        public static Dictionary<string, dynamic> Make(ListPaginator<TSource> paginator)
        {
            return new UrlWindow<TSource>(paginator).Get();
        }

        public Dictionary<string, dynamic> Get()
        {
            var onEachSide = Paginator.OnEachSide;

            if (Paginator.LastPage < onEachSide * 2 + 8)
            {
                return GetSmallSlider();
            }

            return GetUrlSlider(onEachSide);
        }

        protected Dictionary<string, dynamic> GetSmallSlider()
        {
            return new()
            {
                { "first", Paginator.GetUrlRange(1, Paginator.LastPage) },
                { "slider", null },
                { "last", null },
            };
        }

        protected Dictionary<string, dynamic> GetUrlSlider(int onEachSide)
        {
            var window = onEachSide + 4;

            if (!Paginator.HasPages)
            {
                return new() { { "first", null }, { "slider", null }, { "last", null } };
            }

            // If the current page is very close to the beginning of the page range, we will
            // just render the beginning of the page range, followed by the last 2 of the
            // links in this list, since we will not have room to create a full slider.
            if (Paginator.CurrentPage <= window)
            {
                return GetSliderTooCloseToBeginning(window, onEachSide);
            }

            // If the current page is close to the ending of the page range we will just get
            // this first couple pages, followed by a larger window of these ending pages
            // since we're too close to the end of the list to create a full on slider.

            if (Paginator.CurrentPage > Paginator.LastPage - window)
            {
                return GetSliderTooCloseToEnding(window, onEachSide);
            }

            // If we have enough room on both sides of the current page to build a slider we
            // will surround it with both the beginning and ending caps, with this window
            // of pages in the middle providing a Google style sliding paginator setup.
            return GetFullSlider(onEachSide);
        }

        protected Dictionary<string, dynamic> GetSliderTooCloseToBeginning(int window, int onEachSide)
        {
            return new()
            {
                { "first", Paginator.GetUrlRange(1, window + onEachSide) },
                { "slider", null },
                { "last", GetFinish() },
            };
        }

        protected Dictionary<string, dynamic> GetSliderTooCloseToEnding(int window, int onEachSide)
        {
            var last = Paginator.GetUrlRange(
                Paginator.LastPage - (window + (onEachSide - 1)),
                Paginator.LastPage
            );

            return new()
            {
                { "first", GetStart() },
                { "slider", null },
                { "last", last },
            };
        }

        protected Dictionary<string, dynamic> GetFullSlider(int onEachSide)
        {
            return new()
            {
                { "first", GetStart() },
                { "slider", GetAdjacentUrlRange(onEachSide) },
                { "last", GetFinish() },
            };
        }

        public Dictionary<int, string> GetAdjacentUrlRange(int onEachSide)
        {
            return Paginator.GetUrlRange(
                Paginator.CurrentPage - onEachSide,
                Paginator.CurrentPage + onEachSide
            );
        }

        public Dictionary<int, string> GetStart()
        {
            return Paginator.GetUrlRange(1, 2);
        }

        public Dictionary<int, string> GetFinish()
        {
            return Paginator.GetUrlRange(
                Paginator.LastPage - 1,
                Paginator.LastPage
            );
        }

    }
}
