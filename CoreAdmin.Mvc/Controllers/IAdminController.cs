﻿using System.Threading.Tasks;
using CoreAdmin.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CoreAdmin.Mvc.Controllers
{
    public interface IAdminController
    {
        public BaseDbContext DbContext { get; set; }
        public ModelStateDictionary ModelState { get; }
        public HttpRequest Request { get; }

        public JsonResult Json(object data);
        Task<IActionResult> Update(BForm bForm, int v);
        RedirectResult Redirect(string url);
        Task<IActionResult> Store(BForm bForm);
    }
}
