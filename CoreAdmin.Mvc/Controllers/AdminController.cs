﻿using CoreAdmin.EF;
using CoreAdmin.Mvc.Layout;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace CoreAdmin.Mvc.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public abstract class AdminController : Controller, IAdminController
    {
        /// <summary>
        /// 获取或设置当前页面的标题
        /// </summary>
        protected string Title { get; set; } = "Title";
        protected Content content => HttpContext?.RequestServices.GetRequiredService<Content>();
        private BaseDbContext _dbContext;

        public BaseDbContext DbContext
        {
            get => _dbContext ??= Utils.DefaultDbContext;
            set => _dbContext = value;
        }

        protected AdminDbContext AdminContext => Utils.DefaultDbContext;

        private Admin _utils;
        protected Admin Utils => _utils ??= HttpContext?.RequestServices.GetRequiredService<Admin>();

        protected Dictionary<string, string> Description = new()
        {
            //        'index'  => 'Index',
            //        'show'   => 'Show',
            //        'edit'   => 'Edit',
            //        'create' => 'Create',
        };

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        [Route("")]
        public virtual async Task<IActionResult> Index()
        {
            Description.TryGetValue("index", out var desc);
            await content.WithController(this)
                .Title(Title)
                .Description(desc ?? Admin.Trans("list"))
                .Body(Grid());
            return await content.Render();
        }
        protected virtual new ViewResult NotFound()
        {
            return new ViewResult() { ViewName = "~/Views/StateCode/404.cshtml", StatusCode = 404 };
        }
        /// <summary>
        /// 详情页
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public virtual async Task<IActionResult> Show(int id)
        {
            // 详情页返回null代表没找到数据
            var detail = Detail(id);
            if (detail == null)
            {
                return NotFound();//此处暂采用MVC的,泛型方法会重载此方法使用自定义的404页面
            }
            Description.TryGetValue("show", out var desc);
            return await content.WithController(this)
                  .Title(Title)
                .Description(desc ?? Admin.Trans("show"))
                .Body(detail)
                .Render();
        }
        
        /// <summary>
        /// 编辑页
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[action]/{id:int}")]
        public async Task<IActionResult> Edit(int id)
        {
            Description.TryGetValue("edit", out var edit);
            return await content.WithController(this)
                .Title(Title)
                .Description(edit ?? Admin.Trans("edit"))
                .Body(Form().Edit(id))
                .Render();
        }
        
        /// <summary>
        /// 新增页
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> Create()
        {
            Description.TryGetValue("create", out var create);
            return await content.WithController(this)
                .Title(Title)
                .Description(create ?? Admin.Trans("create"))
                .Body(Form()).Render();
        }


        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [NonAction]
        public async Task<IActionResult> Store(BForm form)
        {
            Description.TryGetValue("create", out var create);
            return await content.WithController(this)
                .Title(Title)
                .Description(create ?? Admin.Trans("create"))
                .Body(form).Render();
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual Task<IActionResult> Update(int id)
        {
            return Form().Update(null);
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="form"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [NonAction]
        public async Task<IActionResult> Update(BForm form, int id)
        {
            Description.TryGetValue("edit", out var edit);
            return await content.WithController(this)
                .Title(Title)
                .Description(edit ?? Admin.Trans("edit"))
                .Body(form.Edit(id))
                .Render(); ;
        }

        /// <summary>
        /// 详情页
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        protected abstract BShow Detail(int id);
        //将"运行时"出错提前到"开发时"
        //protected virtual BShow Detail(int id)
        //{
        //    throw new Exception("未实现详情页");
        //}

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">根据id删除[单个、批量]</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> Destroy(string id)
        {
            return await Form().Destroy(id.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k)));
        }
        
        /// <summary>
        /// 清空表请求
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public virtual async Task<IActionResult> Truncate()
        {
            var grid = Grid();
            if (!grid.ShowTruncateBtn())
            {
                Admin.Error("无权访问", "无权访问");
                return Redirect("");
            }
            return await Form().Truncate();
        }
        protected virtual BForm Form() => null;
        protected virtual BGrid Grid() => null;
    }
}
