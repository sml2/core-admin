﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.Controllers
{
    [Authorize]
    [Route("/[controller]/")]
    public abstract class AdminController<TModel> : AdminController
        where TModel : class, IIdexerModel, new()
    {

        /// <summary>
        /// 表单保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("")]//?? 1 [HttpPost()==HttpPost(null)=>HttpMethod(["POST"])=>HttpMethod(["POST"],null)][HttpPost("xxx")=HttpMethod(["POST"],"xxx")], 1.5 {}, 2 Order,3 Route
        public virtual Task<IActionResult> Post(TModel model)
        {
            return Form().Store(model, ModelState);
        }

        /// <summary>
        /// 行内编辑更新路由
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        public virtual IActionResult Index(int id)
        {
            return Form().UpdateInlineEdit<TModel>(id);
        }

        /// <summary>
        /// 表单页，新增、修改
        /// </summary>
        /// <returns></returns>
        protected abstract override BForm Form();

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        protected abstract override BGrid Grid();
    }
}