﻿using CoreAdmin.Models;
using CoreAdmin.Mvc.Layout;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace CoreAdmin.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> Loger;

        public HomeController(ILogger<HomeController> logger)
        {
            Loger = logger;
            Loger.LogInformation("HomeController Be Create");
        }

        [Authorize]
        public Task<IActionResult> Index([FromServices] Content content)
        {

            return content.WithController(this).Title("Dashboard")
                .Description("Description...")
                .Row(Dashboard.Title())
                .Row((row) =>
                {
                    row.Column(4, (column) =>
                    {
                        column.Append(Dashboard.Environment());
                    });

                    row.Column(4, (column) =>
                    {
                        column.Append(Dashboard.Extensions());
                    });

                    row.Column(4, (column) =>
                    {
                        column.Append(Dashboard.Dependencies());
                    });
                })
                .Render();
        }
        
        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View("Index");
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //    return NotFound();
        //}
    }
}
