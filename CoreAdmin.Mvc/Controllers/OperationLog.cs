﻿using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Grid.Displayers;
using Microsoft.AspNetCore.Mvc;
using Model = CoreAdmin.Mvc.Models.OperationLog;

// using Microsoft.EntityFrameworkCore.Relational;

namespace CoreAdmin.Mvc.Controllers
{
    public class OperationLog : AdminController
    {
        public OperationLog()
        {
            Title = Admin.Trans("log"); ;
        }
        protected override BGrid Grid()
        {
            var operationLogModel = AdminContext.OperationLog.OrderByDescending(l => l.ID);

        var methodColors = new Dictionary<object, BgColor> { { "GET", BgColor.Green }, { "POST", BgColor.Yellow }, { "PUT", BgColor.Blue }, { "DELETE", BgColor.Red } };
        
            var grid = new BGrid(operationLogModel, HttpContext);
            grid.Column(nameof(Model.ID), "Id").Sortable();
            grid.Column(nameof(Model.Username), "User");
            grid.Column(nameof(Model.RequestInfoUI));
            grid.Column(nameof(Model.IP), "IP").Label(LabelColor.Primary);
            grid.Column(nameof(Model.Host), "Host");
            grid.Column(nameof(Model.Method), "Method").Badge(methodColors);
            grid.Column(nameof(Model.Path), "Path").Label(LabelColor.Info).MaxWidth(500);
            grid.Column(nameof(Model.Query), "Query").MaxWidth(500);
            grid.Column(nameof(Model.Body), "Body").MaxWidth(300).Display<string>(Value => string.IsNullOrWhiteSpace(Value) ? "<code>{}</code>" : $"<pre title='{Value}' style='overflow:hidden;padding: 2px 4px;  margin: 0; '>{Value}</pre>");
            grid.Column(nameof(Model.CreatedAt));

            grid.DisableTruncateButton(false);
            grid.Actions((actions) => {
                actions.DisableEdit();
                actions.DisableView();
            });
            return grid;
        }

        protected override BForm Form()
        {
            var operationLogModel = AdminContext.OperationLog;
            return new BForm(operationLogModel).WithController(this);
        }

        protected override BShow Detail(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
