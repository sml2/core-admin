﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreAdmin.EF;
using CoreAdmin.Mvc.Form;
using CoreAdmin.Mvc.Layout;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace CoreAdmin.Mvc.Controllers
{
    public class AuthController : AdminController
    {
        private readonly SignInManager<UserModel> _signInManager;
        private readonly ILogger<AuthController> _logger;
        private readonly AdminDbContext _dbContext;
        private readonly UserManager<UserModel> _userManager;
        public static string DefaultLoginSuccessPath = null;

        /// <summary>
        /// 登录成功事件
        /// </summary>
        private event Action<UserModel> LoginSuccessHandler;

        /// <summary>
        /// 注销成功事件
        /// </summary>
        private event Action LogoutSuccessHandler;

        public AuthController(SignInManager<UserModel> signInManager, ILogger<AuthController> logger,
            AdminDbContext dbContext, UserManager<UserModel> userManager)
        {
            _signInManager = signInManager;
            _logger = logger;
            _dbContext = dbContext;
            _userManager = userManager;

            LoginSuccessHandler += _ => { Admin.Toastr(Admin.Trans("login_successful")); };

            LogoutSuccessHandler += () => { _logger.LogInformation("User logged out."); };
        }

        [HttpGet("/login")]
        [HttpGet("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user != null) return RedirectToAction("Index", "Home");
            var model = new LoginViewModel();
            return View(model);
        }

        [HttpPost("/login")]
        [HttpPost("[action]")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe,
                    false);

                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    OnLoginSuccessHandler(await _signInManager.UserManager.GetUserAsync(User));
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            return View(model);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else if (Url.IsLocalUrl(DefaultLoginSuccessPath))
            {
                return Redirect(DefaultLoginSuccessPath);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        [HttpGet("/logout")]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            OnLogoutSuccessHandler();
            return RedirectToAction(nameof(Login), "Auth");
        }

        [HttpGet]
        public Task<IActionResult> Setting()
        {
            var form = SettingForm().WithController(this);
            form.Tools(
                tools =>
                {
                    tools.DisableList();
                    tools.DisableDelete();
                    tools.DisableView();
                }
            );

            return content
                .WithController(this)
                .Title(Admin.Trans("user_setting"))
                .Body(form.Edit(Convert.ToInt32(_signInManager.UserManager.GetUserId(User))))
                .Render();
        }

        [HttpPost("setting")]
        public async Task<IActionResult> PutSetting()
        {
            var user = await _signInManager.UserManager.GetUserAsync(User);

            user.Name = Request.Form["Name"];
            user.Avatar = Request.Form["Avatar"];
            user.PasswordHash =
                _signInManager.UserManager.PasswordHasher.HashPassword(user, Request.Form["PasswordHash"]);
            return await SettingForm().Update(user);
        }

        protected BForm SettingForm()
        {
            //$class = config("admin.database.users_model');

            var form = (new BForm(_dbContext.Users)).WithController(this);

            form.Display(nameof(UserModel.UserName));
            form.Text(nameof(UserModel.Name));
            form.Image(nameof(UserModel.Avatar));
            form.Password(nameof(UserModel.PasswordHash));
            form.Password("password_confirmation", Admin.Trans("admin.password_confirmation"))
                .Default(bForm => (bForm.Original as UserModel)?.PasswordHash);

            form.SetAction(Admin.Url("auth/setting"));

            form.Ignore(new List<string>
            {
                "password_confirmation"
            });

            //             form.Saving(form =>
            // {
            //     if (form.Password && $form->model()->password != $form->password) {
            //                     $form->password = Hash::make($form->password);
            //     }
            // });

            form.Saved(() =>
            {
                // _signInManager.UserManager.ChangePasswordAsync()
                Admin.Toastr(Admin.Trans("update_succeeded"));

                return Redirect(Admin.Url("auth"));
            });

            return form;
        }

        public string Username()
        {
            return _signInManager.UserManager.GetUserName(User);
        }

        /// <summary>
        /// 登录成功触发器
        /// </summary>
        /// <param name="obj"></param>
        protected virtual void OnLoginSuccessHandler(UserModel obj)
        {
            LoginSuccessHandler?.Invoke(obj);
        }

        /// <summary>
        /// 退出成功触发器
        /// </summary>
        protected virtual void OnLogoutSuccessHandler()
        {
            LogoutSuccessHandler?.Invoke();
        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }
    }
}