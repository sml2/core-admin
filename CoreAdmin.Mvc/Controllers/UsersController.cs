﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Identity;

namespace CoreAdmin.Mvc.Controllers
{
    public class UsersController : AdminController<UserModel>
    {
        private readonly UserManager<UserModel> _userManager;

        public UsersController(UserManager<UserModel> userManager)
        {
            _userManager = userManager;
            Title = Admin.Trans("user");
        }

        protected override BGrid Grid()
        {
            var usermodel = AdminContext.Users.Include(u => u.Roles);
            var grid = new BGrid(usermodel, HttpContext);
            var a = Utils;

            grid.Column(nameof(UserModel.Id), "Id").Sortable();
            grid.Column(nameof(UserModel.UserName)).MinWidth(60);
            grid.Column(nameof(UserModel.Name), Admin.Trans("name")).MinWidth(60);
            grid.Column("Roles.Name", Admin.Trans("roles"))
                .Display<IEnumerable<object>>(value => string.Join(",", value)).MinWidth(60);
            // grid.Column("roles", Admin.Trans("roles")).Call("pluck", "name").Call("label");
            grid.Column(nameof(UserModel.CreatedAt), Admin.Trans("created_at")).MinWidth(60);
            grid.Column(nameof(UserModel.UpdatedAt)).MinWidth(60);

            grid.Actions((actions) =>
            {
                if (actions.GetKey() == 1)
                {
                    actions.DisableDelete();
                }
            });

            grid.Tools(tools => { tools.Batch((actions) => { actions.DisableDelete(); }); });

            return grid;
        }

        protected override BShow Detail(int id)
        {
            var userModel = AdminContext.Users.Include(m => m.Roles).Include(m => m.Permissions);
            var show = new BShow(userModel.Single(m => m.Id == id), null, HttpContext);

            show.Field(nameof(UserModel.Id));
            show.Field(nameof(UserModel.UserName));
            show.Field(nameof(UserModel.Name));
            show.Field(nameof(UserModel.Roles)).As<IEnumerable<Roles>>((roles) => roles.Pluck("Name")).Label();
            show.Field(nameof(UserModel.Permissions), Admin.Trans("permissions"))
                .As<IEnumerable<Permissions>>((permission) => permission.Pluck("Name")).Label();
            show.Field(nameof(UserModel.CreatedAt));
            show.Field(nameof(UserModel.UpdatedAt));

            return show;
        }


        protected override BForm Form()
        {
            var userModel = AdminContext.Users;
            var roleModel = AdminContext.Roles;
            var permissionsModel = AdminContext.Permissions;

            var form = new BForm(userModel).WithController(this);

            form.Display(nameof(UserModel.Id), "Id");
            form.Text(nameof(UserModel.UserName), Admin.Trans("username"))
                .CreationRules(new() {"required"})
                .UpdateRules(new() {"required"});

            form.Text(nameof(UserModel.Name));
            form.Image(nameof(UserModel.Avatar)).Default("/_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg");
            form.Password(nameof(UserModel.PasswordHash));
            //form.Password("password_confirmation", Admin.Trans("password_confirmation")).Rules("required")
            //    .Default((form) =>
            //    {
            //        return form.Model().password;
            //    });

            form.Ignore(new() {"password_confirmation"});
            form.MultipleSelect(nameof(UserModel.RolesList)).Options(roleModel
                .Select(role => KeyValuePair.Create(role.Id.ToString(), role.Name))
                .ToDictionary(x => x.Key, x => x.Value));
            form.MultipleSelect(nameof(UserModel.PermissionsList)).Options(permissionsModel
                .Select(r => KeyValuePair.Create(r.ID.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value));

            form.Display("created_at");
            form.Display("updated_at");

            form.Saving((bForm) =>
            {
                var user = bForm.Original as UserModel;
                var password = user?.PasswordHash;
                if (!string.IsNullOrEmpty(password) && (bForm.Model() as UserModel)?.PasswordHash != password)
                {
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, password);
                }

                user.NormalizedUserName = _userManager.KeyNormalizer.NormalizeName(user.Name);
                _userManager.UpdateSecurityStampAsync(user);
                return null;
            });

            return form;
        }

        public override Task<IActionResult> Post(UserModel model)
        {
            var PermissionsList = model.PermissionsList.Where(p => !string.IsNullOrEmpty(p))
                .Select(p => Convert.ToInt32(p));
            model.Permissions = AdminContext.Permissions.Where(p => PermissionsList.Contains(p.ID)).ToList();
            var RolesList = model.RolesList.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            model.Roles = AdminContext.Roles.Where(p => RolesList.Contains(p.Id)).ToList();
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            return base.Post(model);
        }
    }
}