﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Controllers
{
    public class PermissionsController : AdminController
    {
        public PermissionsController()
        {
            Title = "权限";
        }

        protected new Dictionary<string, string> Description = new()
        {
            //        'index'  => 'Index',
            //        'show'   => 'Show',
            //        'edit'   => 'Edit',
        };


        protected override BForm Form()
        {
            var Model = AdminContext.Permissions;

            var form = new BForm(Model).WithController(this);

            form.Text(nameof(Permissions.Slug), "标识")
                .CreationRules(new() { "required" })
                .UpdateRules(new() { "required" });

            form.Text(nameof(Permissions.Name), "名称");
            var method = new Dictionary<string, string>() { { "GET", "GET" }, { "POST", "POST" }, { "PUT", "PUT" }, { "DELETE", "DELETE" }, { "PATCH", "PATCH" }, { "OPTIONS", "OPTIONS" }, { "HEAD", "HEAD" } };

            form.MultipleSelect(nameof(Permissions.HttpMethodList), "HTTP方法").Options(method);
            form.Textarea(nameof(Permissions.HttpPathList), "HTTP路径");
            return form;
        }

        protected override BGrid Grid()
        {
            var model = AdminContext.Permissions;

            var Grid = new BGrid(model, HttpContext);


            Grid.Column(nameof(Permissions.ID), "Id");
            Grid.Column(nameof(Permissions.Slug), "标识").MinWidth(60);
            Grid.Column(nameof(Permissions.Name), "名称").MinWidth(60);
            Grid.Column(nameof(Permissions.HttpMethod), "路由").MinWidth(120);
            Grid.Column(nameof(Permissions.HttpPath), "路径").Display<string>((value, model) =>
            {
                var data = (Permissions)model;
                
                //路由
                List<string> Method = data.HttpMethod.Split(",").Where(m => !string.IsNullOrEmpty(m)).ToList();
                string methodStr = "";
                if (Method.Count() > 0)
                {
                    foreach (string i in Method)
                    {
                        if (!string.IsNullOrEmpty(i))
                        {
                            methodStr += "<span class=\"label label-primary\">" + i + "</span>&nbsp;";
                        }

                    }
                }
                else
                {
                    methodStr = "<span class=\"label label-primary\">ANY</span>&nbsp;";
                }

                //路径
                List<string> Path = value.Split(",").ToList();
                List<string> PStringList = new List<string>();
                foreach (string i in Path)
                {
                    if (!string.IsNullOrEmpty(i))
                    {
                        PStringList.Add("<div style=\"margin-bottom: 5px; \">" + methodStr + "&nbsp;<code>" + i + "</code></div>");
                    }

                }
                return string.Join("\r\n", PStringList);
            }).MinWidth(260);
            Grid.Column(nameof(Permissions.CreatedAt), "创建时间").MinWidth(120);
            Grid.Column(nameof(Permissions.UpdatedAt), "更新时间").MinWidth(120);

            return Grid;
        }

        //[HttpPost]
        //public dynamic Store([Bind("Slug,Name,HttpMethodList,HttpPath")] Mvc.Models.Permissions model)
        //{
        //    model.CreatedAt = DateTime.Now;
        //    model.UpdatedAt = DateTime.Now;
        //    AdminContext.Permissions.Add(model);
        //    AdminContext.SaveChanges();
        //    return Redirect("/permissions");
        //}

        [HttpPost]
        public Task<IActionResult> Index([Bind("Slug,Name,HttpMethodList,HttpPathList")] Permissions model)
        {
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            return Form().Store(model);
        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }
    }
}
