﻿#nullable enable
using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Widgets;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using Microsoft.AspNetCore.Routing;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Controllers
{
    public class MenuController : AdminController
    {
        /// <summary>
        /// 拿到路由终结点集合服务
        /// </summary>
        private EndpointDataSource _endpointDataSource { get; }

        public MenuController(EndpointDataSource endpointDataSource)
        {
            _endpointDataSource = endpointDataSource;
        }


        /// <summary>
        /// 筛选，加工终结点
        /// </summary>
        /// <returns></returns>
        private Dictionary<string,string> GetSelectItems() 
        {
            List<string> lst = new();
            foreach (var item in _endpointDataSource.Endpoints.Filter())
            {
                if (item is RouteEndpoint one) 
                {
                    var RoutePattern = one.RoutePattern;
                    var RawText = RoutePattern.RawText;
                    var Parameters = RoutePattern.Parameters;
                    var ParameterPolicies = RoutePattern.ParameterPolicies;
                    var RequiredValues = RoutePattern.RequiredValues;
                    if (!string.IsNullOrEmpty(RawText) && ParameterPolicies.Count == 0)
                    {
                        var Temp = RawText;
                        foreach (var kp in RequiredValues)
                        {
                            Temp = Temp.Replace($"{{{kp.Key}}}", $"{kp.Value}");
                        }
                        if (Parameters.Count == 1) {
                            var p = Parameters[0];
                            if (p.IsOptional && p.Default == null) { 
                                Temp = Temp.Replace($"{{{p.Name}?}}", "");
                            }
                        }
                        if (Temp.LastIndexOfAny("{}".ToCharArray())==-1 && !lst.Contains(Temp)) lst.Add(Temp);
                    }
                }
            }
            return lst.ToDictionary(s => s, s => s);
        }

        public override async Task<IActionResult> Index()
        {
            var menuModel = AdminContext.Menu;
            var permissionModel = AdminContext.Permissions;
            var roleModel = AdminContext.Roles;

            var options = await menuModel.SelectOptions();
            var htmlContent = await TreeView().Render();
            return await content.WithController(this)
                .Title(Admin.Trans("menu"))
                .Description(Admin.Trans("list"))
                .Row((row) =>
                {
                    row.Column(6, htmlContent);

                row.Column(6, (column) => {
                    var form = new Widgets.Form(HttpContext);
                    form.SetType(typeof(MenuModel));
                    form.Action(Admin.Url("menu"));

                    form.Select(nameof(MenuModel.ParentID)).Options(options);
                    form.Text(nameof(MenuModel.Title));
                    form.Icon(nameof(MenuModel.Icon)).Default("fa-bars").Help(IconHelp());
                    //form.Text(nameof(MenuModel.Uri));
                    form.Select(nameof(MenuModel.Uri)).Options(GetSelectItems());
                    form.MultipleSelect(nameof(MenuModel.UiRoles)).Options(roleModel.ToList().Pluck("Name", "Id"));
                    //if ((new $menuModel())->withPermission()) {
                    form.Select(nameof(MenuModel.UiPermission)).Options(permissionModel.ToList().Pluck("Name", "Slug"));
                    //}
                    //form.Hidden("_token").Default(csrf_token());

                    column.Append((new Box(Admin.Trans("new"), form.Render())).Style("success").Render());
                });
            }).Render();
        }

        //public function show($id)
        //{
        //    return redirect()->route('admin.auth.menu.edit', ['menu' => $id]);
        //}

        private BTree<MenuModel> TreeView()
        {
            var menuModel = AdminContext.Menu;

            var tree = new BTree<MenuModel>(menuModel, HttpContext);

            tree.DisableCreate();

            tree.Branch((menu) =>
            {
                var payload = $"<i class='fa {menu.Icon}'></i>&nbsp;<strong>{menu.Title}</strong>";

                if (menu.HasChildren)
                {
                    string uri;
                    var url = menu.Uri;
                    if (url.IsValidUrl())
                    {
                        uri = menu.Uri;
                    }
                    else
                    {
                        uri = Admin.Url(menu.Uri);
                    }

                    payload += $"&nbsp;&nbsp;&nbsp;<a href=\"{url}\" class=\"dd-nodrag\">{uri}</a>";
                }

                return payload;
            });

            return tree;
        }
        [HttpGet("{id:int}/[action]")]
        public new async Task<IActionResult> Edit(int id)
        {
            return await content.WithController(this)
                .Title(Admin.Trans("menu"))
                .Description(Admin.Trans("edit"))
                .Row((await Form(true)).Edit(id))
                .Render();
        }

        protected override BForm Form()
        {
            var menuModel = AdminContext.Menu;
            var form = new BForm(menuModel).WithController(this);

            return form;
        }

        private async Task<BForm> Form(bool async)
        {
            var menuModel = AdminContext.Menu;
            var permissionModel = AdminContext.Permissions;
            var roleModel = AdminContext.Roles;
            var options = await menuModel.SelectOptions();
            var form = new BForm(menuModel.Include(m => m.Roles)).WithController(this);

            form.Display(nameof(MenuModel.Id), "Id");
            form.Select(nameof(MenuModel.ParentID)).Options(options);
            form.Text(nameof(MenuModel.Title));
            form.Icon(nameof(MenuModel.Icon)).Default("fa-bars").Help(IconHelp());
            //form.Text(nameof(MenuModel.Uri));
            form.Select(nameof(MenuModel.Uri)).Options(GetSelectItems());
            form.MultipleSelect(nameof(MenuModel.UiRoles))
                .Default(bForm => ((MenuModel)bForm.Original).Roles.Select(r => r.Id.ToString()).ToList())
                .Options(roleModel.ToList().Pluck("Name", "Id"));
            //if ((new $menuModel())->withPermission()) {
            form.Select(nameof(MenuModel.UiPermission)).Options(permissionModel.ToList().Pluck("Name", "Slug")).Default(bform => ((MenuModel)bform.Original).Permission);
            //}

            form.Display(nameof(MenuModel.CreatedAt));
            form.Display(nameof(MenuModel.UpdatedAt));

            return form;
        }
        
        [HttpPost]
        public Task<IActionResult> Index(MenuModel model)
        {
            var rolesList = model.UiRoles.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            model.Roles = AdminContext.Roles.Where(p => rolesList.Contains(p.Id)).ToList();
            var permissionList = model.UiPermission.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            model.Roles = AdminContext.Roles.Where(p => rolesList.Contains(p.Id)).ToList();
            model.Permission = string.Join(',' ,model.UiPermission);
            return Form().Store(model);
        }

        /// <summary>
        /// 目录排序   
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Order()
        {
            var tree = JsonSerializer.Deserialize<List<TreeStruct>>(Request.Form["_order"]);
            if (tree == null) return BadRequest();
            
            var menuList = AdminContext.Menu.ToList();
            BuildTree(tree);
            AdminContext.SaveChanges();
            return Ok();
        }

        /// <summary>
        ///        Order为0未指定顺序（默认值）
        /// </summary>
        /// <param name="tree"></param>
        private void BuildTree(IEnumerable<TreeStruct> tree)
        {
            var order = 0;
            OrderTree(tree, ref order);
        }
        /// <summary>
        /// 路由Order排序
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="order"></param>
        /// <param name="parentId"></param>
        private void OrderTree(IEnumerable<TreeStruct> tree, ref int order, int parentId = 0)
        {
            foreach (var node in tree)
            {
                order++;
                var model = AdminContext.Menu.SingleOrDefault(m => m.Id == node.id) ?? throw new ArgumentNullException("目录结构异常");
                model.ParentID = parentId;
                model.Order = order;
                
                if (!node.HasChildren) continue;
                
                Debug.Assert(node.children != null, "node.children != null");
                OrderTree(node.children, ref order, node.id);
            }
        }
        /// <summary>
        /// 用于上面路由的Order排序   由OrderTree调用的一个类
        /// </summary>
        public struct TreeStruct
        {
            public int id { get; set; }
            public List<TreeStruct>? children { get; set; }
            [JsonIgnore]
            public bool HasChildren => children != null && children.Count > 0;
        }
        
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, MenuModel model)
        {
            var menu = await AdminContext.Menu.SingleAsync(m => m.Id == id);
            // var permissionsList = model.UiPermission.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            // var permissions = AdminContext.Permissions.Where(p => permissionsList.Contains(p.Id)).ToList();
            menu.Permission = string.Join(',', model.UiPermission);
            var rolesList = model.UiRoles.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            var roles = AdminContext.Roles.Where(p => rolesList.Contains(p.Id)).ToList();
            menu.Roles = menu.Roles.Intersect(roles).ToList();

            menu.ParentID = model.ParentID;
            menu.Icon = model.Icon;
            menu.Title = model.Title;
            menu.Uri = model.Uri;
            menu.Icon = model.Icon;

            return await Form().Update(menu);
        }

        private static string IconHelp()
        {
            return "For more icons please see <a href=\"http://fontawesome.io/icons/\" target=\"_blank\">http://fontawesome.io/icons/</a>";
        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }
    }
}
