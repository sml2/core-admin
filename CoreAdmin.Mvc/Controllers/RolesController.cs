﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Models;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Grid.Displayers;

namespace CoreAdmin.Mvc.Controllers
{
    public class RolesController : AdminController
    {
        public RolesController() : base()
        {
            Title = "角色";
        }

        protected new Dictionary<string, string> Description = new()
        {
            //        'index'  => 'Index',
            //        'show'   => 'Show',
            //        'edit'   => 'Edit',
        };


        protected override BForm Form()
        {
            var RoleModels = AdminContext.Roles.Include(r => r.Permissions);
            var PermissionsModel = AdminContext.Permissions;

            var form = new BForm(RoleModels).WithController(this);

            //form.SetAction("/roles/store");

            form.Text(nameof(Roles.Slug), "标识")
                .CreationRules(new() { "required" })
                .UpdateRules(new() { "required" });

            form.Text(nameof(Roles.Name), "名称");
            form.MultipleSelect(nameof(Roles.PermissionsList), "权限").Options(PermissionsModel.Select(r => KeyValuePair.Create(r.ID.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value));
            return form;
        }

        protected override BGrid Grid()
        {
            var RolesModel = AdminContext.Roles.Include(r => r.Permissions);
            //RolesModel.OrderByDescending(r => r.Id);

            var Grid = new BGrid(RolesModel, HttpContext);

            //DbSet<Roles> model = Grid.Model();


            Grid.Column(nameof(Roles.Id), "Id").Sortable();
            Grid.Column(nameof(Roles.Slug), "标识").MinWidth(60);
            Grid.Column(nameof(Roles.Name), "名称").Label(new Dictionary<string, LabelColor>() {{"TEST", LabelColor.Primary}}).MinWidth(150);
            Grid.Column("Permissions.Name", "权限").Label().MinWidth(60);
            Grid.Column(nameof(Roles.CreatedAt), "创建时间").MinWidth(160);
            Grid.Column(nameof(Roles.UpdatedAt), "更新时间").MinWidth(160);

            return Grid;
        }

        [HttpPost]
        public Task<IActionResult> Index([Bind("Slug,Name,PermissionsList")] Roles model)
        {
            var PermissionsList = model.PermissionsList.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            model.Permissions = AdminContext.Permissions.Where(p => PermissionsList.Contains(p.ID)).ToList();
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            return Form().Store(model);
        }

        //[HttpPost("{id:int}")]
        //public virtual IActionResult Update(int id, [Bind("Slug,Name,PermissionsList")] Roles model)
        //{
        //    model.Id = id;
        //    var PermissionsList = model.PermissionsList.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
        //    model.Permissions = _context.Permissions.Where(p => PermissionsList.Contains(p.Id)).ToList();
        //    model.CreatedAt = DateTime.Now;
        //    model.UpdatedAt = DateTime.Now;
        //    return Form().Update(id);
        //}
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, [Bind("Slug,Name,PermissionsList")] Roles model)
        {
            var role = await AdminContext.Roles.Include(m => m.Permissions).SingleAsync(m => m.Id == id);
            var PermissionsList = model.PermissionsList.Where(p => !string.IsNullOrEmpty(p)).Select(p => Convert.ToInt32(p));
            var permissions = AdminContext.Permissions.Where(p => PermissionsList.Contains(p.ID)).ToList();
            role.Permissions = role.Permissions.Intersect(permissions).ToList();
            role.CreatedAt = DateTime.Now;
            role.UpdatedAt = DateTime.Now;
            role.Slug = model.Slug;
            role.Name = model.Name;
            return await Form().Update(role);
        }

        [Route("test")]
        public bool test()
        {
            //HttpContext.Session.Remove("userInfo");

            //content.WithError("test", "test").WithController(this);
            //return content.Render();
            //return AdminContext.Users.FirstOrDefault();
            //return ((Context)HttpContext.Reque
            //stServices.GetService(typeof(Context))).Users.FirstOrDefault();
            //var a = DbContext.Roles.Include(r => r.Permissions).ThenInclude(p => p.Roles).ToList();
            //return Json(a);
            //var a = DbContext.Roles.ToList();
            //return a
            // var userSession = Admin.User;
            //if(userSession == null)
            //{
            //return new();
            //}
            //var a = userSession.IsAdministrator();
            //return new();
            List<int> test = new List<int>() { 1, 2, 3, 4, 5 };

            foreach (var i in test)
            {
                if (i > 3)
                {
                    Console.WriteLine($"{i}结束");
                    return true;
                }
            }

            Console.WriteLine("方法结束");
            return false;

        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }
    }
}
