﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Controllers
{
    public static class Dashboard
    {
        public static HtmlContent Title()
        {
            return new()
            {
                Name = HtmlContentType.Component,
                Value = nameof(ViewComponents.Dashboard.Title),
            };
        }

        public static HtmlContent Environment()
        {
            return new()
            {
                Name = HtmlContentType.Component,
                Value = nameof(ViewComponents.Dashboard.Environment)
            };
        }

        public static HtmlContent Extensions()
        {
            return new()
            {
                Name = HtmlContentType.Component,
                Value = nameof(ViewComponents.Dashboard.Extensions)
            };
        }

        public static HtmlContent Dependencies()
        {
            return new()
            {
                Name = HtmlContentType.Component,
                Value = nameof(ViewComponents.Dashboard.Dependencies)
            };
        }
    }
}
