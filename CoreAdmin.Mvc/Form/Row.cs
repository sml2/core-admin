﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Form
{
    public class Row : IRenderable
    {
        protected Action<Row> callback;

        protected BForm form;

        protected List<Dictionary<string, string>> fields;

        protected int defaultFieldWidth = 12;

        public Row(Action<Row> callback, BForm form)
        {
            this.callback = callback;

            this.form = form;

            this.callback.Invoke(this);
        }

        public List<Dictionary<string, string>> GetFields()
        {
            return fields;
        }

        public Row Width(int width = 12)
        {
            defaultFieldWidth = width;

            return this;
        }

        public string Render() => "";
        public HtmlContent Render(bool t)
        {
            return new()
            {
                Name = HtmlContentType.Partial,
                Value = "admin::form.row",
                Data = new() { { "fields", fields } }
            };
        }

        //public Field Call(string method, params object[] arguments)
        //{
        //var field = form.Call(method, arguments);

        //field.DisableHorizontal();

        ////$this->fields[] = [
        ////    'width'   => $this->defaultFieldWidth,
        ////    'element' => $field,
        ////];

        //    return field;
        //}
    }
}
