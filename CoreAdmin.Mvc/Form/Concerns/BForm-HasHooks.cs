﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc
{
    public partial class BForm
    {
        protected List<string> hooks;

        protected event Action<BForm> CallInitCallbacks;

        public delegate object HandleAction();
        public delegate Action HandleWithIdAction(int id);
        public delegate Action HandleWithIdListAction(IEnumerable<int> id);

        public event Action<BForm> CallEditing;
        public event HandleAction CallSubmitted;
        public event Func<BForm, object, object> CallSaving;
        public event Func<BForm,object, object> CallSaved;
        public event HandleWithIdListAction CallDeleting;
        public event HandleWithIdListAction CallDeleted;

        public void Init(Action<BForm> callback = null)
        {
            CallInitCallbacks += callback;
        }

        public BForm Editing(Action<BForm> callback)
        {
            CallEditing += callback;
            return this;
        }


        public BForm Submitted(HandleAction callback)
        {
            CallSubmitted += callback;
            return this;
        }

        public BForm Saving(HandleAction callback) => Saving(_ => callback);
        public BForm Saving(Func<BForm, object> callback) => Saving((b, m) => callback(b));
        public BForm Saving(Func<BForm,object, object> callback)
        {
            CallSaving += callback;
            return this;
        }

        public BForm Saved(HandleAction callback) => Saved(_ => callback);
        public BForm Saved(Func<BForm, object> callback) => Saved((b, m) => callback(b));
        public BForm Saved(Func<BForm, object, object> callback)
        {
            CallSaved += callback;
            return this;
        }

        public BForm Deleting(HandleWithIdListAction callback)
        {
            CallDeleting += callback;
            return this;
        }

        public BForm Deleted(HandleWithIdListAction callback)
        {
            CallDeleted += callback;
            return this;
        }
    }
}
