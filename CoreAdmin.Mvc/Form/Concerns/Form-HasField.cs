﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Form;
using CoreAdmin.Mvc.Form.Field;

namespace CoreAdmin.Mvc
{
    public partial class BForm
    {
        public static List<Type> AvailableFields { get; set; } = new()
        {
            typeof(Button),
            typeof(Checkbox),
            typeof(Color),
            typeof(Currency),
            typeof(Date),
            typeof(DateRange),
            typeof(DateMultiple),
            typeof(Form.Field.Decimal),
            typeof(Display),
            typeof(Form.Field.File),
            typeof(Hidden),
            typeof(Id),
            typeof(Ip),
            typeof(Mobile),
            typeof(Month),
            typeof(MultipleSelect),
            typeof(Number),
            typeof(Radio),
            typeof(Select),
            typeof(Slider),
            typeof(SwitchField),
            typeof(Text),
            typeof(Tags),
            typeof(Icon),
            typeof(MultipleFile),
            typeof(Listbox),
            //typeof(CheckboxCard) ,
            typeof(Datetime),
            //typeof(DatetimeRange) ,
            //typeof(DatetimeRange) ,
            //typeof(Divider) ,
            //typeof(Embeds) ,
            //typeof(Email) ,
            //typeof(Image) ,
            //typeof(Password) ,
            //typeof(RadioButton) ,
            //typeof(RadioCard) ,
            //typeof(Rate) ,
            typeof(Textarea),
            //typeof(Time) ,
            //typeof(TimeRange) ,
            //typeof(Url) ,
            typeof(Year),
            //typeof(Html) ,
            //typeof(MultipleImage) ,
            //typeof(CheckboxButton) ,
            //typeof(Captcha) ,
            //typeof(Table) ,
            //typeof(Timezone) ,
            //typeof(KeyValue) ,
            //typeof(ListField) ,
            //typeof(HasMany) ,
            //typeof(HasMany) ,
            //typeof(BelongsTo) ,
            //typeof(BelongsToMany) }
            typeof(Ace),
            typeof(EditorMd)
        };

        /// <summary>
        /// 加载静态资源文件，只应该调用一次
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, List<string>> CollectFieldAssets()
        {
            if (CollectedAssets.Count > 0)
            {
                return CollectedAssets;
            }

            var css = new List<string>();
            var js = new List<string>();

            foreach (var attribute in AvailableFields.Select(type => type.GetCustomAttribute(typeof(LoadAssetsAttribute))).OfType<LoadAssetsAttribute>())
            {
                if (attribute.CssList != null && attribute.CssList.Any())
                {
                    css.AddRange(attribute.CssList);
                }
                if (attribute.JsList != null && attribute.JsList.Any())
                {
                    js.AddRange(attribute.JsList);
                }
            }

            CollectedAssets["css"] = css.Distinct().Where(c => c != "").ToList();
            CollectedAssets["js"] = js.Distinct().Where(c => c != "").ToList();

            return CollectedAssets;
        }

        public T Call<T>(params object[] arguments) where T:FField
        {
            // TODO: 生成field
            var type = typeof(T);
            var column = arguments[0] ?? ""; //[0];
            var element = arguments.Length switch
            {
                0 => (T) Activator.CreateInstance(type),
                1 => (T) Activator.CreateInstance(type, column),
                2 => (T) Activator.CreateInstance(type, column, arguments[1]),
                3 => (T) Activator.CreateInstance(type, column, arguments[1], arguments[2]),
                4 => (T) Activator.CreateInstance(type, column, arguments[1], arguments[2], arguments[3]),
                _ => default
            };
            element.SetForm(this);
            PushField(element);

            return element;
        }

        public Display Display(string column, string label = "") => Call<Display>(column, label);
        public Text Text(string column, string label = "") => Call<Text>(column, label);
        public Icon Icon(string column, string label = "") => Call<Icon>(column, label);
        public Image Image(string column, string label = "") => Call<Image>(column, label);
        public Password Password(string column, string label = "") => Call<Password>(column, label);
        public Select Select(string column) => Select(column,ModelType.GetDisplayName(column));
        public Select Select(string column, string label) => Call<Select>(column, label, ModelType.GetProperty(column)?.PropertyType);
        public MultipleSelect MultipleSelect(string column, string label = "") => Call<MultipleSelect>(column, label);

        public Number Number(string column, string label = "") => Call<Number>(column, label);
        public Date Date(string column, string label = "") => Call<Date>(column, label);
        public Datetime Datetime(string column, string label = "") => Call<Datetime>(column, label);
        public Year Year(string column, string label = "") => Call<Year>(column, label);
        public Month Month(string column, string label = "") => Call<Month>(column, label);
        public Time Time(string column, string label = "") => Call<Time>(column, label);
        public Button Button(string column, string label = "") => Call<Button>(column, label);
        public Furl Furl(string column, string label = "") => Call<Furl>(column, label);
        public Divider Divider(string column, string label = "") => Call<Divider>(column, label);
              
        public Color Color(string column, string label = "") => Call<Color>(column, label);
        public Slider Slider(string column, string label = "") => Call<Slider>(column, label);
        public SwitchField Switch(string column, string label = "") => Call<SwitchField>(column, label);
        public Currency Currency(string column, string label = "") => Call<Currency>(column, label);
    }
}
