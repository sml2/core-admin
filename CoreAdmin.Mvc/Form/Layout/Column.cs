﻿using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Form.Layout
{
    public class Column
    {
        protected List<FField> fields;

        public int Width { get; set; }
        /// <summary>
        /// 赋值一个宽度12    //输入框大小
        /// </summary>
        /// <param name="width"></param>
        public Column(int width = 12)
        {
            Width = width;
            fields = new();
        }
        /// <summary>
        /// 字段添加调用逻辑
        /// </summary>
        /// <param name="field"></param>
        public void Add(FField field)
        {
            fields.Add(field);
        }
        /// <summary>
        /// 删除完页面异步刷新调用逻辑
        /// </summary>
        /// <param name="fields"></param>
        public void RemoveFields(List<string> fields)
        {
            this.fields = this.fields.Where((field) =>
            {
                return !fields.Contains(field.Column());
            }).ToList();
        }

        public List<FField> Fields()
        {
            return fields;
        }
    }
}
