﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Form.Layout
{
    public class FLayout
    {
        protected List<Column> columns;

        protected Column current;

        protected BForm parent;

        public FLayout(BForm form)
        {
            parent = form;

            current = new Column();

            columns = new();
        }

        public void AddField(FField field)
        {
            current.Add(field);
        }

        public void Column(int width, Action<BForm> closure)
        {
            Column column;
            if (columns.Count == 0)
            {
                column = current;
                column.Width = width;
            }
            else
            {
                column = new Column(width);
                current = column;
            }

            columns.Add(column);

            closure.Invoke(parent);
        }

        public List<Column> Columns()
        {
            if (columns.Count == 0)
            {
                columns.Add(current);
            }

            return columns;
        }

        public void RemoveReservedFields(List<string> fields)
        {
            if (fields == null)
            {
                return;
            }

            foreach (var column in Columns())
            {
                column.RemoveFields(fields);
            }
        }
    }
}
