﻿using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/AdminLTE/plugins/ionslider/ion.rangeSlider.css," +
                      "/assets/AdminLTE/plugins/ionslider/ion.rangeSlider.skinNice.css",
        Js = "/assets/AdminLTE/plugins/ionslider/ion.rangeSlider.min.js")]
    public class Slider : FField
    {
        private new Dictionary<string, object> options = new Dictionary<string, object>() { { "type", "single" },{ "prettify", false },{ "hasGrid", true } };

        public Slider(string column = "", params object[] arguments) : base(column, arguments)
        {
            
        }

        //protected $options = [
        //    'type'     => 'single',
        //    'prettify' => false,
        //    'hasGrid'  => true,
        //];

        //public function render()
        //    {
        //    $option = json_encode($this->options);

        //    $this->script = "$('{$this->getElementClassSelector()}').ionRangeSlider($option);";

        //        return parent::render();
        //    }

        public Slider Options(Dictionary<string,object> options)
        {
            
            this.options.Merge(options);
            return this;
        }

        public override HtmlContent Render(bool t)
        {
            options.Add("", "");
            options.Add("prettify", false);
            options.Add("hasGrid", true);

            var script =
                $"$('{GetElementClassSelector()}').ionRangeSlider({System.Text.Json.JsonSerializer.Serialize(options)});";

            Admin.Script(script);

            return base.Render(t);
        }
    }
}
