﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Js = "/assets/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js")]
    public class Ip : Text
    {
        protected string Rules = "nullable|ip";

        /**
         * @see https://github.com/RobinHerbots/Inputmask#options
         *
         * @var array
         */
        protected Dictionary<string, string> Options = new()
        {
            { "alias", "ip" },
        };

        //public function render()
        //    {
        //    $this->inputmask($this->options);

        //    $this->prepend('<i class="fa fa-laptop fa-fw"></i>')
        //        ->defaultAttribute('style', 'width: 130px');

        //        return parent::render();
        //    }
    }
}
