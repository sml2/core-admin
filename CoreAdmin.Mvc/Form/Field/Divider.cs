﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Divider : FField
    {
        public Divider(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        public Divider SetText(string title = "")
        {
            AddVariables((vari) =>
            {
                vari.Value = title;
            });
            return this;
        }
    }
}
