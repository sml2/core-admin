﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Table : HasMany
    {

        protected string viewMode = "table";

        /**
         * Table constructor.
         *
         * @param string $column
         * @param array  $arguments
         */
        //public Table(string column, params object[] arguments)
        //{
        //    this.column = column;

        //    if (arguments.Length == 1) {
        //        this.label = FormatLabel();
        //        this.builder = arguments[0];
        //    }

        //    if (arguments.Length == 2) {
        //            this.label = arguments[0];
        //            this.builder = arguments[1];
        //    }
        //}

        ///**
        // * @return array
        // */
        //protected function BuildRelatedForms()
        //{
        //    $forms = [];

        //    if ($values = old($this->column)) {
        //        foreach ($values as $key => $data) {
        //            if ($data[NestedForm::REMOVE_FLAG_NAME] == 1) {
        //                continue;
        //            }

        //            $forms[$key] = $this->buildNestedForm($this->column, $this->builder, $key)->fill($data);
        //        }
        //    } else {
        //        foreach ($this->value ?? [] as $key => $data) {
        //            if (isset($data['pivot'])) {
        //                $data = array_merge($data, $data['pivot']);
        //            }
        //            $forms[$key] = $this->buildNestedForm($this->column, $this->builder, $key)->fill($data);
        //        }
        //    }

        //    return $forms;
        //}

        //public function prepare($input)
        //{
        //    $form = $this->buildNestedForm($this->column, $this->builder);

        //    $prepare = $form->prepare($input);

        //    return collect($prepare)->reject(function ($item) {
        //        return $item[NestedForm::REMOVE_FLAG_NAME] == 1;
        //    })->map(function ($item) {
        //        unset($item[NestedForm::REMOVE_FLAG_NAME]);

        //        return $item;
        //    })->toArray();
        //}

        //protected function getKeyName()
        //{
        //    if (is_null($this->form)) {
        //        return;
        //    }

        //    return 'id';
        //}

        //protected function buildNestedForm($column, \Closure $builder, $key = null)
        //{
        //    $form = new NestedForm($column);

        //    if ($this->form instanceof WidgetForm) {
        //        $form->setWidgetForm($this->form);
        //    } else {
        //        $form->setForm($this->form);
        //    }

        //    $form->setKey($key);

        //    call_user_func($builder, $form);

        //    $form->hidden(NestedForm::REMOVE_FLAG_NAME)->default(0)->addElementClass(NestedForm::REMOVE_FLAG_CLASS);

        //    return $form;
        //}

        //public function render()
        //{
        //    return $this->renderTable();
        //}
    }
}
