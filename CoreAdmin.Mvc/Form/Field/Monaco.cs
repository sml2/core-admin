﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Form.Field;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/monaco-editor/min/vs/editor/editor.main.css")]
    public partial class Monaco : Textarea
    {
        public Monaco(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        public new HtmlContent Render(bool t)
        {
            if (!ShouldRender())
            {
                return null;
            }

            if (value is IEnumerable<dynamic> enumerate)
            {
                value = JsonSerializer.Serialize(enumerate);
            }

            return FieldRender(new Dictionary<string, object>()
            {
                {"append", append},
                {"rows", rows},
            });
        }
    }
}

namespace CoreAdmin.Mvc
{
    public static class MonacoFieldExtension
    {
        public static Monaco Monaco(this BForm form, string column, string label = "")
        {
            var element = new Monaco(column, label);
            element.SetForm(form);
            form.PushField(element);
            return element;
        }
    }
}