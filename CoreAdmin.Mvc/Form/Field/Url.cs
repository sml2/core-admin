﻿using CoreAdmin.Mvc.Models;
namespace CoreAdmin.Mvc.Form.Field
{
    public class Furl : Text
    {
        
        public Furl(string column = "", params object[] arguments) : base(column, arguments)
        {
            // rules = "nullable|url";
            //rules.Add  ("nullable|url");
        }

        public override HtmlContent Render(bool t)
        {
            Prepend("<i class=\"fa fa-internet-explorer fa-fw\"></i>");
            DefaultAttribute("type", "url");
            return base.Render(t);
        }


        // protected string rules = "nullable|url";
        //
        // public override string Render()
        // {
        //     Prepend("<i class=\"fa fa-internet-explorer fa-fw\"></i>");
        //     DefaultAttribute("type", "url");
        //
        //     return base.Render();
        // }
    }
}
