﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Form.Field;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Form.Field
{
    // BUG: 与editormd有冲突
    // [LoadAssets(Js = "/assets/ace/ace.js")]
    public partial class RichText : Textarea
    {
        public RichText(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        public new HtmlContent Render(bool t)
        {
            if (!ShouldRender())
            {
                return null;
            }

            if (value is IEnumerable<dynamic> enumerate)
            {
                value = JsonSerializer.Serialize(enumerate);
            }

            return FieldRender(new Dictionary<string, object>()
            {
                {"append", append},
                {"rows", rows},
            });
        }
    }
}

namespace CoreAdmin.Mvc
{
    public static class RichTextFieldExtension
    {
        public static RichText RichText(this BForm form, string column) {
            return RichText(form, column, form.ModelType.GetDisplayName(column));
        }
        public static RichText RichText(this BForm form, string column, string label)
        {
            var element = new RichText(column, label);
            element.SetForm(form);
            form.PushField(element);
            return element;
        }
    }
}