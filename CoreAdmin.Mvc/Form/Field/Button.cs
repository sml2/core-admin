﻿namespace CoreAdmin.Mvc.Form.Field
{
    /// <summary>
    /// Button用于后台调用
    /// </summary>
    public class Button : FField
    {
        protected string Class = "btn-primary";
        public static string AClass = "btn-primary";

        public Button(string column = "", params object[] arguments) : base(column, arguments)
        {
        }


        public Button Info()
        {
            Class = "btn-info";

            return this;
        }

        public void On(string _event, string callback)
        {
            Script = @"

            $('{$this->getElementClassSelector()}').on('" + _event + @"', function() {
                " + callback + @"
            });

            ";
        }
    }
}
