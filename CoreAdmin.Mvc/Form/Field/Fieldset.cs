﻿using CoreAdmin.Lib;

namespace CoreAdmin.Mvc.Form.Field
{
    public class Fieldset
    {
        protected string name = "";

        public Fieldset()
        {
            name = Str.Uniqid("fieldset-");
        }

        public string Start(string title)
        {
            var script = $@"
$('.{name}-title').click(function () {{
    $(""i"", this).toggleClass(""fa-angle-double-down fa-angle-double-up"");
}});
";

            Admin.Script(script);

            return $@"
<div>
    <div style=""height: 20px; border-bottom: 1px solid #eee; text-align: center;margin-top: 20px;margin-bottom: 20px;"">
      <span style=""font-size: 16px; background-color: #ffffff; padding: 0 10px;"">
        <a data-toggle=""collapse"" href=""#{name}"" class=""{name}-title"">
          <i class=""fa fa-angle-double-up""></i>&nbsp;&nbsp;{title}
        </a>
      </span>
    </div>
    <div class=""collapse in"" id=""{name}"">
";
        }

        public string End()
        {
            return "</div></div>";
        }

        public Fieldset Collapsed()
        {
            var script = $@"
$(""#{name}"").removeClass(""in"");
$("".{name}-title i"").toggleClass(""fa-angle-double-down fa-angle-double-up"");
";

            Admin.Script(script);

            return this;
        }
    }
}
