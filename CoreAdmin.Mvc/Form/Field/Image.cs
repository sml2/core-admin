﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Image : File
    {
        //use ImageField;

        protected new string rules = "image";

        public Image(string column = "", params object[] arguments) : base(column, arguments)
        {
            view = "~/Views/Form/File.cshtml";
        }


        public string Prepare(dynamic image)
        {
            //    if (picker) {
            //        return base.Prepare(image);
            //    }

            //    //if (request()->has(static::FILE_DELETE_FLAG)) {
            //    //    return $this->destroy();
            //    //}

            //    name = GetStoreName(image);

            //CallInterventionMethods(image.GetRealPath());

            //var path = UploadAndDeleteOriginal(image);

            //UploadAndDeleteOriginalThumbnail(image);

            //    return path;
            return "";
        }

        /**
         * force file type to image.
         *
         * @param $file
         *
         * @return array|bool|int[]|string[]
         */
        //public Dictionary<string, dynamic> GuessPreviewType(dynamic file)
        //{
        //    var extra = base.GuessPreviewType(file);
        //    extra["type"] = "image";

        //    return extra;
        //}
    }
}
