﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Form.Field;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Form.Field
{
    //[LoadAssets(Js = "/assets/editor.md/editormd.min.js",
    //    Css = "/assets/editor.md/css/editormd.min.css")]
    public partial class EditorMd : Textarea
    {
        public EditorMd(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        public new HtmlContent Render(bool t)
        {
            if (!ShouldRender())
            {
                return null;
            }

            if (value is IEnumerable<dynamic> enumerate)
            {
                value = JsonSerializer.Serialize(enumerate);
            }

            return FieldRender(new Dictionary<string, object>()
            {
                {"append", append},
                {"rows", rows},
            });
        }
    }
}

namespace CoreAdmin.Mvc
{
    public static class EditorMdFieldExtension
    {
        public static EditorMd EditorMd(this BForm form, string column)
        {
            return EditorMd(form, column, form.ModelType.GetDisplayName(column));
        }
        public static EditorMd EditorMd(this BForm form, string column, string label = "")
        {
            var element = new EditorMd(column, label);
            element.SetForm(form);
            form.PushField(element);
            return element;
        }
    }
}