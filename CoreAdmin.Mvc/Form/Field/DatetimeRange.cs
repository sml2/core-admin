﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class DatetimeRange : DateRange
    {
        protected string format = "YYYY-MM-DD HH:mm:ss";
    }
}
