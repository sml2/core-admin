﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Time : Date
    {
        //protected string format = "HH:mm:ss";

        public Time(string column = "", params object[] arguments) : base(column, arguments)
        {
            Format = "HH:mm:ss";
        }

        public override string Render()
        {
            Prepend("<i class=\"fa fa-clock-o fa-fw\"></i>");
            DefaultAttribute("style", "width: 150px");

            return base.Render();
        }
    }
}
