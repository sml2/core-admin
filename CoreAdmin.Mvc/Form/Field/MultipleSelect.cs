﻿using CoreAdmin.Mvc.Struct;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Form.Field
{
    public class MultipleSelect : Select
    {
        protected string otherKey;
        protected new ViewFormMultiSelect variables { get; set; } = new();

        public MultipleSelect(string column, string label,Type PropertyType) : base(column, label, PropertyType) { }

        //   /**
        //    * Get other key for this many-to-many relation.
        //    *
        //    * @throws \Exception
        //    *
        //    * @return string
        //    */
        //   protected function getOtherKey()
        //       {
        //           if ($this->otherKey) {
        //               return $this->otherKey;
        //           }

        //           if (is_callable([$this->form->model(), $this->column]) &&
        //               ($relation = $this->form->model()->{$this->column} ()) instanceof BelongsToMany
        //       ) {
        //           /* @var BelongsToMany $relation */
        //           $fullKey = $relation->getQualifiedRelatedPivotKeyName();
        //           $fullKeyArray = explode('.', $fullKey);

        //               return $this->otherKey = end($fullKeyArray);
        //           }

        //           throw new \Exception('Column of this field must be a `BelongsToMany` relation.');
        //       }

        //       /**
        //        * {@inheritdoc}
        //        */
        //       public function fill($data)
        //       {
        //           if ($this->form && $this->form->shouldSnakeAttributes()) {
        //           $key = Str::snake($this->column);
        //           } else
        //           {
        //           $key = $this->column;
        //           }

        //       $relations = Arr::get($data, $key);

        //           if (is_string($relations))
        //           {
        //           $this->value = explode(',', $relations);
        //           }

        //           if (!is_array($relations))
        //           {
        //           $this->applyCascadeConditions();

        //               return;
        //           }

        //       $first = current($relations);

        //           if (is_null($first))
        //           {
        //           $this->value = null;

        //               // MultipleSelect value store as an ont-to-many relationship.
        //           }
        //           elseif(is_array($first)) {
        //               foreach ($relations as $relation) {
        //               $this->value[] = Arr::get($relation, "pivot.{$this->getOtherKey()}");
        //               }

        //               // MultipleSelect value store as a column.
        //           } else
        //           {
        //           $this->value = $relations;
        //           }

        //       $this->applyCascadeConditions();
        //       }

        //       /**
        //        * {@inheritdoc}
        //        */
        //       public function setOriginal($data)
        //       {
        //       $relations = Arr::get($data, $this->column);

        //           if (is_string($relations))
        //           {
        //           $this->original = explode(',', $relations);
        //           }

        //           if (!is_array($relations))
        //           {
        //               return;
        //           }

        //       $first = current($relations);

        //           if (is_null($first))
        //           {
        //           $this->original = null;

        //               // MultipleSelect value store as an ont-to-many relationship.
        //           }
        //           elseif(is_array($first)) {
        //               foreach ($relations as $relation) {
        //               $this->original[] = Arr::get($relation, "pivot.{$this->getOtherKey()}");
        //               }

        //               // MultipleSelect value store as a column.
        //           } else
        //           {
        //           $this->original = $relations;
        //           }
        //       }
        protected override void InitVariables()
        {
            AddVariables((vari) =>
            {
                vari.Options = options;
                vari.Groups = groups.Cast<dynamic>().ToList();
                vari.Value = (List<string>)Value();
            });
        }

        protected FField AddVariables(Action<ViewFormMultiSelect> action)
        {
            action?.Invoke(variables);
            return this;
        }

        public override ViewFormMultiSelect Variables()
        {
            var variables = this.variables;
            return (ViewFormMultiSelect)MergeVariables(variables);
        }

        public IEnumerable<string> Prepare(string value) => Prepare(new List<string> { value });
        public IEnumerable<string> Prepare(IEnumerable<string> value)
        {
            return value.Where(v => !string.IsNullOrEmpty(v));
        }
        
        public MultipleSelect Default(List<string> defaultValue) => (MultipleSelect)SetDefault(defaultValue);
        public MultipleSelect Default(Func<BForm, IEnumerable<string>> defaultValue) => (MultipleSelect)SetDefault(defaultValue);
        
    }
}
