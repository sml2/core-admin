﻿using System;

namespace CoreAdmin.Mvc.Form.Field
{
    public class CheckboxCard : CheckboxButton
    {
        public CheckboxCard(string column, string label, Type PropertyType) : base(column, label, PropertyType) { }

        protected void AddStyle()
        {
            var style = $@"
.card-group label {{
    cursor: pointer;
    margin-right: 8px;
    font-weight: 400;
}}

.card-group .panel {{
    margin-bottom: 0px;
}}

.card-group .panel-body {{
    padding: 10px 15px;
}}

.card-group .active {{
    border: 2px solid #367fa9;
}}
";

            Admin.Style(style);
        }

        public override string Render()
        {
            AddStyle();

            return base.Render();
        }
    }
}
