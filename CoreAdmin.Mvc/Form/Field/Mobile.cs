﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Js = "/assets/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js")]
    public class Mobile : Text
    {
        /**
         * @see https://github.com/RobinHerbots/Inputmask#options
         */
        protected Dictionary<string, string> Options = new()
        {
            { "mask", "99999999999" },
        };

        //public function render()
        //    {
        //    $this->inputmask($this->options);

        //    $this->prepend('<i class="fa fa-phone fa-fw"></i>')
        //        ->defaultAttribute('style', 'width: 150px');

        //        return parent::render();
        //    }
    }
}
