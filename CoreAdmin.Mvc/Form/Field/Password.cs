﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Form.Field
{
    public class Password : Text
    {
        public Password(string column = "", params object[] arguments) : base(column, arguments)
        {

        }

        public override HtmlContent Render(bool t)
        {
            Prepend("<i class=\"fa fa-eye-slash fa-fw\"></i>");
            DefaultAttribute("type", "password");
            return base.Render(t);
        }
    }
}
