﻿using CoreAdmin.Mvc.Widgets;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.IO;

namespace CoreAdmin.Mvc.Form.Field
{
    public abstract class AbsWidget : IRenderable
    {
        public abstract string Render();

        static string RenderViewToString(Controller controller, string viewPath, object model = null, bool partial = false)
        {
            var context = controller.ControllerContext;
            var mvcViewOptions = controller.HttpContext.RequestServices.GetService<IOptions<MvcViewOptions>>();
            //controller.
            //controller.PartialView
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;

            if (partial)
            {
                //viewEngineResult = CompositeViewEngine.FindView(context, viewPath, false);
                var actionResult = controller.PartialView(viewPath);
                viewEngineResult = actionResult.ViewEngine.FindView(context, viewPath, false);
            }
            else
            {
                //actionResult = ViewEngines.Engines.FindView(context, viewPath, null);
                var actionResult = controller.View(viewPath);
                viewEngineResult = actionResult.ViewEngine.FindView(context, viewPath, false);
            }

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            controller.ViewData.Model = model;
            //context.Controller.ViewData.Model = model;
            string result = null;

            using (var sw = new StringWriter())
            {

                var HtmlHelperOptions = controller.HttpContext.RequestServices.GetService<HtmlHelperOptions>();
                var ctx = new ViewContext(context, view, controller.ViewData, controller.TempData, sw, HtmlHelperOptions);
                view.RenderAsync(ctx);
                result = sw.ToString();
            }

            return result;
        }
    }
}
