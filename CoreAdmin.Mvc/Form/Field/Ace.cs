﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Form.Field;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Form.Field
{
    // BUG: 与editormd有冲突
    // [LoadAssets(Js = "/assets/ace/ace.js")]
    /// <summary>
    /// Ace编辑器 用于调用
    /// </summary>
    public partial class Ace : Textarea
    {
        public Ace(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        public new HtmlContent Render(bool t)
        {
            if (!ShouldRender())
            {
                return null;
            }

            if (value is IEnumerable<dynamic> enumerate)
            {
                value = JsonSerializer.Serialize(enumerate);
            }

            return FieldRender(new Dictionary<string, object>()
            {
                {"append", append},
                {"rows", rows},
            });
        }
    }
}

namespace CoreAdmin.Mvc
{
    public static class AceFieldExtension
    {
        public static Ace Ace(this BForm form, string column) {
            return Ace(form, column, form.ModelType.GetDisplayName(column));
        }
        public static Ace Ace(this BForm form, string column, string label)
        {
            var element = new Ace(column, label);
            element.SetForm(form);
            form.PushField(element);
            return element;
        }
    }
}