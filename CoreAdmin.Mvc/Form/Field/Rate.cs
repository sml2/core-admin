﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Rate : Text
    {
        public override string Render()
        {
            Prepend("")
               .Append("%");
            DefaultAttribute("style", "text-align:right;");
            DefaultAttribute("placeholder", "0");

            return base.Render();
        }
    }
}
