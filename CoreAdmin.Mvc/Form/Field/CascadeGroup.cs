﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Form.Field
{
    /// <summary>
    /// CascadeGroup 用于调用
    /// </summary>
    public class CascadeGroup : FField
    {
        protected Dictionary<string, dynamic> dependency; 
        protected string hide = "hide"; 
        public CascadeGroup(Dictionary<string, dynamic> dependency)
        {
            this.dependency = dependency;
        }
        public bool DependsOn(FField field)
        {
            return dependency["column"] == field.Column();
        }
        public int Index()
        {
            return dependency["index"];
        }
        public void Visiable()
        {
            hide = "";
        }
        public override string Render()
        {
            return $@"
    <div class=""cascade-group {dependency["class"]} {hide}"">
";
        }
        public void End()
        {
        }
    }
}
