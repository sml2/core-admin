﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css",
        Js = "/assets/bootstrap-switch/dist/js/bootstrap-switch.min.js")]
    public class SwitchField : FField
    {
        public SwitchField(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        protected Dictionary<string, Dictionary<string, object>> states =
            new Dictionary<string, Dictionary<string, object>>
            {
                {
                    "on", new Dictionary<string, object>
                    {
                        {"value", true},
                        {"text", "ON"},
                        {"color", "primary"},
                    }
                },
                {
                    "off", new Dictionary<string, object>
                    {
                        {"value", false},
                        {"text", "OFF"},
                        {"color", "default"},
                    }
                }
            };

        protected string Size = "small";

        public SwitchField SetSite(string size)
        {
            Size = size;
            return this;
        }

        public FField DefaultOn() => SetDefault(true);
        public FField DefaultOff() => SetDefault(false);

        public SwitchField States(Dictionary<string, Dictionary<string, object>> states)
        {
            this.states = states;
            return this;
        }

        public object PerPare(string value)
        {
            if (states.ContainsKey(value))
            {
                return states[value];
            }

            return value;
        }

        public override HtmlContent Render(bool t)
        {
            var script = $@"$('{GetElementClassSelector()}.la_checkbox').bootstrapSwitch({{
                                size:'{Size}',
                                onText: '{states["on"]["text"]}',
                                offText: '{states["off"]["text"]}',
                                onColor: '{states["on"]["color"]}',
                                offColor: '{states["off"]["color"]}',
                                onSwitchChange: function(event, state) {{
                                    $(event.target).closest('.bootstrap-switch').next().val(state ? 'true' : 'false').change();
                                }}
                            }});";

            Admin.Script(script);

            return base.Render(t);
        }
    }
}