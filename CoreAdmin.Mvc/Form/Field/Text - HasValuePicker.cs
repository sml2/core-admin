﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Form.Field
{
    public partial class Text : FField
    {
        protected ValuePicker picker;

        public Text Pick(string picker, string column = "")
        {
            this.picker = new ValuePicker(picker, column);

            return this;
        }

        public Text PickMany(string picker, string column = "", string separator = ";")
        {
            this.picker = new ValuePicker(picker, column, true, separator);

            return this;
        }

        protected Text MountPicker(Action<string> callback = null)
        {
            if (picker != null) picker.Mount(this, callback);

            return this;
        }

        public new string GetRules()
        {
            var rules = base.GetRules();
            rules.Remove("image");

            return string.Join(";", rules);
        }

        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
         */
        protected HtmlContent RenderFilePicker()
        {
            MountPicker()
                .SetView("admin::form.filepicker")
                .Attribute("type", "text")
                .Attribute("id", id)
                .Attribute("name", elementName ?? FormatName(column))
                //.Attribute("value", old($this->elementName ?: $this->column, $this->value()))
                .Attribute("class", "form-control " + GetElementClassString())
                .Attribute("placeholder", GetPlaceholder())
                .AddVariables((vari) =>
                {
                    //{ "preview", picker.GetPreview(get_called_class()) },
                });

            return Admin.Component("admin::form.filepicker", null, Variables());
        }
    }
}