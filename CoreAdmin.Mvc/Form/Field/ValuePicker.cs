﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Form.Field
{
    public class ValuePicker
    {

        protected string modal;

        /**
         * @var Text|File
         */
        protected FField field;

        protected string column;

        protected string selecteable;

        protected string separator;

        protected bool multiple = false;

        public ValuePicker(string selecteable, string column = "", bool multiple = false, string separator = ";")
        {
            this.selecteable = selecteable;
            this.column = column;
            this.multiple = multiple;
            this.separator = separator;
        }

        protected string GetLoadUrl()
        {
            var selectable = selecteable.Replace("\\", "_");

            //$args = [$this->multiple, $this->column];

            //    return route('admin.handle-selectable', compact('selectable', 'args'));
            return "";
        }

        public void Mount(FField field, Action<string> callback = null)
        {
            this.field = field;
            modal = $"picker-modal-{field.GetElementClassString()}";

            AddPickBtn(callback);

            Admin.Component("admin::components.filepicker", new Dictionary<string, dynamic>{
                {"url"      , GetLoadUrl() },
                {"modal"    , modal},
                {"selector" , this.field.GetElementClassSelector()},
                {"separator", separator},
                {"multiple" , multiple},
                {"is_file"  , this.field is File},
                {"is_image" , this.field is Image},
                { "url_tpl"  , this.field is File file? file.ObjectUrl("__URL__") : ""},
            });
        }

        protected void AddPickBtn(Action<string> callback = null)
        {
            var text = Admin.Trans("browse");

            var btn = $@"
        <a class=""btn btn-primary"" data-toggle=""modal"" data-target=""#{modal}"">
            <i class=""fa fa-folder-open""></i>  {text}
        </a>
        ";

            if (callback != null)
            {
                callback.Invoke(btn);
            }
            else
            {
                field.AddVariables((vari) => { vari.Btn = btn; });
            }
        }

        //    /**
        //     * @param string $field
        //     *
        //     * @return array|\Illuminate\Support\Collection
        //     */
        //    public function getPreview(string $field)
        //{
        //    if (empty($value = $this->field->value()))
        //    {
        //        return [];
        //    }

        //    if ($this->multiple) {
        //            $value = explode($this->separator, $value);
        //}

        //return collect(Arr::wrap($value))->map(function($item) use($field) {
        //    return [
        //        'url'     => $this->field->objectUrl($item),
        //        'value'   => $item,
        //        'is_file' => $field == File::class,
        //            ];
        //        });
        //    }
    }
}
