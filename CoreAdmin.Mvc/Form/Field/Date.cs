﻿using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Models;
using static System.Text.Json.JsonSerializer;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
        Js = "/assets/moment/min/moment-with-locales.min.js," 
        + "/assets/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js")]
    public class Date : Text
    {
        protected string Format;

        public Date(string column = "", params object[] arguments) : base(column, arguments)
        {
            Format = "YYYY-MM-DD";
        }


        public Date format(string value)
        {
            Format = value;
            return this;
        }

        public override string Prepare(string value)
        {
            return string.IsNullOrEmpty(value) ? null : value;
        }

        public override HtmlContent Render(bool t)
        {
            options["format"] = Format;
            options["locale"] = options.ContainsKey("locale") ? options["locale"] : "zh-CN";
            options["allowInputToggle"] = true;
            var script =
                $"$('{GetElementClassSelector()}').parent().datetimepicker({Serialize(options)});";

            Admin.Script(script);

            Prepend("<i class=\"fa fa-calendar fa-fw\"></i>");
            DefaultAttribute("style", "width: 110px");

            return base.Render(t);
        }

       
    }
}