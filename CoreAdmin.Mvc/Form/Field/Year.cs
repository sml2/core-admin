﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Year : Date
    {
        public Year(string column = "", params object[] arguments) : base(column, arguments)
        {
            Format = "YYYY";
        }
    }
}
