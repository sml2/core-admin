﻿using System;

namespace CoreAdmin.Mvc.Form.Field
{
    public class Timezone : Select
    {
        protected new string view = "admin::form.select";
        public Timezone(string column, string label,Type PropertyType) : base(column, label, PropertyType) { }

        public override string Render()
        {
            //options = collect(DateTimeZone::listIdentifiers(DateTimeZone::ALL))->mapWithKeys(function($timezone) {
            //    return [$timezone => $timezone];
            //})->toArray();

            return base.Render();
        }
    }
}
