﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Datetime : Date
    {
        //protected string Format = "YYYY-MM-DD HH:mm:ss";

        public Datetime(string column = "", params object[] arguments) : base(column, arguments)
        {
            Format = "YYYY-MM-DD HH:mm:ss";
        }

        public override string Render()
        {
            DefaultAttribute("style", "width: 160px");

            return base.Render();
        }
    }
}