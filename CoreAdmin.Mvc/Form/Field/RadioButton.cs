﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class RadioButton : Radio
    {
        protected string cascadeEvent = "change";

        protected void AddScript()
        {
            var script = $@"
$('.radio-group-toggle label').click(function() {{
    $(this).parent().children().removeClass('active');
    $(this).addClass('active');
}});
";

            Admin.Script(script);
        }

        public override string Render()
        {
            AddScript();

            //AddCascadeScript();

            AddVariables((vari) =>
            {
                vari.Options = options;
                vari.Checked = _checked;
            });

            //return parent::fieldRender();
            return "";
        }
    }
}
