﻿using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Js = "/assets/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js")]
    public class Decimal : Text
    {
        ///**
        // * @see https://github.com/RobinHerbots/Inputmask#options
        // *
        // * @var array
        // */
        //protected $options = [
        //    'alias'      => 'decimal',
        //    'rightAlign' => true,
        //];

        //public function render()
        //    {
        //    $this->inputmask($this->options);

        //    $this->prepend('<i class="fa '.$this->icon.' fa-fw"></i>')
        //        ->defaultAttribute('style', 'width: 130px');

        //        return parent::render();
        //    }
    }
}
