﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Form.Field;

namespace CoreAdmin.Mvc.Form.Field
{
    public partial class Textarea : FField
    {
        protected int rows = 5;

        protected string append = "";

        public Textarea(string column = "", params object[] arguments) : base(column, arguments) { }


        public Textarea Rows(int rows = 5)
        {
            this.rows = rows;

            return this;
        }

        public new HtmlContent Render(bool t)
        {
            if (!ShouldRender())
            {
                return null;
            }

            //if (value is IEnumerable<dynamic> enumer)
            //{
            //        $this->value = json_encode($this->value, JSON_PRETTY_PRINT);
            //}

            MountPicker(AddPickBtn);

            return FieldRender(new Dictionary<string, object>()
            {
                { "append", append },
                { "rows", rows },
            });
        }


        protected void AddPickBtn(string btn)
        {
            var style = $@"
.textarea-picker {{
    padding: 5px;
    border-bottom: 1px solid #d2d6de;
    border-left: 1px solid #d2d6de;
    border-right: 1px solid #d2d6de;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    background-color: #f1f2f3;
}}

.textarea-picker .btn {{
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
}}
                    ";
            Admin.Style(style);

            append = $@"
<div class=""text-right textarea-picker"">
    {btn}
</div>
        ";
        }
    }
}

namespace CoreAdmin.Mvc
{
    public static class TextAreaFieldExtension
    {
        /// <summary>
        /// 表单文本框
        /// </summary>
        /// <param name="form"></param>
        /// <param name="column"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static Textarea Textarea(this BForm form, string column, string label = "")
        {
            var element = new Textarea(column, label);
            element.SetForm(form);
            form.PushField(element);
            return element;
        }
    }
}
