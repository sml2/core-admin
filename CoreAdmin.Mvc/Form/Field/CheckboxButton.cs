﻿using System;

namespace CoreAdmin.Mvc.Form.Field
{
    public class CheckboxButton : Checkbox
    {
        protected new string cascadeEvent = "change";
        public CheckboxButton(string column, string label, Type PropertyType) : base(column, label, PropertyType) {
            
        }

        protected void AddScript()
        {
            var script = $@"
$('.checkbox-group-toggle label').click(function(e) {{
    e.stopPropagation();
    e.preventDefault();

    if ($(this).hasClass('active')) {{
        $(this).removeClass('active');
        $(this).find('input').prop('checked', false);
    }} else {{
        $(this).addClass('active');
        $(this).find('input').prop('checked', true);
    }}

    $(this).find('input').trigger('change');
}});
";

            Admin.Script(script);
        }
        
        public override string Render()
        {
            AddScript();

            //AddCascadeScript();

            AddVariables((vari) =>
            {
                vari.Options = options;
                vari.Checked = _checked;
            });      

            //return base.FieldRender();
            return "";
        }
    }
}
