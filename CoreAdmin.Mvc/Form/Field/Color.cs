﻿using CoreAdmin.Mvc.Models;
using System.Text.Json;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.css",
        Js = "/assets/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js")]
    public class Color : Text
    {
        public Color(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        ///**
        // * Use `hex` format.
        // *
        // * @return $this
        // */
        //public function hex()
        //    {
        //        return $this->options(['format' => 'hex']);
        //    }

        //    /**
        //     * Use `rgb` format.
        //     *
        //     * @return $this
        //     */
        //    public function rgb()
        //    {
        //        return $this->options(['format' => 'rgb']);
        //    }

        //    /**
        //     * Use `rgba` format.
        //     *
        //     * @return $this
        //     */
        //    public function rgba()
        //    {
        //        return $this->options(['format' => 'rgba']);
        //    }

        //    /**
        //     * Render this filed.
        //     *
        //     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
        //     */
        //    public function render()
        //    {
        //    $options = json_encode($this->options);

        //    $this->script = "$('{$this->getElementClassSelector()}').parent().colorpicker($options);";

        //    $this->prepend('<i></i>')
        //        ->defaultAttribute('style', 'width: 140px');

        //        return parent::render();
        //    }
        public Color Hex()
        {
            options.Add("format", "hex");
            return this;
        }


        public Color Rgba()
        {
            options.Add("format", "rgba");
            return this;
        }

        public Color Rgb()
        {
            options.Add("format", "rgb");
            return this;
        }


        public override HtmlContent Render(bool t)
        {
            var script = $"$('{GetElementClassSelector()}').parent().colorpicker({JsonSerializer.Serialize(options)});";

            Admin.Script(script);

            Prepend("<i></i>");
            DefaultAttribute("style", "width: 140px");

            return base.Render(t);
        }
    }
}
