﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class MultipleImage : MultipleFile
    {
        //use ImageField;

        protected string view = "admin::form.multiplefile";

        protected string rules = "image";

        /**
         * Prepare for each file.
         *
         * @param UploadedFile $image
         *
         * @return mixed|string
         */
        //protected function PrepareForeach(UploadedFile $image = null)
        //{
        //    $this->name = $this->getStoreName($image);

        //    $this->callInterventionMethods($image->getRealPath());

        //    /* return tap($this->upload($image), function () {
        //        $this->name = null;
        //    }); */

        //    /* Copied from single image prepare section and made necessary changes so the return
        //    value is same as before, but now thumbnails are saved to the disk as well. */

        //    $path = $this->upload($image);
        //    $this->uploadAndDeleteOriginalThumbnail($image);
        //    $this->name = null;

        //    return $path;
        //}
    }
}
