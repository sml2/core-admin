﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Email : Text
    {
        protected string rules = "nullable|email";

        public override string Render()
        {
            Prepend("<i class=\"fa fa-envelope fa-fw\"></i>");
            DefaultAttribute("type", "email");

            return base.Render();
        }
    }
}
