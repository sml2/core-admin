﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css",
        Js = "/assets/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.min.js")]
    public class Icon : Text
    {
        public Icon(string column = "", params object[] arguments) : base(column, arguments) {
            _default = "fa-pencil";
        }


        public override HtmlContent Render(bool t)
        {
                Script = $@"

        $('{GetElementClassSelector()}').iconpicker({{ placement: 'bottomLeft'}});

            ";

            Prepend("<i class=\"fa fa-pencil fa-fw\"></i>");
            DefaultAttribute("style", "width: 140px");

            return base.Render(t);
        }
    }
}
