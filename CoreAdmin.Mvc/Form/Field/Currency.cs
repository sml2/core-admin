﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Js = "/assets/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js")]
    public class Currency : Text
    {
        protected string symbol = "$";

        public Currency(string column = "", params object[] arguments) : base(column, arguments)
        {
        }
        /**
         * @see https://github.com/RobinHerbots/Inputmask#options
         *
         * @var array
         */
        protected Dictionary<string, object> Options = new()
        {
            { "alias", "currency" },
            { "radixPoint", "." },
            { "prefix", "" },
            //{ "removeMaskOnSubmit" , true }
        };

        public Currency Symbol(string symbol)
        {
            this.symbol = symbol;
            return this;
        }

        public Currency Digits(int digits)
        {
            this.Options.Add("digits", digits);
            return this;
        }

        public float Perpare(int value)
        {
            return (float)value;
        }


        public override HtmlContent Render(bool t)
        {
            var script =
                $"$('{GetElementClassSelector()}').inputmask({System.Text.Json.JsonSerializer.Serialize(Options)});";

            Admin.Script(script);

            Prepend(symbol);
            DefaultAttribute("style", "width: 120px");

            return base.Render(t);
        }


        ///**
        // * Set symbol for currency field.
        // *
        // * @param string $symbol
        // *
        // * @return $this
        // */
        //public function symbol($symbol)
        //    {
        //    $this->symbol = $symbol;

        //        return $this;
        //    }

        //    /**
        //     * Set digits for input number.
        //     *
        //     * @param int $digits
        //     *
        //     * @return $this
        //     */
        //    public function digits($digits)
        //    {
        //        return $this->options(compact('digits'));
        //    }

        //    /**
        //     * {@inheritdoc}
        //     */
        //    public function prepare($value)
        //    {
        //        return (float) $value;
        //    }

        //    /**
        //     * {@inheritdoc}
        //     */
        //    public function render()
        //    {
        //    $this->inputmask($this->options);

        //    $this->prepend($this->symbol)
        //        ->defaultAttribute('style', 'width: 120px');

        //        return parent::render();
        //    }
    }
}
