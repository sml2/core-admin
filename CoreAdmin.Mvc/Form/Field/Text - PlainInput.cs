﻿namespace CoreAdmin.Mvc.Form.Field
{
    public partial class Text : FField
    {

        protected string prepend;

        protected string append;

        public Text Prepend(string str)
        {
            if (string.IsNullOrEmpty(prepend))
            {
                prepend = str;
            }

            return this;
        }

        public Text Append(string str)
        {
            if (string.IsNullOrEmpty(append))
            {
                append = str;
            }

            return this;
        }

        protected void InitPlainInput()
        {
            if (string.IsNullOrEmpty(view))
            {
                view = "~/Views/Form/Input.cshtml";
            }
        }

        protected Text DefaultAttribute(string attribute, string value)
        {
            if (!Attributes.ContainsKey(attribute))
            {
                Attributes.Add(attribute, value);
            }

            return this;
        }
    }
}
