﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Extensions;

namespace CoreAdmin.Mvc.Form.Field
{
    public partial class Text : FField
    {
        protected string icon = "fa-pencil";

        protected bool withoutIcon = false;
        public Text(string column = "", params object[] arguments) : base(column, arguments) { }

        public Text Icon(string icon)
        {
            this.icon = icon;

            return this;
        }

        public override HtmlContent Render(bool t)
        {
            InitPlainInput();

            if (!withoutIcon)
            {
                Prepend($"<i class=\"fa {icon} fa-fw\"></i>");
            }
            
            DefaultAttribute("type", "text")
            .DefaultAttribute("id", id)
            .DefaultAttribute("name", elementName ?? FormatName(column))
            .DefaultAttribute("value", form.Request.Old((elementName ?? column), $"{Value()}"))
            .DefaultAttribute("class", "form-control " + GetElementClassString())
            .DefaultAttribute("placeholder", GetPlaceholder())
            .MountPicker()
            .AddVariables((vari) =>
            {
                vari.Prepend = prepend;
                vari.Append = append;
            });

            return base.Render(t);
        }

        /* 输入框长度 */
        public Text SetWidth(string value)
        {
            Attribute("style", "width: " + value);
            return this;
        }

        /**
         * Add inputmask to an elements.
         *
         * @param array $options
         *
         * @return $this
         */
        //public function inputmask($options)
        //{
        //$options = json_encode_options($options);

        //$this->script = "$('{$this->getElementClassSelector()}').inputmask($options);";

        //    return $this;
        //}

        ///**
        // * Add datalist element to Text input.
        // *
        // * @param array $entries
        // *
        // * @return $this
        // */
        //public function datalist($entries = [])
        //{
        //$this->defaultAttribute('list', "list-{$this->id}");

        //$datalist = "<datalist id=\"list-{$this->id}\">";
        //    foreach ($entries as $k => $v) {
        //    $datalist.= "<option value=\"{$k}\">{$v}</option>";
        //    }
        //$datalist.= '</datalist>';

        //    return $this->append($datalist);
        //}

        public Text WithoutIcon()
        {
            withoutIcon = true;

            return this;
        }
    }
}
