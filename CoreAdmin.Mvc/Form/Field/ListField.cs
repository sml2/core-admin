﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class ListField : FField
    {

        protected int max;

        protected int min = 0;

        /**
         * @var array
         */
        //protected $value = [''];

        public ListField Max(int size)
        {
            max = size;

            return this;
        }

        public ListField Min(int size)
        {
            min = size;

            return this;
        }

        ///**
        // * Fill data to the field.
        // *
        // * @param array $data
        // *
        // * @return void
        // */
        //public function fill($data)
        //{
        //    $this->data = $data;

        //    $this->value = Arr::get($data, $this->column, $this->value);

        //    $this->formatValue();
        //}

        ///**
        // * {@inheritdoc}
        // */
        //public function getValidator(array $input)
        //{
        //    if ($this->validator) {
        //        return $this->validator->call($this, $input);
        //    }

        //    if (!is_string($this->column)) {
        //        return false;
        //    }

        //    $rules = $attributes = [];

        //    if (!$fieldRules = $this->getRules()) {
        //        return false;
        //    }

        //    if (!Arr::has($input, $this->column)) {
        //        return false;
        //    }

        //    $rules["{$this->column}.values.*"] = $fieldRules;
        //    $attributes["{$this->column}.values.*"] = __('Value');

        //    $rules["{$this->column}.values"][] = 'array';

        //    if (!is_null($this->max)) {
        //        $rules["{$this->column}.values"][] = "max:$this->max";
        //    }

        //    if (!is_null($this->min)) {
        //        $rules["{$this->column}.values"][] = "min:$this->min";
        //    }

        //    $attributes["{$this->column}.values"] = $this->label;

        //    return validator($input, $rules, $this->getValidationMessages(), $attributes);
        //}

        protected void SetupScript()
        {
            Script = $@"

$('.{column}-add').on('click', function () {{
    var tpl = $('template.{column}-tpl').html();
    $('tbody.list-{column}-table').append(tpl);
}});

$('tbody').on('click', '.{column}-remove', function () {{
    $(this).closest('tr').remove();
}});

";
        }

        /**
         * {@inheritdoc}
         */
        //public function prepare($value)
        //{
        //    return array_values($value['values']);
        //}

        public override string Render()
        {
            SetupScript();

            Admin.Style("td .form-group {margin-bottom: 0 !important;}");

            return base.Render();
        }
    }
}
