﻿namespace CoreAdmin.Mvc.Form.Field
{
    public class Month : Date
    {

        public Month(string column = "", params object[] arguments) : base(column, arguments)
        {
            Format = "MM";
        }

    }
}
