﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Attributes;
using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Js = "/assets/number-input/bootstrap-number-input.js")]
    public class Number : Text
    {
        protected string _default;

        /* 实例化 */
        public Number(string column = "", params object[] arguments) : base(column, arguments)
        {
        }

        public override HtmlContent Render(bool t)
        {
            Default(_default);


            //转换json字符串
            var configs = System.Text.Json.JsonSerializer.Serialize(
                new Dictionary<string, dynamic>
                {
                    {"upClass", "success"},
                    {"downClass", "primary"},
                    {"center", true}
                });

            var script =
                $"$('{GetElementClassSelector()}:not(.initialized)').addClass('initialize').bootstrapNumber({configs});";

            Admin.Script(script);
            withoutIcon = true;
            Prepend("");
            DefaultAttribute("style", "width: 100px");

            return base.Render(t);
        }

        /* 最小值 */
        public Number min(int value)
        {
            Attribute("min", value);
            return this;
        }

        /* 最大值 */
        public Number max(int value)
        {
            Attribute("max", value);
            return this;
        }

        /* 追加值 */

        public Number Step(int value)
        {
            Attribute("step", value);
            return this;
        }
    }
}