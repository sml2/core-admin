﻿using System;

namespace CoreAdmin.Mvc.Form.Field
{
    public class Html : FField
    {
        protected string html = "";

        protected bool plain = false;

        public Html(string html, params object[] arguments)
        {
            this.html = html;
            label = (string)arguments[0];
        }

        public Html Plain()
        {
            plain = true;

            return this;
        }

        public override string Render()
        {
            if (html is Action)
            {
                //$this->html = $this->html->call($this->form->model(), $this->form);
            }

            if (plain)
            {
                return html;
            }

            var viewClass = GetViewElementClasses();

            return $@"
<div class=""{viewClass.FormGroup}"">
    <label  class=""{viewClass.Label} control-label"">{label}</label>
    <div class=""{viewClass.Field}"">
        {html}
    </div>
</div>
";
        }
    }
}
