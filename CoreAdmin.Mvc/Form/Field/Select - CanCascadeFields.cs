﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace CoreAdmin.Mvc.Form.Field
{
    public partial class Select : FField
    {
        protected List<Dictionary<string, string>> conditions = new();

        //    /**
        //     * @param $operator
        //     * @param $value
        //     * @param $closure
        //     *
        //     * @return $this
        //     */
        //    public function when($operator, $value, $closure = null)
        //    {
        //        if (func_num_args() == 2) {
        //            $closure = $value;
        //            $value = $operator;
        //            $operator = '=';
        //        }

        //        $this->formatValues($operator, $value);

        //        $this->addDependents($operator, $value, $closure);

        //        return $this;
        //    }

        //    /**
        //     * @param string $operator
        //     * @param mixed  $value
        //     */
        //    protected function formatValues(string $operator, &$value)
        //    {
        //        if (in_array($operator, ['in', 'notIn'])) {
        //            $value = Arr::wrap($value);
        //        }

        //        if (is_array($value)) {
        //            $value = array_map('strval', $value);
        //        } else {
        //            $value = strval($value);
        //        }
        //    }

        //    /**
        //     * @param string   $operator
        //     * @param mixed    $value
        //     * @param \Closure $closure
        //     */
        //    protected function addDependents(string $operator, $value, \Closure $closure)
        //    {
        //        $this->conditions[] = compact('operator', 'value', 'closure');

        //        $this->form->cascadeGroup($closure, [
        //            'column' => $this->column(),
        //            'index'  => count($this->conditions) - 1,
        //            'class'  => $this->getCascadeClass($value),
        //        ]);
        //    }

        //    /**
        //     * {@inheritdoc}
        //     */
        //    public function fill($data)
        //    {
        //        parent::fill($data);

        //        $this->applyCascadeConditions();
        //    }


        protected string GetCascadeClass(List<string> value) => GetCascadeClass(string.Join("-", value));
        protected string GetCascadeClass(string value)
        {
            return $"cascade-{GetElementClassString()}-{value}";
        }

        //    /**
        //     * Apply conditions to dependents fields.
        //     *
        //     * @return void
        //     */
        //    protected function applyCascadeConditions()
        //    {
        //        if ($this->form) {
        //            $this->form->fields()
        //                ->filter(function (Form\Field $field) {
        //                    return $field instanceof CascadeGroup
        //                        && $field->dependsOn($this)
        //                        && $this->hitsCondition($field);
        //                })->each->visiable();
        //        }
        //    }

        //    /**
        //     * @param CascadeGroup $group
        //     *
        //     * @throws \Exception
        //     *
        //     * @return bool
        //     */
        //    protected function hitsCondition(CascadeGroup $group)
        //    {
        //        $condition = $this->conditions[$group->index()];

        //        extract($condition);

        //        $old = old($this->column(), $this->value());

        //        switch ($operator) {
        //            case '=':
        //                return $old == $value;
        //            case '>':
        //                return $old > $value;
        //            case '<':
        //                return $old < $value;
        //            case '>=':
        //                return $old >= $value;
        //            case '<=':
        //                return $old <= $value;
        //            case '!=':
        //                return $old != $value;
        //            case 'in':
        //                return in_array($old, $value);
        //            case 'notIn':
        //                return !in_array($old, $value);
        //            case 'has':
        //                return in_array($value, $old);
        //            case 'oneIn':
        //                return count(array_intersect($value, $old)) >= 1;
        //            case 'oneNotIn':
        //                return count(array_intersect($value, $old)) == 0;
        //            default:
        //                throw new \Exception("Operator [$operator] not support.");
        //        }
        //    }

        /**
         * Add cascade scripts to contents.
         *
         * @return void
         */
        protected void AddCascadeScript()
        {
            if (conditions.Count == 0)
            {
                return;
            }

            var cascadeGroups = JsonSerializer.Serialize(conditions.Select((condition) =>
                new Dictionary<string, string>(){
                {"class"    , GetCascadeClass(condition["value"]) },
                {"operator" , condition["operator"]},
                { "value"    , condition["value"]},
                }
            ));

            var script = $@"
;(function () {{
    var operator_table = {{
        '=': function(a, b) {{
            if ($.isArray(a) && $.isArray(b)) {{
                return $(a).not(b).length === 0 && $(b).not(a).length === 0;
            }}

            return a == b;
        }},
        '>': function(a, b) {{ return a > b; }},
        '<': function(a, b) {{ return a < b; }},
        '>=': function(a, b) {{ return a >= b; }},
        '<=': function(a, b) {{ return a <= b; }},
        '!=': function(a, b) {{
             if ($.isArray(a) && $.isArray(b)) {{
                return !($(a).not(b).length === 0 && $(b).not(a).length === 0);
             }}

             return a != b;
        }},
        'in': function(a, b) {{ return $.inArray(a, b) != -1; }},
        'notIn': function(a, b) {{ return $.inArray(a, b) == -1; }},
        'has': function(a, b) {{ return $.inArray(b, a) != -1; }},
        'oneIn': function(a, b) {{ return a.filter(v => b.includes(v)).length >= 1; }},
        'oneNotIn': function(a, b) {{ return a.filter(v => b.includes(v)).length == 0; }},
    }};
    var cascade_groups = {cascadeGroups};
        
    cascade_groups.forEach(function (event) {{
        var default_value = '{GetDefault()}' + '';
        var class_name = event.class;
        if(default_value == event.value) {{
            $('.'+class_name+'').removeClass('hide');
        }}
    }});
    
    $('{GetElementClassSelector()}').on('{cascadeEvent}', function (e) {{

        {GetFormFrontValue()}

        cascade_groups.forEach(function (event) {{
            var group = $('div.cascade-group.'+event.class);
            if( operator_table[event.operator](checked, event.value) ) {{
                group.removeClass('hide');
            }} else {{
                group.addClass('hide');
            }}
        }});
    }})
}})();
";

            Admin.Script(script);
        }

        protected string GetFormFrontValue()
        {
            switch (GetType().Name)
            {
                case nameof(Radio):
                case nameof(RadioButton):
                case nameof(RadioCard):
                case nameof(Select):
                case nameof(BelongsTo):
                case nameof(BelongsToMany):
                case nameof(MultipleSelect):
                    return "var checked = $(this).val();";
                case nameof(Checkbox):
                case nameof(CheckboxButton):
                case nameof(CheckboxCard):
                    return $@"
var checked = $('{GetElementClassSelector()}:checked').map(function(){{
  return $(this).val();
}}).get();
";
                default:
                    throw new ArgumentException("Invalid form field type");
            }
        }
    }
}
