﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Attributes;

namespace CoreAdmin.Mvc.Form.Field
{
    [LoadAssets(Css = "/assets/bootstrap-fileinput/css/fileinput.min.css?v=4.5.2",
        Js = "/assets/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js," +
             "/assets/bootstrap-fileinput/js/fileinput.min.js?v=4.5.2")]
    public partial class File : FField
    {
        public File(string column, params object[] arguments) : base(column, arguments)
        {
            //InitStorage();
        }

        //        /**
        //         * Default directory for file to upload.
        //         *
        //         * @return mixed
        //         */
        //        public function defaultDirectory()
        //        {
        //            return config('admin.upload.directory.file');
        //        }

        //        /**
        //         * {@inheritdoc}
        //         */
        //        public function getValidator(array $input)
        //        {
        //            if (request()->has(static::FILE_DELETE_FLAG)) {
        //                return false;
        //            }

        //            if ($this->validator) {
        //                return $this->validator->call($this, $input);
        //            }

        //            /*
        //             * If has original value, means the form is in edit mode,
        //             * then remove required rule from rules.
        //             */
        //            if ($this->original()) {
        //            $this->removeRule('required');
        //            }

        //            /*
        //             * Make input data validatable if the column data is `null`.
        //             */
        //            if (Arr::has($input, $this->column) && is_null(Arr::get($input, $this->column)))
        //            {
        //            $input[$this->column] = '';
        //            }

        //        $rules = $attributes = [];

        //            if (!$fieldRules = $this->getRules()) {
        //                return false;
        //            }

        //        $rules[$this->column] = $fieldRules;
        //        $attributes[$this->column] = $this->label;

        //            return \validator($input, $rules, $this->getValidationMessages(), $attributes);
        //        }

        //        /**
        //         * Prepare for saving.
        //         *
        //         * @param UploadedFile|array $file
        //         *
        //         * @return mixed|string
        //         */
        //        public function prepare($file)
        //        {
        //            if ($this->picker) {
        //                return parent::prepare($file);
        //            }

        //            if (request()->has(static::FILE_DELETE_FLAG)) {
        //                return $this->destroy();
        //            }

        //        $this->name = $this->getStoreName($file);

        //            return $this->uploadAndDeleteOriginal($file);
        //        }

        //        /**
        //         * Upload file and delete original file.
        //         *
        //         * @param UploadedFile $file
        //         *
        //         * @return mixed
        //         */
        //        protected function uploadAndDeleteOriginal(UploadedFile $file)
        //        {
        //        $this->renameIfExists($file);

        //        $path = null;

        //            if (!is_null($this->storagePermission))
        //            {
        //            $path = $this->storage->putFileAs($this->getDirectory(), $file, $this->name, $this->storagePermission);
        //            }
        //            else
        //            {
        //            $path = $this->storage->putFileAs($this->getDirectory(), $file, $this->name);
        //            }

        //        $this->destroy();

        //            return $path;
        //        }

        protected string Preview()
        {
            return ObjectUrl(value.ToString());
        }

        public File HidePreview()
        {
            return (File)Options(new()
            {
                { "showPreview", false },
            });
        }
        protected string InitialCaption(string caption)
        {
            //return basename(caption);
            return caption;
        }

        protected List<Dictionary<string, dynamic>> InitialPreviewConfig()
        {
            var config = new Dictionary<string, object> { { "caption", InitialCaption(value.ToString()) }, { "key", 0 } };
            config = config.Merge(GuessPreviewType(value.ToString()));
            return new() { config };
        }

        protected void SetupScripts(string options)
        {
            Script = $@"
$(""input{GetElementClassSelector()}"").fileinput({options});
";

            if (fileActionSettings["showRemove"])
            {
                var text = new Dictionary<string, string>() {
                {"title"   , Admin.Trans("delete_confirm")},
                {"confirm" , Admin.Trans("confirm")},
                {"cancel"  , Admin.Trans("cancel")},
            };

                Script += $@"
$(""input{GetElementClassSelector()}"").on('filebeforedelete', function() {{

    return new Promise(function(resolve, reject) {{

        var remove = resolve;

        swal({{
            title: ""{text["title"]}"",
            type: ""warning"",
            showCancelButton: true,
            confirmButtonColor: ""#DD6B55"",
            confirmButtonText: ""{text["confirm"]}"",
            showLoaderOnConfirm: true,
            cancelButtonText: ""{text["cancel"]}"",
            preConfirm: function() {{
                return new Promise(function(resolve) {{
                    resolve(remove());
                }});
            }}
        }});
    }});
}});
";
            }
        }

        public override HtmlContent Render(bool t)
        {
            if (picker != null)
            {
                return RenderFilePicker();
            }

            Options(new() { { "overwriteInitial", true }, { "msgPlaceholder", Admin.Trans("choose_file") } });

            SetupDefaultOptions();

            if (value != null)
            {
                Attribute("data-initial-preview", Preview());
                Attribute("data-initial-caption", InitialCaption(value.ToString()));

                SetupPreviewOptions();
                /*
                 * If has original value, means the form is in edit mode,
                 * then remove required rule from rules.
                 */
                Attributes.Remove("required");
            }

            //var options = json_encode_options(this.options);
            var options = JsonSerializer.Serialize(this.options);
            SetupScripts(options);

            return base.Render(t);
        }
    }
}
