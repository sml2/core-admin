﻿using CoreAdmin.Lib;
using File = CoreAdmin.Mvc.Form.Field.File;
using CoreAdmin.Mvc.Form.Field;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Html;

namespace CoreAdmin.Mvc.Form
{
    /// <summary>
    /// 用于添加类的调用的实体类
    /// </summary>
    public class Builder
    {
        public const string PREVIOUS_URL_KEY = "_previous_";

        protected int id;

        protected BForm form;

        protected string action;

        protected List<FField> fields;

        //    /**
        //     * @var array
        //     */
        //    protected $options = [];

        public const string MODE_EDIT = "edit";
        public const string MODE_CREATE = "create";
        /**
         * Form action mode, could be create|view|edit.
         *
         * @var string
         */
        protected string mode = "create";

        protected List<FField> hiddenFields;

        protected Tools tools;

        protected Footer footer;

        protected Dictionary<string, int> width = new()
        {
            { "label", 2 },
            { "field", 8 },
        };

        protected string view = "Form";

        protected string title;

        protected string formClass;
        /// <summary>
        /// 赋值执行Init方法
        /// </summary>
        /// <param name="form"></param>
        public Builder(BForm form)
        {
            this.form = form;
            fields = new();
            hiddenFields = new();
            Init();
        }
        /// <summary>
        /// 用于上面方法的调用
        /// </summary>
        public void Init()
        {
            tools = new Tools(this);
            footer = new Footer(this);

            formClass = "model-form-" + Str.Uniqid();
        }

        public Tools GetTools()
        {
            return tools;
        }

        public Footer GetFooter()
        {
            return footer;
        }

        public void SetMode(string mode = "create")
        {
            this.mode = mode;
        }

        public string GetMode()
        {
            return mode;
        }
        
        public bool IsMode(string mode)
        {
            return this.mode == mode;
        }
        /// <summary>
        /// 用于页面调用   传值create
        /// </summary>
        /// <returns></returns>
        public bool IsCreating()
        {
            return IsMode(MODE_CREATE);
        }

        public bool IsEditing()
        {
            return IsMode(MODE_EDIT);
        }

        public void SetResourceId(int id)
        {
            this.id = id;
        }
        /// <summary>
        /// 获取到ID
        /// </summary>
        /// <returns></returns>
        public int GetResourceId()
        {
            return id;
        }

        public string GetResource(int? slice = null)
        {
            if (mode == MODE_CREATE)
            {
                return form.Resource(-1);
            }
            if (slice != null)
            {
                return form.Resource((int)slice);
            }

            return form.Resource();
        }

        public Builder SetWidth(int field = 8, int label = 2)
        {
            width = new()
            {
                { "label", label },
                { "field", field },
            };

            return this;
        }

        public Dictionary<string, int> GetWidth()
        {
            return width;
        }

        public void SetAction(string action)
        {
            this.action = action;
        }

        public string GetAction()
        {
            if (!string.IsNullOrEmpty(action))
            {
                return action;
            }

            if (IsMode(MODE_EDIT))
            {
                return form.Resource() + "/" + id;
            }

            if (IsMode(MODE_CREATE))
            {
                return form.Resource(-1);
            }

            return "";
        }

        public Builder SetView(string view)
        {
            this.view = view;

            return this;
        }

        public Builder SetTitle(string title)
        {
            this.title = title;

            return this;
        }

        public List<FField> Fields()
        {
            return fields;
        }

        ///**
        // * Get specify field.
        // *
        // * @param string $name
        // *
        // * @return mixed
        // */
        //public function field($name)
        //{
        //    return $this->fields()->first(function(Field $field) use($name) {
        //        return $field->column() === $name;
        //    });
        //    }

        public bool HasRows()
        {
            return form.rows != null && form.rows.Count > 0;
        }

        public List<Row> GetRows()
        {
            return form.rows;
        }

        public List<FField> GetHiddenFields()
        {
            return hiddenFields;
        }

        public void AddHiddenField(FField field)
        {
            field.SetForm(form);
            hiddenFields.Add(field);
        }

        ///**
        // * Add or get options.
        // *
        // * @param array $options
        // *
        // * @return array|null
        // */
        //public function options($options = [])
        //{
        //    if (empty($options))
        //    {
        //        return $this->options;
        //    }

        //        $this->options = array_merge($this->options, $options);
        //}

        ///**
        // * Get or set option.
        // *
        // * @param string $option
        // * @param mixed  $value
        // *
        // * @return $this
        // */
        //public function option($option, $value = null)
        //{
        //    if (func_num_args() === 1)
        //    {
        //        return Arr::get($this->options, $option);
        //    }

        //        $this->options[$option] = $value;

        //    return $this;
        //}

        public string Title()
        {
            if (!string.IsNullOrEmpty(title))
            {
                return title;
            }

            if (mode == MODE_CREATE)
            {
                return Admin.Trans("create");
            }

            if (mode == MODE_EDIT)
            {
                return Admin.Trans("edit");
            }

            return "";
        }

        public bool HasFile()
        {
            foreach (var field in Fields())
            {
                if (field is Form.Field.File || field is MultipleFile)
                {
                    return true;
                }
            }

            return false;
        }

        protected void AddRedirectUrlField()
        {
            //    var previous = URL::previous();

            //if (!$previous || $previous === URL::current()) {
            //    return;
            //}

            //if (Str::contains($previous, url($this->getResource())))
            //{
            //        $this->addHiddenField((new Hidden(static::PREVIOUS_URL_KEY))->value($previous));
            //}
        }

        public IHtmlContent Open(Dictionary<string, string> options = null)
        {
            Dictionary<string, object> attributes = new();

            if (IsMode(MODE_EDIT))
            {
                AddHiddenField(new Hidden("_method").Value("PUT"));
            }

            AddRedirectUrlField();

            attributes["action"] = GetAction();
            attributes["method"] = options != null ? options["method"] ?? "post" : "post";
            attributes["class"] = string.Join(" ", new string[] { "form-horizontal", formClass });
            attributes["accept-charset"] = "UTF-8";

            if (HasFile())
            {
                attributes["enctype"] = "multipart/form-data";
            }

            List<string> html = new();
            foreach (var pair in attributes)
            {
                html.Add($"{pair.Key}=\"{pair.Value}\"");
            }

            return new HtmlString($"<form {string.Join(" ", html)} pjax-container>");
        }

        public IHtmlContent Close()
        {
            form = null;
            fields = null;

            return new HtmlString("</form>");
        }

        ///**
        // * @param string $message
        // */
        //public function confirm(string $message)
        //{
        //        $trans = [
        //            'confirm' => trans('admin.confirm'),
        //            'cancel'  => trans('admin.cancel'),
        //        ];

        //        $script = <<< SCRIPT
        //$('form.{$this->formClass} button[type=submit]').click(function(e) {
        //        e.preventDefault();
        //        var form = $(this).parents('form');
        //        swal({
        //        title: "$message",
        //        type: "warning",
        //        showCancelButton: true,
        //        confirmButtonColor: "#DD6B55",
        //        confirmButtonText: "{$trans['confirm']}",
        //        cancelButtonText: "{$trans['cancel']}",
        //    }).then(function(result) {
        //    if (result.value)
        //    {
        //        form.submit();
        //    }
        //});
        //});
        //SCRIPT;

        //Admin::script($script);
        //    }

        /**
         * Remove reserved fields like `id` `created_at` `updated_at` in form fields.
         *
         * @return void
         */
        protected void RemoveReservedFields()
        {
            if (!IsCreating())
            {
                return;
            }

            List<string> reservedColumns = new()
            {
                "created_at",
                "updated_at",
                //form->model()->getCreatedAtColumn(),
                //    $this->form->model()->getUpdatedAtColumn(),
                "id",
            };

            //if ($this->form->model()->incrementing) {
            //        $reservedColumns[] = $this->form->model()->getKeyName();
            //}

            form.GetLayout().RemoveReservedFields(reservedColumns);

            fields = Fields().Where((field) => !reservedColumns.Contains(field.Column())).ToList();
        }
        /// <summary>
        /// 不等于null返回
        /// </summary>
        /// <returns></returns>
        public string RenderTools()
        {
            return tools != null ? tools.Render() : "";
        }

        public HtmlContent RenderFooter()
        {
            return footer.Render();
        }
               
         /// <summary>
         /// Add script for tab form.
         /// </summary>
        protected void AddTabformScript()
        {
            var script = $@"

        var hash = document.location.hash;
            if (hash)
            {{
            $('.nav-tabs a[href=""' + hash + '""]').tab('show');
            }}

        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab', function(e) {{
                history.pushState(null, null, e.target.hash);
            }});

            if ($('.has-error').length) {{
            $('.has-error').each(function() {{
                    var tabId = '#' +$(this).closest('.tab-pane').attr('id');
                $('li a[href=""' + tabId + '""] i').removeClass('hide');
                }});

                var first = $('.has-error:first').closest('.tab-pane').attr('id');
            $('li a[href=""#' + first + '""]').tab('show');
            }}

            ";
            Admin.Script(script);
        }

        protected void AddCascadeScript()
        {
            var script = $@"
        ; (function() {{
            $('form.{formClass}').submit(function(e) {{
                    e.preventDefault();
                $(this).find('div.cascade-group.hide :input').attr('disabled', true);
                }});
            }})();
            ";

            Admin.Script(script);
        }

        public string Render() => "";

        public HtmlContent Render(bool t)
        {
            RemoveReservedFields();

            var tabObj = form.SetTab();

            if (!tabObj.IsEmpty())
            {
                AddTabformScript();
            }

            AddCascadeScript();

            var data = new Dictionary<string, dynamic> {
                    {"form"   , this },
                    {"tabObj" , tabObj},
                    {"width"  , width},
                    { "layout" , form.GetLayout()},
                };

            return new()
            {
                Name = HtmlContentType.Partial,
                Value = view,
                Data = data,
            };
        }

        public void AddErrorMessage(Dictionary<string, string> errors)
        {
            fields.ForEach(field =>
            {
                if (errors.ContainsKey(field.Column()))
                {
                    field.AddVariables(variables => {
                        variables.ErrorMessage = errors[field.Column()];
                    });
                }
            });
        }
    }
}
