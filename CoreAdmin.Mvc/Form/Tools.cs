﻿using CoreAdmin.Lib;
//using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Form
{
    public class Tools : IRenderable
    {
        protected Builder form;

        protected List<string> tools = new() { "delete", "view", "list" };

        protected List<dynamic> appends;

        protected List<dynamic> prepends;

        public Tools(Builder builder)
        {
            form = builder;
            appends = new();
            prepends = new();
        }
        /// <summary>
        /// 添加调用
        /// </summary>
        /// <param name="tool"></param>
        /// <returns></returns>
        public Tools Append(string tool)
        {
            appends.Add(tool);
            return this;
        }

        public Tools Prepend(string tool)
        {
            prepends.Add(tool);
            return this;
        }

        public Tools DisableList(bool disable = true)
        {
            return DisableTool("list", disable);
        }

        public Tools DisableTool(string tool, bool disable = true)
        {
            if (disable)
            {
                tools.Remove(tool);
            }
            else if (!tools.Contains(tool))
            {
                tools.Add(tool);
            }

            return this;
        }

        public Tools DisableDelete(bool disable = true)
        {
            return DisableTool("delete", disable);
        }

        public Tools DisableView(bool disable = true)
        {
            return DisableTool("view", disable);
        }

        protected string GetListPath()
        {
            return form.GetResource();
        }
        /// <summary>
        /// 调用删除的方法
        /// </summary>
        /// <returns></returns>
        protected string GetDeletePath()
        {
            return GetViewPath();
        }
        /// <summary>
        /// 获取链接是否有Id   *
        /// </summary>
        /// <returns></returns>
        protected string GetViewPath()
        {
            var key = form.GetResourceId();

            if (key > 0)
            {
                return GetListPath() + "/" + key;
            }
            else
            {
                return GetListPath();
            }
        }

        public Builder Form()
        {
            return form;
        }

        /// <summary>
        /// 渲染一个A标签
        /// </summary>
        /// <returns></returns>
        public string RenderList()
        {
            var text = Admin.Trans("list");

            return $@"
    <div class=""btn-group pull-right"" style=""margin-right: 5px"">
    <a href=""{GetListPath()}"" class=""btn btn-sm btn-default"" title=""{text}""><i class=""fa fa-list""></i><span class=""hidden-xs"">&nbsp;{text}</span></a>
</div>
";
        }

        /// <summary>
        /// 渲染一个A标签
        /// </summary>
        /// <returns></returns>
        public string RenderView()
        {
            var view = Admin.Trans("view");

            return $@"
<div class=""btn-group pull-right"" style=""margin-right: 5px"">
    <a href=""{GetViewPath()}"" class=""btn btn-sm btn-primary"" title=""{view}"">
        <i class=""fa fa-eye""></i><span class=""hidden-xs""> {view}</span>
</a>
</div>
";
        }

        /// <summary>
        /// AJAX的删除操作
        /// </summary>
        /// <returns></returns>
        public string RenderDelete()
        {
            var trans = new Dictionary<string, string>{
            {"delete_confirm" ,Admin.Trans("delete_confirm") },
            {"confirm"        ,Admin.Trans("confirm") },
            {"cancel"         ,Admin.Trans("cancel") },
            { "delete"        , Admin.Trans("delete") },
        };

            var _class = Str.Uniqid();

            var script = $@"

$('.{_class}-delete').unbind('click').click(function() {{

    swal({{
    title: ""{trans["delete_confirm"]}"",
        type: ""warning"",
        showCancelButton: true,
        confirmButtonColor: ""#DD6B55"",
        confirmButtonText: ""{trans["confirm"]}"",
        showLoaderOnConfirm: true,
        cancelButtonText: ""{trans["cancel"]}"",
        preConfirm: function() {{
            return new Promise(function(resolve) {{
                $.ajax({{
                    method: 'delete',
                    url: '{GetDeletePath()}',
                    data: {{
                        _method:'delete',
                        _token:LA.token,
                    }},
                    success: function (data) {{
                        $.pjax({{container:'#pjax-container', url: '{GetListPath()}' }});

            resolve(data);
        }}
    }});
}});
        }}
    }}).then(function(result) {{
    var data = result.value;
    if (typeof data === 'object')
    {{
        if (data.status)
        {{
            swal(data.message, '', 'success');
        }}
        else
        {{
            swal(data.message, '', 'error');
        }}
    }}
}});
}});

";

            Admin.Script(script);

            return $@"
<div class=""btn-group pull-right"" style = ""margin-right: 5px"" >

<a href=""javascript:void(0);"" class=""btn btn-sm btn-danger {_class}-delete"" title=""{trans["delete"]}"" >

      <i class=""fa fa-trash"" ></i><span class=""hidden-xs"" >  {trans["delete"]}</span>
          </a>
      </div>
      ";
        }

        public Tools Add(string tool)
        {
            return Append(tool);
        }

        [Obsolete("Use disableList instead.")]
        public Tools DisableListButton()
        {
            return DisableList();
        }
        /// <summary>
        /// 用与调用   判断是否是create调用DisableView和DisableDelete页面  不是就返回空
        /// </summary>
        /// <param name="tools"></param>
        /// <returns></returns>
        public string RenderCustomTools(List<dynamic> tools)
        {
            if (form.IsCreating())
            {
                DisableView();
                DisableDelete();
            }

            if (tools.Count == 0)
            {
                return "";
            }

            return string.Join(" ", tools.Select((tool) =>
            {
                if (tool is IRenderable renderable)
                {
                    return renderable.Render();
                }

                if (tool is Interface.IHtmlable htmlable)
                {
                    return htmlable.ToHtml();
                }

                return (string)tool;
            }));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Render()
        {
            var output = RenderCustomTools(prepends);

            foreach (var tool in tools)
            {
                var renderMethod = "Render" + tool.UpperCaseFirst();
                var methodInfo = GetType().GetMethod(renderMethod);
                if (methodInfo == null)
                {
                    throw new ArgumentException($"[{renderMethod}] not exists");
                }
                output += methodInfo.Invoke(this, null);
            }

            return output + RenderCustomTools(appends);
        }
    }
}
