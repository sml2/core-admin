﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Form
{
    public class Tab
    {
        protected BForm form;

        protected List<Dictionary<string, dynamic>> tabs;

        protected int offset = 0;

        public Tab(BForm form)
        {
            this.form = form;
            tabs = new();
        }

        public Tab Append(string title, Action<BForm> content, bool active = false)
        {
            var fields = CollectFields(content);
            var id = "form-" + (tabs.Count + 1);
            tabs.Add(new()
            {
                { "id", id },
                { "title", title },
                { "fields", fields },
                { "active", active },
            });

            return this;
        }

        protected List<FField> CollectFields(Action<BForm> content)
        {
            content.Invoke(form);

            //$fields = clone     form.Fields();
            var fields = form.Fields();

            var all = fields;

            foreach (var row in form.rows)
            {

                var rowFields = row.GetFields().Select(field => field["element"]);


                var match = false;

                //foreach (var field in rowFields) {
                //    if ((var index = array_search($field, $all)) !== false) {
                //        if (!$match) {
                //        $fields->put($index, $row);
                //        } else
                //        {
                //        $fields->pull($index);
                //        }

                //    $match = true;
                //    }
                //}
            }

            //fields = fields->slice($this->offset);

            offset += fields.Count();

            return fields;
        }

        public List<Dictionary<string, dynamic>> GetTabs()
        {
            // If there is no active tab, then active the first.
            if (tabs.Where((tab) => tab["active"]).Count() == 0)
            {
                var first = tabs.First();
                first["active"] = true;

                tabs[0] = first;
            }

            return tabs;
        }

        public bool IsEmpty()
        {
            return tabs.Count == 0;
        }
    }
}
