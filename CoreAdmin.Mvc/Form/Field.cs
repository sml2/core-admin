﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Form
{
    public abstract class FField : Widgets.IRenderable
    {
        //        use Macroable;

        public const string FILE_DELETE_FLAG = "_file_del_";
        public const string FILE_SORT_FLAG = "_file_sort_";

        protected string id;

        protected object value;

        protected object data;

        protected object original;

        protected object _default;

        protected string label;

        protected string column;

        protected string elementName;

        protected List<string> elementClass;

        protected virtual ViewFormField variables { get; set; }

        protected Dictionary<string, dynamic> options { get; set; }

        protected Dictionary<string, dynamic> _checked;

        protected List<string> rules;

        public List<string> creationRules;

        public List<string> updateRules;

        //    /**
        //     * @var \Closure
        //     */
        //    protected $validator;

        protected Dictionary<string, List<string>> validationMessages { get; set; }

        protected static List<string> Css { get; set; }
        protected static List<string> Js { get; set; }

        protected string Script = "";

        protected HtmlAttributes Attributes;

        protected IForm form = null;

        protected string view = "";

        protected ViewFormHelp help;

        protected string errorKey;

        protected string placeholder;

        protected Dictionary<string, int> width = new()
        {
            { "label", 2 },
            { "field", 8 },
        };

        protected bool horizontal = true;

        protected Func<object, object> customFormat = null;

        protected bool display = true;

        protected List<string> labelClass;

        protected List<string> groupClass = new();

        protected Func<dynamic, dynamic, FField, dynamic> callback;

        public bool isJsonType = false;

        public FField(string column = "", params object[] arguments)
        {
            this.column = FormatColumn(column);
            label = FormatLabel(arguments);
            id = FormatId(column);
            validationMessages = new();
            Attributes = new();
            variables = new();
            labelClass = new();
            _checked = new();
            options = new();
        }

        protected string FormatColumn(string column = "")
        {
            if (column.Contains("->"))
            {
                isJsonType = true;
                column = column.Replace("->", ".");
            }

            return column;
        }

        protected string FormatId(string column)
        {
            return column.Replace(".", "_");
        }

        protected string FormatLabel(object[] arguments)
        {
            return string.Empty;
            //var column = this.column;
            //string label = arguments.Length > 0 ? (string)arguments[0] : column.UpperCaseFirst();

            //return label.Replace(new string[]{".","_", "->",}, " ");
        }

        protected string FormatName(string column)
        {
            List<string> name;
            if (column.Contains("->"))
            {
                name = column.Split("->").ToList();
            }
            else
            {
                name = column.Split(".").ToList();
            }

            if (name.Count == 1)
            {
                return name[0];
            }

            var html = name[0];
            foreach (var piece in name.Skip(1))
            {
                html += $"[{piece}]";
            }

            return html;
        }
        protected string FormatName(List<string> column)
        {
            //if (is_array($this->column))
            //{
            //        $names = [];
            //    foreach ($this->column as $key => $name) {
            //            $names[$key] = $this->formatName($name);
            //    }

            //    return $names;
            //}

            return "";
        }

            public FField SetElementName(string name)
            {
                elementName = name;

                return this;
            }

        public void Fill<T>(T data)
        {
            this.data = data;
            if (data == null) return;
            //if (is_array($this->column))
            //{
            //    foreach ($this->column as $key => $column) {
            //            $this->value[$key] = Arr::get($data, $column);
            //    }

            //    return;
            //}
            var property = data.GetType().GetProperty(column);
            if (property != null)
            {
                value = property.GetValue(data);
                FormatValue();
            }
        }
        
        protected void FormatValue()
        {
            if (customFormat != null)
            {
                value = customFormat(value);
            }
        }

        //    /**
        //     * custom format form column data when edit.
        //     *
        //     * @param \Closure $call
        //     *
        //     * @return $this
        //     */
        //    public function customFormat(\Closure $call): self
        //    {
        //        $this->customFormat = $call;

        //        return $this;
        //    }


        public void SetOriginal(IIdexerModel data)
        {
            //if (is_array($this->column))
            //{
            //    foreach ($this->column as $key => $column) {
            //            $this->original[$key] = Arr::get($data, $column);
            //    }

            //    return;
            //}

            //    $this->original = Arr::get($data, $this->column);
        }

        public FField SetForm(IForm form = null)
        {
            this.form = form;
            return this;
        }


        public FField SetWidgetForm(IForm form)
        {
            this.form = form;
            return this;
        }

        public FField SetWidth(int field = 8, int label = 2)
        {
            width = new()
            {
                { "label", label },
                { "field", field },
            };

            return this;
        }

        public virtual FField Options(Dictionary<string, dynamic> options)
        {
            this.options = this.options.Merge(options);
            return this;
        }

        public FField Checked(Dictionary<string, dynamic> _checked)
        {
            this._checked = this._checked.Merge( _checked);
            return this;
        }

        /**
         * Add `required` attribute to current field if has required rule,
         * except file and image fields.
         *
         * @param array $rules
         */
        protected void AddRequiredAttribute(List<string> rules)
        {
            //if (!is_array($rules))
            //{
            //    return;
            //}

            //if (!in_array('required', $rules, true))
            //{
            //    return;
            //}

            //    $this->setLabelClass(['asterisk']);

            //// Only text field has `required` attribute.
            //if (!$this instanceof Form\Field\Text) {
            //    return;
            //}

            ////do not use required attribute with tabs
            //if ($this->form && $this->form->getTab()) {
            //    return;
            //}

            //    $this->required();
        }

        /**
         * If has `required` rule, add required attribute to this field.
         */
        protected void AddRequiredAttributeFromRules()
        {
            dynamic rules;
            if (data == null)
            {
                // Create page
                rules = creationRules ?? this.rules;
            }
            else
            {
                // Update page
                rules = updateRules ?? this.rules;
            }

            AddRequiredAttribute(rules);
        }

        protected List<string> FormatRules(List<string> rules)
        {
            //if (is_string($rules)) {
            //    $rules = array_filter(explode('|', $rules));
            //}

            return rules.Where(rule => !string.IsNullOrEmpty(rule)).ToList();
        }

        protected List<string> MergeRules(List<string> input, List<string> original)
        {
            //if ($input instanceof Closure) {
            //        $rules = $input;
            //} else
            //{
            if (original != null && original.Count > 0)
            {
                original = FormatRules(original);
            }

            var rules = original;
            if (rules == null)
            {
                rules = FormatRules(input);
            }
            else
            {
                rules.AddRange(FormatRules(input));
            }
            //}

            return rules.Distinct().ToList();
        }

        [Obsolete]
        public FField Rules(string rules, List<string> messages = null) => Rules(new List<string> { rules }, messages);
        [Obsolete]
        public FField Rules(List<string> rules = null, List<string> messages = null)
        {
            this.rules = MergeRules(rules, this.rules);

            SetValidationMessages("default", messages);

            return this;
        }

        public FField UpdateRules(List<string> rules = null, List<string> messages = null)
        {
            updateRules = MergeRules(rules, updateRules);

            SetValidationMessages("update", messages);

            return this;
        }

        public FField CreationRules(List<string> rules = null, List<string> messages = null)
        {
            creationRules = MergeRules(rules, creationRules);

            SetValidationMessages("creation", messages);

            return this;
        }

        public FField SetValidationMessages(string key, List<string> messages)
        {
            validationMessages[key] = messages;

            return this;
        }

        //    /**
        //     * Get validation messages for the field.
        //     *
        //     * @return array|mixed
        //     */
        //    public function getValidationMessages()
        //    {
        //        // Default validation message.
        //        $messages = $this->validationMessages['default'] ?? [];

        //        if (request()->isMethod('POST')) {
        //            $messages = $this->validationMessages['creation'] ?? $messages;
        //        } elseif (request()->isMethod('PUT')) {
        //            $messages = $this->validationMessages['update'] ?? $messages;
        //        }

        //        return $messages;
        //    }

        /**
         * Get field validation rules.
         *
         * @return string
         */
        protected virtual List<string> GetRules()
        {
            //if (request()->isMethod('POST'))
            //{
            //        $rules = $this->creationRules ?: $this->rules;
            //}
            //elseif(request()->isMethod('PUT')) {
            //        $rules = $this->updateRules ?: $this->rules;
            //} else
            //{
            var rules = this.rules;
            //}

            //if ($rules instanceof \Closure) {
            //        $rules = $rules->call($this, $this->form);
            //}

            //if (is_string($rules))
            //{
            //        $rules = array_filter(explode('|', $rules));
            //}

            //if (!$this->form || !$this->form instanceof Form) {
            //    return $rules;
            //}

            //if (!$id = $this->form->model()->getKey()) {
            //    return $rules;
            //}

            //if (is_array($rules))
            //{
            //foreach (var rule in rules) {

            //     rule = rule.Replace("{{id}}", id);
            //}
            //}

            return rules;
        }

        //    /**
        //     * Remove a specific rule by keyword.
        //     *
        //     * @param string $rule
        //     *
        //     * @return void
        //     */
        //    protected function removeRule($rule)
        //    {
        //        if (is_array($this->rules)) {
        //            array_delete($this->rules, $rule);

        //            return;
        //        }

        //        if (!is_string($this->rules)) {
        //            return;
        //        }

        //        $pattern = "/{$rule}[^\|]?(\||$)/";
        //        $this->rules = preg_replace($pattern, '', $this->rules, -1);
        //    }

        //    /**
        //     * Set field validator.
        //     *
        //     * @param callable $validator
        //     *
        //     * @return $this
        //     */
        //    public function validator(callable $validator): self
        //    {
        //        $this->validator = $validator;

        //        return $this;
        //    }

        public string GetErrorKey()
        {
            return errorKey ?? column;
        }

        //    /**
        //     * Set key for error message.
        //     *
        //     * @param string $key
        //     *
        //     * @return $this
        //     */
        //    public function setErrorKey($key): self
        //    {
        //        $this->errorKey = $key;

        //        return $this;
        //    }

        public object Value() => (value ?? GetDefault());

        public FField Value(dynamic value)
        {
            this.value = value;
            return this;
        }

        //    /**
        //     * Set or get data.
        //     *
        //     * @param array $data
        //     *
        //     * @return mixed
        //     */
        //    public function data(array $data = null)
        //    {
        //        if ($data === null) {
        //            return $this->data;
        //        }

        //        $this->data = $data;

        //        return $this;
        //    }

        public FField Default(string defaultValue) => SetDefault(defaultValue);
        public FField Default(Func<BForm, string> defaultValue) => SetDefault(defaultValue);

        protected FField SetDefault(object defaultValue)
        {
            _default = defaultValue;
            return this;
        }

        protected object GetDefault()
        {
            return _default switch
            {
                Func<BForm, string> action => action.Invoke(form as BForm),
                Func<BForm, IEnumerable<string>> list => list.Invoke(form as BForm),
                _ => _default
            };
        }

        public FField Help(string text = "", string icon = "fa-info-circle")
        {
            help.Text = text;
            help.Icon = icon;

            return this;
        }

        public string Column() => column;

        public string Label() => label;

        public dynamic Original() => original;


        //    /**
        //     * Get validator for this field.
        //     *
        //     * @param array $input
        //     *
        //     * @return bool|\Illuminate\Contracts\Validation\Validator|mixed
        //     */
        //    public function getValidator(array $input)
        //    {
        //        if ($this->validator) {
        //            return $this->validator->call($this, $input);
        //        }

        //        $rules = $attributes = [];

        //        if (!$fieldRules = $this->getRules()) {
        //            return false;
        //        }

        //        if (is_string($this->column)) {
        //            if (!Arr::has($input, $this->column)) {
        //                return false;
        //            }

        //            $input = $this->sanitizeInput($input, $this->column);

        //            $rules[$this->column] = $fieldRules;
        //            $attributes[$this->column] = $this->label;
        //        }

        //        if (is_array($this->column)) {
        //            foreach ($this->column as $key => $column) {
        //                if (!array_key_exists($column, $input)) {
        //                    continue;
        //                }
        //                $input[$column.$key] = Arr::get($input, $column);
        //                $rules[$column.$key] = $fieldRules;
        //                $attributes[$column.$key] = $this->label."[$column]";
        //            }
        //        }

        //        return \validator($input, $rules, $this->getValidationMessages(), $attributes);
        //    }

        //    /**
        //     * Sanitize input data.
        //     *
        //     * @param array  $input
        //     * @param string $column
        //     *
        //     * @return array
        //     */
        //    protected function sanitizeInput($input, $column)
        //    {
        //        if ($this instanceof Field\MultipleSelect) {
        //            $value = Arr::get($input, $column);
        //            Arr::set($input, $column, array_filter($value));
        //        }

        //        return $input;
        //    }

        public FField Attribute(Dictionary<string, string> attribute)
        {
            foreach (var pair in attribute)
            {
                Attributes[pair.Key] = pair.Value;
            }
            return this;
        }
        public FField Attribute(string attribute, object value = null)
        {
            Attributes[attribute] = value;
            return this;
        }

        public FField RemoveAttribute(string attribute)
        {
            Attributes.Remove(attribute);
            return this;
        }

        public FField Style(string attr, string value)
        {
            Attributes.StyleDic.Add(attr, value);
            return this;
        }

        public FField Width(string width) => Style("width", width);

        public FField Pattern(string regexp) => Attribute("pattern", regexp);

        public FField Required(bool isLabelAsterisked = true)
        {
            if (isLabelAsterisked)
            {
                SetLabelClass(new() { "asterisk" });
            }

            return Attribute("required", true);
        }

        public FField Autofocus() => Attribute("autofocus", true);


        public FField Readonly() => Attribute("readonly", true);

        public FField Disable() => Attribute("disabled", true);

        public FField Placeholder(string placeholder = "")
        {
            this.placeholder = placeholder;

            return this;
        }

        public string GetPlaceholder()
        {
            return placeholder ?? Admin.Trans("input") + " " + label;
        }

        //    /**
        //     * Add a divider after this field.
        //     *
        //     * @return $this
        //     */
        //    public function divider()
        //    {
        //        $this->form->divider();

        //        return $this;
        //    }

        public virtual string Prepare(string value) => value;

        protected HtmlAttributes FormatAttributes()
        {
            return Attributes;
        }

            public FField DisableHorizontal()
            {
                horizontal = false;

                return this;
            }

        public ViewFormViewClass GetViewElementClasses()
        {
            if (horizontal)
            {
                return new (){
                        Label      = $"col-sm-{width["label"]} {GetLabelClass()}",
                        Field      = $"col-sm-{width["field"]}",
                        FormGroup = GetGroupClass(true),
                    };
            }

            return new() { Label= GetLabelClass(), Field= "", FormGroup= "" };
        }

        public FField SetElementClass(List<string> classes)
        {
            elementClass.AddRange(classes);
            return this;
        }

        public List<string> GetElementClass()
        {
            if (elementClass == null || elementClass.Count == 0)
            {
                var name = elementName ?? FormatName(column);

                elementClass = new() { name.Replace('[', '_').Replace(']', '_') };
            }

            return elementClass;
        }

        public string GetElementClassString()
        {
            var elementClass = GetElementClass();
            return string.Join(' ', elementClass);
        }

        public string GetElementClassSelector()
        {
            var elementClass = GetElementClass();
            return "." + string.Join('.', elementClass);
        }

        //    /**
        //     * Add the element class.
        //     *
        //     * @param $class
        //     *
        //     * @return $this
        //     */
        //    public function addElementClass($class): self
        //    {
        //        if (is_array($class) || is_string($class)) {
        //            $this->elementClass = array_unique(array_merge($this->elementClass, (array) $class));
        //        }

        //        return $this;
        //    }

        //    /**
        //     * Remove element class.
        //     *
        //     * @param $class
        //     *
        //     * @return $this
        //     */
        //    public function removeElementClass($class): self
        //    {
        //        $delClass = [];

        //        if (is_string($class) || is_array($class)) {
        //            $delClass = (array) $class;
        //        }

        //        foreach ($delClass as $del) {
        //            if (($key = array_search($del, $this->elementClass, true)) !== false) {
        //                unset($this->elementClass[$key]);
        //            }
        //        }

        //        return $this;
        //    }

        //    /**
        //     * Set form group class.
        //     *
        //     * @param string|array $class
        //     *
        //     * @return $this
        //     */
        //    public function setGroupClass($class): self
        //    {
        //        if (is_array($class)) {
        //            $this->groupClass = array_merge($this->groupClass, $class);
        //        } else {
        //            $this->groupClass[] = $class;
        //        }

        //        return $this;
        //    }

        protected string GetGroupClass(bool _default = false)
        {
            return (_default ? "form-group " : "") + string.Join(" ", groupClass.Where(cls => !string.IsNullOrEmpty(cls)));
        }

        //    /**
        //     * reset field className.
        //     *
        //     * @param string $className
        //     * @param string $resetClassName
        //     *
        //     * @return $this
        //     */
        //    public function resetElementClassName(string $className, string $resetClassName): self
        //    {
        //        if (($key = array_search($className, $this->getElementClass())) !== false) {
        //            $this->elementClass[$key] = $resetClassName;
        //        }

        //        return $this;
        //    }
        public virtual FField AddVariables(Action<ViewFormField> action)
        {
            action?.Invoke(variables);
            return this;
        }
        [Obsolete]
        public FField AddVariables(string key, object value)
        {
            //variables[key] = value;
            return this;
        }
        [Obsolete]
        public FField AddVariables(Dictionary<string, object> variables)
        {
            foreach (var pair in variables)
            {
                //this.variables[pair.Key] = pair.Value;
            }

            return this;
        }

        public string GetLabelClass()
        {
            return string.Join(" ", labelClass);
        }

        public FField SetLabelClass(List<string> labelClass, bool replace = false)
        {
            if (replace)
            {
                this.labelClass = labelClass;
            }
            else
            {
                this.labelClass.AddRange(labelClass);
                this.labelClass = this.labelClass.Distinct().ToList();
            }

            return this;
        }

        public virtual ViewFormField Variables()
        {
            var variables = this.variables;
            return MergeVariables(variables);
        }
        protected ViewFormField MergeVariables(ViewFormField variables)
        {
            variables.Id ??= id;
            variables.Name ??= elementName ?? FormatName(column);
            variables.Help ??= help;
            variables.Class ??= GetElementClassString();
            variables.Value ??= Value()?.ToString();
            variables.Label ??= label;
            variables.ViewClass ??= GetViewElementClasses();
            variables.Column ??= column;
            variables.ErrorKey ??= GetErrorKey();
            variables.Attributes ??= FormatAttributes();
            variables.Placeholder ??= GetPlaceholder();
            variables.Data ??= data as BaseModel;
            variables.ModelType ??= form.ModelType ?? data.GetType();

            return variables;
        }

        public string GetView()
        {
            if (!string.IsNullOrEmpty(view))
            {
                return view;
            }

            var Class = GetType().Name;
            return "~/Views/Form/" + Class.ToLower() + ".cshtml";
        }

        public FField SetView(string view)
        {
            this.view = view;
            return this;
        }

        public string GetScript() => Script;

        public FField SetScript(string script)
        {
            Script = script;
            return this;
        }

        public FField SetDisplay(bool display)
        {
            this.display = display;
            return this;
        }

        protected bool ShouldRender()
        {
            return display;
        }

        public FField With(Func<dynamic, dynamic, FField, dynamic> callback)
        {
            this.callback = callback;
            return this;
        }

        public virtual string Render() => "";

        public virtual HtmlContent Render(bool t)
        {
            if (!ShouldRender())
            {
                return null;
            }

            AddRequiredAttributeFromRules();

            if (callback != null)
            {
                value = callback.Invoke(form.Model(), value, this);
            }
            if (!string.IsNullOrEmpty(Script))
            {
                Admin.Script(Script);
            }

            return Admin.Component(GetView(), null, Variables());
        }

        protected HtmlContent FieldRender(Dictionary<string, dynamic> variables)
        {
            if (variables.Count > 0)
            {
                AddVariables(variables);
            }

            return Render(true);
        }
    }
}
