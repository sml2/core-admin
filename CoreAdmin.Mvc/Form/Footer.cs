﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Form
{
    public class Footer : IRenderable
    {
        protected string view = "~/Views/Form/Footer.cshtml";

        protected Builder builder;

        protected List<string> buttons = new() { "reset", "submit" };

        protected List<string> checkboxes = new() { "view", "continue_editing", "continue_creating" };

        protected string defaultCheck;

        public Footer(Builder builder)
        {
            this.builder = builder;
        }

        public Footer DisableButton(string btn, bool disable = true)
        {
            if (disable)
            {
                buttons.Remove(btn);
            }
            else if (!buttons.Contains(btn))
            {
                buttons.Add(btn);
            }

            return this;
        }

        public Footer DisableReset(bool disable = true)
        {
            return DisableButton("reset", disable);
        }

        public Footer DisableSubmit(bool disable = true)
        {
            return DisableButton("submit", disable);
        }

        public Footer DisableCheck(string check, bool disable = true)
        {
            if (disable)
            {
                checkboxes.Remove(check);
            }
            else if (!checkboxes.Contains(check))
            {
                checkboxes.Add(check);
            }

            return this;
        }

        public Footer DisableViewCheck(bool disable = true)
        {
            return DisableCheck("view", disable);
        }
        public Footer DisableEditingCheck(bool disable = true)
        {
            return DisableCheck("continue_editing", disable);
        }

        public Footer DisableCreatingCheck(bool disable = true)
        {
            return DisableCheck("continue_creating", disable);
        }

        private Footer Check(string check)
        {
            defaultCheck = check;
            return this;
        }

        public Footer CheckView() => Check("view");
        public Footer CheckCreating() => Check("continue_creating");
        public Footer CheckEditing() => Check("continue_editing");

        protected void SetupScript()
        {
            var script = $@"
            
$('.after-submit').iCheck({{ checkboxClass: 'icheckbox_minimal-blue'}}).on('ifChecked', function() {{
    $('.after-submit').not(this).iCheck('uncheck');
            }});
            ";

            Admin.Script(script);
        }

        public HtmlContent Render()
        {
            SetupScript();

            var submitRedirects = new Dictionary<int, string> {
               {     1 , "continue_editing" },
               { 2 , "continue_creating"},
               { 3 , "view"},
            };

            var data = new Dictionary<string, dynamic> {
                {"width"           , builder.GetWidth() },
                {"buttons"         , buttons},
                {"checkboxes"      , checkboxes},
                {"submit_redirects", submitRedirects},
                { "default_check"   , defaultCheck},
            };
            return new HtmlContent()
            {
                Name = HtmlContentType.Partial,
                Value = view,
                Data = data,
            };
        }
    }
}
