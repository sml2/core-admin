﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace CoreAdmin.Form
//{
//    public class NestedForm
//    {
//        public const string DEFAULT_KEY_NAME = "__LA_KEY__";

//        public const string REMOVE_FLAG_NAME = "_remove_";

//        public const string REMOVE_FLAG_CLASS = "fom-removed";

//        protected string key;

//        protected string relationName;

//        protected dynamic model;

//        protected List<FField> fields;

//        protected Dictionary<string, dynamic> original;

//        /**
//         * @var \Encore\Admin\Form|\Encore\Admin\Widgets\Form
//         */
//        protected BForm form;

//        public NestedForm(string relation, dynamic model = null)
//        {
//            this.relationName = relation;

//            this.model = model;

//            this.fields = new();
//        }

//        public dynamic Model()
//        {
//            return model;
//        }

//        public string GetKey()
//        {
//            string key = null;
//            if (model != null)
//            {
//                key = model.GetKey();
//            }

//            if ((this.key != null))
//            {
//                key = this.key;
//            }

//            if (key != null)
//            {
//                return key;
//            }

//            return "new_" + DEFAULT_KEY_NAME;
//        }

//        public NestedForm SetKey(string key)
//        {
//            this.key = key;

//            return this;
//        }

//        public NestedForm SetForm(BForm form = null)
//        {
//            this.form = form;

//            return this;
//        }

//        /**
//         * Set Widget/Form.
//         *
//         * @param WidgetForm $form
//         *
//         * @return $this
//         */
//        //public function SetWidgetForm(WidgetForm $form = null)
//        //{
//        //$this->form = $form;

//        //    return $this;
//        //}

//        public BForm GetForm()
//        {
//            return form;
//        }

//        public NestedForm SetOriginal(Dictionary<string, dynamic> data, string relatedKeyName = null)
//        {
//            if (data == null)
//            {
//                return this;
//            }

//            foreach (var pair in data)
//            {
//                /*
//                 * like $this->original[30] = [ id = 30, .....]
//                 */
//                var key = pair.Key;
//                if (!string.IsNullOrEmpty(relatedKeyName))
//                {
//                    key = pair.Value[relatedKeyName];
//                }

//                original[key] = pair.Value;
//            }

//            return this;
//        }

//        public Dictionary<string, dynamic> Prepare(Dictionary<string, dynamic> input)
//        {
//            foreach (var pair in input)
//            {
//            SetFieldOriginalValue(pair.Key);
//            input[pair.Key] = PrepareRecord(pair.Value);
//            }

//            return input;
//        }

//        protected void SetFieldOriginalValue(string key)
//        {
//        dynamic values = null;
//            if (original.ContainsKey(key))
//            {
//            values = original[key];
//            }

//            fields.ForEach((field) => {
//                field.SetOriginal(values);
//            });
//        }

//        /**
//         * Do prepare work before store and update.
//         *
//         * @param array $record
//         *
//         * @return array
//         */
//        //protected function PrepareRecord(dynamic record)
//        //{
//        //    if (record[REMOVE_FLAG_NAME] == 1) {
//        //        return record;
//        //    }

//        //var prepared = [];

//        //    /* @var Field $field */
//        //    foreach ($this->fields as $field) {
//        //    $columns = $field->column();

//        //    $value = $this->fetchColumnValue($record, $columns);

//        //        if (is_null($value))
//        //        {
//        //            continue;
//        //        }

//        //        if (method_exists($field, 'prepare'))
//        //        {
//        //        $value = $field->prepare($value);
//        //        }

//        //        if (($field instanceof \Encore\Admin\Form\Field\Hidden) || $value != $field->original()) {
//        //            if (is_array($columns))
//        //            {
//        //                foreach ($columns as $name => $column) {
//        //                    Arr::set($prepared, $column, $value[$name]);
//        //                }
//        //            }
//        //            elseif(is_string($columns)) {
//        //                Arr::set($prepared, $columns, $value);
//        //            }
//        //        }
//        //    }

//        //$prepared[static::REMOVE_FLAG_NAME] = $record[static::REMOVE_FLAG_NAME];

//        //    return $prepared;
//        //}

//        /**
//         * Fetch value in input data by column name.
//         *
//         * @param array        $data
//         * @param string|array $columns
//         *
//         * @return array|mixed
//         */
//        //protected function fetchColumnValue($data, $columns)
//        //{
//        //    if (is_string($columns))
//        //    {
//        //        return Arr::get($data, $columns);
//        //    }

//        //    if (is_array($columns))
//        //    {
//        //    $value = [];
//        //        foreach ($columns as $name => $column) {
//        //            if (!Arr::has($data, $column))
//        //            {
//        //                continue;
//        //            }
//        //        $value[$name] = Arr::get($data, $column);
//        //        }

//        //        return $value;
//        //    }
//        //}

//        public NestedForm PushField(FField field)
//        {
//            fields.Add(field);

//            return this;
//        }

//        public List<FField> Fields()
//        {
//            return fields;
//        }

//        public dynamic Fill(dynamic data)
//        {
//            /* @var Field $field */
//            foreach (var field in Fields()) {
//            field.Fill(data);
//            }

//            return this;
//        }

//        /**
//         * Get the html and script of template.
//         *
//         * @return array
//         */
//        //public function getTemplateHtmlAndScript()
//        //{
//        //$html = '';
//        //$scripts = [];

//        //    /* @var Field $field */
//        //    foreach ($this->fields() as $field) {

//        //    //when field render, will push $script to Admin
//        //    $html.= $field->render();

//        //        /*
//        //         * Get and remove the last script of Admin::$script stack.
//        //         */
//        //        if ($field->getScript()) {
//        //        $scripts[] = array_pop(Admin::$script);
//        //        }
//        //    }

//        //    return [$html, implode("\r\n", $scripts)];
//        //}

//        ///**
//        // * Set `errorKey` `elementName` `elementClass` for fields inside hasmany fields.
//        // *
//        // * @param Field $field
//        // *
//        // * @return Field
//        // */
//        //protected function formatField(Field $field)
//        //{
//        //$column = $field->column();

//        //$elementName = $elementClass = $errorKey = [];

//        //$key = $this->getKey();

//        //    if (is_array($column))
//        //    {
//        //        foreach ($column as $k => $name) {
//        //        $errorKey[$k] = sprintf('%s.%s.%s', $this->relationName, $key, $name);
//        //        $elementName[$k] = sprintf('%s[%s][%s]', $this->relationName, $key, $name);
//        //        $elementClass[$k] = [$this->relationName, $name];
//        //        }
//        //    }
//        //    else
//        //    {
//        //    $errorKey = sprintf('%s.%s.%s', $this->relationName, $key, $column);
//        //    $elementName = sprintf('%s[%s][%s]', $this->relationName, $key, $column);
//        //    $elementClass = [$this->relationName, $column];
//        //    }

//        //    return $field->setErrorKey($errorKey)
//        //        ->setElementName($elementName)
//        //        ->setElementClass($elementClass);
//        //}

//        ///**
//        // * Add nested-form fields dynamically.
//        // *
//        // * @param string $method
//        // * @param array  $arguments
//        // *
//        // * @return mixed
//        // */
//        //public function __call($method, $arguments)
//        //{
//        //    if ($className = Form::findFieldClass($method)) {
//        //    $column = Arr::get($arguments, 0, '');

//        //    /* @var Field $field */
//        //    $field = new $className($column, array_slice($arguments, 1));

//        //        if ($this->form instanceof WidgetForm) {
//        //        $field->setWidgetForm($this->form);
//        //        } else
//        //        {
//        //        $field->setForm($this->form);
//        //        }

//        //    $field = $this->formatField($field);

//        //    $this->pushField($field);

//        //        return $field;
//        //    }

//        //    return $this;
//        //}
//    }
//}
