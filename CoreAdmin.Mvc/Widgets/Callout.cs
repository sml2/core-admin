using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;

namespace CoreAdmin.Mvc.Widgets
{
    class Callout : Widget
    {

        protected string view = "admin::widgets.callout";

        protected string title = "";

        protected string content = "";

        protected string style = "danger";

        public Callout(string content, string title = "", string style = "danger")
        {
            this.content = content;

            this.title = title;

            Style(style);
        }

        public Callout Style(string style = "info")
        {
            this.style = style;

            return this;
        }

        protected Dictionary<string, dynamic> Variables()
        {
            //this.class("callout callout-this.style");
            return new Dictionary<string, dynamic>
            {
                { "title" ,title },
                { "content" ,content },
                //{ "attributes" ,this.formatAttributes() },
            };
        }

        //public function Render()
        //{
        //    return view(this.view, this.variables()).render();
        //}
    }
}
