using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Widgets
{
    class Table : Widget
    {

        protected string View = "admin::widgets.table";

        protected string[] Headers = Array.Empty<string>();

        protected List<List<dynamic>> Rows = new();

        protected string[] Style = Array.Empty<string>();

        public Table(string[] headers = null, string[] rows = null, string[] style = null)
        {
            SetHeaders(headers);
            SetRows(rows);
            SetStyle(style);

            // Class("table " + string.Join("", Style));
        }

        public Table SetHeaders(string[] headers = null)
        {
            Headers = headers;
            return this;
        }

        public Table SetRows(string[] rows = null)
        {
            ////if (Arr::isAssoc($rows))
            //if (rows != null)
            //{
            //    //foreach ($rows as $key => $item) {
            //    for (int i = 0; i < rows.Length; i++) {

            //        this.Rows.Add(new (){ i, rows[i] });
            //    }
            //    return this;
            //}

            //this.Rows = rows;

            return this;
        }

        public Table SetStyle(string[] style)
        {
            Style = style;
            return this;
        }

        public Dictionary<string, dynamic> Render()
        {
            return new Dictionary<string, dynamic> {
               { "headers"    , Headers },
               { "rows"       , Rows },
               { "style"      , Style },
               //{ "attributes" , this.formatAttributes() }
            };
            //return view(this.view, vars)->render();
        }
    }
}
