using CoreAdmin.Lib;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Widgets;
using CoreAdmin.Mvc.Widgets;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace CoreAdmin.Widgets
{
    class Box : Widget
    {
        protected override string view { get => nameof(Box); set { } }
        protected string title;

        protected HtmlContent content = new() { Value = "here is the box content." };

        protected HtmlContent footer;

        protected List<string> tools = new();

        protected string script;

        public Box(string title = "", HtmlContent content = null, HtmlContent footer = null)
        {
            if (title != null) {
                Title(title);
            }

            if (content != null) {
                Content(content);
            }

            if (footer != null) {
                Footer(footer);
            }

            attributes["class"] = "box";
        }

        public Box Content(HtmlContent content)
        {
            this.content = content;

            return this;
        }


        public Box Footer(HtmlContent footer)
        {
                this.footer = footer;
            return this;
        }

        public Box Title(string title)
        {
            this.title = title;

            return this;
        }

        public Box Collapsable()
        {
            tools.Add("<button class=\"btn btn-box-tool\" data-widget=\"collapse\"><i class=\"fa fa-minus\"></i></button>");

            return this;
        }

        public Box Scrollable(object options, string nodeSelector = "" )
        {
            var id = Str.Uniqid("box-slim-scroll-");
            var scrollOptions = JsonSerializer.Serialize(options);
            nodeSelector ??= ".box-body";

            script = $@"
                $(""#{id} {nodeSelector}"").slimScroll({scrollOptions});
                ";
            return this;
        }

        public Box Removable()
        {
            tools.Add("<button class=\"btn btn-box-tool\" data-widget=\"remove\"><i class=\"fa fa-times\"></i></button>");

            return this;
        }

        public Box Style(string style) => Style(new List<string> { style });
        public Box Style(List<string> styles)
        {
            styles = styles.Select((style) => "box-"+style).ToList();
            attributes["class"] = (attributes.ContainsKey("class") ? attributes["class"]: "") +" "+string.Join(" ", styles);

            return this;
        }

        public Box Solid()
        {
            return Style("solid");
        }

        protected ViewBox Variables()
        {
            return new () {
                Title = title,
                Content = content,
                Footer = footer,
                Tools = tools,
                Attributes = FormatAttributes(),
                Script = script,
            };
        }

        public HtmlContent Render()
        {
            return new()
            {
                Name =HtmlContentType.Partial,
                Value = view,
                Form = Variables()
            };
        }
    }
}

