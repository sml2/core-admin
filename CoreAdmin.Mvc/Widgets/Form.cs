﻿using CoreAdmin.Lib;
using CoreAdmin.Mvc.Form;
using CoreAdmin.Mvc.Form.Field;
using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Attributes;
using CoreAdmin.Mvc.Struct.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.Widgets
{
    public class Form : IForm
    {
        private readonly HttpContext _context;
        //use BaseForm\Concerns\HandleCascadeFields;

        public string Title { get; }

        public string Description { get; }

        protected List<FField> fields;

        public BaseModel Data { get; protected set; }

        protected HtmlAttributes attributes;

        protected string[] buttons = new[] { "reset", "submit" };

        protected Dictionary<string, int> width = new()
        {
            { "label", 2 },
            { "field", 8 },
        };

        public bool inbox = true;

        public string confirm = "";

        protected BForm form;
        public Type ModelType { get; set; }

        public Form(HttpContext context, BaseModel model = null)
        {
            _context = context;
            fields = new();

            Fill(model);
            InitFormAttributes();
        }

        public Form Confirm(string message)
        {
            confirm = message;

            return this;
        }

        public Form Fill(BaseModel data)
        {
            if (data == null) return this;
            Data = data;
            return SetType(data.GetType());
        }

        public Form SetType(Type type)
        {
            ModelType = type;
            return this;
        }

        public Form Sanitize()
        {
            //foreach (['_form_', '_token'] as $key) {
            //    request()->request->remove($key);
            //}

            return this;
        }

        protected void InitFormAttributes()
        {
            attributes = new()
            {
                {"id", "widget-form-" + Str.Uniqid()}, {"method", "POST"}, {"action", ""},
                {"accept-charset", "UTF-8"},
                {"pjax-container", true}
            };
            attributes.ClassList.Add("form-horizontal");
        }

        public Form Attribute(Func<HtmlAttributes, HtmlAttributes> func)
        {
            attributes = func.Invoke(attributes);
            return this;
        }

        [Obsolete]
        public string FormatAttribute() => FormatAttribute(attributes);
        [Obsolete]
        public string FormatAttribute(HtmlAttributes attributes)
        {
            if (HasFile())
            {
                attributes["enctype"] = "multipart/form-data";
            }
            ;
            var html = new List<string>();
            foreach (var property in attributes.GetType().GetProperties())
            {
                var value = property.GetValue(attributes);
                if (value == null) continue;
                html.Add($"{property.Name.ToLower()}=\"{property.GetValue(attributes)}\"");
            }

            return string.Join(" ", html);
        }

        public Form Action(string action)
        {
            return Attribute(attr => { attr["action"] = action; return attr; });
        }

        public Form Method(string method = "POST")
        {
            if (method.ToLower() == "put")
            {
                Hidden("_method").Default(method);

                return this;
            }

            return Attribute(attr => { attr["method"] = method.ToUpper(); return attr; });
        }

        public Form DisablePjax()
        {
            return Attribute(attr => { attr["pjax-container"] = false; return attr; });
        }

        public Form DisableReset()
        {
            buttons = buttons.Where(btn => btn != "reset").ToArray();
            return this;
        }

        public Form DisableSubmit()
        {
            buttons = buttons.Where(btn => btn != "submit").ToArray();
            return this;
        }

        public Form SetWidth(int fieldWidth = 8, int labelWidth = 2)
        {
            fields.ForEach(field =>
            {
                field.SetWidth(fieldWidth, labelWidth);
            });

            width = new()
            {
                { "label", labelWidth },
                { "field", fieldWidth },
            };

            return this;
        }

        public bool HasField(Type type)
        {
            return BForm.AvailableFields.Contains(type);
        }

        public Form PushField(FField field)
        {
            field.SetWidgetForm(this);

            fields.Add(field);

            return this;
        }

        public List<FField> Fields()
        {
            return fields;
        }

        protected ViewForm GetVariables()
        {
            Fields().ForEach(field => field.Fill(Data));

            return new()
            {
                Fields = fields,
                Attributes = attributes,
                Method = attributes["method"].ToString(),
                Buttons = buttons.ToList(),
                Width = width,
                Data = Data,
                ModelType = ModelType,
            };
        }

        public bool HasFile()
        {
            foreach (var field in fields)
            {
                if (field is Mvc.Form.Field.File)
                {
                    return true;
                }
            }

            return false;
        }

        public Fieldset Fieldset(string title, Action<Form> setCallback)
        {
            var fieldset = new Fieldset();

            Html(fieldset.Start(title)).Plain();

            setCallback(this);

            Html(fieldset.End()).Plain();

            return fieldset;
        }

        public Form Unbox()
        {
            inbox = false;
            return this;
        }

        protected void AddConfirmScript()
        {
            var id = attributes["id"];

            var trans = new Dictionary<string, string> {
            {"cancel", Admin.Trans("admin.cancel")},
            { "submit", Admin.Trans("admin.submit")},
        };

            var settings = new
            {
                type = "question",
                showCancelButton = true,
                confirmButtonText = trans["submit"],
                cancelButtonText = trans["cancel"],
                title = confirm,
                text = "",
            };

            var settingsStr = JsonSerializer.Serialize(settings).Trim();

            var script = $@"
    
$('form#{id}').off('submit').on('submit', function(e) {{
                e.preventDefault();
                var form = this;
    $.admin.swal({settingsStr}).then(function(result) {{
                    if (result.value == true)
                    {{
                        form.submit();
                    }}
                }});
                return false;
            }});
            ";

            Admin.Script(script);
        }

        protected void AddCascadeScript()
        {
            var id = attributes["id"];

            var script = $@"
; (function() {{
    $('form#{id}').submit(function(e) {{
                    e.preventDefault();
        $(this).find('div.cascade-group.hide :input').attr('disabled', true);
                }});
            }})();
            ";

            Admin.Script(script);
        }

        protected void PrepareForm()
        {
            //if (method_exists($this, 'form'))
            //{
            //$this->form();
            //}

            if (!string.IsNullOrEmpty(confirm))
            {
                AddConfirmScript();
            }

            AddCascadeScript();
        }

        protected void PrepareHandle()
        {
            //if (method_exists($this, 'handle'))
            //{
            //$this->method('POST');
            //$this->action(admin_url('_handle_form_'));
            //$this->hidden('_form_')->default(get_called_class());
            //}
        }

        public HtmlContent Render()
        {
            PrepareForm();

            PrepareHandle();

            var form = new HtmlContent()
            {
                Name = HtmlContentType.Partial,
                Value = "~/Views/Widgets/Form.cshtml",
                Form = GetVariables()
            };

            if (string.IsNullOrEmpty(Title) || !inbox)
            {
                return form;
            }

            //return (new Box($title, $form))->render();
            return null;
        }

        public T Call<T>(params object[] arguments) where T : FField
        {
            Type type = typeof(T);
            if (!HasField(type))
            {
                throw new ArgumentException($"{type.FullName} 不存在");
            }
            if (type != null)
            {
                var column = arguments[0] ?? ""; //[0];
                T element = default;
                if (arguments.Length == 0)
                {
                    element = (T)Activator.CreateInstance(type);
                }
                else if (arguments.Length == 1)
                {
                    element = (T)Activator.CreateInstance(type, column);
                }
                else if (arguments.Length == 2)
                {
                    element = (T)Activator.CreateInstance(type, column, arguments[1]);
                }
                else if (arguments.Length == 3)
                {
                    element = (T)Activator.CreateInstance(type, column, arguments[1], arguments[2]);
                }
                else if (arguments.Length == 4)
                {
                    element = (T)Activator.CreateInstance(type, column, arguments[1], arguments[2], arguments[3]);
                }

                PushField(element);

                return element;
            }

            //admin_error('Error', "Field type [$method] does not exist.");
            return null;
            //return new Form.Field.Nullable();
        }


        public IQueryable<IIdexerModel> Model()
        {
            return null;
        }

        public HttpRequest Request => _context.Request;

        //public override string ToString()
        //{
        //    return Render();
        //}

        public Hidden Hidden(string column) => Call<Hidden>(column);
        public Html Html(string column) => Call<Html>(column);
        public Select Select(string column, string label = "") => Call<Select>(column, label,  ModelType.GetProperty(column)?.PropertyType);
        public Text Text(string column, string label = "") => Call<Text>(column, label);
        public Icon Icon(string column, string label = "") => Call<Icon>(column, label);
        public MultipleSelect MultipleSelect(string column, string label = "") => Call<MultipleSelect>(column, label, ModelType.GetProperty(column)?.PropertyType);

    }
}
