using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;

namespace CoreAdmin.Mvc.Widgets
{
    class InfoBox : Widget
    {
        protected string View = "admin::widgets.info-box";

        protected Dictionary<string, dynamic> Data;

        public InfoBox(string name, string icon, string color, string link, string info)
        {
            Data = new Dictionary<string, dynamic> {
                { "name", name },
                { "icon", icon },
                { "link", link },
                { "info", info },
            };

            //this.class("small-box bg-" + color);
        }

        //public function Render()
        //{
        //    //variables = array_merge($this->data, ['attributes' => $this->formatAttributes()]);

        //    //return view($this->view, $variables)->render();
        //}
    }
}