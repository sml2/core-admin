using System.Collections.Generic;

namespace CoreAdmin.Mvc.Widgets
{
    class Alert : Widget
    {

        protected string View = "admin::widgets.alert";

        protected string Title = "";

        protected string Content = "";

        protected string style = "danger";

        protected string icon = "ban";

        public Alert(string content, string title = "", string style = "danger")
        {
            Content = content;

            //this.Title = title.Length > 0 ? title : Trans("admin.alert");

            Style(style);
        }

        public Alert Style(string style = "info")
        {
            this.style = style;

            return this;
        }

        public Alert Icon(string icon)
        {
            this.icon = icon;

            return this;
        }

        /**
         * @return array
         */
        protected Dictionary<string, dynamic> Variables()
        {
            ////this.class("alert alert-" + this.Style + " alert-dismissable");
            return
                new Dictionary<string, dynamic> {
                { "title", Title },
                { "content", Content },
                { "icon", icon },
                //{ "attributes", this.FormatAttributes() },
            };
        }

        //public function Render()
        //{
        //return view(this.view, this.variables()).render();
        //}
    }
}
