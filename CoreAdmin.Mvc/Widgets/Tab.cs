using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CoreAdmin.Mvc.Widgets
{
    public class Tab : Widget
    {

        public const int TypeContent = 1;
        public const int TypeLink = 2;

        protected string View = "admin::widgets.tab";

        protected Dictionary<string, object> Data =
            new () {
                    { "id"       , "" },
                    { "title"    , "" },
                    { "tabs"     , null },
                    { "dropDown" , null },
                    { "active"   , 0 }
                };

        public Tab()
        {
            //this.class("nav-tabs-custom");
        }

        public Tab Add(string title, string content, bool active = false, string id = null)
        {
            Data["tabs"] = new Dictionary<string, object> {
            { "id" , id != null ? id : GetRandom(9) },
            { "title" , title},
            { "content" , content },
            { "type" , TypeContent }
        };

            if (active)
            {
                Data["active"] = ((Dictionary<string, object>)Data["tabs"]).Count - 1;
            }
            return this;
        }

        public Tab AddLink(string title, string href, bool active = false)
        {
            Data["tabs"] = new Dictionary<string, dynamic> {
                { "id" , GetRandom(9) },
                { "title" , title},
                { "href" , href },
                { "type" , TypeLink }
            };


            if (active)
            {
                Data["active"] = ((Dictionary<string, object>)Data["tabs"]).Count - 1;
            }

            return this;
        }

        //public string Title(string title = "")
        //{
        //this.data["title"] = title;
        //}

        public Tab DropDown(string[] links)
        {
            //if (is_array($links[0]))
            if (links[0].GetType().IsArray)
            {
                //foreach (var link in links) {
                //call_user_func([this, 'dropDown'], link);
                //}
                return this;
            }

            Data["dropDown"] =
            new Dictionary<string, dynamic>
            {
                { "name" , links[0]},
                { "href" , links[1]},
            };
            return this;
        }

        //public function Render()
        //{
        //    data = array_merge(
        //            this.data,
        //            ["attributes" = this.formatAttributes()]
        //        );

        //        this.setupScript();

        //    return view(this.view, data).render();
        //}

        //protected Tab SetupScript()
        //{
        //    script = "SCRIPT"
        //    var hash = document.location.hash;
        //    if (hash !== null)
        //    {
        //        $('.nav-tabs a[href="' + hash + '"]').tab('show');
        //    }

        //// Change hash for page-reload
        //    $('.nav-tabs a').on('shown.bs.tab', function(e) {
        //        history.pushState(null, null, e.target.hash);
        //    });
        //    SCRIPT;
        //    Admin::script($script);
        //return;
        //}

        public static string GetRandom(int length)
        {
            byte[] random = new byte[length / 2];
            // 使用加密服务提供程序 (CSP) 提供的实现来实现加密随机数生成器 (RNG)。无法继承此类
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetNonZeroBytes(random);
            StringBuilder sb = new StringBuilder(length);
            int i;
            for (i = 0; i < random.Length; i++)
            {
                // 以16进制格式输出
                sb.Append(string.Format("{0:X2}", random[i]));
            }
            return sb.ToString();

        }
    }
}
