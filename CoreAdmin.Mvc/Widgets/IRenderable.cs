﻿namespace CoreAdmin.Mvc.Widgets
{
    public interface IRenderable
    {
        public string Render();
    }
}
