using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Widgets
{
    public abstract class Widget
    {
        protected virtual string view { get; set; }
        protected HtmlAttributes attributes = new();

        //abstract Widget Render();

        public Widget View(string view)
        {
            this.view = view;
            return this;
        }

        public HtmlAttributes FormatAttributes()
        {
            return attributes;
        }

        //public function __toString()
        //{
        //return $this->render();
        //}
    }
}

