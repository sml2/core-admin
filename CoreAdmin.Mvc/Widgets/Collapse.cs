using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;

namespace CoreAdmin.Mvc.Widgets
{
    class Collapse : Widget
    {
        protected string View = "admin::widgets.collapse";

        protected Dictionary<string, dynamic> Items;

        public Collapse()
        {
            //this.id("accordion-".uniqid());
            //this.class("box-group");
            //this.style("margin-bottom: 20px");
        }

        public Collapse Add(string title, string content)
        {
            Items.Add("title", title);
            Items.Add("content", content);
            return this;
        }

        protected Dictionary<string, dynamic> Variables()
        {
            return new Dictionary<string, dynamic> {
                //{ "id" ,this.id},
                { "items" ,Items},
                //{ "attributes" ,this.formatAttributes()},
            };
        }

        //public function Render()
        //{
        //    return view(this.view, this.variables()).render();
        //}
    }
}
