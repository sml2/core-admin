using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;

namespace CoreAdmin.Mvc.Widgets
{
    class MultipleSteps
    {

        protected string Current;

        protected Dictionary<string, dynamic> Steps;

        protected string StepName = "step";

        public MultipleSteps(Dictionary<string, dynamic> steps, string current = null)
        {
            Steps = steps;

            Current = ResolveCurrentStep(steps, current);
        }

        //public static MultipleSteps Make(Dictionary<string, dynamic>  steps, string current = null)
        //{
        //    return new static (steps, current);
        //}

        protected string ResolveCurrentStep(Dictionary<string, dynamic> steps, string current)
        {
            //current = current ?: request(this.stepName, 0);
            current = "";

            //if (!isset($steps[$current]))
            if (steps.ContainsKey("current"))
            {
                current = steps[current];
            }

            return current;
        }

        //public function Render()
        //{
        //    class = this.steps[this.current];

        //    if (!is_subclass_of(class, StepForm::class)) {
        //        admin_error("Class [{$class}] must be a sub-class of [Encore\Admin\Widgets\StepForm].");

        //        return;
        //    }

        //    step = new class();

        //    return step
        //            .setSteps(array_keys(this.steps))
        //            .setCurrent(this.current)
        //            .render();
        //}
    }
}

