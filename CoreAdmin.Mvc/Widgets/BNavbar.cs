﻿using CoreAdmin.Mvc.Widgets.Navbar;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Widgets
{
    public class BNavbar : IRenderable
    {
        protected Dictionary<string, List<IRenderable>> Elements = new() { };

        /**
         * Navbar constructor.
         */
        public BNavbar()
        {
            Elements = new()
            {
                { "left", new() },
                { "right", new() },
            };
        }

        public BNavbar Left(IRenderable element)
        {
            Elements["left"].Add(element);
            return this;
        }

        public BNavbar Right(IRenderable element)
        {
            Elements["right"].Add(element);
            return this;
        }

        public BNavbar Add(IRenderable element) => Right(element);

        /**
         * @param string $part
         *
         * @return mixed
         */
        public string Render() => Render("right");
        public string Render(string part)
        {
            if ("right" == part)
            {
                Right(new RefreshButton());
            }

            if (!Elements.ContainsKey(part) || Elements[part].Count == 0)
            {
                return "";
            }
            var rs = Elements[part].Select(element =>
            {
                //if ($element instanceof Htmlable) {
                //    return $element->toHtml();
                //}

                if (element is IRenderable)
                {
                    return element.Render();
                }
                // TODO: 转string
                return "";
                //return (string)element;
            });
            return string.Join("", rs);
        }
    }
}
