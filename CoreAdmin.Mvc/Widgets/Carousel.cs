using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;

namespace CoreAdmin.Mvc.Widgets
{
    class Carousel : Widget
    {

        protected string view = "admin::widgets.carousel";

        protected string items;

        protected string title = "Carousel";


        //public Carousel(string items[] = { })
        //{
        //    this.items = items;//不知什么类型

        //this.id(carousel-'.uniqid());
        //this.class("carousel slide");
        //this.offsetSet("data-ride", "carousel");
        //}

        public Carousel Title(string title)
        {
            this.title = title;
            return this;
        }

        public Dictionary<string, dynamic> Render()
        {
            return new Dictionary<string, dynamic> {
                { "items" , items },
                { "title" , title },
                //{ "attributes" , this.formatAttributes() },
                //{ "id" , this.id },
                //{ "width" , this.width??300 },
                //{ "height" , this.height??200 },
            };
            //return view(this.view, variables).render();
        }
    }
}
