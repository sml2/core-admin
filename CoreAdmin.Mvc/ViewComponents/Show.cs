﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreAdmin.Mvc.ViewComponents
{
    public class Show : ViewComponent
    {
        public IViewComponentResult Invoke(HtmlContent value)
        {
            var data = value.Data;
            ViewData["panel"] = data["panel"];
            ViewData["relations"] = data["relations"];

            return View("~/Views/Show.cshtml", data);
        }
    }
}
