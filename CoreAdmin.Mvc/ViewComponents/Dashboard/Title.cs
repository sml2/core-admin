﻿using Microsoft.AspNetCore.Mvc;

namespace CoreAdmin.Mvc.ViewComponents.Dashboard
{
    /// <summary>
    /// 默认dashboard视图
    /// </summary>
    public class Title : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Views/Dashboard/Title.cshtml");
        }
    }
}
