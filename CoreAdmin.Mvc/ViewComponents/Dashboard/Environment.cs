﻿using CoreAdmin.Mvc.Struct;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using CoreAdmin.Mvc.Configure;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.ViewComponents.Dashboard
{
    /// <summary>
    /// 运行环境组件
    /// </summary>
    public class Environment : ViewComponent
    {
        private readonly IRequestCultureFeature _culture;
        private readonly IOptions<CoreAdminConfigure> _configure;
        private readonly IWebHostEnvironment _env;

        public Environment(IOptions<CoreAdminConfigure> configure, IWebHostEnvironment env, IHttpContextAccessor context)
        {
            _culture = context.HttpContext?.Features.Get<IRequestCultureFeature>();
            _configure = configure;
            _env = env;
        }

        public IViewComponentResult Invoke()
        {
            if (_env is null)
            {
                throw new ArgumentNullException(nameof(_env));
            }
            ViewData["envs"] = new Dictionary<string, string>()
            {
                {"NetCoreVersion", Framework.NetCoreVersion},
                {"CoreAdmin version", _configure.Value.Version},
                {"CGI", "Kerstrel"},
                {"AppName", _env.ApplicationName},
                {"Server", Framework.OsPlatform},
                {"Cache driver", nameof(Microsoft.Extensions.Caching.Memory)},
                {"Session driver", nameof(Microsoft.AspNetCore.Session)},
                {"Queue driver", nameof(System.Threading.Tasks.Task)},
                {"Timezone", TimeZoneInfo.Local.DisplayName},
                {"Locale", _culture?.RequestCulture.Culture.DisplayName},
                {"Env", _env.EnvironmentName},
                {"URL", _configure.Value.Route.Prefix},
            };
            return View("~/Views/Dashboard/Environment.cshtml");
        }
    }
}