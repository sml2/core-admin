﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.ViewComponents.Dashboard
{
    /// <summary>
    /// 依赖卡片组件，展示依赖项
    /// </summary>
    public class Dependencies : ViewComponent
    {
        private static readonly Dictionary<string, string> DependenciesVariables = new() {
            { "PHPVersion", "PHP/" },
            { "Laravel version", "111"},
            {"CGI", "222"},
            {"Uname", "3333"},
            {"Server", "33333"},

            {"Cache driver", "Cache driver"},
            {"Session driver", "Session driver"},
            {"Queue driver", "Queue driver"},

            {"Timezone", "Timezone"},
            {"Locale", "Locale"},
            {"Env","Env"},
            {"URL","URL"}
        };
        public IViewComponentResult Invoke()
        {
            ViewData["dependencies"] = DependenciesVariables;
            return View("~/Views/Dashboard/Dependencies.cshtml");
        }
    }
}
