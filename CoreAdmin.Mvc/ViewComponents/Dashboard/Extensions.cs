﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.ViewComponents.Dashboard
{
    /// <summary>
    /// 扩展信息组件
    /// </summary>
    public class Extensions : ViewComponent
    {
        private static readonly List<Dictionary<string, string>> ExtensionsVariables = new() {
            new(){
                { "name" , "laravel-admin-ext/helpers" },
                { "link" , "https://github.com/laravel-admin-extensions/helpers" },
                { "icon" , "gears" },
            },
            new(){
                { "name" , "laravel-admin-ext/log-viewer"},
                { "link" , "https://github.com/laravel-admin-extensions/log-viewer"},
                { "icon" , "database"},
            },
            new(){
                { "name" , "laravel-admin-ext/backup"},
                { "link" , "https://github.com/laravel-admin-extensions/backup"},
                { "icon" , "copy"},
            }
        };

        public IViewComponentResult Invoke()
        {
            ViewData["extensions"] = ExtensionsVariables;
            return View("~/Views/Dashboard/Extensions.cshtml");
        }
    }
}
