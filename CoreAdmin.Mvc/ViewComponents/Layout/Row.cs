﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using LRow = CoreAdmin.Mvc.Layout.Row;

namespace CoreAdmin.Mvc.ViewComponents.Layout
{
    public class Row : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(LRow row)
        {
            //var items = await GetItemsAsync(maxPriority, isDone);
            return await Task.Run(() => View("~/Views/Layout/Row.cshtml", row));
        }
    }
}
