﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using LColumn = CoreAdmin.Mvc.Layout.Column;

namespace CoreAdmin.Mvc.ViewComponents.Layout
{
    public class Column : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(LColumn column)
        {
            return await Task.Run(() => View("~/Views/Layout/Column.cshtml", column));
        }
    }
}
