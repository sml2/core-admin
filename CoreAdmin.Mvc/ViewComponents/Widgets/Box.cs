﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Struct.Widgets;

namespace CoreAdmin.Mvc.ViewComponents.Widgets
{
    public class Box : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(HtmlContent value)
        {
            return await Task.Run(() => View("~/Views/Widgets/Box.cshtml", (ViewBox)value.Form));
        }
    }
}
