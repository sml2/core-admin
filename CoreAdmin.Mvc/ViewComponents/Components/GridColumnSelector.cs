﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using LColumn = CoreAdmin.Mvc.Layout.Column;

namespace CoreAdmin.Mvc.ViewComponents.Components
{
    public class GridColumnSelector : ViewComponent
    {
        //private readonly ToDoContext db;

        public GridColumnSelector()
        {
            //db = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(HtmlContent value)
        {
            ViewData["columns"] = value.Data["columns"];
            ViewData["visible"] = value.Data["visible"];
            ViewData["defaults"] = value.Data["defaults"];
            //var items = await GetItemsAsync(maxPriority, isDone);
            return await Task.Run(() => View("~/Views/Components/GridColumnSelector.cshtml", value));
        }
    }
}
