﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreAdmin.Mvc.ViewComponents
{
    public class Form : ViewComponent
    {
        public IViewComponentResult Invoke(HtmlContent value)
        {
            foreach (var (key, o) in value.Data)
            {
                ViewData[key] = o;
            }
            return View("~/Views/Form.cshtml");
        }
    }
}
