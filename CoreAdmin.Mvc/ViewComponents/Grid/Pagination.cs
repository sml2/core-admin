﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreAdmin.Mvc.ViewComponents.Grid
{
    public class Pagination : ViewComponent
    {
        public IViewComponentResult Invoke(HtmlContent value)
        {
            var paginator = value.Data["paginator"];
            return View("~/Views/Pagination.cshtml", paginator);
        }
    }
}
