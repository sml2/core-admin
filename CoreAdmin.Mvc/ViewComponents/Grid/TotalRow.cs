﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreAdmin.Mvc.ViewComponents.Grid
{
    public class TotalRow : ViewComponent
    {
        public IViewComponentResult Invoke(HtmlContent value)
        {
            ViewData["columns"] = value.Data["columns"];
            return View("~/Views/Grid/TotalRow.cshtml");
        }
    }
}
