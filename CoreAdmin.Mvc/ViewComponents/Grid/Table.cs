﻿using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using LRow = CoreAdmin.Mvc.Layout.Row;

namespace CoreAdmin.Mvc.ViewComponents.Grid
{
    public class Table : ViewComponent
    {
        public IViewComponentResult Invoke(HtmlContent value)
        {
            var grid = value.Data["grid"];
            return View("~/Views/Grid/Table.cshtml", grid);
        }
    }
}
