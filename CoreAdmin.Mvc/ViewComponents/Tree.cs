﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.ViewComponents
{
    public class Tree : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(HtmlContent value)
        {
            var data = (ViewTree<MenuModel>)value.Form;
            ViewData["keyName"] = data.KeyName;
            ViewData["path"] = data.Path;
            ViewData["branchView"] = data.BranchView;
            ViewData["branchCallback"] = data.BranchCallback;

            return await Task.Run(() => View("~/Views/Tree.cshtml", data));
        }
    }
}
