﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc
{
    public partial class Admin
    {
        public List<string> ScriptList = new();

        public List<string> DeferredScriptList = new();

        public List<string> StyleList = new();


        public static List<string> CssList { get; set; } = new();

        public static List<string> JsList = new();

        public static List<string> HtmlList = new();

        public static List<string> HeaderJsList = new();

        public static string Manifest = "/assets/minify-manifest.json";

        public static Dictionary<string, List<string>> ManifestData = new();

        public static Dictionary<string, string> MinList = new()
        {
            { "js", "/assets/laravel-admin.min.js" },
            { "css", "/assets/laravel-admin.min.css" },
        };

        public static List<string> BaseCssList = new()
        {
            "/assets/AdminLTE/bootstrap/css/bootstrap.min.css",
            "/assets/font-awesome/css/font-awesome.min.css",
            "/assets/laravel-admin/laravel-admin.css",
            "/assets/nprogress/nprogress.css",
            "/assets/sweetalert2/dist/sweetalert2.css",
            "/assets/nestable/nestable.css",
            "/assets/toastr/build/toastr.min.css",
            "/assets/bootstrap3-editable/css/bootstrap-editable.css",
            "/assets/google-fonts/fonts.css",
            "/assets/AdminLTE/dist/css/AdminLTE.min.css",
        };

        private static List<string> BaseJsList = new()
        {
            "/assets/AdminLTE/bootstrap/js/bootstrap.min.js",
            "/assets/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js",
            "/assets/AdminLTE/dist/js/app.min.js",
            "/assets/jquery/jquery-pjax/jquery.pjax.js",
            "/assets/nprogress/nprogress.js",
            "/assets/nestable/jquery.nestable.js",
            "/assets/toastr/build/toastr.min.js",
            "/assets/bootstrap3-editable/js/bootstrap-editable.min.js",
            "/assets/sweetalert2/dist/sweetalert2.min.js",
            "/assets/laravel-admin/laravel-admin.js",
        };

        // public static string jQuery = "/assets/AdminLTE/plugins/jQuery/jquery-3.5.1.min.js";
        public static string jQuery = "/assets/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js";
        public static List<string> MinifyIgnores = new();

        public static List<string> Css() {
            return CssList;
        }

        /// <summary>
        /// css列表，只应该调用一次
        /// </summary>
        /// <param name="css"></param>
        /// <param name="minify"></param>
        public static void Css(List<string> css, bool minify = true)
        {
            IgnoreMinify(css, minify);
            CssList = CssList.Union(css).Union(BaseCss()).ToList();

                // css = GetMinifiedCss();
        }

        public static List<string> BaseCss() => BaseCss(new List<string>());

        public static List<string> BaseCss(List<string> css, bool minify = true)
        {
            IgnoreMinify(css, minify);

            if (css.Count > 0)
            {
                return BaseCssList = css;
            }

            //$skin = config('admin.skin', 'skin-blue-light');
            var skin = "skin-blue-light";
            BaseCssList.Insert(0, $"/assets/AdminLTE/dist/css/skins/{skin}.min.css");

            return BaseCssList;
        }

        public static List<string> Js() => JsList;
        public static List<string> Js(List<string> js, bool minify = true)
        {
            IgnoreMinify(js, minify);

            if (JsList.Count == 0)
            {
                JsList = BaseJs().ToList();
            }

            if (js.Count > 0)
            {
                JsList = JsList.Union(js).ToList();
            }

            js = GetMinifiedJs();

            return JsList;
        }

        public static List<string> HeaderJs() => HeaderJsList;
        public static List<string> HeaderJs(List<string> js)
        {
            if (js.Count > 0)
            {
                HeaderJsList = HeaderJsList.Union(js).ToList();
            }

            return HeaderJsList;
        }

        public static List<string> BaseJs() => BaseJs(new List<string>());
        public static List<string> BaseJs(List<string> js, bool minify = true)
        {
            IgnoreMinify(js, minify);

            if (js.Count > 0)
            {
                BaseJsList = js;
            }

            return BaseJsList;
        }
        
        public static void IgnoreMinify(List<string> assets, bool ignore = true) => assets.ForEach(a => IgnoreMinify(a, ignore));
        
        public static void IgnoreMinify(string assets, bool ignore = true)
        {
            if (!ignore)
            {
                MinifyIgnores.Add(assets);
            }
        }

       /// <summary>
       /// 获取数据
       /// </summary>
        public List<string> Script()
        {
            //.Select(function($line) {
            //return $line;
            //    //@see https://stackoverflow.com/questions/19509863/how-to-remove-js-comments-using-php
            //    $pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
            //    $line = preg_replace($pattern, '', $line);

            //    return preg_replace('/\s+/', ' ', $line);
            //});
            
            // 没有去重，应该也没必要
            return Instance.ScriptList.Concat(Instance.DeferredScriptList).ToList();
        }

        /// <summary>
       /// 判断添加数据
       /// </summary>
        public void NewScript(string script, bool deferred = false)
        {
            if (string.IsNullOrEmpty(script)) return;
            
            if (deferred)
            {
                if (!DeferredScriptList.Contains(script)) Instance.DeferredScriptList.Add(script);
            }
            else
            {
                if (!ScriptList.Contains(script)) Instance.ScriptList.Add(script);
            }

        }
        
       /// <summary>
       /// 判断是否添加
       /// </summary>
        public static void Script(string script, bool deferred = false)
        {

            if (!string.IsNullOrEmpty(script))
            {
                if (deferred)
                {
                    if (!Instance.DeferredScriptList.Contains(script)) Instance.DeferredScriptList.Add(script);
                }
                else
                {
                    if (!Instance.ScriptList.Contains(script)) Instance.ScriptList.Add(script);
                }
            }

        }
        public static List<string> Style() => Instance.StyleList;

        /// <summary>
       /// 判断是否要添加数据
       /// </summary>
        public static List<string> Style(string style)
        {
            if (!string.IsNullOrEmpty(style) && !Instance.StyleList.Contains(style))
            {
                Instance.StyleList.Add(style);
            }
            return Instance.StyleList;
        }

       /// <summary>
       /// 判断获取数据显示的方法
       /// </summary>
        public static List<string> Style(List<string> style)
        {
            if (style?.Count > 0)
            {
                Instance.StyleList = Instance.StyleList.Union(style).ToList();
            }

            style = Instance.StyleList;

            return style;
        }

        public static List<string> Html() => HtmlList;

        /// <summary>
       /// 判断添加
       /// </summary>
        public static List<string> Html(string html)
        {
            if (string.IsNullOrEmpty(html) && !HtmlList.Contains(html))
            {
                HtmlList.Add(html);
            }

            return HtmlList;
        }

        
        protected static List<string> GetManifestData(string key)
        {
            if (ManifestData.Count > 0)
            {
                return ManifestData[key];
            }

            //ManifestData = json_decode(
            //    file_get_contents(public_path(Manifest)),
            //    true
            //    );

            return ManifestData[key];
        }

        protected static List<string> GetMinifiedCss()
        {
            //if (!config('admin.minify_assets') || !file_exists(public_path(static::$manifest))) {
            if (true)
            {
                return new();
            }

            return GetManifestData("css");
        }

        protected static List<string> GetMinifiedJs()
        {
            //if (!config('admin.minify_assets') || !file_exists(public_path(static::$manifest))) {
            if (true)
            {
                return new();
            }

            return GetManifestData("js");
        }

        public static string JQuery() => Assets(jQuery);

        public static HtmlContent Component(string component, Dictionary<string, dynamic> data, ViewFormField field = null)
        {
            return new()
            {
                Name = HtmlContentType.Partial,
                Value = component,
                Data = data,
                Form = field,
            };
        }

    }
}
