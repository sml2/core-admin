﻿using CoreAdmin.Lib;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Grid.Displayers;
using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace CoreAdmin.Mvc
{
    /// <summary>
    /// 显示时候需要的数据类型和方法页面数据
    /// </summary>
    public partial class BGrid
    {
        protected IQueryable<IIdexerModel> model;
        public Type ModelType { get; private set; }

        protected List<GColumn> columns;

        protected List<Row> rows;

        protected Action<Row> rowsCallback;

        public List<string> columnNames;

        protected Action<BGrid> builder;

        protected bool builded = false;

        protected Dictionary<string, dynamic> variables;

        protected string resourcePath;

        protected string keyName = "id";

        protected string view = nameof(ViewComponents.Grid.Table);

        public int[] perPages { set; get; } = { 10, 20, 30, 50, 100 };

        public int perPage { get; set; } = 20;

        protected List<Action<BGrid>> renderingCallbacks;

        public ListPaginator<IIdexerModel> DataPaginator;

        public Dictionary<string, bool> options = new()
        {
            { "show_pagination", true },
            { "show_tools", true },
            { "show_filter", true },
            { "show_exporter", true },
            { "show_actions", true },
            { "show_row_selector", true },
            { "show_create_btn", true },
            { "show_truncate_btn", false },
            { "show_column_selector", true },
            { "show_define_empty_page", true },
            { "show_perpage_selector", true },
        };
        public const string PerPageKeyName = "pageSize";
        public string tableID;
        // TODO: 依赖分离
        public HttpContext Context { get; init; }
        protected static List<Action<BGrid>> InitCallbacks { get; set; }
        public Dictionary<string, dynamic> Variables
        {
            get
            {
                if (variables == null) variables = new();
                variables["grid"] = this;

                return variables;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        public BGrid(IQueryable<IIdexerModel> model, HttpContext context)
        {
            this.model = model;
            Context = context;
            ModelType = model.GetType().GetGenericArguments()[0];
            keyName = model.GetKeyName();
            //columnNames = new();
            Initialize();

            CallInitCallbacks();
        }
        /// <summary>
        /// 
        /// </summary>
        protected void Initialize()
        {
            tableID = Str.Uniqid("grid-table");
            columns = new List<GColumn>();
            rows = new();
            var reqSize = Context.Request.Get(PerPageKeyName);
            if (!string.IsNullOrEmpty(reqSize) && int.TryParse(reqSize,out var v) && perPages.Contains(v)) { 
                //string.Empty null
                perPage = v;
            }


            InitTools().InitFilter();
        }
        public static void Init(Action<BGrid> callback = null)
        {
            InitCallbacks.Add(callback);
        }

        protected void CallInitCallbacks()
        {
            if (InitCallbacks == null || InitCallbacks.Count == 0)
            {
                return;
            }

            foreach (var callback in InitCallbacks.Where(cb => cb != null))
            {
                callback.Invoke(this);
            }
        }

        public bool Option(string key) => options[key];

        public BGrid Option(string key, bool value)
        {
            options[key] = value;
            return this;
        }

        public string GetKeyName()
        {
            return keyName ?? "id";
        }

        public GColumn Column(string name) {
            var lable = ModelType.GetDisplayName(name);
            return Column(name, lable); 
        }
        public virtual GColumn Column(string name, string label)
        {
            if (name.Contains("."))
            {
                return AddRelationColumn(name, label);
            }

            if (name.Contains("->"))
            {
                throw new NotImplementedException("Not implements yet");
                //return AddJsonColumn(name, label);
            }

            return Call(name, label);
        }

        public List<GColumn> Columns() => columns;

        public void Columns(Dictionary<string, string> columns)
        {
            foreach (var pair in columns)
            {
                Column(pair.Key, pair.Value);
            }
        }
        public void Columns(params string[] columns)
        {
            foreach (var column in columns)
            {
                Column(column);
            }
        }

        /// <summary>
        /// 传值到到数据类型里
        /// </summary>
        /// <param name="column"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        protected virtual GColumn AddColumn(string column = "", string label = "")
        {
            var columnCls = new GColumn(column, label);
            columnCls.SetGrid(this);
            columns.Add(columnCls);
            return columnCls;
        }

        public List<GColumn> GetColumns()
        {
            return columns;
        }

        protected GColumn AddRelationColumn(string name, string label = "")
        {
            var nameList = name.Split(".");
            var relation = nameList[0];
            var column = nameList[1];

            var model = Model().GetType().GetGenericArguments()[0];
            var relationColumn = model.GetProperty(relation);
            //model.name
            if (relationColumn == null || (relationColumn.PropertyType.IsAssignableTo(typeof(BaseModel))))
            {

                //Admin.Error($"Call to undefined relationship [{relation}] on model [{model.FullName}].");
                return null;
            }
            name = (ShouldSnakeAttributes() ? relation.Snake() : relation) + "." + column;


            this.model = this.model.Include(relation);

            return AddColumn(name, label).SetRelation(relation, column);
        }

        protected GColumn AddJsonColumn(string name, string label = "")
        {
            var column = name[(name.IndexOf("->") + 2)..];

            name = name.Replace("->", ".");

            return AddColumn(name, label ?? column.First().ToString().ToUpper() + column[1..]);
        }

        public GColumn PrependColumn(string column = "", string label = "")
        {
            var columnCls = new GColumn(column, label);
            columnCls.SetGrid(this);
            columns.Insert(0, columnCls);
            return columnCls;
        }

        public IQueryable<IIdexerModel> Model()
        {
            return model;
        }
        /// <summary>
        /// 指定每页数据条数
        /// </summary>
        /// <param name="perPage"></param>
        /// <returns></returns>
        public BGrid Paginate(int perPage)
        {
            if (perPage < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(perPage));
            }
            if (perPages.Contains(perPage))
            {
                this.perPage = perPage;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(perPage));
            }
            return this;
        }

        public Paginator Paginator()
        {
            return new Paginator(this, options["show_perpage_selector"]);
        }

        //    /**
        //     * Disable grid pagination.
        //     *
        //     * @return $this
        //     */
        //    public function disablePagination(bool $disable = true)
        //    {
        //        model->usePaginate(!$disable);

        //        return $this->option('show_pagination', !$disable);
        //    }

        public bool ShowPagination()
        {
            return Option("show_pagination");
        }

        public void PerPages(int[] perPages)
        {
            this.perPages = perPages;
        }

        public BGrid DisablePerPageSelector(bool disable = true)
        {
            return Option("show_perpage_selector", !disable);
        }

        public BGrid DisableRowSelector(bool disable = true)
        {
            return DisableBatchActions(disable);
        }

        protected void PrependRowSelectorColumn()
        {
            if (!Option("show_row_selector"))
            {
                return;
            }

            var checkAllBox = $"<input type=\"checkbox\" class=\"{GetSelectAllName()}\" />&nbsp;";

            PrependColumn(GColumn.SELECT_COLUMN_NAME, " ")
                .DisplayUsing(typeof(RowSelector))
                    .AddHeader(checkAllBox);
        }

        /**
         * Apply column filter to grid query.
         *
         * @return void
         */
        protected static void ApplyColumnFilter()
        {
            //foreach (var gColumn in columns)
            //{
            //    // gColumn.BindFilterQuery(model);
            //}
        }

        //    /**
        //     * Apply column search to grid query.
        //     *
        //     * @return void
        //     */
        //    protected function applyColumnSearch()
        //    {
        //        $this->columns->each->bindSearchQuery($this->model());
        //    }

        /**
         * @return array|Collection|mixed
         */
        public void ApplyQuery()
        {
            ApplyQuickSearch();

            ApplyColumnFilter();

            //$this->applyColumnSearch();

            //$this->applySelectorQuery();
        }

        protected void AddDefaultColumns()
        {
            PrependRowSelectorColumn();

            AppendActionsColumn();
        }

        public async Task Build()
        {
            if (builded)
            {
                return;
            }

            ApplyQuery();

            var collection = DataPaginator = await ApplyFilter(false);

            AddDefaultColumns();

            

            var data = collection.ToList();
            if (columnNames == null) columnNames = new();
            var uiData = new List<Dictionary<string, object>>(new Dictionary<string, object>[data.Count]);
            columns.ForEach(column =>
            {
                column.SetOriginalGridModels(collection);
                column.Fill(data, ref uiData);
                columnNames.Add(column.GetName());

            });


            BuildRows(data, collection, uiData);

            builded = true;
        }

        protected void BuildRows(IEnumerable<IIdexerModel> data, List<IIdexerModel> collection, List<Dictionary<string, object>> uiData)
        {
            var _ = collection;
            rows = data.Select((model, number) => new Row(number, model, model.GetKey().ToString()) {Data = uiData[number]}).ToList();

            if (rowsCallback != null)
            {
                rows.ForEach(rowsCallback);
            }
        }

        public List<Row> Rows() => rows;
        public void Rows(Action<Row> callable)
        {
            rowsCallback = callable;
        }

        public string GetCreateUrl()
        {
            var queryString = "";
            
            return $"{Resource()}/create{(!string.IsNullOrEmpty(queryString) ? "?" + queryString : "")}";
        }
        
        public BGrid DisableCreateButton(bool disable = true)
        {
            return Option("show_create_btn", !disable);
        }
        public BGrid DisableDefineEmptyPage(bool disable = true)
        {
            return Option("show_define_empty_page", !disable);
        }
        public bool ShowDefineEmptyPage()
        {
            return Option("show_define_empty_page");
        }
        
        /// <summary>
        /// 展示新增按钮
        /// </summary>
        /// <returns></returns>
        public bool ShowCreateBtn()
        {
            return Option("show_create_btn");
        }

        /// <summary>
        /// 渲染新增按钮
        /// </summary>
        /// <returns></returns>
        public string RenderCreateButton()
        {
            return new CreateButton(this).Render();
        }

        /// <summary>
        /// 判断清空后面链接的空格 /
        /// </summary>
        /// <returns></returns>
        public string Resource() => (resourcePath ?? Context.Request.Path).TrimEnd('/');

        /// <summary>
        /// 判断链接是否为空 赋值并返回
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public BGrid Resource(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                resourcePath = path;
            }
            return this;
        }

        //    /**
        //     * Handle get mutator column for grid.
        //     *
        //     * @param string $method
        //     * @param string $label
        //     *
        //     * @return bool|Column
        //     */
        //    protected function handleGetMutatorColumn($method, $label)
        //    {
        //        if ($this->model()->eloquent()->hasGetMutator($method)) {
        //            return $this->addColumn($method, $label);
        //        }

        //        return false;
        //    }

        //    /**
        //     * Handle relation column for grid.
        //     *
        //     * @param string $method
        //     * @param string $label
        //     *
        //     * @return bool|Column
        //     */
        //    protected function handleRelationColumn($method, $label)
        //    {
        //        $model = $this->model()->eloquent();

        //        if (!method_exists($model, $method)) {
        //            return false;
        //        }

        //        if (!($relation = $model->$method()) instanceof Relations\Relation) {
        //            return false;
        //        }

        //        if ($relation instanceof Relations\HasOne ||
        //            $relation instanceof Relations\BelongsTo ||
        //            $relation instanceof Relations\MorphOne
        //        ) {
        //            $this->model()->with($method);

        //            return $this->addColumn($method, $label)->setRelation(
        //                $this->shouldSnakeAttributes() ? Str::snake($method) : $method
        //            );
        //        }

        //        if ($relation instanceof Relations\HasMany
        //            || $relation instanceof Relations\BelongsToMany
        //            || $relation instanceof Relations\MorphToMany
        //            || $relation instanceof Relations\HasManyThrough
        //        ) {
        //            $this->model()->with($method);

        //            return $this->addColumn($this->shouldSnakeAttributes() ? Str::snake($method) : $method, $label);
        //        }

        //        return false;
        //    }

        /// <summary>
        /// 判断是否有值调用AddColumn方法
        /// </summary>
        /// <param name="method"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public GColumn Call(string method, params object[] arguments)
        {
            //if (HasMacro($method)) {
            //    return $this->macroCall($method, $arguments);
            //}

            var label = (string)arguments[0] ?? null;

            //if ($this->model()->eloquent() instanceof MongodbModel) {
            //    return $this->addColumn($method, $label);
            //}

            //if ($column = $this->handleGetMutatorColumn($method, $label)) {
            //    return $column;
            //}

            //if ($column = $this->handleRelationColumn($method, $label)) {
            //    return $column;
            //}

            return AddColumn(method, label);
        }


        /// <summary>
        /// 返回View
        /// </summary>
        /// <param name="variables"></param>
        /// <returns></returns>
        public BGrid With(Dictionary<string, dynamic> variables)
        {
            this.variables = variables;

            return this;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <param name="variables"></param>
        public void SetView(string view, Dictionary<string, dynamic> variables)
        {
            With(variables);
            this.view = view;
        }
        /// <summary>
        /// 判断设置Title
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public BGrid SetTitle(string title)
        {
            if (null != variables)
            {
                variables[nameof(title)] = title;
            }
            else
            {
                With(new() { { nameof(title), title } });
            }

            return this;
        }

        //    public function setRelation(Relations\Relation $relation)
        //    {
        //        $this->model()->setRelation($relation);

        //        return $this;
        //    }

        public BGrid SetResource(string path)
        {
            resourcePath = path;

            return this;
        }

        public BGrid Rendering(Action<BGrid> callback)
        {
            if (renderingCallbacks == null)
            {
                renderingCallbacks = new();
            }
            renderingCallbacks.Add(callback);

            return this;
        }

        protected void CallRenderingCallback()
        {
            if (renderingCallbacks == null) return;
            foreach (var callback in renderingCallbacks)
            {
                callback.Invoke(this);
            }
        }

        public async Task<HtmlContent> Render()
        {
            HandleExportRequest(true);

            //try
            //{
                await Build();
            //}
            //catch (Exception e)
            //{
            //    Debug.Assert(false);
            //    //return Handler::renderException($e);
            //}

            CallRenderingCallback();

            return Admin.Component(view, Variables);

            //return view($this->view, $this->variables());
        }
    }
}
