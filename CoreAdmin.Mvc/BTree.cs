﻿using CoreAdmin.Lib;
using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct;
using CoreAdmin.Mvc.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CoreAdmin.Extensions;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc
{
    public class BTree<T> where T : ModelTree<T>
    {
        protected List<T> items;

        protected string elementId = "tree-";

        protected IQueryable<T> model;
        private readonly HttpContext _context;

        protected Action queryCallback;

        protected Dictionary<string, string> view = new()
        {
            { "tree", nameof(ViewComponents.Tree) },
            { "branch", "~/Views/Tree/Branch.cshtml" },
        };

        protected Action callback;

        protected Func<T, string> branchCallback = null;

        public bool useCreate = true;

        public bool useSave = true;

        public bool useRefresh = true;

        protected Dictionary<string, dynamic> nestableOptions = new();

        public Tools<T> tools;

        public BTree(IQueryable<T> model, HttpContext context, Action<BTree<T>> callback = null)
        {
            this.model = model;
            _context = context;

            elementId += Str.Uniqid();
            SetupTools();
            callback?.Invoke(this);

            InitBranchCallback();
        }

        public void SetupTools()
        {
            tools = new Tools<T>(this);
        }

        protected void InitBranchCallback()
        {
            if (branchCallback == null)
            {
                var type = model.GetType().GetGenericArguments()[0];
                var keyProperty = type.GetProperty(model.GetKeyName());
                var titleProperty = type.GetProperty(ModelTree<T>.TitleColumn);

                branchCallback = (branch) =>
                {
                    var key = keyProperty.GetValue(branch);
                    var title = titleProperty.GetValue(branch);

                    return $"{key} - {title}";
                };
            }
        }

        public BTree<T> Branch(Func<T, string> branchCallback)
        {
            this.branchCallback = branchCallback;

            return this;
        }

        public BTree<T> Query(Action callback)
        {
            queryCallback = callback;

            return this;
        }

        public BTree<T> Nestable(Dictionary<string, dynamic> options)
        {
            nestableOptions = nestableOptions.Merge(options);

            return this;
        }

        public void DisableCreate()
        {
            useCreate = false;
        }

        public void DisableSave()
        {
            useSave = false;
        }

        public void DisableRefresh()
        {
            useRefresh = false;
        }

        public bool SaveOrder(string serialize)
        {
            var tree = JsonSerializer.Deserialize(Encoding.UTF8.GetBytes(serialize), typeof(object));

            //if (json_last_error() != JSON_ERROR_NONE)
            //{
            //    throw new \InvalidArgumentException(json_last_error_msg());
            //}

            //$this->model->saveOrder($tree);

            return true;
        }

        protected string Script()
        {
            var trans = new Dictionary<string, string>() {
            {"delete_confirm"   , Admin.Trans("delete_confirm") },
            {"save_succeeded"   , Admin.Trans("save_succeeded")},
            {"refresh_succeeded", Admin.Trans("refresh_succeeded")},
            {"delete_succeeded" , Admin.Trans("delete_succeeded")},
            {"confirm"          , Admin.Trans("confirm")},
            { "cancel"           , Admin.Trans("cancel")},
        };

            var nestableOptions = JsonSerializer.Serialize(this.nestableOptions);
            var url = _context.Request.Path;
            return $@"
    
        $('#{elementId}').nestable({nestableOptions});

        $('.tree_branch_delete').click(function() {{
                var id = $(this).data('id');
                swal({{
                title: ""{trans["delete_confirm"]}"",
                type: ""warning"",
                showCancelButton: true,
                confirmButtonColor: ""#DD6B55"",
                confirmButtonText: ""{trans["confirm"]}"",
                showLoaderOnConfirm: true,
                cancelButtonText: ""{trans["cancel"]}"",
                preConfirm: function() {{
                        return new Promise(function(resolve) {{
                        $.ajax({{
                            method: 'delete',
                            url: '{url}/' + id,
                            data: {{
                                _method:'delete',
                                _token:LA.token,
                            }},
                            success: function (data) {{
                                $.pjax.reload('#pjax-container');
                        toastr.success('{trans["delete_succeeded"]}');
                        resolve(data);
                    }}
                }});
            }});
        }}
    }}).then(function(result) {{
        var data = result.value;
        if (typeof data === 'object')
        {{
            if (data.status)
            {{
                swal(data.message, '', 'success');
            }}
            else
            {{
                swal(data.message, '', 'error');
            }}
        }}
    }});
        }});

$('.{elementId}-save').click(function() {{
    var serialize = $('#{elementId}').nestable('serialize');

    $.ajax({{
        'url': '{url}',
        'method': 'put',
        'data': {{ _token: LA.token,
                _order: JSON.stringify(serialize)
        }},
        'success': function(data){{
                $.pjax.reload('#pjax-container');
                toastr.success('{trans["save_succeeded"]}');
        }}
    }});
}});

        $('.{elementId}-refresh').click(function() {{
            $.pjax.reload('#pjax-container');
    toastr.success('{trans["refresh_succeeded"]}');
}});

        $('.{elementId}-tree-tools').on('click', function(e){{
    var action = $(this).data('action');
    if (action === 'expand')
    {{
                $('.dd').nestable('expandAll');
    }}
    if (action === 'collapse')
    {{
                $('.dd').nestable('collapseAll');
    }}
}});


";
        }

        public Task<List<T>> GetItems()
        {
            return model.ToTree();
        }

        public async Task<ViewTree<T>> Variables()
        {
            return new()
            {
                Id = elementId,
                Tools = tools.Render(),
                Items = await GetItems(),
                UseCreate = useCreate,
                UseSave = useSave,
                UseRefresh = useRefresh,
                Path = _context.Request.Path,
                KeyName = model.GetKeyName(),
                BranchView = view["branch"],
                BranchCallback = branchCallback,
            };
        }

        public void Tools(Action<Tools<T>> callback)
        {
            callback?.Invoke(tools);
        }

        public async Task<HtmlContent> Render()
        {
            Admin.Script(Script());

            return new()
            {
                Name = HtmlContentType.Partial,
                Value = view["tree"],
                Form = await Variables()
            };
        }

    }
}
