﻿using CoreAdmin.Mvc.Models;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc
{
    public class BGrid<T> : BGrid where T:BaseModel
    {
        public BGrid(IQueryable<BaseModel> model, HttpContext context) : base(model, context)
        {

        }
        protected override GColumn<T> AddColumn(string column = "", string label = "")
        {
            var columnCls = new GColumn<T>(column, label);
            columnCls.SetGrid(this);
            columns.Add(columnCls);
            return columnCls;
        }
        public override GColumn<T> Column(string name, string label) => (GColumn<T>)base.Column(name, label);
    }
}
