﻿using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc
{
    public class GColumn<T> : GColumn where T:BaseModel 
    {
        public GColumn(string name, string label) : base(name, label)
        {
        }
        public GColumn Display(Func<T, string> callback) => Display<object>((col, model) => callback.Invoke((T)model));

    }
}