﻿using CoreAdmin.Mvc.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;

namespace CoreAdmin.Mvc.Middleware
{
    internal class AuthenticateMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticateMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, UserManager<UserModel> userManager)
        {
            var route = httpContext.Request.Path;
            var user = await userManager.GetUserAsync(httpContext.User);
            if ((null == user) && !Permission.ShouldPassThrough(route))
            {
                const string loginPath = "/login";
                httpContext.Response.Redirect(loginPath);
                return;
            }

            await _next(httpContext);
        }

    }

    public static class AuthenticateMiddlewareExtensions
    {
        public static IApplicationBuilder AuthenticateMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticateMiddleware>();
        }
    }
}
