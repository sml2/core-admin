﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting.StaticWebAssets;
using Microsoft.Extensions.Configuration;

namespace CoreAdmin.Mvc.Middleware
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseCoreAdminMiddleware(this IApplicationBuilder app, IWebHostEnvironment env, IConfiguration Configuration)
        {
            app.UseOperationLog();
            app.UseDefaultFiles();//index.html index
            app.UseStaticFiles();//wwwroot js
            //env == "Development"
            //if (env.IsDevelopment()) StaticWebAssetsLoader.UseStaticWebAssets(env, Configuration);
            if (!env.IsProduction()) StaticWebAssetsLoader.UseStaticWebAssets(env, Configuration);

            #region 国际化
            var supportedCultures = new[] { "zh-CN", "en" };
            app.UseRequestLocalization(o => o.SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures));
            #endregion
            app.UseRouting();
            app.UseSession();
            app.UseAuthentication();
            app.UseAuthorization();
            #region Session


            app.InjectAdminMiddleware();

            //app.AuthenticateMiddleware();
            //app.PermissionsMiddleware();
            app.UseFlashSession();
            #endregion
            //app.UseAuthorization();
            #region 注入db
            #endregion
            #region Pjax
            app.UsePjax();//
            #endregion


            return app;
        }
    }
}
