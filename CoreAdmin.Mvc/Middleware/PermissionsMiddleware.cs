﻿using CoreAdmin.Mvc.Auth;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Identity;

namespace CoreAdmin.Mvc.Middleware
{
    class PermissionsMiddleware
    {
        private readonly RequestDelegate _next;

        public PermissionsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, UserManager<UserModel> userManager)
        {

            var route = httpContext.Request.Path;
            var user = await userManager.GetUserAsync(httpContext.User);
            //需要用户登录
            //if (null == Admin.User || Permission.ShouldPassThrough(route))
                //无需用户登录
            if (null != user || Permission.ShouldPassThrough(route))
            {
                await _next(httpContext);
                return;
            }

            //laravel-admin 权限中间件
            //if ($this->checkRoutePermission($request)) {
            //    return $next($request);
            //}

            var permissions = user.allPermissions();
            if(permissions.Count == 0 && !user.IsAdministrator())
            {
                await Permission.Error(httpContext);
                return;

            }
            foreach (var P in permissions)
            {
                var a = P.ShouldPassThrough(httpContext.Request.Path,httpContext.Request.Method);
                if (!P.ShouldPassThrough(httpContext.Request.Path, httpContext.Request.Method))
                {
                    await Permission.Error(httpContext);
                    return;
                }
            }
            await _next(httpContext);
        }
    }

    public static class PermissionsMiddlewareExtensions
    {
        public static IApplicationBuilder PermissionsMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<PermissionsMiddleware>();
        }
    }
}
