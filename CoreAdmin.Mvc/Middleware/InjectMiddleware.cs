﻿using CoreAdmin.EF;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.Middleware
{
    internal class InjectMiddleware
    {
        private readonly RequestDelegate _next;

        public InjectMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, AdminDbContext baseContext, Admin admin, ILoggerFactory factory)
        {
            Admin.LoggerFactory ??= factory;

            await _next(httpContext);
        }
    }
    /// <summary>
    /// 注入中间件扩展类
    /// </summary>
    public static class InjectMiddlewareExtensions
    {
        /// <summary>
        /// 注入Admin中间件扩展[<see cref="ILoggerFactory"/>...]
        /// </summary>
        public static IApplicationBuilder InjectAdminMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<InjectMiddleware>();
        }
    }
}
