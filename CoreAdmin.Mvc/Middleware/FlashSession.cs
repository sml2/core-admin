﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.Middleware
{
    public class FlashSessionMiddleware
    {
        public const string SessionOld = "old";
        public const string SessionNew = "new";

        private readonly RequestDelegate _next;
        private static ISession _session;

        public FlashSessionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            // FIXME: 单次请求会触发两次
            _session = httpContext.Session;
            _session.TryGetValue(SessionNew, out var value);
            if (value != null && value.Length > 0)
            {
                _session.Remove(SessionNew);
                _session.Set(SessionOld, value);
            }

            httpContext.Response.OnStarting(_ => Task.CompletedTask, httpContext);
            await _next(httpContext);

            _session.Remove(SessionOld);
        }

        public static void Flash(string key, string value)
        {
            _session.TryGetValue(SessionNew, out var newValue);
            Dictionary<string, string> list;
            if (newValue != null && newValue.Length > 0)
            {
                list = (Dictionary<string, string>)JsonSerializer.Deserialize(Encoding.UTF8.GetString(newValue), typeof(Dictionary<string, string>));
                if (list != null) list[key] = value;
            } else
            {
                list = new Dictionary<string, string>() { { key, value } };
            }
            newValue = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(list));
            _session.Set(SessionNew, newValue);
        }

        public static void GetFlash(string key, out string value)
        {
            _session.TryGetValue(SessionNew, out var newValue);
            Dictionary<string, string> list;
            if (newValue != null && newValue.Length > 0)
            {
                list = (Dictionary<string, string>)JsonSerializer.Deserialize(Encoding.UTF8.GetString(newValue), typeof(Dictionary<string, string>));
                if (list != null && list.TryGetValue(key, out value))
                {
                    return;
                }
            }

            _session.TryGetValue(SessionOld, out var oldValue);
            if (oldValue != null && oldValue.Length > 0)
            {
                list = (Dictionary<string, string>)JsonSerializer.Deserialize(Encoding.UTF8.GetString(oldValue), typeof(Dictionary<string, string>));
                if (list != null && list.TryGetValue(key, out value))
                {
                    return;
                }
            }

            value = null;
        }
    }

    public static class FlashSessionMiddlewareExtensions
    {
        public static IApplicationBuilder UseFlashSession(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<FlashSessionMiddleware>();
        }
    }
}
