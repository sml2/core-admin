﻿using CoreAdmin.EF;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Identity;
using Model = CoreAdmin.Mvc.Models.OperationLog;

namespace CoreAdmin.Mvc.Middleware
{
    /// <summary>
    /// 记录操作日志中间件
    /// </summary>
    class OperationLog
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<OperationLog> _logger;

        public OperationLog(RequestDelegate next, ILogger<OperationLog> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext httpContext, AdminDbContext DBContext, UserManager<UserModel> userManager)
        {
            var Request = httpContext.Request;
            var Response = httpContext.Response;
            bool Enable = true;//Config
            var user = await userManager.GetUserAsync(httpContext.User);
            if (Enable && !InExceptArray(httpContext, _logger) && inAllowedMethods(Request.Method))// && null != user
            {
                Model log = new()
                {
                    UserId = user != null ? user.Id : 0,
                    Username = user != null ? user.Name : "游客",
                    IP = httpContext.GetIPv4(),
                    Host = Request.Host.ToString(),
                    Path = Request.Path,
                    Query = Request.QueryString.ToString(),
                    Method = Request.Method
                };
                if (Request.HasFormContentType) { 
                    var HasForm = Request.Form.Count > 0;
                    var FormJson = string.Empty;
                    if (HasForm) FormJson = string.Join(',', Request.Form.Select(kp => $"\"{kp.Key}\":{(kp.Value.Count > 1 ? $"[{string.Join(',', kp.Value.Select(v => $"\"{v}\""))}]" : $"\"{kp.Value}\"")}"));
                    var HasFiles = Request.Form.Files.Count > 0;
                    var FileJson = string.Empty;
                    if (HasFiles) FileJson = string.Join(',', Request.Form.Files.Select(f => $"{{\"Name\":\"{f.Name}\",\"FileName\":\"{f.FileName}\",\"Length\":\"{f.Length}\"}}"));
                    if (HasForm || HasFiles) log.Body = $"{{{FormJson}{(HasForm && HasFiles ? "," : string.Empty)}{(HasFiles ? $"\"Files\":[{FileJson}]" : string.Empty)}}}";
                }
                await _next(httpContext);
                log.ResponseCode = Response.StatusCode;
                log.ContentLength = Response.ContentLength.GetValueOrDefault();
                DBContext.Add(log);
                await DBContext.SaveChangesAsync();
            }
            else
            {
                await _next(httpContext);
            }

        }


        private bool inAllowedMethods(string Method)
        {
            var AllowedMethods = new List<string> { "GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH" };

            if (AllowedMethods.Count == 0)
            {
                return true;
            }
            AllowedMethods.ForEach((item) => { item = item.ToUpper(); });

            return AllowedMethods.Contains(Method);
        }
        private static readonly string[] Excepts = new[] { $"/{nameof(OperationLog)}", "/_content" };
        private static bool InExceptArray(HttpContext httpContext, ILogger ILogger)
        {
            foreach (string Except in Excepts)
            {
                if (string.IsNullOrWhiteSpace(Except))
                {
                    ILogger.LogWarning("Config Of Excepts Has Invalid[NullOrEmpty] Data");
                    continue;
                }
                else if (!Except.StartsWith('/'))
                {
                    ILogger.LogWarning($"Config Of Excepts Has Invalid[NotStartsWithPathSeqarator] Data [{Except}]");
                    continue;
                }
                string[] Methods = null;
                string TempExcept;
                if (Except.Contains(":"))
                {
                    var Temp = Except.Split(':');
                    if (Temp.Length == 2)
                    {
                        TempExcept = Temp[0];
                        Methods = Temp[1].Split(',', StringSplitOptions.RemoveEmptyEntries);
                    }
                    else
                    {
                        ILogger.LogWarning($"Config Of Excepts Has Invalid[:] Data [{Except}]");
                        continue;
                    }
                }
                else
                {
                    TempExcept = Except;
                }
                var Request = httpContext.Request;
                if (Request.Path.ToString().StartsWith(TempExcept, StringComparison.OrdinalIgnoreCase) && (Methods == null || Methods.Contains(Request.Method)))
                {
                    return true;
                }
            }
            return false;
        }
    }

    public static class OperationLogMiddlewareExtensions
    {
        /// <summary>
        /// 注入记录操作日志中间件
        /// </summary>
        public static IApplicationBuilder UseOperationLog(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<OperationLog>();
        }
    }
}
