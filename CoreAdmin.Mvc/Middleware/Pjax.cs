﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Extensions;

namespace CoreAdmin.Mvc.Middleware
{
    /// <summary>
    /// Pjax中间件
    /// </summary>
    public class PjaxMiddleware
    {
        public const string PjaxSign = "X-PJAX";
        public const string PjaxContainer = "X-PJAX-Container";
        private const string PjaxResponseUrl = "X-PJAX-URL";

        private readonly RequestDelegate _next;
        /// <summary>
        /// Pajx中间件构造器
        /// </summary>
        /// <param name="next"></param>
        public PjaxMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            var request = httpContext.Request;
            if (request.IsPjax())
            {
                httpContext.Response.OnStarting(state =>
                {
                    var context = (HttpContext)state;
                    if (context.Response.StatusCode != 302) context.Response.Headers[PjaxResponseUrl] = request.GetEncodedPathAndQuery();
                    return _next(httpContext);
                }, httpContext);
            }
            else
            {
                await _next(httpContext);
            }
        }
    }
    /// <summary>
    /// Pjax中间件扩展
    /// </summary>
    public static class PjaxMiddlewareExtensions
    {
        /// <summary>
        /// 使用Pjax中间件扩展
        /// </summary>
        public static IApplicationBuilder UsePjax(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<PjaxMiddleware>();
        }
    }
}
