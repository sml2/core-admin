﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Configure
{
    public class CoreAdminConfigure
    {
        //private IWebHostEnvironment env;

        public const string Section = "CoreAdmin";

        public string LoginBackgroundImage { get; set; }
        public string Name { get; set; }
        public bool RememberMe { get; set; }
        public string Title { get; set; }
        public string AppLocale { get; set; }
        public string Favicon { get; set; }
        public string Skin { get; set; }
        public List<string> Layout { get; set; }
        public string LayoutStr { get => Layout!=null?string.Join(" ", Layout):""; }

        public string TopAlert { get; set; }
        public string LogoMini { get; set; }
        public string Logo { get; set; }
        public bool ShowEnvironment { get; set; }
        //public string Env { get => env.EnvironmentName; }
        public string Env { get; }

        public bool ShowVersion { get; set; }
        public string Version { get; set; } = "1.0.0.0";
        public bool EnableMenuSearch { get; set; }
        public bool EnableDefaultBreadcrumb { get; set; }

        public RouteStruct Route { get; set; }


        public struct RouteStruct
        {
            public string Prefix { get; set; }
        }

        public string FeedBackEmail { get; set; }
    }


}
