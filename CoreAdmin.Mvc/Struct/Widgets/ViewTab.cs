﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewTab : IViewData
    {
        public string Title { get; set; }
        public HtmlContent Content { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public List<TabItem> Tabs {get; set;}
        public int Active { get; set; }
        public List<DropDownItem> DropDown { get; set; }

    }

    public struct TabItem
    {
        public int Type { get; set; }
        public string Title { get; set; }
        public string Id { get; set; }
        public string Href { get; set; }
    }

    public struct DropDownItem
    {
        public string Name { get; set; }
        public string Href { get; set; }
    }
}
