﻿using CoreAdmin.Mvc.Interface;
using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewCollapse : IViewData
    {
        public string Id { get; set; }
        public List<CollapseItem> Items { get; set; }
        public HtmlAttributes Attributes { get; set; }
    }

    public struct CollapseItem
    {
        public string Title { get; set; }
        public string Content { get; set; }

    }
}
