﻿using CoreAdmin.Mvc.Form;
using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Attributes;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewForm : IViewData
    {
        public List<FField> Fields { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public string Method { get; set; }
        public List<string> Buttons { get; set; }
        public Dictionary<string, int> Width { get; set; }
        public BaseModel Data { get; set; }
        public Type ModelType { get; internal set; }
    }
}
