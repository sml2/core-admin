﻿using CoreAdmin.Mvc.Interface;
using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewTable : IViewData
    {
        public string Headers { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public List<List<string>> Rows { get; set; }
    }
}
