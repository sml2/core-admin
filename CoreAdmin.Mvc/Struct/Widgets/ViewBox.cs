﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewBox : IViewData
    {
        public string Title { get; set; }
        public HtmlContent Content { get; set; }
        public HtmlContent Footer { get; set; }
        public List<string> Tools { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public string Script { get; set; }
    }
}
