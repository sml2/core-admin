﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewCarousel : IViewData
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public HtmlContent Content { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public List<CarouselItem> Items { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }

    }

    public struct CarouselItem
    {
        public string Image { get; set; }
        public string Caption { get; set; }

    }
}
