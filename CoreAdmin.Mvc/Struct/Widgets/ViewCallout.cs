﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewCallout : IViewData
    {
        public string Title { get; set; }
        public HtmlContent Content { get; set; }
        public HtmlAttributes Attributes { get; set; }
    }
}
