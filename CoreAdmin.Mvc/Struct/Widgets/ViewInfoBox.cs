﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct.Widgets
{
    public struct ViewInfoBox : IViewData
    {
        public string Name { get; set; }
        public string Info { get; set; }
        public string Link { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public string Icon { get; set; }
    }
}
