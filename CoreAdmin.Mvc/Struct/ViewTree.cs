﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Struct
{
    public struct ViewTree<T> : IViewData where T :ModelTree<T>
    {
        public string Id { get; set; }
        public bool UseSave { get; set; }
        public bool UseRefresh { get; set; }
        public bool UseCreate { get; set; }
        public string Tools { get; set; }
        public List<T> Items { get; set; }

        public string Path { get; set; }
        public string KeyName { get; set; }
        public string BranchView { get; set; }
        public Func<T, string> BranchCallback { get; set; }
    }
}
