﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Show;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Struct.Show
{
    public struct ViewShowPanel : IViewData
    {
        public string Style { get; set; }
        public string Title { get; set; }
        public Tools Tools { get; set; }
        public List<Field> Fields { get; set; }
    }
}
