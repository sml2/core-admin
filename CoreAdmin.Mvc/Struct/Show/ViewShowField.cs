﻿using CoreAdmin.Mvc.Interface;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Struct.Show
{
    public struct ViewShowField : IViewData
    {
        public object Content { get; set; }
        public bool Escape { get; set; }
        public string Label { get; set; }
        public bool Wrapped { get; set; }
        public Dictionary<string, int> Width { get; set; }

    }
}
