﻿
using System.Text.Json.Serialization;

namespace CoreAdmin.Mvc.Struct
{
    public struct MessageBag
    {
        public string Title { get; set; }

        public string Message { get; set; }
        public MessageType Type { get; set; }
        [JsonIgnore]
        public string TypeStr { get => Type.ToString().ToLower(); }
    }
}
