﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Struct
{
    public class ViewFormField : IViewData
    {
        public ViewFormViewClass? ViewClass;
        public string Id { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Placeholder { get; set; }
        public string Value { get; set; }
        public string Class { get; set; }
        public string Column { get; set; }
        public HtmlAttributes Attributes { get; set; }
        public string ErrorKey { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError() => !string.IsNullOrEmpty(ErrorMessage);

        public string Prepend { get; set; }
        public bool HasPrepend() => !string.IsNullOrEmpty(Prepend);
        public string Append { get; set; }
        public bool HasApend() => !string.IsNullOrEmpty(Append);

        public string Btn { get; set; }
        public string Rows { get; set; }

        public ViewFormHelp? Help { get; set; }
        // TODO: dynamic
        public List<dynamic> Groups;
        // TODO: dynamic
        public Dictionary<string, dynamic> Options { get; set; }
        // TODO: dynamic
        public Dictionary<string, dynamic> Checked { get; set; }
        public BaseModel Data { get; internal set; }
        public Type ModelType { get; set; }
        public object Model => (object) Data ?? ModelType;
    }
    public class ViewFormSelect :   ViewFormField
    {
        public new Dictionary<string, string> Options { get; set; }
    }

    public class ViewFormMultiSelect : ViewFormSelect
    {
        public new List<string> Value { get; set; }
    }
    public struct ViewFormHelp
    {
        public string Icon { get; set; }
        public string Text { get; set; }
        public bool IsNull { get => string.IsNullOrEmpty(Icon) && string.IsNullOrEmpty(Text); }
    }

    public struct ViewFormViewClass
    {
        public string FormGroup { get; set; }
        public string Label { get; set; }
        public string Field { get; set; }
    }

}
