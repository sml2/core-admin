﻿namespace CoreAdmin.Mvc.Struct.Attributes
{
    public struct FormAttr
    {
        public string Id { get; set; }
        public string Method { get; set; }
        public string Action { get; set; }
        public string Class { get; set; }
        public string AcceptCharset { get; set; }
        public bool PjaxContainer { get; set; }
        public string Enctype { get; set; }
    }
}
