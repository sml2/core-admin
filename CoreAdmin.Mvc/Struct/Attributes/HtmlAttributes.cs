﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Struct.Attributes
{
    public class HtmlAttributes : Dictionary<string, object>
    {
        //public Style StyleDic =>
        //    (TryGetValue(Style.Name, out var style)
        //        ? style
        //        : this[Style.Name] = new Style()) as Style;
        public Style StyleDic { get; } = new ();
        //public CSS ClassList =>
        //    (TryGetValue(CSS.Name, out var css)
        //        ? css
        //        : this[CSS.Name] = new CSS()) as CSS;e;
        public Css ClassList { get; } = new ();
    }
}