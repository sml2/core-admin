﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Struct.Attributes
{
    public class ComplexAttr : Dictionary<string, string>
    {
        internal void Add(string value)
        {

            foreach (var attr in value.Split(';'))
            {
                var eles = attr.Split('=');
                string k = null;
                string v = null;
                switch (eles.Length)
                {
                    case 1:
                        k = eles[0];
                        break;
                    case 2:
                        k = eles[0];
                        v = eles[1].Trim();
                        break;
                    default:
                        throw new ArgumentException(null, nameof(value));
                }
                this[k] = v;
            }
        }

        public override string ToString()
        {
            return string.Join(';', this.Select(kp => $"{kp.Key}{(kp.Value != null ? $":{kp.Value}" : string.Empty)} "));
        }
    }

    public class Style : ComplexAttr
    {
        public const string Name = "style";
    }

    public class Css : List<string>
    {
        public const string Name = "class";
        public override string ToString()
        {
            return string.Join(' ', this);
        }
    }
}