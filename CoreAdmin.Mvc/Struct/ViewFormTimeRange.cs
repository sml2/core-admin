﻿namespace CoreAdmin.Mvc.Struct
{
    public class ViewFormTimeRange : ViewFormField
    {
        public struct Range
        {
            public string Start;
            public string End;
        }

        public new Range Id;
        public new Range Name;
        public new Range Class;

    }
}
