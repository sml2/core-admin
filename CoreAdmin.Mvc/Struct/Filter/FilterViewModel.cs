﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Grid.Filter.Presenter;
using CoreAdmin.Mvc.Interface;

namespace CoreAdmin.Mvc.Struct.Filter
{
    public struct FilterViewModel : IViewData
    {
        public string Id { get; set; }
        public string Column { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public object Value { get; set; }
        public Presenter Presenter { get; set; }
        
        public PresenterViewModel PresenterData { get; set; }
        
        public bool HasGroup => PresenterData.Group != null && PresenterData.Group.Count > 0;
        
        public string GroupName { get; set; }
        
        public Dictionary<string, string> Default { get; set; }
    }

    public struct PresenterViewModel
    {
        #region Text

        public string Placeholder { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
        public List<string> Group { get; set; }

        #endregion

        #region Select
        public Dictionary<string, object> Options { get; set; }
        public string Class { get; set; }
        
        #endregion

        #region DateTime

        

        #endregion

        #region Radio

        public bool Inline { get; set; }

        #endregion
    }
}