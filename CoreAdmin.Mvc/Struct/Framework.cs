﻿using System.Reflection;
using System.Runtime.Versioning;

namespace CoreAdmin.Mvc.Struct
{
    public static class Framework
    {
        public static string NetCoreVersion => Assembly
                        .GetEntryAssembly()?
                        .GetCustomAttribute<TargetFrameworkAttribute>()?
                        .FrameworkName;
        public static string OsPlatform => System.Runtime.InteropServices.RuntimeInformation.OSDescription;
    }
}
