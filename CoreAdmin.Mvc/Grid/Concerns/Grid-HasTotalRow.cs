﻿using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        /**
     * @var array
     */
        protected Dictionary<string, Func<int, int>> totalRowColumns;

        public BGrid AddTotalRow(string column, Func<int, int> callback)
        {
            if (totalRowColumns == null) totalRowColumns = new();
            if (totalRowColumns.ContainsKey(column))
            {
                totalRowColumns[column] = callback;
            }
            else
            {
                totalRowColumns.Add(column, callback);
            }

            return this;
        }

        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
         */
        public HtmlContent RenderTotalRow(List<GColumn> columns = null)
        {
            if (totalRowColumns == null || totalRowColumns.Count == 0)
            {
                return null;
            }

            //$query = Model().GetQueryBuilder();
            string query = null;
            var totalRow = new TotalRow(query, totalRowColumns);

            totalRow.SetGrid(this);

            if (columns != null)
            {
                totalRow.SetVisibleColumns(columns);
            }

            return totalRow.Render(true);
        }
    }
}
