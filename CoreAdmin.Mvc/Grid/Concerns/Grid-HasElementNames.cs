﻿using System;
using System.Collections.Generic;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        public string Name { get; set; }

        protected Dictionary<string, string> elementNames = new()
        {
            { "grid_row", "grid-row" },
            { "grid_select_all", "grid-select-all" },
            { "grid_per_page", "grid-per-pager" },
            { "grid_batch", "grid-batch" },
            { "export_selected", "export-selected" },
            { "selected_rows", "selectedRows" },
        };

        //public Grid SetName(string name)
        //{
        //    Name = name;

        //    Model()->setPerPageName("{$name}_{$this->model()->getPerPageName()}");

        //    GetFilter()->setName(name);

        //    return this;
        //}


        public string GetGridRowName()
        {
            return ElementNameWithPrefix("grid_row");
        }

        public string GetSelectAllName()
        {
            return ElementNameWithPrefix("grid_select_all");
        }

        public string GetPerPageName()
        {
            return ElementNameWithPrefix("grid_per_page");
        }

        public string GetGridBatchName()
        {
            return ElementNameWithPrefix("grid_batch");
        }

        public string GetExportSelectedName()
        {
            return ElementNameWithPrefix("export_selected");
        }

        public string GetSelectedRowsName()
        {
            var elementName = elementNames["selected_rows"];

            if (!string.IsNullOrEmpty(Name)) {
                return $"{Name}{elementName.UpperCaseFirst()}";
            }

            return elementName;
        }

        protected string ElementNameWithPrefix(string name)
        {
            var elementName = elementNames?[name];
            if (elementName == null)
            {
                throw new ArgumentNullException(paramName: "index not exists");
            }

            if (!string.IsNullOrEmpty(Name))
            {
                return $"{Name}-{elementName}";
            }

            return elementName;
        }
    }
}
