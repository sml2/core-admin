﻿namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        public BGrid EnableDblClick()
        {
            var script = $@"
$('body').on('dblclick', 'table#{tableID}>tbody>tr', function(e) {{
    var url = ""{Resource()}/""+$(this).data('key')+""/edit"";
    $.admin.redirect(url);
}});
";
            Admin.Script(script);

            return this;
        }
    }
}
