﻿using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Extensions;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected string exporter;
        
    protected void HandleExportRequest(bool forceExport = false)
        {
            // TODO request
            var scope = Context.Request.Get(Exporter.queryName);
            if (!string.IsNullOrEmpty(scope)) {
                return;
            }

            // clear output buffer.
            //if (ob_get_length())
            //{
            //    ob_end_clean();
            //}

        // DisablePagination();

        //    if (forceExport) {
        //        GetExporter(scope).Export();
        //    }
        }

        /**
         * @param string $scope
         *
         * @return AbstractExporter
         */
        //protected AbstractExporter GetExporter($scope)
        //{
        //    return (new Exporter($this))->resolve($this->exporter)->withScope($scope);
        //}

        ///**
        // * Set exporter driver for Grid to export.
        // *
        // * @param $exporter
        // *
        // * @return $this
        // */
        //public function exporter($exporter)
        //{
        //$this->exporter = $exporter;

        //    return $this;
        //}

        // int
        public string GetExportUrl(string scope = "1", dynamic args = null)
        {
            // TODO request
        //var input = array_merge(request()->all(), Exporter::formatExportQuery($scope, $args));

            //if ($constraints = $this->model()->getConstraints()) {
            //$input = array_merge($input, $constraints);
            //}

            //return Resource()+"?"+http_build_query($input);
            return Resource() + "?";
        }

        public bool ShowExportBtn()
        {
            return Option("show_exporter");
        }

        /// <summary>
        /// Disable export.
        /// </summary>
        /// <param name="disable"></param>
        /// <returns></returns>
        public BGrid DisableExport(bool disable = true)
        {
            return Option("show_exporter", !disable);
        }

        /// <summary>
        /// Render export button.
        /// </summary>
        /// <returns></returns>
        public string RenderExportButton()
        {
            return (new ExportButton(this, Context)).Render();
        }

        ///**
        // * @param \Closure $callback
        // */
        //public function export(\Closure $callback)
        //{
        //    if (!$scope = request(Exporter::$queryName)) {
        //        return;
        //    }

        //$this->getExporter($scope)->setCallback($callback);

        //    return $this;
        //}
    }
}
