﻿using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Extensions;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        public List<string> hiddenColumns;

        public BGrid DisableColumnSelector(bool disable = true)
        {
            return Option("show_column_selector", !disable);
        }

        public bool ShowColumnSelector()
        {
            return Option("show_column_selector");
        }

        public HtmlContent RenderColumnSelector()
        {
            return (new ColumnSelector(this)).Render(true);
        }

        public BGrid HideColumns(string columns)
        {
            hiddenColumns.Add(columns);
            return this;
        }

        protected List<string> GetVisibleColumnsFromQuery()
        {
            var requestColumns = Context.Request.Query.ContainsKey(ColumnSelector.SELECT_COLUMN_NAME) ? Context.Request.Query[ColumnSelector.SELECT_COLUMN_NAME].ToString() : "";
            var columns = requestColumns.Split(',', StringSplitOptions.RemoveEmptyEntries);

            if (hiddenColumns == null) hiddenColumns = new();
            return columns.Length > 0 ? columns.ToList() : columnNames.Diff(hiddenColumns);
        }

        public List<GColumn> VisibleColumns()
        {
            var visible = GetVisibleColumnsFromQuery();

            if (visible.Count == 0)
            {
                return columns;
            }
            visible.Add(GColumn.SELECT_COLUMN_NAME);
            visible.Add(GColumn.ACTION_COLUMN_NAME);

            return columns.Where((column) => visible.Contains(column.GetName())).ToList();
        }

        public List<string> VisibleColumnNames()
        {
            var visible = GetVisibleColumnsFromQuery();

            if (visible.Count == 0)
            {
                return columnNames;
            }
            visible.Add(GColumn.SELECT_COLUMN_NAME);
            visible.Add(GColumn.ACTION_COLUMN_NAME);

            return columnNames.Where((column) => visible.Contains(column)).ToList();
        }

        public List<string> GetDefaultVisibleColumnNames()
        {
            return columnNames.Where(name => !(hiddenColumns.Contains(name) || name == GColumn.SELECT_COLUMN_NAME || name == GColumn.ACTION_COLUMN_NAME)).ToList();
        }
    }
}
