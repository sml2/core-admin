﻿namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        public void FixHeader()
        {
            Admin.Style(
                $@"
.wrapper, .grid-box .box-body {{
    overflow: visible;
}}

.grid-table {{
    position: relative;
    border-collapse: separate;
}}

.grid-table thead tr:first-child th {{
    background: white;
    position: sticky;
    top: 0;
    z-index: 1;
}}
"
            );
        }
    }
}
