﻿using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected Func<HtmlContent> header;

        public Func<HtmlContent> Header() => header;

        public BGrid Header(Func<HtmlContent> closure)
        {
            header = closure;

            return this;
        }

        public string RenderHeader()
        {
            if (header == null)
            {
                return "";
            }

            return (new Header(this)).Render();
        }
    }
}
