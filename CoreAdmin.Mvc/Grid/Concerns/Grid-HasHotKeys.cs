﻿namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected void AddHotKeyScript()
        {
            var filterID = GetFilter().GetFilterID();

            var refreshMessage = Admin.Trans("refresh_succeeded");

            var script = $@"

$(document).off('keydown').keydown(function(e) {{
    var tag = e.target.tagName.toLowerCase();
    
    if (tag == 'input' || tag == 'textarea' || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey) {{
        return;
    }}

    var \$box = $(""#{tableID}"").closest('.box');
    var \$current_page = \$box.find('.pagination .page-item.active');

    switch(e.which) {{
        case 82: // `r` for reload
            $.admin.reload();
            $.admin.toastr.success('{refreshMessage}', '', {{positionClass:""toast-top-center""}});
            break;
        case 83: // `s` for search
            \$box.find('input.grid-quick-search').trigger('focus');
            break; 
        case 70: // `f` for open filter
            \$box.find('#{filterID}').toggleClass('hide');
            break;
        case 67: // `c` go to create page 
            \$box.find('.grid-create-btn>a').trigger('click');
            break; 
        case 37: // `left` for go to prev page
            \$current_page.prev().find('a').trigger('click');
            break;
        case 39: // `right` for go to next page
            \$current_page.next().find('a').trigger('click');
            break;
        default: return;
    }}
    e.preventDefault();
}});

";

            Admin.Script(script);
        }

        public BGrid EnableHotKeys()
        {
            AddHotKeyScript();

            return this;
        }
    }
}
