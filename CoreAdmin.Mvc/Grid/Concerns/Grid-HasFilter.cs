﻿using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Models;
using System;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {

        protected BFilter _filter;

        protected BGrid InitFilter()
        {
            _filter = new BFilter(Model(), Context);
            _filter.InitGrid(this);
            return this;
        }

        public BGrid DisableFilter(bool disable = true)
        {
            tools.DisableFilterButton(disable);

            return Option("show_filter", !disable);
        }

        public BFilter GetFilter()
        {
            return _filter;
        }

        public Task<ListPaginator<IIdexerModel>> ApplyFilter(bool toArray = true)
        {
            if (builder != null)
            {
                builder.Invoke(this);
            }

            return _filter.Execute(toArray);
        }

        public void Filter(Action<BFilter> callback)
        {
            callback.Invoke(_filter);
        }

        public HtmlContent RenderFilter()
        {
            if (!Option("show_filter"))
            {
                return null;
            }

            return _filter.Render(true);
        }

        public BGrid ExpandFilter()
        {
            _filter.Expand();

            return this;
        }
    }
}
