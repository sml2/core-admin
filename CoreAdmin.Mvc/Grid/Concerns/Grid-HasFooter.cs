﻿using CoreAdmin.Mvc.Grid.Tools;
using System;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected Action footer;

        public Action Footer() => footer;
    public BGrid Footer(Action closure)
        {
            footer = closure;
            return this;
        }

        public string RenderFooter()
        {
            if (footer == null) {
                return "";
            }

            return (new Footer(this)).Render();
        }
    }
}
