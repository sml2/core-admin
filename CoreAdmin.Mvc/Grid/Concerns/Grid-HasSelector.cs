﻿using CoreAdmin.Mvc.Models;
using System;
using GSelector = CoreAdmin.Mvc.Grid.Tools.Selector;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected GSelector selector;

        public BGrid Selector(Action<GSelector> closure)
        {
            selector = new GSelector();

            closure.Invoke(selector);

            Header(() => RenderSelector());

            return this;
        }

        protected BGrid ApplySelectorQuery()
        {
            if (selector == null)
            {
                return this;
            }

            var active = GSelector.ParseSelected();
            foreach (var pair in selector.GetSelectors())
            {
                var _selector = pair.Value;
                var column = pair.Key;
                if (!active.ContainsKey(column))
                {
                    continue;
                }

                var values = active[column];

                //if (selector["type"] == "one")
                //{
                //    values = current(values);
                //}

                //if (is_null(selector["query"]))
                //{
                //    model()->whereIn(column, values);
                //}
                //else
                //{
                //    call_user_func(selector["query"], model(), values);
                //}
            }


            return this;
        }

        public HtmlContent RenderSelector()
        {
            return selector.Render();
        }
    }
}
