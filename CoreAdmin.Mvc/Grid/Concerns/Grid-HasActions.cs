﻿using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Grid.Tools;
using System;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected Action<Grid.Displayers.Actions> actionsCallback;

        protected Type actionsClass;

        public BGrid Actions(Action<Grid.Displayers.Actions> actions)
        {
            actionsCallback = actions;

            return this;
        }

        public Type GetActionClass()
        {
            if (null != actionsClass)
            {
                return actionsClass;
            }
            // TODO config
            //        if ($class = config('admin.grid_action_class')) {
            //                return $class;
            //}

            return typeof(Grid.Displayers.Actions);
        }

        public BGrid SetActionClass(Type actionClass)
        {
            if (actionClass.IsAssignableTo(typeof(Grid.Displayers.Actions)))
            {
                actionsClass = actionClass;
            }

            return this;
        }

        public BGrid DisableActions(bool disable = true)
        {
            return Option("show_actions", !disable);
        }

        public BGrid BatchActions(Action<BatchActions> closure)
        {
            Tools((tools) =>
            {
                tools.Batch(closure);
            });

            return this;
        }

        public BGrid DisableBatchActions(bool disable = true)
        {
            tools.DisableBatchActions(disable);

            return Option("show_row_selector", !disable);
        }

        protected void AppendActionsColumn()
        {
            if (!Option("show_actions"))
            {
                return;
            }

            AddColumn(GColumn.ACTION_COLUMN_NAME, Admin.Trans("action"))
                .DisplayUsing(GetActionClass(), actionsCallback);
        }
    }
}
