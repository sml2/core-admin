﻿using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        //    use HasQuickSearch;

        public BTools tools;

        protected BGrid InitTools()
        {
            tools = new BTools(this);

            return this;
        }

        public BGrid DisableTools(bool disable = true)
        {
            return Option("show_tools", !disable);
        }

        public void Tools(Action<BTools> callback)
        {
            callback.Invoke(tools);
        }

        public List<HtmlContent> RenderHeaderTools()
        {
            return tools.Render();
        }

        public bool ShowTools()
        {
            return Option("show_tools");
        }
    }
}
