﻿using CoreAdmin.Mvc.Grid;
using System.Collections.Generic;
using GFixColumns = CoreAdmin.Mvc.Grid.Tools.FixColumns;
namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected GFixColumns fixColumns;

        public void FixColumns(int head, int tail = -1)
        {
            fixColumns = new GFixColumns(this, head, tail);

            Rendering(fixColumns.Apply());
        }

        public List<GColumn> LeftVisibleColumns()
        {
            return fixColumns.LeftColumns();
        }

        public List<GColumn> RightVisibleColumns()
        {
            return fixColumns.RightColumns();
        }
    }
}
