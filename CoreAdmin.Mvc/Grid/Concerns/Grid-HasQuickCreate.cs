﻿using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc
{
    public partial class BGrid
    {
        protected bool hasQuickCreate = false;

    /**
     * @var QuickCreate
     */
    protected QuickCreate quickCreate;

    /**
     * @param \Closure $closure
     *
     * @return $this
     */
    public BGrid QuickCreate(Action<QuickCreate> closure)
        {
            quickCreate = new QuickCreate(this);

            closure.Invoke(quickCreate);

            return this;
        }

        /**
         * Indicates grid has quick-create.
         *
         * @return bool
         */
        public bool HasQuickCreate()
        {
            return quickCreate != null;
        }

        /**
         * Render quick-create form.
         *
         * @return array|string
         */
        public HtmlContent RenderQuickCreate()
        {
            var columnCount = VisibleColumns().Count;

            return quickCreate.Render(columnCount);
        }
    }
}
