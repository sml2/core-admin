﻿using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Grid.Displayers;
using CoreAdmin.Mvc.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Struct.Attributes;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace CoreAdmin.Mvc.Grid
{
    public partial class GColumn
    {
        public const string SELECT_COLUMN_NAME = "__row_selector__";

        public const string ACTION_COLUMN_NAME = "__actions__";

        public BGrid grid;

        protected string name;

        protected string label;

        public object Original;

        protected string relation;

        protected string relationColumn;

        protected IEnumerable<IIdexerModel> originalGridModels;

        public delegate string DisplayCallbackHandler<in T>(T value, IIdexerModel row);
        protected List<DisplayCallbackHandler<object>> displayCallbacks;

        public static Dictionary<string, dynamic> defined;

        protected  HtmlAttributes Attributes { get; set; }

        protected static HtmlAttributes rowAttributes { get; set; }

        public Type Model { get; set; }
        protected Dictionary<string, string> appendModel;
        private readonly ILogger<GColumn> logger;
        public HttpContext Context => grid?.Context;

        public GColumn(string name, string label)
        {
            this.name = name;
            this.label = FormatLabel(label);
            // TODO: 可能无法获取
            logger = Admin.LoggerFactory?.CreateLogger<GColumn>();
            InitAttributes();
        }

        protected void InitAttributes()
        {
            Attributes = new HtmlAttributes();
            Attributes.ClassList.Add(GetClassName());
        }

        public static void Define(string name, dynamic definition)
        {
            if (defined == null) defined = new();
            if (defined.ContainsKey(name))
            {
                defined[name] = definition;
            }
            else
            {
                defined.Add(name, definition);
            }
        }

        /// <summary>
        /// 传值到后台的数据页面
        /// </summary>
        /// <param name="grid"></param>
        public void SetGrid(BGrid grid)
        {
            this.grid = grid;
            SetModel(grid.ModelType);
        }

        public void SetModel(Type type)
        {
            if (Model == null)
            {
                Model = type;
            }
        }

        public void SetOriginalGridModels(IEnumerable<IIdexerModel> collection)
        {
            originalGridModels = collection;
        }
        
        public GColumn SetAttributes(string attributeName, object value)
        {
            attributeName = attributeName.ToLower();
            switch (attributeName)
            {
                case Struct.Attributes.Style.Name:
                    if (value == null) break;
                    Attributes.StyleDic.Add(value.ToString());
                    break;
                case Css.Name:
                    if (value == null) break;
                    Attributes.ClassList.Add(value.ToString());
                    break;
                default:
                    Attributes.Add(attributeName, value);
                    break;
            }
            return this;
        }

        //throw NotImplementedException("可以优化成静态")
        /// <summary>
        /// 
        /// </summary>
        /// <see cref="GetAttributes"/>
        /// <returns></returns>
        [Obsolete("不再使用字符串")]
        public string FormatHtmlAttributes()
        {
            return string.Concat(Attributes.Select(kp=>$"{kp.Key}{(kp.Value!=null?$"=\"{kp.Value}\"":string.Empty)} "));
        }
        public HtmlAttributes GetAttributes()
        {
            return Attributes;
        }
        

        public GColumn Style(string style)
        {
            return SetAttributes(Struct.Attributes.Style.Name, style);
        }
        public GColumn Width(double width)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must great than 0");
            }
            else if (width > 5)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must less than 5");
            }
            return Width($"{width * 100}%");
        }
        public GColumn Width(int width)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must great than 0");
            }
            return Width($"{width}px");
        }
        public GColumn Width(string width)
        {
            return Style($"width:{width};max-width:{width};word-wrap: break-word;word-break: normal;");
        }
        public GColumn MinWidth(double width)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must great than 0");
            }
            else if (width > 5)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must less than 5");
            }
            return MinWidth($"{width * 100}%");
        }
        public GColumn MinWidth(int width)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must great than 0");
            }
            return MinWidth ($"{width}px;");
        }
        public GColumn MinWidth(string width)
        {
            return Style($"min-width:{width};");
        }
        public GColumn MaxWidth(double width)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must great than 0");
            }
            else if(width>5) {
                throw new ArgumentOutOfRangeException(nameof(width), width, "must less than 5");
            }
            return MaxWidth($"{width*100}%");
        }
        public GColumn MaxWidth(int width)
        {
            if (width < 0) {
                throw new ArgumentOutOfRangeException(nameof(width), width,"must great than 0");
            }
            return MaxWidth($"{width}px");
        }
        public GColumn MaxWidth(string width)
        {
            return Style($"max-width:{width};white-space:nowrap;overflow:hidden;text-overflow:ellipsis;");
        }

        public GColumn Center()
        {
            return Align("center");
        }
        public GColumn Left()
        {
            return Align("left");
        }
        public GColumn Right()
        {
            return Align("right");
        }
        public GColumn Align(string styleValue)
        {
            return Style($"text-align: {styleValue};");
        }

        public GColumn Color(string color)
        {
            return Style($"color:{color};");
        }

        public object GetOriginal()
        {
            return Original;
        }

        public string GetName()
        {
            return name;
        }

        public string GetClassName()
        {
            var name = GetName().Replace(".", "-");

            return $"column-{name}";
        }

        protected string FormatLabel(string label)
        {
            if (!string.IsNullOrEmpty(label))
            {
                return label;
            }

            label = name.UpperCaseFirst();

            return Admin.Trans(label.Replace(new string[] { ".", "_", }, " "));
        }

        public string GetLabel()
        {
            return label;
        }

        public GColumn SetRelation(string relation, string relationColumn = null)
        {
            this.relation = relation;
            this.relationColumn = relationColumn;

            return this;
        }

        protected bool IsRelation()
        {
            return !string.IsNullOrEmpty(relation);
        }

        public GColumn Sortable(string cast = null)
        {
            return AddSorter(cast);
        }

        public GColumn Help(string help = "")
        {
            return AddHelp(help);
        }

        public GColumn Display<T>(Func<T, string> callback) => Display<T>((col, model) => callback.Invoke(col));
        public GColumn Display<T>(DisplayCallbackHandler<T> callback)
        {
            displayCallbacks ??= new List<DisplayCallbackHandler<object>>();
            string ObjCallback(object value, IIdexerModel row) => callback((T) value, row);
            displayCallbacks.Add(ObjCallback);

            return this;
        }

        [Obsolete("尽量不使用")]
        public GColumn DisplayUsing(Type abs, params object[] arguments)
        {
            var grid = this.grid;
            var column = this;

            return Display<object>((value, model) =>
            {
                var displayer = (AbstractDisplayer)grid.Context.RequestServices.GetRequiredService(abs);
                displayer.InitData(value, grid, column, model);
                if (arguments?.Length == 1)
                {
                    return displayer.Display((Action<Displayers.Actions>)arguments[0]);
                }
                return displayer.Display();
            });
        }
        public GColumn DisplayUsing(Type type, Func<AbstractDisplayer, string> cb)
        {
            return Display<object>((value, model) =>
            {
                var displayer = (AbstractDisplayer)grid.Context.RequestServices.GetRequiredService(type);
                displayer.InitData(value, grid, this, model);
                return cb(displayer);
            });
        }
        
        public GColumn DisplayUsing<T>(Func<T, string> cb) where T : AbstractDisplayer
        {
            return Display<object>((value, model) =>
            {
                var displayer = grid.Context.RequestServices.GetRequiredService<T>();
                displayer.InitData(value, grid, this, model);
                return cb(displayer);
            });
        }
        
        public GColumn DisplayUsing<T>(Func<T, HtmlContent> cb) where T : AbstractDisplayer
        {
            throw new NotImplementedException();
            // return Display<object>((value, model) =>
            // {
            //     var displayer = grid.Context.RequestServices.GetRequiredService<T>();
            //     displayer.InitData(value, grid, this, model);
            //     return cb(displayer);
            // });
        }

        /// <summary>
        /// 隐藏当前列
        /// </summary>
        /// <returns></returns>
        public GColumn Hide()
        {
            grid.HideColumns(GetName());
            return this;
        }

        public GColumn TotalRow(Func<int, int> display = null)
        {
            grid.AddTotalRow(name, display);
            return this;
        }

        //public function action($action)
        //{
        //    if (!is_subclass_of($action, RowAction::class)) {
        //    throw new \InvalidArgumentException("Action class [$action] must be sub-class of [Encore\Admin\Actions\GridAction]");
        //}

        //        $grid = $this->grid;

        //return $this->display(function($_, $column) use($action, $grid) {
        //            /** @var RowAction $action */
        //            $action = new $action();

        //    return $action
        //        ->asColumn()
        //        ->setGrid($grid)
        //        ->setColumn($column)
        //        ->setRow($this);
        //});
        //    }
        protected bool HasDisplayCallbacks()
        {
            return displayCallbacks != null && displayCallbacks.Count > 0;
        }

        protected object CallDisplayCallbacks(object value, int key)
        {
            foreach (var callback in displayCallbacks)
            {
                var previous = value;
                var rowModel = originalGridModels.ElementAt(key);

                value = callback.Invoke(value, rowModel);

                // TODO
                //if ((value is GColumn) &&
                //    ($last = array_pop($this->displayCallbacks))
                //    ) {
                //        $last = $this->bindOriginalRowModel($last, $key);
                //        $value = call_user_func_array($last, [$previous, $this]);
                //}
            }

            return value;
        }

        public void Fill(IEnumerable<IIdexerModel> data, ref List<Dictionary<string, object>> uiData)
        {
            var count = data.Count();
            for (var key = 0; key < count; key++)
            {
                var row = data.ElementAt(key);
                
                object value =  Original = row.Get(name);
                uiData[key] ??= new();
                uiData[key][name] = WebUtility.HtmlEncode(value?.ToString()) ?? string.Empty;
                if (IsDefinedColumn())
                {
                    UseDefinedColumn();
                }

                if (HasDisplayCallbacks())
                {
                    value = CallDisplayCallbacks(Original, key);
                }
                uiData[key][name] = value;
            }
        }

        protected bool IsDefinedColumn()
        {
            return defined != null && defined.ContainsKey(name);
        }

        protected void UseDefinedColumn()
        {
            // clear all display callbacks.
            displayCallbacks = new();

            var className = defined[name];

            if (className is DisplayCallbackHandler<object> cb)
            {
                Display(cb);
                return;
            }

            if (!(className is Type type) || !type.IsAssignableTo(typeof(AbstractDisplayer)))
            {
                throw new ArgumentException($"Invalid column definition [{className.ToString()}]");
            }

            DisplayUsing(type, d => d.Display());
            // Display<object>((value, model) =>
            // {
            //     var definition = (AbstractDisplayer)Activator.CreateInstance(type, new object[] { value, grid, this, model });
            //     return definition.Display();
            // });
        }

        protected static object HtmlEntityEncode(object item)
        {
            if (item == null) return null;
            if (item is IEnumerable<object>)
            {
                throw new ArgumentException("递归");
                //array_walk_recursive($item, function(&$value) {
                //            $value = htmlentities($value);
                //});
            }
            else
            {
               item = WebUtility.HtmlEncode(item.ToString());
            }

            return item;
        }

        protected GColumn CallSupportDisplayer(string abs, params object[] arguments)
        {
            throw new ArgumentException();
            return this;
            //            return Display((value) => {
            //            if (value is IEnumerable<dynamic>) {
            //                return call_user_func_array([collect($value), abs], arguments);
            //                                }

            //                                if (is_string($value)) {
            //                                    return call_user_func_array([Str::class, $abstract], array_merge([$value], $arguments));
            //                                }

            //return $value;
            //                            });
        }
        
        protected GColumn CallBuiltinDisplayer<T>(Func<T, string> cb) where T : AbstractDisplayer =>
            DisplayUsing(cb);

        public GColumn Call(string method, params object[] arguments)
        {
            if (IsRelation() && !string.IsNullOrEmpty(relationColumn))
            {
                name = $"{relation}.{method}";
                label = FormatLabel(arguments[0].ToString() ?? null);

                relationColumn = method;

                return this;
            }

            // return ResolveDisplayer(method, arguments);
            return null;
        }
    }
}
