﻿using CoreAdmin.Mvc.Actions;
using CoreAdmin.Mvc.Grid.Tools;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid
{
    public class BTools
    {
        protected BGrid grid;

        protected List<IRenderable> tools;

        public BTools(BGrid grid)
        {
            this.grid = grid;

            tools = new();

            AppendDefaultTools();
        }

        protected void AppendDefaultTools()
        {
            Append(new BatchActions())
                .Append(new FilterButton());
        }

        public BTools Append(IRenderable tool)
        {
            if (tool is GridAction gaTool)
            {
                gaTool.SetGrid(grid);
            }

            tools.Add(tool);

            return this;
        }
        
        public BTools Prepend(AbstractTool tool)
        {
            tools.Insert(0, tool);
            return this;
        }

        public void DisableFilterButton(bool disable = true)
        {
            tools = tools.Select(tool =>
            {
                if (tool is FilterButton filterButton)
                {
                    return filterButton.Disable(disable);
                }
                return tool;
            }).ToList();
        }

        public void DisableRefreshButton(bool disable = true)
        {
            //
        }

        public void DisableBatchActions(bool disable = true)
        {
            tools = tools.Select(tool =>
            {
                if (tool is BatchActions batch)
                {
                    return batch.Disable(disable);
                }
                return tool;
            }).ToList();
        }

        public void Batch(Action<BatchActions> closure)
        {
            closure.Invoke((BatchActions)tools.First((tool) => tool is BatchActions));
        }

        public List<HtmlContent> Render()
        {
            return tools.Select((tool) =>
            {
                if (tool is AbstractTool abstractTool)
                {
                    if (!abstractTool.Allowed)
                    {
                        return null;
                    }

                    return abstractTool.SetGrid(grid).Render(true);
                }

                //if (tool is IRenderable)
                //{
                //    return tool.Render();
                //}

                //if (tool is Interface.IHtmlable htmlTool)
                //{
                //    return htmlTool.ToHtml();
                //}

                return new HtmlContent { Name = HtmlContentType.Str, Value = tool.ToString() };
            }).ToList();
        }
    }
}
