﻿using CoreAdmin.Mvc.Grid.Selectable;
using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.Grid
{
    abstract class GSelectable
    {
        public string model;

        protected BGrid grid;

        protected string key = "";

        protected bool multiple = false;

        protected int perPage = 10;

        protected bool imageLayout = false;

        public GSelectable(bool multiple = false, string key = "")
        {
            this.key = key ?? this.key;
            this.multiple = multiple;

            InitGrid();
        }

        abstract public BGrid Make();

        protected void ImageLayout()
        {
            imageLayout = true;
        }

        public async Task<HtmlContent> Render()
        {
            Make();

            if (imageLayout)
            {
                grid.SetView("admin::grid.image", new() { { "key", key } });
            }
            else
            {
                AppendRemoveBtn(true);
            }

            //DisableFeatures();
            grid.Paginate(perPage).ExpandFilter();

            var displayer = multiple ? typeof(Checkbox) : typeof(Radio);

            grid.PrependColumn("__modal_selector__", " ").DisplayUsing(displayer, new List<string> { key });

            return await grid.Render();
        }

        /**
         * @return $this
         */
        //protected GSelectable DisableFeatures()
        //{
        //    return grid.DisableExport()
        //        .DisableActions()
        //        .DisableBatchActions()
        //        .DisableCreateButton()
        //        .DisableColumnSelector()
        //        .DisablePerPageSelector();
        //}

        //public BGrid RenderFormGrid($values)
        //{
        //    Make();

        //    AppendRemoveBtn(false);

        //        //Model().WhereKey(Arr::wrap($values));

        //        //grid.DisableFeatures().DisableFilter();

        //    if (!multiple) {
        //            //grid.DisablePagination();
        //    }

        //        grid.Tools((tools) => {
        //        tools.Append(new BrowserBtn());
        //    });

        //    return grid;
        //}

        protected void AppendRemoveBtn(bool hide = true)
        {
            var hideStr = hide ? "hide" : "";
            var key = this.key;

            grid.Column("__remove__", " ").Display<object>((model) =>
            {
                return "";
                //return $@"
                //<a href=""javascript:void(0);"" class=""grid-row-remove {hide}"" data-key=""{grid.GetAttribute(key)}"">
                //    <i class=""fa fa-trash""></i>
                //</a>
                //";
            });
        }

        protected void InitGrid()
        {
            //if (!class_exists($this->model) || !is_subclass_of($this->model, Model::class)) {
            //    throw new \InvalidArgumentException("Invalid model [{$this->model}]");
            //}

            /** @var Model $model */
            //$model = new $this->model();

            //grid = new BGrid(new $model());

            if (string.IsNullOrEmpty(key))
            {
                //key = $model->getKeyName();
            }
        }

    }
}
