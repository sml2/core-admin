﻿using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class TotalRow : AbstractTool
    {

        protected dynamic query;

        protected Dictionary<string, Func<int, int>> columns;

        protected List<GColumn> visibleColumns;

        public TotalRow(dynamic query, Dictionary<string, Func<int, int>> columns)
        {
            this.query = query;

            this.columns = columns;
        }

        /**
         * Get total value of current column.
         *
         * @param string $column
         * @param mixed  $display
         *
         * @return mixed
         */
        protected string Total(string column, string display) => display;

        protected int Total(string column, Func<int, int> display)
        {

            //$sum = $this->query->sum($column);
            var sum = 10;
            if (display != null)
            {
                return display.Invoke(sum);
            }

            return sum;
        }

        public void SetVisibleColumns(List<GColumn> columns)
        {
            visibleColumns = columns;
        }

        public List<GColumn> GetVisibleColumns()
        {
            if (visibleColumns != null && visibleColumns.Count > 0)
            {
                return visibleColumns;
            }

            return Grid.VisibleColumns();
        }
        public override string Render() => "";

        public HtmlContent Render(bool t)
        {
            var columns = GetVisibleColumns().Select((column) =>
            {
                var name = column.GetName();

                var total = "";

                if (this.columns.ContainsKey(name))
                {
                    total = Total(name, this.columns[name]).ToString();
                }

                return new Dictionary<string, string> {
                    { "class", column.GetClassName() },
                    { "value", total },
                };
            });

            return new HtmlContent
            {
                Name = HtmlContentType.Component,
                Value = "TotalRow",
                Data = new Dictionary<string, dynamic> { { "columns", columns } },
            };
        }
    }
}
