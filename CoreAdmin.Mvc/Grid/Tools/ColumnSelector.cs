﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.ViewComponents.Components;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class ColumnSelector : AbstractTool
    {
        public const string SELECT_COLUMN_NAME = "_columns_";

        protected static List<string> Ignored = new()
        {
            GColumn.SELECT_COLUMN_NAME,
            GColumn.ACTION_COLUMN_NAME,
        };

        public ColumnSelector(BGrid grid)
        {
            Grid = grid;
        }

        protected Dictionary<string, string> GetGridColumns()
        {
            return Grid.Columns().Where(column => !Ignored.Contains(column.GetName())).Select((column) =>
            {
                return KeyValuePair.Create(column.GetName(), column.GetLabel());
            }).ToDictionary(x => x.Key, x => x.Value);
        }

        public static void Ignore(string name) => Ignore(new List<string> { name });
        public static void Ignore(List<string> name)
        {
            Ignored.AddRange(name);
            Ignored = Ignored.Distinct().ToList();
        }

        public override string Render() => "";

        public HtmlContent Render(bool t)
        {
            if (!Grid.ShowColumnSelector())
            {
                return null;
            }

            return Admin.Component(nameof(GridColumnSelector), new Dictionary<string, dynamic> {
                {"columns"  ,GetGridColumns() },
                {"visible"  ,Grid.VisibleColumnNames() },
                { "defaults", Grid.GetDefaultVisibleColumnNames() },
            });
        }
    }
}
