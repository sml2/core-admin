﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class BatchDelete : BatchAction
    {
        public BatchDelete(string title)
        {
            Title = title;
        }

        /**
         * Script of batch delete action.
         */
        public override string Script()
        {
            var trans = new Dictionary<string, string> {
               {"delete_confirm" ,Admin.Trans("delete_confirm") },
               {"confirm"        ,Admin.Trans("confirm") },
               { "cancel"        , Admin.Trans("cancel") },
            };

            return @"
    
$('" + GetElementClass() + @"').on('click', function() {

                swal({
                title: """ + trans["delete_confirm"] + @""",
        type: ""warning"",
        showCancelButton: true,
        confirmButtonColor: ""#DD6B55"",
        confirmButtonText: """ + trans["confirm"] + @""",
        showLoaderOnConfirm: true,
        cancelButtonText: """ + trans["cancel"] + @""",
        preConfirm: function() {
                        return new Promise(function(resolve) {
                $.ajax({
                    method: 'delete',
                    url: '"+resource+@"/' + $.admin.grid.selected().join(),
                    data: {
                        _method:'delete',
                        _token:'{$this->getToken()}'
                    },
                    success: function (data) {
                        $.pjax.reload('#pjax-container');

                        resolve(data);
                    }
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object')
        {
            if (data.status)
            {
                swal(data.message, '', 'success');
            }
            else
            {
                swal(data.message, '', 'error');
            }
        }
    });
});

";
        }
    }
}
