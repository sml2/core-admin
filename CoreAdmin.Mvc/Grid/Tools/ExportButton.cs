﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class ExportButton : AbstractTool
    {
        private HttpContext _context;
        public ExportButton(BGrid grid, HttpContext context)
        {
            Grid = grid;
            _context = context;
        }

        /**
         * Set up script for export button.
         */
        protected void SetUpScripts()
        {
            var script = $@"
    
$('.{Grid.GetExportSelectedName()}').click(function(e) {{
                e.preventDefault();

                var rows = $.admin.grid.selected().join();

                if (!rows)
                {{
                    return false;
                }}

                var href = $(this).attr('href').replace('__rows__', rows);
                location.href = href;
            }});

            ";

            Admin.Script(script);
        }

        /**
         * Render Export button.
         *
         * @return string
         */
        public override string Render()
        {
            if (!Grid.ShowExportBtn())
            {
                return "";
            }

            SetUpScripts();

            var trans = new Dictionary<string, string>
            {
                {"export", Admin.Trans("export")},
                {"all", Admin.Trans("all")},
                {"current_page", Admin.Trans("current_page")},
                {"selected_rows", Admin.Trans("selected_rows")},
            };
            var page = _context.Request.Get("page", "1");

            return $@"

    <div class=""btn-group pull-right"" style=""margin-right: 10px"">
    <a href = ""{Grid.GetExportUrl("all")}"" target=""_blank"" class=""btn btn-sm btn-twitter"" title=""{trans["export"]}""><i class=""fa fa-download""></i><span class=""hidden-xs""> {trans["export"]
                }</span></a>
    <button type = ""button"" class=""btn btn-sm btn-twitter dropdown-toggle"" data-toggle=""dropdown"">
        <span class=""caret""></span>
        <span class=""sr-only"">Toggle Dropdown</span>
    </button>
    <ul class=""dropdown-menu"" role=""menu"">
        <li><a href=""{Grid.GetExportUrl("all")}"" target=""_blank"">{trans["all"]
                }</a></li >

<li ><a href = ""{Grid.GetExportUrl("page", page)}"" target = ""_blank"" >{trans["current_page"]}</a></li>

<li><a href=""{Grid.GetExportUrl("selected", "__rows__")}"" target = ""_blank"" class='{Grid.GetExportSelectedName()}' >{trans["selected_rows"]}</a></li>

</ul>
</div>
";
        }
    }
}