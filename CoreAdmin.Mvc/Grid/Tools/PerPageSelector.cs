﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class PerPageSelector : AbstractTool
    {
        private readonly HttpContext _context;

        protected int PerPage;

        protected const string PerPageName = "pageSize";

        public PerPageSelector(BGrid grid, HttpContext context)
        {
            _context = context;
            Grid = grid;

            Initialize();
        }

        protected void Initialize()
        {
            PerPage = Convert.ToInt32(_context.Request.Get(
                PerPageName,
                Grid.perPage.ToString()
            ));
            // 如果用户输入分页不在预设内，寻找最近的
            if (!Grid.perPages.Contains(PerPage))
            {
                PerPage = Grid.perPages.Closest(PerPage);
            }
            Grid.Paginate(PerPage);
        }

        public IEnumerable<int> GetOptions()
        {
            return Grid.perPages.Distinct().ToList();
        }

        public override string Render()
        {
            Admin.Script(Script());

            var options = string.Join("\r\n", GetOptions().Select((option) =>
            {
                var selected = option == PerPage ? "selected" : "";
                var url = _context.Request.FullUrlWithQuery(PerPageName, option);

                return $"<option value=\"{url}\" {selected}>{option}</option>";
            }));

            var trans = new Dictionary<string, string>{
            { "show"    , Admin.Trans("show") },
            { "entries" , Admin.Trans("entries") },
        };

            return $@"

            <label class=""control-label pull-right"" style=""margin-right: 10px; font-weight: 100;"">
                <small>{trans["show"]}</small>&nbsp;
                <select class=""input-sm {Grid.GetPerPageName()}"" name=""per-page"">
                    {options}
                </select>
                &nbsp;<small>{trans["entries"]}</small>
            </label>
            ";
        }

        protected string Script()
        {
            return $@"

            $('.{Grid.GetPerPageName()}').on(""change"", function(e) {{
                $.pjax({{ url: this.value, container: '#pjax-container'}});
            }});

            ";
        }
    }
}
