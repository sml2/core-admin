﻿namespace CoreAdmin.Mvc.Grid.Tools
{
    public class CreateButton : AbstractTool
    {
        private static readonly string NewTrans = Admin.Trans("new");
        public CreateButton(BGrid grid)
        {
            Grid = grid;
        }

        public override string Render()
        {
            return !Grid.ShowCreateBtn() ? string.Empty : $@"
<div class=""btn-group pull-right grid-create-btn"" style=""margin-right: 10px"">
    <a href=""{Grid.GetCreateUrl()}"" class=""btn btn-sm btn-success"" title=""{NewTrans}"">
        <i class=""fa fa-plus""></i><span class=""hidden-xs"">&nbsp;&nbsp;{NewTrans}</span>
    </a>
</div>
";
        }
    }
}
