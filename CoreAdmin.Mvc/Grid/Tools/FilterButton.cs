﻿using CoreAdmin.Lib;
using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class FilterButton : AbstractTool
    {
        public override string Render() => "";
        public override HtmlContent Render(bool b)
        {
            var label = "";
            var filter = Grid.GetFilter();
            var scope = filter.GetCurrentScope();
            if (scope != null)
            {
                label = $"&nbsp;{scope.Call("GetLabel")}&nbsp;";
            }

            return Admin.Component("~/Views/Filter/Button.cshtml", new()
            {
                { "scopes", filter.Scopes },
                { "label", label },
                { "cancel", filter.UrlWithoutScopes() },
                { "btn_class", Str.Uniqid() + "-filter-btn" },
                { "expand", filter.expand },
                { "filter_id", filter.GetFilterID() },
            });
        }
    }
}
