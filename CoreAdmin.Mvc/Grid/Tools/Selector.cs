﻿using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class Selector : IRenderable
    {
        protected Dictionary<string, dynamic> selectors;

        /**
         * @var array
         */
        protected static Dictionary<string, dynamic> selected;

        public Selector()
        {
            selectors = new();
        }

        public Selector Select(string column, Dictionary<string, dynamic> options, Action query) => Select(column, "", options, query);
        public Selector Select(string column, string label, Dictionary<string, dynamic> options, Action query)
        {
            return AddSelector(column, label, options, query);
        }

        public Selector SelectOne(string column, string label, Dictionary<string, dynamic> options, Action query = null)
        {
            return AddSelector(column, label, options, query, "one");
        }

        protected Selector AddSelector(string column, string label, Dictionary<string, dynamic> options, Action query = null, string type = "many")
        {
            //$label = __(Str::title($column));

            selectors[column] = new Dictionary<string, dynamic>()
        {
            { "lable", label },
            { "options", options },
            { "type", type },
            { "query", query },
        };

            return this;
        }

        public Dictionary<string, dynamic> GetSelectors()
        {
            return selectors;
        }

        public static Dictionary<string, dynamic> ParseSelected()
        {
            if (selected != null)
            {
                return selected;
            }

            //$selected = request('_selector', []);

            //if (!is_array($selected))
            //{
            //    return [];
            //}

            //$selected = array_filter($selected, function($value) {
            //    return !is_null($value);
            //});

            //foreach ($selected as &$value) {
            //    $value = explode(',', $value);
            //}

            //return static::$selected = $selected;
            return null;
        }

        /**
         * @param string $column
         * @param mixed  $value
         * @param bool   $add
         *
         * @return string
         */
        //public static function url($column, $value = null, $add = false)
        //{
        //    $query = request()->query();
        //    $selected = static::parseSelected();

        //    $options = Arr::get($selected, $column, []);

        //    if (is_null($value)) {
        //        Arr::forget($query, "_selector.{$column}");

        //        return request()->fullUrlWithQuery($query);
        //    }

        //    if (in_array($value, $options)) {
        //        array_delete($options, $value);
        //    } else {
        //        if ($add) {
        //            $options = [];
        //        }

        //        array_push($options, $value);
        //    }

        //    if (!empty($options)) {
        //        Arr::set($query, "_selector.{$column}", implode(',', $options));
        //    } else {
        //        Arr::forget($query, "_selector.{$column}");
        //    }

        //    return request()->fullUrlWithQuery($query);
        //}

        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public HtmlContent Render()
        {
            return Admin.Component("admin::grid.selector", new()
            {
                { "selectors", selectors },
                { "selected", ParseSelected() },
            });
        }
    }
}
