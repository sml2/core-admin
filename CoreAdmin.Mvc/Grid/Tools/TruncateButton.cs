﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Grid.Tools;

namespace CoreAdmin.Mvc.Grid.Tools
{
    /// <summary>
    /// 清空数据表按钮
    /// </summary>
    public class TruncateButton : AbstractTool
    {
        private static readonly Dictionary<string, string> Trans = new()
        {
            {"truncate_confirm", Admin.Trans("truncate_confirm")},
            {"confirm", Admin.Trans("confirm")},
            {"cancel", Admin.Trans("cancel")},
            {"truncate", Admin.Trans("truncate")},
        };

        public TruncateButton(BGrid grid)
        {
            Grid = grid;
        }


        private string Script()
        {
            return @"
    
$('." + GetElementClass() + @"').on('click', function() {

                swal({
                title: """ + Trans["truncate_confirm"] + @""",
        type: ""warning"",
        showCancelButton: true,
        confirmButtonColor: ""#DD6B55"",
        confirmButtonText: """ + Trans["confirm"] + @""",
        showLoaderOnConfirm: true,
        cancelButtonText: """ + Trans["cancel"] + @""",
        preConfirm: function() {
                        return new Promise(function(resolve) {
                $.ajax({
                    method: 'delete',
                    url: '" + Grid.Resource() + @"/',
                    data: {
                        _method:'delete',
                        _token:'{$this->getToken()}'
                    },
                    success: function (data) {
                        $.pjax.reload('#pjax-container');

                        resolve(data);
                    }
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object')
        {
            if (data.status)
            {
                swal(data.message, '', 'success');
            }
            else
            {
                swal(data.message, '', 'error');
            }
        }
    });
});

";
        }

        private string GetElementClass()
        {
            return "grid-truncate-btn";
        }

        public override string Render()
        {
            if (!Grid.ShowTruncateBtn())
            {
                return string.Empty;
            }

            Admin.Script(Script());

            return $@"
<div class=""btn-group pull-right {GetElementClass()}"" style=""margin-right: 10px"">
    <a class=""btn btn-sm btn-danger"" title=""{Trans["truncate"]}"">
        <i class=""fa fa-trash""></i><span class=""hidden-xs"">&nbsp;&nbsp;{Trans["truncate"]}</span>
    </a>
</div>
";
        }
    }
}

namespace CoreAdmin.Mvc
{
    /// <summary>
    /// 清空按钮扩展
    /// </summary>
    public static class GridTruncateButtonExtension
    {
        /// <summary>
        /// 是否禁用清空按钮
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="disable"></param>
        /// <returns></returns>
        public static BGrid DisableTruncateButton(this BGrid grid, bool disable = true)
        {
            return grid.Option("show_truncate_btn", !disable);
        }
        
        /// <summary>
        /// 展示清空按钮
        /// </summary>
        /// <returns></returns>
        public static bool ShowTruncateBtn(this BGrid grid)
        {
            return grid.Option("show_truncate_btn");
        }
        
        /// <summary>
        /// 渲染清空按钮
        /// </summary>
        /// <returns></returns>
        public static string RenderTruncateButton(this BGrid grid)
        {
            return new TruncateButton(grid).Render();
        }
    }
}