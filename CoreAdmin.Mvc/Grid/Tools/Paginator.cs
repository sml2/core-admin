﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.ViewComponents.Grid;
using System.Collections.Generic;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class Paginator : AbstractTool
    {
        protected ListPaginator<IIdexerModel> paginator => Grid.DataPaginator;

        protected bool perPageSelector;
        public Paginator(BGrid grid, bool perPageSelector = true)
        {
            Grid = grid;
            this.perPageSelector = perPageSelector;
            InitPaginator();
        }

        protected void InitPaginator()
        {
            //$this->paginator = Grid.Model()->eloquent();
            //if ($this->paginator instanceof LengthAwarePaginator) {
            //$this->paginator->appends(request()->all());
            //}
        }

        protected HtmlContent PaginationLinks()
        {
            return new()
            {
                Name = HtmlContentType.Component,
                Value = nameof(Pagination),
                Data = new Dictionary<string, object> { { "paginator", paginator } }
            };
        }

        protected HtmlContent PerPageSelector()
        {
            if (!perPageSelector)
            {
                return null;
            }
            return new()
            {
                Name = HtmlContentType.Str,
                Value = new PerPageSelector(Grid, Grid.Context).Render(),
            };
        }

        protected HtmlContent PaginationRanger()
        {
            var parameters = new Dictionary<string, uint> {
                {"first" , paginator.FirstItem },
                {"last"  , paginator.LastItem },
                { "total" , paginator.Total },
            };

            ////$parameters = collect($parameters)->flatMap(function($parameter, $key) {
            ////        return [$key => "<b>$parameter</b>"];
            ////    });

            return new()
            {
                Name = HtmlContentType.Str,
                Value = Admin.Trans("pagination.range", paginator.FirstItem, paginator.LastItem, paginator.Total)
            };
        }

        public override string Render() => "";
        public List<HtmlContent> Render(bool t)
        {
            if (!Grid.ShowPagination())
            {
                return null;
            }

            return new()
            {
                PaginationRanger(),
                PaginationLinks(),
                PerPageSelector(),
            };
        }
    }
}
