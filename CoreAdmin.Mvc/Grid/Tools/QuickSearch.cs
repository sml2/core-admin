﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class QuickSearch : AbstractTool
    {

        protected string view = "admin::grid.quick-search";

        protected string placeholder;

        public QuickSearch Placeholder(string text = "")
        {
            placeholder = text;

            return this;
        }

        public override string Render() => "";

        public HtmlContent Render(bool t)
        {
            //$query = request()->query();

            //Arr::forget($query, HasQuickSearch::$searchKey);

            //$vars = [
            //    'action'      => request()->url().'?'.http_build_query($query),
            //    'key'         => HasQuickSearch::$searchKey,
            //    'value'       => request(HasQuickSearch::$searchKey),
            //    'placeholder' => $this->placeholder,
            //];

            //return view($this->view, $vars);
            return null;
        }
    }
}
