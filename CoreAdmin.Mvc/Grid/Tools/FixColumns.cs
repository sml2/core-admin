﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class FixColumns
    {

        protected BGrid grid;

        protected int head;

        protected int tail;

        protected List<GColumn> left;

        protected List<GColumn> right;

        protected string view = "admin::grid.fixed-table";

        public FixColumns(BGrid grid, int head, int tail = -1)
        {
            this.grid = grid;
            this.head = head;
            this.tail = tail;
            left = new();
            right = new();
        }

        public List<GColumn> LeftColumns()
        {
            return left;
        }

        public List<GColumn> RightColumns()
        {
            return right;
        }

        public Action<BGrid> Apply()
        {
            grid.SetView(view, new()
            {
                { "allName", grid.GetSelectAllName() },
                { "rowName", grid.GetGridRowName() },
            });

            return (grid) =>
            {
                if (head > 0)
                {
                    left = grid.VisibleColumns().Skip(head).ToList();
                }

                if (tail < 0)
                {
                    right = grid.VisibleColumns().Skip(tail).ToList();
                }
            };
        }
    }
}
