﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public abstract class AbstractTool : IRenderable
    {

        public BGrid Grid { get; protected set; }

        protected bool disabled = false;

        public AbstractTool Disable(bool disable = true)
        {
            disabled = disable;
            return this;
        }

        public bool Allowed { get => !disabled; }

        public AbstractTool SetGrid(BGrid grid)
        {
            Grid = grid;
            return this;
        }

        public abstract string Render();
        public virtual HtmlContent Render(bool t) => null;


        public override string ToString()
        {
            return Render();
        }
    }
}
