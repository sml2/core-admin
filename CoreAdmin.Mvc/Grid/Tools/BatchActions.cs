﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public class BatchActions : AbstractTool
    {

        protected List<BatchAction> actions;

        protected bool enableDelete = true;

        private bool holdAll = false;

        public BatchActions()
        {
            actions = new();

            AppendDefaultAction();
        }

        protected void AppendDefaultAction()
        {
            Add(new BatchDelete(Admin.Trans("batch_delete")));
        }

        public BatchActions DisableDelete(bool disable = true)
        {
            enableDelete = !disable;

            return this;
        }


        public BatchActions DisableDeleteAndHodeSelectAll()
        {
            enableDelete = false;

            holdAll = true;

            return this;
        }
        public BatchActions Add(string title, BatchAction action)
        {
            action.SetTitle(title);
            return Add(action);
        }

        public BatchActions Add(BatchAction action)
        {
            var id = actions.Count;
            var type = action.GetType();
            var method = type.GetMethod("SetId");
            if (method != null)
            {
                method.Invoke(action, new object[] { id });
            }

            actions.Add(action);

            return this;
        }

        protected void AddActionScripts()
        {
            actions.ForEach((action) =>
            {
                action.SetGrid(Grid);

                var type = action.GetType();
                var method = type.GetMethod("Script");
                if (method != null)
                {
                    Admin.Script((string)method.Invoke(action, null));
                }
            });
        }

        override public string Render() => "";

        public override HtmlContent Render(bool t)
        {
            if (!enableDelete)
            {
                // TODO: 不确定是否会重新排序
                actions.RemoveAt(0);
            }

            AddActionScripts();

            return Admin.Component("~/Views/Grid/BatchActions.cshtml", new()
            {
                { "all", Grid.GetSelectAllName() },
                { "row", Grid.GetGridRowName() },
                { "actions", actions },
                { "holdAll", holdAll },

            });
        }
    }
}
