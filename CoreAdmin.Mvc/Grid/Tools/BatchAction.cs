﻿using CoreAdmin.Mvc.Widgets;

namespace CoreAdmin.Mvc.Grid.Tools
{
    public abstract class BatchAction : IRenderable
    {
        public int Id { get; }

        public string Title { get; protected set; }

        protected string resource;

        protected BGrid grid;

        public BatchAction SetTitle(string title)
        {
            Title = title;
            return this;
        }

        public void SetGrid(BGrid grid)
        {
            this.grid = grid;

            resource = grid.Resource();
        }

        public string GetToken()
        {
            //return csrf_token();
            // TODO csrf
            return "csrf";
        }

        public string GetElementClass(bool dotPrefix = true)
        {
            return $"{(dotPrefix ? "." : "")}{grid.GetGridBatchName()}-{Id}";
        }

        public string Render()
        {
            return Title;
        }

        abstract public dynamic Script();
    }
}
