﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public class NotEqual : AbstractFilter
    {
        public Dictionary<string, dynamic> Condition(Dictionary<string, dynamic> inputs)
        {
            var value = inputs?[Column];

            if (null == value)
            {
                return null;
            }

            this.Value = value;

            return BuildCondition(Column, "!=", value);
        }
    }
}
