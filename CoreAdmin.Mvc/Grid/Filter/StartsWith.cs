﻿namespace CoreAdmin.Mvc.Grid.Filter
{
    public class StartsWith : Like
    {
        protected string exprFormat = "{value}%";
    }
}
