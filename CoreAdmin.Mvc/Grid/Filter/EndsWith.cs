﻿namespace CoreAdmin.Mvc.Grid.Filter
{
    public class EndsWith : Like
    {
        protected new string exprFormat = "%{value}";
    }
}
