﻿using CoreAdmin.Mvc.Grid.Filter.Presenter;
using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Struct.Filter;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public abstract class AbstractFilter
    {
        public string Id { get; set; }

        protected string Label;

        protected object Value;

        protected string defaultValue;

        protected string Column;

        protected Presenter.Presenter presenter;

        protected string query = "where";

        protected BFilter parent;

        protected string View = "~/Views/Filter/Where.cshtml";

        public List<string> group;

        protected bool ignore = false;

        public AbstractFilter() : this("")
        {
        }

        public AbstractFilter(string column, string label = "")
        {
            this.Column = column;
            this.Label = FormatLabel(label);
            Id = FormatId(column);

            SetupDefaultPresenter();
        }

        protected void SetupDefaultPresenter()
        {
            SetPresenter(new Text(Label));
        }

        protected string FormatLabel(string label)
        {
            label ??= $"{Column.First().ToString().ToUpper()}{Column[1..]}";

            return label.Replace(".", " ").Replace("_", "");
        }

        protected string FormatName(string column)
        {
            var columns = column.Split('.');
            var name = columns[0];
            if (columns.Length > 1)
            {
                name = columns.Skip(1).Aggregate(name, (current, col) => current + $"[{col}]");
            }

            var parenName = parent.GetName();
            return !string.IsNullOrEmpty(parenName) ? $"{parenName}_{name}" : name;
        }

        protected static string FormatId(string columns)
        {
            return columns.Replace('.', '_');
        }

        public void SetParent(BFilter filter)
        {
            parent = filter;
        }

        //        /**
        //         * Get siblings of current filter.
        //         *
        //         * @param null $index
        //         *
        //         * @return AbstractFilter[]|mixed
        //         */
        //        public function siblings($index = null)
        //        {
        //            if (!is_null($index))
        //            {
        //                return Arr::get($this->parent->filters(), $index);
        //            }

        //            return $this->parent->filters();
        //        }

        //        /**
        //         * Get previous filter.
        //         *
        //         * @param int $step
        //         *
        //         * @return AbstractFilter[]|mixed
        //         */
        //        public function previous($step = 1)
        //        {
        //            return $this->siblings(
        //                array_search($this, $this->parent->filters()) - $step
        //            );
        //        }

        //        /**
        //         * Get next filter.
        //         *
        //         * @param int $step
        //         *
        //         * @return AbstractFilter[]|mixed
        //         */
        //        public function next($step = 1)
        //        {
        //            return $this->siblings(
        //                array_search($this, $this->parent->filters()) + $step
        //            );
        //        }

        /// <summary>
        /// 搜索条件
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public KeyValuePair<string, string>? Condition(Dictionary<string, object> inputs)
        {
            if (ignore) {
                return null;
            }

            if (!inputs.ContainsKey(Column))
            {
                return null;
            }

            Value = inputs[Column];

            return BuildCondition(Column, Value);
        }

        //        /**
        //         * Ignore this query filter.
        //         *
        //         * @return $this
        //         */
        //        public function ignore()
        //        {
        //        $this->ignore = true;

        //            return $this;
        //        }

        //        /**
        //         * Select filter.
        //         *
        //         * @param array|\Illuminate\Support\Collection $options
        //         *
        //         * @return Select
        //         */
        //        public function select($options = [])
        //        {
        //            return $this->setPresenter(new Select($options));
        //        }

        //        /**
        //         * @param array|\Illuminate\Support\Collection $options
        //         *
        //         * @return MultipleSelect
        //         */
        //        public function multipleSelect($options = [])
        //        {
        //            return $this->setPresenter(new MultipleSelect($options));
        //        }

        //        /**
        //         * @param array|\Illuminate\Support\Collection $options
        //         *
        //         * @return Radio
        //         */
        //        public function radio($options = [])
        //        {
        //            return $this->setPresenter(new Radio($options));
        //        }

        //        /**
        //         * @param array|\Illuminate\Support\Collection $options
        //         *
        //         * @return Checkbox
        //         */
        //        public function checkbox($options = [])
        //        {
        //            return $this->setPresenter(new Checkbox($options));
        //        }

        //        /**
        //         * Datetime filter.
        //         *
        //         * @param array|\Illuminate\Support\Collection $options
        //         *
        //         * @return DateTime
        //         */
        //        public function datetime($options = [])
        //        {
        //            return $this->setPresenter(new DateTime($options));
        //        }

        //        /**
        //         * Date filter.
        //         *
        //         * @return DateTime
        //         */
        //        public function date()
        //        {
        //            return $this->datetime(['format' => 'YYYY-MM-DD']);
        //        }

        //        /**
        //         * Time filter.
        //         *
        //         * @return DateTime
        //         */
        //        public function time()
        //        {
        //            return $this->datetime(['format' => 'HH:mm:ss']);
        //        }

        //        /**
        //         * Day filter.
        //         *
        //         * @return DateTime
        //         */
        //        public function day()
        //        {
        //            return $this->datetime(['format' => 'DD']);
        //        }

        //        /**
        //         * Month filter.
        //         *
        //         * @return DateTime
        //         */
        //        public function month()
        //        {
        //            return $this->datetime(['format' => 'MM']);
        //        }

        //        /**
        //         * Year filter.
        //         *
        //         * @return DateTime
        //         */
        //        public function year()
        //        {
        //            return $this->datetime(['format' => 'YYYY']);
        //        }
        
        protected Presenter.Presenter SetPresenter(Presenter.Presenter presenter)
        {
            presenter.SetParent(this);

            return this.presenter = presenter;
        }

        protected Presenter.Presenter Presenter()
        {
            return presenter;
        }


        public AbstractFilter Default(string defaultValue = null)
        {
            if (!string.IsNullOrEmpty(defaultValue))
            {
                this.defaultValue = defaultValue;
            }

            return this;
        }

        public string GetFilterBoxId()
        {
            return parent != null ? parent.GetFilterID() : "filter-box";
        }

        //    /**
        //     * Set element id.
        //     *
        //     * @param string $id
        //     *
        //     * @return $this
        //     */
        //    public function setId($id)
        //    {
        //        $this->id = $this->formatId($id);

        //        return $this;
        //    }

        public string GetColumn()
        {
            var parentName = parent.GetName();

            return !string.IsNullOrEmpty(parentName) ? $"{parentName}_{Column}" : Column;
        }

        protected KeyValuePair<string, string> BuildCondition(params object[] param)
        {
            var column = this.Column.Split(".");

            if (column.Length == 1)
            {
                return KeyValuePair.Create(query, $"{param[0]}={param[1]}");
            }

            throw new NotImplementedException("关联查询未实现");
            //return BuildRelationQuery(param);
            //return null;
        }

        protected KeyValuePair<string, object[]> BuildRelationQuery(object[] args)
        {
            var column = this.Column.Split(".");
            var relation = column.First();
            args[0] = column.Last();
            Action<string> action = (relation) =>
            {
                // TODO 类名  query 方法
                //call_user_func_array([$relation, $this->query], $args);
            };
            return KeyValuePair.Create("whereHas", new object[] {relation, action});
        }
        
        protected FilterViewModel Variables()
        {
            return new FilterViewModel()
            {
                Id = Id,
                Column = Column,
                Name = FormatName(Column),
                Label = Label,
                Value = Value ?? defaultValue,
                Presenter = presenter,
                PresenterData = presenter.Variables()
            };
        }

        public HtmlContent Render()
        {
            return new()
            {
                Name = HtmlContentType.Partial,
                Value = View,
                Form = Variables()
            };
        }

        //    /**
        //     * Render this filter.
        //     *
        //     * @return \Illuminate\View\View|string
        //     */
        //    public function __toString()
        //    {
        //        return $this->render();
        //    }

        //    /**
        //     * @param $method
        //     * @param $params
        //     *
        //     * @throws \Exception
        //     *
        //     * @return mixed
        //     */
        //    public function __call($method, $params)
        //    {
        //        if (method_exists($this->presenter, $method))
        //        {
        //            return $this->presenter()->{$method} (...$params);
        //        }

        //        throw new \Exception('Method "'.$method.'" not exists.');
        //    }
    }
}