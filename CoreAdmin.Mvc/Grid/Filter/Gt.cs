﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Gt : AbstractFilter
    {

        protected string view = "admin::filter.gt";

        public string Condition(Dictionary<string, dynamic> inputs)
        {
            var value = inputs?["value"];

            if (null == value)
            {
                return "";
            }

            this.Value = value;

            return BuildCondition(Column, ">=", value);
        }
    }
}
