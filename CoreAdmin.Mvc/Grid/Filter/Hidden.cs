﻿namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Hidden : AbstractFilter
    {
        protected string name;

        protected string value;

        public Hidden(string name, string value)
        {
            this.name = name;

            this.value = value;
        }

        public string Condition(dynamic inputs)
        {
            return null;
        }

        public string Render()
        {
            return $"<input type='hidden' name='{name}' value='{value}'>";
        }
    }
}
