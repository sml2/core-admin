﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Lt : AbstractFilter
    {
        protected string view = "admin::filter.lt";

        public Dictionary<string, dynamic> Condition(Dictionary<string, dynamic> inputs)
        {
            var value = inputs[Column];

            if (value == null)
            {
                return null;
            }

            //$this->value = $value;

            return BuildCondition(Column, "<=", value);
        }
    }
}
