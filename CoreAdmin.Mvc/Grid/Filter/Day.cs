﻿namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Day : Date
    {
        protected new string query = "whereDay";

        protected new string fieldName = "day";
    }
}
