﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Filter.Presenter
{
    class MultipleSelect: Select
    {
        public MultipleSelect(Dictionary<string, dynamic> options) : base(options)
        {

        }

    public MultipleSelect LoadMore(string target, string resourceUrl, string idField = "id", string textField = "text")
    {
        var column = Filter.GetColumn();

        var script = $@"

$(document).on('change', "".{GetClass(column)}"", function () {{
    var target = $(this).closest('form').find("".{GetClass(target)}"");
     var ids = $(this).find(""option:selected"").map(function(index,elem) {{
            return $(elem).val();
        }}).get().join(',');
    $.get(""{resourceUrl}?q=""+ids, function (data) {{
        target.find(""option"").remove();
        $.each(data, function (i, item) {{
            $(target).append($('<option>', {{
                value: item.{idField},
                text : item.{textField}
            }}));
        }});
        
        $(target).trigger('change');
    }});
}});
";

        Admin.Script(script);

        return this;
    }
    }
}
