﻿using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Mvc.Struct.Filter;

namespace CoreAdmin.Mvc.Grid.Filter.Presenter
{
    class DateTime : Presenter
    {

        protected Dictionary<string, dynamic> options = new();

        protected string format = "YYYY-MM-DD HH:mm:ss";

        public DateTime(Dictionary<string, dynamic> options)
        {
            this.options = GetOptions(options);
        }

        protected Dictionary<string, dynamic> GetOptions(Dictionary<string, dynamic> options)
        {
            options["format"] = options["format"] ?? format;
            //options["locale"] = options["locale"] ?? config("app.locale");

            return options;
        }

        protected void Prepare()
        {
            var script = $"$('#{Filter.Id}').datetimepicker({JsonSerializer.Serialize(options)});";

            Admin.Script(script);
        }

        public override PresenterViewModel Variables()
        {
            Prepare();
            return new PresenterViewModel()
            {
                Group = Filter.@group
            };
        }
    }
}
