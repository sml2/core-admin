﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Filter.Presenter
{
    class Checkbox : Radio
    {
        public Checkbox(Dictionary<string, dynamic> options) : base(options)
        {

        }

        protected void Prepare()
    {
        var script = $"$('.{Filter.Id}').iCheck({{checkboxClass:'icheckbox_minimal-blue'}});";

        Admin.Script(script);
    }
    }
}
