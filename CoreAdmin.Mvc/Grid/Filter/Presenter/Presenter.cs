﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Filter;

namespace CoreAdmin.Mvc.Grid.Filter.Presenter
{
    public abstract class Presenter
    {
        protected AbstractFilter Filter;

        public void SetParent(AbstractFilter filter)
        {
            this.Filter = filter;
        }
        
        public HtmlContent View()
        {
            return new()
            {
                Name = HtmlContentType.Partial,
                Value = "~/Views/Filter/" + GetType().Name + ".cshtml"
            };
        }

        public Presenter Default(string defaultValue)
        {
            Filter.Default(defaultValue);

            return this;
        }

        public abstract PresenterViewModel Variables();
    }
}
