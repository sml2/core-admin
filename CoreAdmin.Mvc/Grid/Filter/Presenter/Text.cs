﻿using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Mvc.Struct.Filter;

namespace CoreAdmin.Mvc.Grid.Filter.Presenter
{
    public class Text : Presenter
    {
        protected string placeholder;

        protected string icon = "pencil";

        protected string type = "text";

        public Text(string placeholder)
        {
            Placeholder(placeholder);
        }

        public override PresenterViewModel Variables()
        {
            return new()
            {
                Placeholder = placeholder,
                Icon = icon,
                Type = type,
                Group = Filter.group,
            };
        }

        public Text Placeholder(string placeholder)
        {
            this.placeholder = placeholder;

            return this;
        }

        public Text Url()
        {
            return Inputmask(new() { { "alias", "url" } }, "internet-explorer");
        }

        public Text Email()
        {
            return Inputmask(new() { { "alias", "email" } }, "envelope");
        }

        public Text Integer()
        {
            return Inputmask(new() { { "alias", "integer" } });
        }

        public Text Decimal(Dictionary<string, dynamic> options)
        {
            options.Add("alias", "decimal");
            return Inputmask(options);
        }

        public Text Currency(Dictionary<string, dynamic> options)
        {
            options.Add("alias", "currency");
            options.Add("prefix", "");
            options.Add("removeMaskOnSubmit", true);

            return Inputmask(options);
        }

        public Text Percentage(Dictionary<string, dynamic> options)
        {
            options.Add("alias", "percentage");

            return Inputmask(options);
        }

        public Text Ip()
        {
            return Inputmask(new() { { "alias", "ip" } }, "laptop");
        }

        public Text Mac()
        {
            return Inputmask(new() { { "alias", "mac" } }, "laptop");
        }

        public Text Mobile(string mask = "19999999999")
        {
            return Inputmask(new() { { "mask", mask } }, "phone");
        }

        public Text Inputmask(Dictionary<string, dynamic> options, string icon = "pencil")
        {
            var optionsStr = JsonSerializer.Serialize(options);

            Admin.Script($"$('#{Filter.GetFilterBoxId()} input.{Filter.Id}').inputmask({optionsStr});");

            this.icon = icon;

            return this;
        }
    }
}
