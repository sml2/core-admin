﻿using System.Collections.Generic;
using CoreAdmin.Mvc.Struct.Filter;

namespace CoreAdmin.Mvc.Grid.Filter.Presenter
{
    class Radio : Presenter
    {

        protected Dictionary<string, dynamic> options = new();

        protected bool inline = true;

        public Radio(Dictionary<string, dynamic> options)
        {
            this.options = options;
        }

        public Radio Stacked()
        {
            inline = false;
            return this;
        }

        protected void Prepare()
        {
            var script = $"$('.{Filter.Id}').iCheck({{radioClass:'iradio_minimal-blue'}});";

            Admin.Script(script);
        }

        public override PresenterViewModel Variables()
        {
            Prepare();
            return new()
            {
                Options = options,
                Inline = inline
            };
        }
    }
}
