﻿using CoreAdmin.Mvc.Widgets;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Scope : IRenderable
    {
        public const string QUERY_NAME = "_scope_";
        public const string SEPARATOR = "_separator_";

        public string key = "";

        public string Label { get; } = "";

        protected List<Dictionary<string, dynamic>> queries;

        /**
         * Scope constructor.
         *
         * @param $key
         * @param string $label
         */
        public Scope(string key, string label = "")
        {
            this.key = key;
            //this.label = !string.IsNullOrEmpty(label) ? label: Str::studly($key);
            // TODO studly
            Label = !string.IsNullOrEmpty(label) ? label : key;

            queries = new();
        }

        /**
         * Get model query conditions.
         *
         * @return array
         */
        public List<dynamic> Condition()
        {
            return queries.Select(d => KeyValuePair.Create(d["method"], d["arguments"])).ToList();
        }

        /**
         * @return string
         */
        public string Render()
        {
            if (key == SEPARATOR)
            {
                return "<li role=\"separator\" class=\"divider\"></li>";
            }
            // TODO url
            //$url = request()->fullUrlWithQuery([static::QUERY_NAME => $this->key]);
            var url = "";
            return $"<li><a href=\"{url}\">{Label}</a></li>";
        }

        /**
         * Set this scope as default.
         *
         * @return self
         */
        public Scope AsDefault()
        {
            // TODO url

            //if (!request()->input('_scope_'))
            //{
            //    request()->merge(['_scope_' => $this->key]);
            //}

            return this;
        }

        /**
         * @param string $method
         * @param array  $arguments
         *
         * @return $this
         */
        public Scope Call(string method, params object[] arguments)
        {
            queries.Add(new() { { "method", method }, { "arguments", arguments } });

            return this;
        }
    }
}
