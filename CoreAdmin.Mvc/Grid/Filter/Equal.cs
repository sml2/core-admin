﻿namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Equal : AbstractFilter
    {
        public Equal(string column, string label = "") : base(column, label) {}
    }
}
