﻿namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Ilike : Like
    {
        protected new string Operator = "ilike";
    }
}
