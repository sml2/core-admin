﻿using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Like : AbstractFilter
    {
        protected string exprFormat = "%{value}%";

        protected string Operator = "like";

        /**
         * Get condition of this filter.
         *
         * @param array $inputs
         *
         * @return array|mixed|void
         */
        public Dictionary<string, dynamic> Condition(Dictionary<string, dynamic> inputs)
        {
            var value = inputs?[Column];

            if (value is List<string> list)
            {
                value = list.Where(v => !string.IsNullOrEmpty(v));
            }

            if (null == value)
            {
                return null;
            }

            this.Value = value;

            var expr = exprFormat.Replace("{value}", value);

            return BuildCondition(Column, Operator, expr);
        }
    }
}
