﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Filter.Layout
{
    public class Column
    {
        public List<AbstractFilter> Filters { get; protected set; }

        public int Width { get; set; }

        public Column(int width = 12)
        {
            Width = width;
            Filters = new();
        }

        public void AddFilter(AbstractFilter filter)
        {
            Filters.Add(filter);
        }

        public void RemoveFilterByID(int id)
        {
            Filters = Filters.Where(f => Convert.ToInt32(f.Id) != id).ToList();
        }
    }
}
