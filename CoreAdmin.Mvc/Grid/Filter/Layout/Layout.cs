﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Filter.Layout
{
    public class Layout
    {
        protected List<Column> columns;

        protected Column current;

        protected BFilter parent;

        public Layout(BFilter filter)
        {
            parent = filter;

            current = new Column();

            columns = new();
        }


        public void AddFilter(AbstractFilter filter)
        {
            current.AddFilter(filter);
        }

        public void Column(int width, Action<BFilter> closure)
        {
            Column column;
            if (columns.Count == 0)
            {
                column = current;

                column.Width = width;
            }
            else
            {
                column = new Column(width);

                current = column;
            }

            columns.Add(column);

            closure.Invoke(parent);
        }

        public List<Column> Columns()
        {
            if (columns.Count == 0)
            {
                columns.Add(current);
            }

            return columns;
        }

        public void RemoveDefaultIDFilter()
        {
            columns.RemoveAt(0);
        }
    }
}
