﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Grid.Filter
{
    public class Between : AbstractFilter
    {
        public new Dictionary<string, string> Id;
        protected string view = "admin::filter.between";

        public Between(string column, string label = "") : base(column, label) { }

        /**
         * Format id.
         *
         * @param string $column
         *
         * @return array|string
         */
        public new Dictionary<string, string> FormatId(string column)
        {
            var id = column.Replace(".", "_");

            return new() { { "start", $"{id}_start" }, { "end", $"{id}_end" } };
        }

        protected Dictionary<string, string> FormatName(string column)
        {
            var columns = column.Split(".");
            string name;
            if (columns.Length == 1)
            {
                name = columns[0];
            }
            else
            {
                name = columns.First();
                foreach (var item in columns.Skip(1))
                {
                    name += $"[{item}]";
                }
            }

            return new() { { "start", $"{name}[start]" }, { "end", $"{name}[end]" } };

        }

        public KeyValuePair<string, object[]>? Condition(Dictionary<string, object> inputs)
        {
            Dictionary<string, string> value = inputs[Column] as Dictionary<string, string>;
            if (ignore)
            {
                return null;
            }

            if (null == value)
            {
                return null;
            }

            this.Value = value;
            value = value.RemoveEmpty();

            if (value.Count == 0)
            {
                return null;
            }
            throw new NotImplementedException("between 未实现");
            //if (!value.ContainsKey("start"))
            //{
            //    return BuildCondition(Column, "<=", value["end"]);
            //}

            //if (!value.ContainsKey("end"))
            //{
            //    return BuildCondition(Column, ">=", value["start"]);
            //}

            //query = "whereBetween";

            //return BuildCondition(Column, value);
        }

        /**
         * @param array $options
         *
         * @return $this
         */
        public Between Datetime(Dictionary<string, string> options)
        {
            view = "admin::filter.betweenDatetime";

            SetupDatetime(options);

            return this;
        }
        protected void SetupDatetime(Dictionary<string, string> options)
        {
            options["format"] = options?["format"] ?? "YYYY-MM-DD HH:mm:ss";
            // TODO config('app.locale')
            options["locale"] = options?["locale"] ?? "app.locale";

            var startOptions = JsonSerializer.Serialize(options);
            // TODO false
            // options["useCurrent"] = false;
            var endOptions = JsonSerializer.Serialize(options);

            var script = @"
                $('#" + Id["start"] + @"').datetimepicker(" + startOptions + @");
            $('#" + Id["end"] + @"').datetimepicker(" + endOptions + @");
            $(""#" + Id["start"] + @""").on(""dp.change"", function(e) {
                $('#" + Id["end"] + @"').data(""DateTimePicker"").minDate(e.date);
            });
            $(""#" + Id["end"] + @""").on(""dp.change"", function(e) {
                $('#" + Id["start"] + @"').data(""DateTimePicker"").maxDate(e.date);
            });
            ";

            Admin.Script(script);
        }
    }
}
