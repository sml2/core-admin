﻿using CoreAdmin.Mvc.Actions;

namespace CoreAdmin.Mvc.Grid.Actions
{
    public class Edit : RowAction
    {
        public Edit() : base()
        {
            Name = Admin.Trans("edit");
        }

        public string Href()
        {
            //return $"{GetResource()}/{parent.GetKey()}/edit";
            return "";
        }
    }
}
