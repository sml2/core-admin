﻿using CoreAdmin.Mvc.Actions;

namespace CoreAdmin.Mvc.Grid.Actions
{
    public class Show : RowAction
    {
        public override string Name { get; set; } = Admin.Trans("show");

        public string Href()
        {
            //return "{$this->getResource()}/{$this->getKey()}";
            return null;
        }
    }
}
