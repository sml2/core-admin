﻿using CoreAdmin.Mvc.Actions;

namespace CoreAdmin.Mvc.Grid.Actions
{
    public class Delete : RowAction
    {
        public override string Name { get; set; } = Admin.Trans("delete");

        /**
         * @param Model $model
         *
         * @return Response
         */
        //public function handle(Model $model)
        //{
        //    $trans = [
        //        'failed'    => trans('admin.delete_failed'),
        //        'succeeded' => trans('admin.delete_succeeded'),
        //    ];

        //    try {
        //        DB::transaction(function () use ($model) {
        //            $model->delete();
        //        });
        //    } catch (\Exception $exception) {
        //        return $this->response()->error("{$trans['failed']} : {$exception->getMessage()}");
        //    }

        //    return $this->response()->success($trans['succeeded'])->refresh();
        //}

        public void Dialog()
        {
            //Question(trans('admin.delete_confirm'), '', ['confirmButtonColor' => '#d33']);
        }
    }
}
