﻿using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Struct.Attributes;

namespace CoreAdmin.Mvc.Grid
{
    public class Row
    {
        public int Number;

        protected IIdexerModel OriginData { get; set; }
        
        public Dictionary<string, object> Data { get; set; } = new();

        protected HtmlAttributes Attributes { get; set; }

        public string Key { get; }

        public Row(int number, IIdexerModel originData, string key)
        {
            OriginData = originData;
            Number = number;
            Key = key;

            Attributes = new()
            {
                { "data-key", key },
            };
        }

        public HtmlAttributes GetRowAttributes()
        {
            return Attributes;
        }

        //public string GetColumnAttributes(string column)
        //{
        //    var attributes = GColumn.GetAttributes(column, GetKey());
        //    if (attributes.Count > 0)
        //    {
        //        return FormatHtmlAttribute(attributes);
        //    }

        //    return "";
        //}

        private string FormatHtmlAttribute(Dictionary<string, string> attributes)
        {
            List<string> attrArr = new();
            foreach (var pair in attributes)
            {
                attrArr.Add($"{pair.Key}=\"{pair.Value}\"");
            }

            return string.Join(' ', attrArr);
        }

        public void SetAttributes(string key, string value)
        {
            Attributes[key] = value;
        }

        public void Style(Dictionary<string, string> style)
        {
            var styleStr = string.Join(";", style.Select(pair => $"{pair.Key}:{pair.Value}"));
            Style(styleStr);
        }
        public void Style(string style)
        {
            Attributes["style"] = style;
        }

        public IIdexerModel Model()
        {
            return OriginData;
        }

        public string Column(string name)
        {
            if (!Data.ContainsKey(name))
            {
                Data[name] = OriginData.Get(name);
            }

            return Output(Data[name]);
        }
        public Row Column(string name, Func<Row, string, object> value)
        {
            return Column(name, value.Invoke(this, Column(name)));
        }
        public Row Column(string name, object value)
        {
            Data[name] = value;
            return this;
        }


        protected static string Output(object value)
        {
            var rs = value switch
            {
                IRenderable renderable => renderable.Render(),
                Interface.IHtmlable htmlable => htmlable.ToHtml(),
                _ => value != null ? value.ToString() : ""
            };

            //if ($value instanceof Jsonable) {
            //    rs = value->toJson();
            //}

            //if (!is_null($value) && !is_scalar($value))
            //{
            //    return sprintf('<pre>%s</pre>', var_export($value, true));
            //}

            return rs;
        }
    }
}
