﻿using CoreAdmin.Mvc.Grid.Displayers;

namespace CoreAdmin.Mvc.Grid.Selectable
{
    public class Radio : AbstractDisplayer
    {
        //public Radio(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}

        public string Display(string key)
        {
            var value = GetAttribute<string>(key);

            return $@"
<input type=""radio"" name=""item"" class=""select"" value=""{value}""/>
";
        }
    }
}
