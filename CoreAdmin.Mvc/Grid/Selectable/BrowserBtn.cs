﻿using CoreAdmin.Mvc.Widgets;

namespace CoreAdmin.Mvc.Grid.Selectable
{
    public class BrowserBtn : IRenderable
    {
        public string Render()
        {
            var text = Admin.Trans("choose");

            var html = $@"
<a href=""javascript:void(0)"" class=""btn btn-primary btn-sm pull-left select-relation"">
    <i class=""glyphicon glyphicon-folder-open""></i>
    &nbsp;&nbsp;{text}
</a>
";

            return html;
        }
    }
}
