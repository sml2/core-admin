﻿using CoreAdmin.Mvc.Grid.Displayers;

namespace CoreAdmin.Mvc.Grid.Selectable
{
    public class Checkbox : AbstractDisplayer
    {
        //public Checkbox(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}

        public string Display(string key)
        {
            var value = GetAttribute<object>(key);

            return $@"
<input type=""checkbox"" name=""item"" class=""select"" value=""{value}""/>
";
        }
    }
}
