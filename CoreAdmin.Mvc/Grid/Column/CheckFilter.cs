﻿using CoreAdmin.Lib;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Column
{
    public class CheckFilter : Filter
    {
        protected Dictionary<string, dynamic> options;

        /**
         * CheckFilter constructor.
         *
         * @param array $options
         */
        public CheckFilter(Dictionary<string, dynamic> options)
        {
            this.options = options;

            Class = new()
            {
                { "all", Str.Uniqid("column-filter-all-") },
                { "item", Str.Uniqid("column-filter-item-") },
            };
        }

        /**
         * Add a binding to the query.
         *
         * @param array $value
         * @param Model $model
         */
        //public function AddBinding($value, Model $model)
        //{
        //    if (empty($value))
        //    {
        //        return;
        //    }

        //    $model->whereIn($this->getColumnName(), $value);
        //}

        /**
         * Add script to page.
         *
         * @return void
         */
        protected void AddScript()
        {
            var script = $@"
$('.{Class["all"]}').on('ifChanged', function() {{
            if (this.checked) {{
        $('.{Class["item"]}').iCheck('check');
                }} else
                {{
        $('.{Class["item"]}').iCheck('uncheck');
                }}
                return false;
                }});

$('.{Class["item"]},.{Class["all"]}').iCheck({{
            checkboxClass: 'icheckbox_minimal-blue'
}});
            ";

            Admin.Script(script);
        }

        /**
         * Render this filter.
         *
         * @return string
         */
        public override string Render()
        {
            //$value = GetFilterValue([]);
            var value = new List<string>();

            var lists = string.Join("\r\n", options.Select((pair) =>
            {
                var _checked = value.Contains(pair.Key) ? "checked" : "";

                return $@"
<li class=""checkbox icheck"" style=""margin: 0;"">
    <label style = ""width: 100%;padding: 3px;"" >
        <input type=""checkbox"" class=""{Class["item"]}"" name=""{GetColumnName()}[]"" value=""{pair.Key}"" {_checked}/> &nbsp; &nbsp; &nbsp;
{pair.Value}
    </label>
</li>
";
            }));

            AddScript();

            var allCheck = value.Count == options.Count ? "checked" : "";
            var active = value.Count == 0 ? "" : "text-yellow";

            return $@"
<span class=""dropdown"" >
<form action=""{GetFormAction()}"" pjax-container style=""display: inline-block;"" >

  <a href=""javascript:void(0);"" class=""dropdown-toggle {active}"" data-toggle=""dropdown"" >
  
          <i class=""fa fa-filter""></i>
   
       </a>
   
       <ul class=""dropdown-menu"" role=""menu"" style=""padding: 10px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.2);left: -70px;border-radius: 0;"" >
       
               <li>
       
                   <ul style='padding: 0;'>
        
                    <li class=""checkbox icheck"" style=""margin: 0;"" >
          
                          <label style=""width: 100%;padding: 3px;"" >
           
                               <input type=""checkbox"" class=""{Class["all"]}"" {allCheck}/> &nbsp; &nbsp; &nbsp;
{Trans("all")}
                </label>
            </li>
                <li class=""divider""></li>
                {lists}
            </ul>
        </li>
        <li class=""divider""></li>
 
         <li class=""text-right"">
  
              <button class= ""btn btn-sm btn-flat btn-primary pull-left"" data-loading-text=""{Trans("search")}..."" ><i class=""fa fa-search""></i> &nbsp; &nbsp;
{Trans("search")}</button>

<span><a href=""{GetFormAction()}"" class=""btn btn-sm btn-flat btn-default""><i class=""fa fa-undo""></i></a></span>

</li>

</ul>
</form>
</span>
";
        }
    }
}
