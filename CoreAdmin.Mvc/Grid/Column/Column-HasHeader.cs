﻿using CoreAdmin.Mvc.Grid.Column;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Form.Field;
using CoreAdmin.Mvc.Grid.Filter;
using CoreAdmin.Mvc.Models;
using Date = CoreAdmin.Mvc.Grid.Filter.Date;

namespace CoreAdmin.Mvc.Grid
{
    public partial class GColumn
    {
        public Column.Filter Filter;
        protected List<object> Headers;

        public GColumn AddHeader(object header)
        {
            // TODO HEADER
            if (header is Column.Filter bFilter) {
                //bFilter.SetParent(this);
                Filter = bFilter;
            }
            Headers ??= new List<object>();
            Headers.Add(header);

            return this;
        }

        protected GColumn AddSorter(string cast = null)
        {
            // TODO MODEL
            //var sortName = grid.Model().GetSortName();
            const string sortName = Sorter.SortName;

            var sorter = new Sorter(sortName, GetName(), cast, grid.Context);

            return AddHeader(sorter);
        }

        protected GColumn AddHelp(string message)
        {
            return AddHeader(new Help(message));
        }

        public GColumn AddFilter(Dictionary<string, object> type)
        {
            return AddHeader(new CheckFilter(type));
        }

        private static readonly List<Type> InputFilters = new ()
        {
            typeof(Equal),
            typeof(Like),
            typeof(Date),
            typeof(Time),
            typeof(Datetime),
        };
        
        protected GColumn AddFilter<T>(string formal = null) where T : AbstractFilter
        {
            
            if (InputFilters.Contains(typeof(T)))
            {
                return AddHeader(new InputFilter(typeof(T)));
            }

            // if (typeof(T) == Form.Field.Range) {
            //     if (is_null($formal))
            //     {
            //     $formal = 'equal';
            //     }
            //
            //     return $this->addHeader(new RangeFilter($formal));
            // }

            return this;
        }
        
        public void BindFilterQuery(BaseModel model)
        {
            Filter?.AddBinding(grid.Context.Request.Get(GetName()), model);
        }

        public string RenderHeader()
        {
            if (Headers == null) return "";
            return string.Join("", Headers.Select((item) =>
            {
                return item switch
                {
                    IRenderable renderable => renderable.Render(),
                    Interface.IHtmlable htmlable => htmlable.ToHtml(),
                    _ => (string) item
                };
            }));
        }
    }
}
