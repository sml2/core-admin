﻿using System;
using System.Collections.Generic;
using CoreAdmin.Mvc.Grid.Displayers;

namespace CoreAdmin.Mvc.Grid
{
    public partial class GColumn
    {
    //     /**
    //  * @param string $selectable
    //  *
    //  * @return $this
    //  */
    // public function belongsTo($selectable)
    // {
    //     if (method_exists($selectable, 'display')) {
    //         $this->display($selectable::display());
    //     }
    //
    //     return $this->displayUsing(Displayers\BelongsTo::class, [$selectable]);
    // }

    // /**
    //  * @param string $selectable
    //  *
    //  * @return $this
    //  */
    // public function belongsToMany($selectable)
    // {
    //     if (method_exists($selectable, 'display')) {
    //         $this->display($selectable::display());
    //     }
    //
    //     return $this->displayUsing(Displayers\BelongsToMany::class, [$selectable]);
    // }

    // /**
    //  * Upload file.
    //  *
    //  * @return $this
    //  */
    // public function upload()
    // {
    //     return $this->displayUsing(Displayers\Upload::class);
    // }
    //
    // /**
    //  * Upload many files.
    //  *
    //  * @return $this
    //  */
    // public function uplaodMany()
    // {
    //     return $this->displayUsing(Displayers\Upload::class, [true]);
    // }

    /// <summary>
    /// Grid inline datetime picker.
    /// </summary>
    /// <param name="format"></param>
    /// <returns></returns>
    public GColumn Datetime(string format = "YYYY-MM-DD HH:mm:ss")
    {
        return DisplayUsing(typeof(Datetime), format);
    }

    /// <summary>
    /// Grid inline date picker.
    /// </summary>
    /// <returns></returns>
    public GColumn Date()
    {
        return Datetime("YYYY-MM-DD");
    }

    /// <summary>
    /// Grid inline time picker.
    /// </summary>
    /// <returns></returns>
    public GColumn Time()
    {
        return Datetime("HH:mm:ss");
    }

    /// <summary>
    /// Grid inline year picker.
    /// </summary>
    /// <returns></returns>
    public GColumn Year()
    {
        return Datetime("YYYY");
    }

    /// <summary>
    /// Grid inline month picker.
    /// </summary>
    /// <returns></returns>
    public GColumn Month()
    {
        return Datetime("MM");
    }

    /// <summary>
    /// Grid inline day picker.
    /// </summary>
    /// <returns></returns>
    public GColumn Day()
    {
        return Datetime("DD");
    }

    /// <summary>
    /// Grid inline input.
    /// </summary>
    /// <returns></returns>
    protected GColumn Input(Dictionary<string, string> mask = null)
    {
        return DisplayUsing(typeof(Input), mask);
    }

    /// <summary>
    /// Grid inline text input.
    /// </summary>
    /// <returns></returns>
    public GColumn Text()
    {
        return Input();
    }

    /// <summary>
    /// Grid inline ip input.
    /// </summary>
    /// <returns></returns>
    public GColumn ip()
    {
        return Input(new ()
        {
            {"alias", "ip"}
        });
    }

    /// <summary>
    /// Grid inline email input.
    /// </summary>
    /// <returns></returns>
    public GColumn Email()
    {
        return Input(new ()
        {
            {"alias", "email"}
        });
    }

    /// <summary>
    /// Grid inline url input.
    /// </summary>
    /// <returns></returns>
    public GColumn url()
    {
        return Input(new ()
        {
            {"alias", "url"}
        });
    }

    /// <summary>
    /// Grid inline currency input.
    /// </summary>
    /// <returns></returns>
    public GColumn currency()
    {
        return Input(
            new ()
            {
                {"alias", "currency"},
                {"radixPoint", "" },
                {"prefix", "" },
                {"removeMaskOnSubmit", "true"},
            });
    }

    /// <summary>
    /// Grid inline decimal input.
    /// </summary>
    /// <returns></returns>
    public GColumn Decimal()
    {
        return Input(new ()
        {
            {"alias", "decimal"},
            {"rightAlign", "true"}
        });
    }

    /// <summary>
    /// Grid inline integer input.
    /// </summary>
    /// <returns></returns>
    public GColumn integer()
    {
        return Input(new ()
        {
            {"alias", "integer"}
        });
    }

    /// <summary>
    /// Grid inline textarea.
    /// </summary>
    /// <returns></returns>
    public GColumn textarea(int rows = 5)
    {
        return DisplayUsing(typeof(Textarea), rows);
    }

    /// <summary>
    /// Grid inline tiemzone select.
    /// </summary>
    /// <returns></returns>
    public GColumn timezone()
    {
        throw new NotImplementedException();
        // var identifiers = System.TimeZoneInfo::listIdentifiers(\DateTimeZone::ALL);
        //
        // $options = collect($identifiers)->mapWithKeys(function ($timezone) {
        //     return [$timezone => $timezone];
        // })->toArray();
        //
        // return $this->select($options);
    }

    /// <summary>
    /// Grid inline select.
    /// </summary>
    /// <param name="???"></param>
    /// <returns></returns>
    public GColumn select(object options)
    {
        return DisplayUsing(typeof(Select), options);
    }

    /// <summary>
    /// Grid inline multiple-select input.
    /// </summary>
    /// <param name="???"></param>
    /// <returns></returns>
    public GColumn multipleSelect(object options)
    {
        return DisplayUsing(typeof(MultipleSelect), options);
    }

    /// <summary>
    /// Grid inline checkbox.
    /// </summary>
    /// <param name="???"></param>
    /// <returns></returns>
    public GColumn checkbox(object options)
    {
        return DisplayUsing(typeof(Checkbox), options);
    }

    /// <summary>
    /// Grid inline checkbox.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public GColumn radio(object options)
    {
        return DisplayUsing(typeof(Radio), options);
    }

    /// <summary>
    /// Grid inline switch.
    /// </summary>
    /// <param name="states"></param>
    /// <returns></returns>
    public GColumn Switch(Dictionary<string, SwitchState> states = null)
    {
        return DisplayUsing<SwitchDisplay>(s => s.Display(states));
    }

    /// <summary>
    /// Grid inline switch group.
    /// </summary>
    /// <param name="columns"></param>
    /// <param name="states"></param>
    /// <returns></returns>
    public GColumn switchGroup(object[] columns, object[] states)
    {
        return DisplayUsing(typeof(SwitchGroup), columns, states);
    }
    }
}