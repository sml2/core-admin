﻿using CoreAdmin.Lib;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace CoreAdmin.Mvc.Grid.Column
{
    public class InputFilter : Filter
    {
        protected new string Class;
        protected string type;

        [Obsolete("使用强类型")]
        public InputFilter(string type)
        {
            this.type = type;
            Class = Str.Uniqid("column-filter-");
        }
        
        public InputFilter(Type type)
        {
            this.type = type.Name;
            Class = Str.Uniqid("column-filter-");
        }

        /**
         * Add a binding to the query.
         *
         * @param string     $value
         * @param Model|null $model
         */
        //public function addBinding($value, Model $model)
        //{
        //    if (empty($value))
        //    {
        //        return;
        //    }

        //    if ($this->type == 'like') {
        //        $model->where($this->getColumnName(), 'like', "%{$value}%");

        //        return;
        //    }

        //    if (in_array($this->type, ['date', 'time']))
        //    {
        //        $method = 'where'.ucfirst($this->type);
        //        $model->{$method} ($this->getColumnName(), $value);

        //        return;
        //    }

        //    $model->where($this->getColumnName(), $value);
        //}

        /**
         * Add script to page.
         *
         * @return void
         */
        protected void AddScript()
        {
            var options = new Dictionary<string, dynamic>{
            {"locale"           ,"zh-cn" },
            { "allowInputToggle", true },
        };

            if (type == "date")
            {
                options["format"] = "YYYY-MM-DD";
            }
            else if (type == "time")
            {
                options["format"] = "HH:mm:ss";
            }
            else if (type == "datetime")
            {
                options["format"] = "YYYY-MM-DD HH:mm:ss";
            }
            else
            {
                return;
            }

            var optionsStr = JsonSerializer.Serialize(options);

            Admin.Script($"$('.{Class}').datetimepicker({options});");
        }

        /**
         * Render this filter.
         *
         * @return string
         */
        public override string Render()
        {
            var script = $@"
$('.dropdown-menu input').click(function(e) {{
            e.stopPropagation();
        }});
        ";
            Admin.Script(script);

            AddScript();

            //var value = GetFilterValue();
            var value = new List<string>();


            var active = value.Count == 0 ? "" : "text-yellow";

            return $@"
<span class=""dropdown"">
    <form action=""{GetFormAction()}"" pjax-container style=""display: inline-block;"" >
  
      <a href=""javascript:void(0);"" class=""dropdown-toggle {active}"" data-toggle=""dropdown"">
        <i class=""fa fa-filter""></i>
    </a>
    <ul class=""dropdown-menu"" role=""menu"" style=""padding: 10px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.2);left: -70px;border-radius: 0;"">
        <li>
            <input type=""text"" name=""{GetColumnName()}"" value=""{value}"" class=""form-control input-sm {Class}"" autocomplete=""off""/>
        </li>
        <li class=""divider""></li>
        <li class=""text-right"">
            <button class=""btn btn-sm btn-flat btn-primary column-filter-submit pull-left"" data-loading-text=""{Trans("search")}...""><i class=""fa fa-search""></i>&nbsp;&nbsp;{Trans("search")}</button>
            <button class=""btn btn-sm btn-flat btn-default column-filter-all"" data-loading-text=""...""><i class=""fa fa-undo""></i></button>
            <span><a href = ""{GetFormAction()}"" class=""btn btn-sm btn-default btn-flat column-filter-all""><i class=""fa fa-undo""></i></a></span>
        </li>
    </ul>
    </form>
</span>
";
        }
    }
}
