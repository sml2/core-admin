﻿using CoreAdmin.Lib;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace CoreAdmin.Mvc.Grid.Column
{
    public class RangeFilter : Filter
    {
        protected string type;

        public RangeFilter(string type)
        {
            this.type = type;
            Class = new()
            {
                { "start", Str.Uniqid("column-filter-start-") },
                { "end", Str.Uniqid("column-filter-end-") },
            };
        }

        /**
         * Add a binding to the query.
         *
         * @param mixed $value
         * @param Model $model
         */
        //public function addBinding($value, Model $model)
        //{
        //    $value = array_filter((array) $value);

        //    if (empty($value))
        //    {
        //        return;
        //    }

        //    if (!isset($value['start']))
        //    {
        //        return $model->where($this->getColumnName(), '<', $value['end']);
        //    }
        //    elseif(!isset($value['end'])) {
        //        return $model->where($this->getColumnName(), '>', $value['start']);
        //    } else
        //    {
        //        return $model->whereBetween($this->getColumnName(), array_values($value));
        //    }
        //}

        protected void AddScript()
        {
            var options = new Dictionary<string, dynamic> {
            {"locale"           ,"zh-cn" },
            { "allowInputToggle", true },
        };

            if (type == "date")
            {
                options["format"] = "YYYY-MM-DD";
            }
            else if (type == "time")
            {
                options["format"] = "HH:mm:ss";
            }
            else if (type == "datetime")
            {
                options["format"] = "YYYY-MM-DD HH:mm:ss";
            }
            else
            {
                return;
            }

            var optionsStr = JsonSerializer.Serialize(options);

            Admin.Script($"$('.{Class["start"]},.{Class["end"]}').datetimepicker({optionsStr});");
        }

        public override string Render()
        {
            var script = $@"
$('.dropdown-menu input').click(function(e) {{
            e.stopPropagation();
        }});
        ";

            Admin.Script(script);

            AddScript();

            //var value = array_merge(['start' => '', 'end' => ''], $this->getFilterValue([]));
            var value = new Dictionary<string, dynamic> { { "start", "" }, { "end", "" } };
            var active = value.Where(v => !string.IsNullOrEmpty((string)v.Value)).Count() == 0 ? "" : "text-yellow";

            return $@"
<span class=""dropdown"">
<form action = ""{GetFormAction()}"" pjax-container style = ""display: inline-block;"" >
  
      <a href=""javascript:void(0);"" class=""dropdown-toggle {active}"" data-toggle=""dropdown"">
        <i class=""fa fa-filter""></i>
    </a>
    <ul class=""dropdown-menu"" role=""menu"" style=""padding: 10px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.2);left: -70px;border-radius: 0;"">
        <li>
            <input type=""text"" class=""form-control input-sm {Class["start"]}"" name=""{GetColumnName()}[start]"" value=""{value["start"]}"" autocomplete=""off""/>
        </li>
        <li style=""margin: 5px;"" ></ li >
        <li>
            <input type=""text"" class=""form-control input-sm {Class["start"]}"" name=""{GetColumnName()}[end]""  value=""{value["end"]}"" autocomplete=""off""/>
        </li>
        <li class=""divider""></li>
        <li class=""text-right"">
            <button class=""btn btn-sm btn-primary btn-flat column-filter-submit pull-left"" data-loading-text=""{Trans("search")}...""><i class=""fa fa-search""></i>&nbsp;&nbsp;{Trans("search")}</button>
            <button class=""btn btn-sm btn-default btn-flat column-filter-all"" data-loading-text=""...""><i class=""fa fa-undo""></i></button>
            <span><a href=""{GetFormAction()}"" class=""btn btn-sm btn-default btn-flat column-filter-all""><i class=""fa fa-undo""></i></a></span>
        </li>
    </ul>
    </form>
</span>
";
        }
    }
}
