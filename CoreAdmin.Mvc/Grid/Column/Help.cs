﻿using CoreAdmin.Mvc.Widgets;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Column
{
    public class Help : IRenderable
    {
        protected string message = "";

        public Help(string message = "")
        {
            this.message = message;
        }

        public string Render()
        {
            var data = new Dictionary<string, string> {
            {"toggle"    ,"tooltip" },
            {"placement" ,"right" },
            {"html"      ,"true" },
            { "title"    , message },
        };

            var dataStr = string.Join(" ", data.Select(pair => $"data-{pair.Key}=\"{pair.Value}\""));

            return @"
    < a href = ""javascript:void(0);"" class=""grid-column-help"" " + dataStr + @">
        <i class=""fa fa-question-circle""></i>
    </a>
    ";
        }
    }
}
