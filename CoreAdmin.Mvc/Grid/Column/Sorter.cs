﻿using CoreAdmin.Mvc.Widgets;
using System.Collections.Generic;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.Grid.Column
{
    public class Sorter : IRenderable
    {
        protected Dictionary<string, string> sort;

        protected string cast;
        private readonly HttpContext _context;

        protected string sortName;

        protected string columnName;
        public const string SortName = "_sort";

        public Sorter(string sortName, string columnName, string cast, HttpContext context)
        {
            this.sortName = sortName;
            this.columnName = columnName;
            this.cast = cast;
            _context = context;
        }

        protected bool IsSorted()
        {
            sort = _context.Request.GetDic(SortName);

            if (null == sort || sort.Count == 0)
            {
                return false;
            }

            return sort.TryGetValue("column", out var col) && col == columnName;
        }

        public string Render()
        {
            var icon = "fa-sort";
            var type = "desc";

            if (IsSorted())
            {
                type = sort["type"] == "desc" ? "asc" : "desc";
                icon += $"-amount-{type}";
            }
            
            sort = new Dictionary<string, string>() { { "column", columnName }, { "type", type } };

            if (!string.IsNullOrEmpty(cast))
            {
                sort["cast"] = cast;
            }

            var url = _context.Request.FullUrlWithQuery(sortName, sort);

            return $"<a class=\"fa fa-fw {icon}\" href=\"{url}\"></a>";
        }
    }
}
