﻿using CoreAdmin.Mvc.Widgets;
using System.Collections.Generic;
using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid.Column
{
    public abstract class Filter : IRenderable
    {
        protected Dictionary<string, string> Class;

        protected GColumn parent;

        public void SetParent(GColumn column)
        {
            parent = column;
        }
        public string GetColumnName()
        {
            return parent.GetName();
        }

        /**
         * Get filter value of this column.
         *
         * @param string $default
         *
         * @return array|\Illuminate\Http\Request|string
         */
        //public function getFilterValue($default = '')
        //{
        //    return request($this->getColumnName(), $default);
        //}

        public string GetFormAction()
        {
            return "";
            //$request = request();

            //$query = $request->query();
            //    Arr::forget($query, [$this->getColumnName(), '_pjax']);

            //$question = $request->getBaseUrl().$request->getPathInfo() == '/' ? '/?' : '?';

            //    return count($request->query()) > 0
            //        ? $request->url().$question.http_build_query($query)
            //        : $request->fullUrl();
        }
        
        protected string Trans(string key)
        {
            return Admin.Trans(key);
        }
        
        public void AddBinding(object value, BaseModel model)
        {
            
        }

        public abstract string Render();
    }
}
