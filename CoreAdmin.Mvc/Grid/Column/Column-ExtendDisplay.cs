﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using CoreAdmin.Extensions;
using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Grid.Displayers;
using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid
{
    
    public partial class GColumn
    {
        public static List<Type> displayers = new()
        {
            typeof(Editable),
            typeof(Image),
            typeof(Label),
            typeof(Button),
            typeof(Link),
            typeof(Badge),
            typeof(ProgressBar),
            typeof(ProgressBar),
            typeof(Orderable),
            typeof(Table),
            typeof(Expand),
            typeof(Modal),
            typeof(Carousel),
            typeof(Downloadable),
            typeof(Copyable),
            typeof(QRCode),
            typeof(Prefix),
            typeof(Suffix),
            typeof(Secret),
            typeof(Limit),
        };

        protected bool searchable = false;

        public GColumn Searchable()
        {
            throw new NotImplementedException();
            searchable = true;

            var name = GetName();
            // $query = request()->query();
            //
            // Prefix(function($_, $original) use($name, $query) {
            //         Arr::set($query, $name, $original);
            //
            //     $url = request()->fullUrlWithQuery($query);
            //
            //         return "<a href=\"{$url}\"><i class=\"fa fa-search\"></i></a>";
            //     }, '&nbsp;&nbsp;');

            return this;
        }

        public void BindSearchQuery(IQueryable<BaseModel> model)
        {
            var value = grid.Context.Request.Get(GetName());
            if (searchable && !string.IsNullOrEmpty(value))
            {
                model.Where($"{GetName()}={value}");
            }
        }

        public GColumn Using<T>(Dictionary<T, string> values, string defaultValue = null)
        {
            return Display<T>((value) =>
            {
                if (value == null)
                {
                    return defaultValue;
                }

                return values.ContainsKey(value) ? values[value] : defaultValue;
            });
        }
        
        public GColumn Replace<T>(Dictionary<T, string> replacements)
        {
            return Display<T>((value) => {
                if (replacements.ContainsKey(value))
                {
                    return replacements[value];
                }

                return value.ToString();
            });
        }
        
        // public GColumn Repeat(string input, string seperator = "")
        // {
        //     if (is_string($input))
        //     {
        //         $input = function() use($input) {
        //             return $input;
        //         };
        //     }
        //
        //     if ($input instanceof Closure) {
        //         return $this->display(function($value) use($input, $seperator) {
        //             return join($seperator, array_fill(0, (int) $value, $input->call($this, [$value])));
        //         });
        //     }
        //
        //     return $this;
        // }

        // public GColumn Repeat(string input, string seperator = "")
        // {
        //     
        // }

        ///**
        // * Render this column with the given view.
        // *
        // * @param string $view
        // *
        // * @return $this
        // */
        //public function view($view)
        //{
        //    return $this->display(function($value) use($view) {
        //        $model = $this;

        //        return view($view, compact('model', 'value'))->render();
        //    });
        //}

        ///**
        // * Convert file size to a human readable format like `100mb`.
        // *
        // * @return $this
        // */
        //public function filesize()
        //{
        //    return $this->display(function($value) {
        //        return file_size($value);
        //    });
        //}

        ///**
        // * Display the fields in the email format as gavatar.
        // *
        // * @param int $size
        // *
        // * @return $this
        // */
        //public function gravatar($size = 30)
        //{
        //    return $this->display(function($value) use($size) {
        //        $src = sprintf(
        //            'https://www.gravatar.com/avatar/%s?s=%d',
        //            md5(strtolower($value)),
        //            $size
        //        );

        //        return "<img src='$src' class='img img-circle'/>";
        //    });
        //}

        ///**
        // * Display field as a loading icon.
        // *
        // * @param array $values
        // * @param array $others
        // *
        // * @return $this
        // */
        //public function loading($values = [], $others = [])
        //{
        //    return $this->display(function($value) use($values, $others) {
        //        $values = (array) $values;

        //        if (in_array($value, $values))
        //        {
        //            return '<i class="fa fa-refresh fa-spin text-primary"></i>';
        //        }

        //        return Arr::get($others, $value, $value);
        //    });
        //}

        ///**
        // * Display column as an font-awesome icon based on it's value.
        // *
        // * @param array  $setting
        // * @param string $default
        // *
        // * @return $this
        // */
        //public function icon(array $setting, $default = '')
        //{
        //    return $this->display(function($value) use($setting, $default) {
        //        $fa = '';

        //        if (isset($setting[$value]))
        //        {
        //            $fa = $setting[$value];
        //        }
        //        elseif($default) {
        //            $fa = $default;
        //        }

        //        return "<i class=\"fa fa-{$fa}\"></i>";
        //    });
        //}

        ///**
        // * Return a human readable format time.
        // *
        // * @param null $locale
        // *
        // * @return $this
        // */
        //public function diffForHumans($locale = null)
        //{
        //    if ($locale) {
        //        Carbon::setLocale($locale);
        //    }

        //    return $this->display(function($value) {
        //        return Carbon::parse($value)->diffForHumans();
        //    });
        //}

        ///**
        // * Display column as boolean , `✓` for true, and `✗` for false.
        // *
        // * @param array $map
        // * @param bool  $default
        // *
        // * @return $this
        // */
        //public function bool (array $map = [], $default = false)
        //{
        //    return $this->display(function ($value) use ($map, $default) {
        //        $bool = empty($map) ? boolval($value) : Arr::get($map, $value, $default);

        //    return $bool? '<i class="fa fa-check text-green"></i>' : '<i class="fa fa-close text-red"></i>';
        //});
        //}

        ///**
        // * Display column as a default value if empty.
        // *
        // * @param string $default
        // *
        // * @return $this
        // */
        //public function default($default = '-')
        //{
        //    return $this->display(function ($value) use ($default) {
        //    return $value ?: $default;
        //});
        //}

        ///**
        // * Add a `dot` before column text.
        // *
        // * @param array  $options
        // * @param string $default
        // *
        // * @return $this
        // */
        //public function dot($options = [], $default = '')
        //{
        //    return $this->prefix(function($_, $original) use($options, $default) {
        //        if (is_null($original))
        //        {
        //            $style = $default;
        //        }
        //        else
        //        {
        //            $style = Arr::get($options, $original, $default);
        //        }

        //        return "<span class=\"label-{$style}\" style='width: 8px;height: 8px;padding: 0;border-radius: 50%;display: inline-block;'></span>";
        //    }, '&nbsp;&nbsp;');
        //}
        public GColumn Editable(string type = "text") => CallBuiltinDisplayer<Editable>(d => d.Display(type));

        /// <summary>
        /// 行内编辑选择器
        /// </summary>
        /// <param name="hidden">需要隐藏的元素</param>
        /// <typeparam name="TEnum">枚举类型</typeparam>
        /// <returns></returns>
        public GColumn EditableOptions<TEnum>(params TEnum[] hidden) where TEnum : struct,Enum
        {
            return CallBuiltinDisplayer<Editable>(d => d.Display("select",
                Lib.Enum.GetItems<TEnum>().Select(x => new Editable.ValueText()
                {
                    text = x.GetDescription() ?? x.ToString(),
                    // value = Convert.ToInt32(x).ToString(),
                    // TODO: 输出int
                    value =x.ToString(),
                    disabled = hidden.Contains(x)
                })));
        }

        /// <summary>
        /// 行内编辑选择器,请优先使用枚举<see cref="EditableOptions{TEnum}"/>
        /// </summary>
        /// <param name="em"></param>
        /// <returns></returns>
        public GColumn EditableOptions(Dictionary<string, object> em)
        {
            return CallBuiltinDisplayer<Editable>(d => d.Display("select", em.Select(kp => new Editable.ValueText(){text = kp.Key, value = kp.Value.ToString()})));
        }

        /// <summary>
        /// 以label的方式展示数据
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public GColumn Label(LabelColor color = LabelColor.Success) => CallBuiltinDisplayer<Label>(d => d.Display(color));

        /// <summary>
        /// 以label的方式展示数据,如无需自定义，请使用枚举<see cref="Label(LabelColor)"/>
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public GColumn Label(string className) => CallBuiltinDisplayer<Label>(d => d.Display(className));
        
        /// <summary>
        /// 以label的方式展示数据,key 为值，value为label颜色
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public GColumn Label(Dictionary<string, LabelColor> className) => CallBuiltinDisplayer<Label>(d => d.Display(className));

        /// <summary>
        /// 以label的方式展示数据,key 为值，value为label颜色,如无需自定义，请使用枚举<see cref="Label(Dictionary&lt;string, LabelColor&gt;)"/>
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public GColumn Label(Dictionary<string, string> className) => CallBuiltinDisplayer<Label>(d => d.Display(className));

        /// <summary>
        /// 展示图片
        /// </summary>
        /// <param name="server">域名</param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public GColumn Image(string server = "", int width = 200, int height = 200) => CallBuiltinDisplayer<Image>(d => d.Display(server, width, height));
        
        /// <summary>
        /// 添加多个class
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public GColumn Button(params LabelColor[] className) => CallBuiltinDisplayer<Button>(d => d.Display(className));

        /// <summary>
        /// 展示button
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public GColumn Button(LabelColor className = LabelColor.Success) => CallBuiltinDisplayer<Button>(d => d.Display(className));

        /// <summary>
        /// 展示链接
        /// </summary>
        /// <param name="cb"></param>
        /// <returns></returns>
        public GColumn Link(Func<IIdexerModel, string> cb) => CallBuiltinDisplayer<Link>(d => d.Display(cb));
        public GColumn Link(string uri = "") => CallBuiltinDisplayer<Link>(d => d.Display(uri));


        /// <summary>
        /// 标签, 值 => 颜色
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public GColumn Badge(Dictionary<object, BgColor> className) => CallBuiltinDisplayer<Badge>(d => d.Display(className));

        /// <summary>
        /// 标签
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public GColumn Badge(BgColor color = BgColor.Green) => CallBuiltinDisplayer<Badge>(d => d.Display(color));

        /// <summary>
        /// 进度条
        /// </summary>
        /// <param name="color"></param>
        /// <param name="size"></param>
        /// <param name="max">最大值</param>
        /// <returns></returns>
        public GColumn ProgressBar(LabelColor color = LabelColor.Primary, string size = "sm", int max = 100) =>
            CallBuiltinDisplayer<ProgressBar>(d => d.Display(color, size, max));

        public GColumn Orderable() => CallBuiltinDisplayer<Orderable>(d => d.Display());
        // public GColumn Table(Func<Table, string> className) => CallBuiltinDisplayer<Table>(d => d.Display(className));
        
        // public GColumn Expand(Action callback = null, bool isExpand = false) => CallBuiltinDisplayer<Expand>(d => d.Display(callback, isExpand));
        // public GColumn Modal(Func<BaseModel, string> callback) => CallBuiltinDisplayer<Modal>(d => d.Display(callback));
        public GColumn Carousel(int width = 300, int height = 200, string server = "") => CallBuiltinDisplayer<Carousel>(d => d.Display(width, height, server));

        public GColumn Downloadable(string server) =>
            CallBuiltinDisplayer<Downloadable>(d => d.Display(server));

        /// <summary>
        /// 可复制
        /// </summary>
        /// <returns></returns>
        public GColumn Copyable() => CallBuiltinDisplayer<Copyable>(d =>
        {     
            return d.Display();
        });

        public GColumn QrCode(Func<object, IIdexerModel, string> formatter = null, int width = 150, int height = 150) => CallBuiltinDisplayer<QRCode>(d => d.Display(formatter, width, height));
        public GColumn Prefix(string prefix = null, string delimiter = "&nbsp;") => CallBuiltinDisplayer<Prefix>(d => d.Display(prefix, delimiter));
        public GColumn Suffix<T>(Func<IIdexerModel, T, string> suffix = null, string delimiter = "&nbsp;") => CallBuiltinDisplayer<Suffix>(d => d.Display(suffix, delimiter));
        public GColumn Secret(int dotCount = 6) => CallBuiltinDisplayer<Secret>(d => d.Display(dotCount));
        public GColumn Limit(int limit = 100, string end = "...") => CallBuiltinDisplayer<Limit>(d => d.Display(limit, end));
    }
}