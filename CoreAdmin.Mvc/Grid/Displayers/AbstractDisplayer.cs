﻿using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Models;
using System;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public abstract class AbstractDisplayer
    {
        protected BGrid Grid { get; set; }

        protected GColumn Column { get; set; }

        public IIdexerModel Row { get; set; }

        protected object Value { get; set; }

        /// <summary>
        /// 初始化数据，TODO: 改为依赖注入
        /// </summary>
        /// <param name="value"></param>
        /// <param name="grid"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        public void InitData(object value, BGrid grid, GColumn column, IIdexerModel row)
        {
            Value = value;
            Grid = grid;
            Column = column;
            Row = row;
        }

        public object GetValue()
        {
            return Value;
        }

        public GColumn GetColumn()
        {
            return Column;
        }

        /// <summary>
        /// 获取每一列的主键id值
        /// </summary>
        /// <returns></returns>
        public int GetKey()
        {
            return Row.GetKey();
        }

        public T GetData<T>() where T: BaseModel
        {
            return (T)Row;
        }

        /// <summary>
        /// 获取每一列指定属性值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetAttribute<T>(string key)
        {
            return (T)Row.Get(key);
        }

        public string GetResource()
        {
            return Grid.Resource();
        }

        public string GetName()
        {
            return GetColumn().GetName();
        }

        public string GetClassName()
        {
            return GetColumn().GetClassName();
        }
        protected string GetPayloadName(string name = "")
        {
            name = !string.IsNullOrEmpty(name) ? name : GetName();
            var keys = name.Split(".");

            return keys.First() + string.Join("", keys.Skip(1).Select(s => $"[{s}]"));
        }

        protected static string Trans(string text)
        {
            return Admin.Trans(text);
        }

        public virtual string Display() => "";

        public virtual string Display(Action<Actions> callback) => "";

    }
}
