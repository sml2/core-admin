﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Text.Json;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class MultipleSelect : AbstractDisplayer
    {
        //public MultipleSelect(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        
        public HtmlContent Display(Dictionary<string, dynamic> options)
        {
            return Admin.Component("~/Views/Grid/InlineEdit/MultipleSelect.cshtml", new()
            {
                { "key", GetKey() },
                { "name", GetPayloadName() },
                { "value", JsonSerializer.Serialize(GetValue()) },
                { "resource", GetResource() },
                { "trigger", $"ie-trigger-{GetClassName()}" },
                { "target", $"ie-template-{GetClassName()}" },
                //{"display"  , string.Join(";", Arr::only(options, GetValue()))},
                { "options", options },
            });
        }
    }
}
