﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Select : AbstractDisplayer
    {
        //public Select(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public HtmlContent Display(Dictionary<string, dynamic> options)
        {
            return Admin.Component("admin::grid.inline-edit.select", new()
            {
                { "key", GetKey() },
                { "value", GetValue() },
                { "display", options[GetValue().ToString()] ?? "" },
                { "name", GetPayloadName() },
                { "resource", GetResource() },
                { "trigger", $"ie-trigger-{GetClassName()}" },
                { "target", $"ie-template-{GetClassName()}" },
                { "options", options },
            });
        }

    }
}
