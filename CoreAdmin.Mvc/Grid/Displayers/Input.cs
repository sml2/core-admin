﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Input : AbstractDisplayer
    {
        //public Input(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        public HtmlContent Display(string mask = "")
        {
            return Admin.Component("admin::grid.inline-edit.input", new()
            {
                { "key", GetKey() },
                { "value", GetValue() },
                { "display", GetValue() },
                { "name", GetPayloadName() },
                { "resource", GetResource() },
                { "trigger", $"ie-trigger-{GetClassName()}" },
                { "target", $"ie-template-{GetClassName()}" },
                { "mask", mask },
            });
        }

    }
}
