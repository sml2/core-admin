﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Upload : AbstractDisplayer
    {
        //public Upload(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public HtmlContent Display(bool multiple = false)
        {
            return Admin.Component("admin::grid.inline-edit.upload", new()
            {
                { "key", GetKey() },
                { "name", GetPayloadName() },
                { "value", GetValue() },
                { "target", $"inline-upload-{GetKey()}" },
                { "resource", GetResource() },
                { "multiple", multiple },
            });
        }
    }
}
