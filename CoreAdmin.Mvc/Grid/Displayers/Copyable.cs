﻿namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Copyable : AbstractDisplayer
    {
        //public Copyable(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        protected void AddScript()
        {
            var script = $@"
$('#{Grid.tableID}').on('click','.grid-column-copyable',(function (e) {{
    var content = $(this).data('content');
    
    var temp = $('<input>');
    
    $(""body"").append(temp);
    temp.val(content).select();
    document.execCommand(""copy"");
    temp.remove();
    
    $(this).tooltip('show');
}}));
";

            Admin.Script(script);
        }

        public override string Display()
        {
            AddScript();

            var content = GetColumn().GetOriginal();

            return $@"
<a href=""javascript:void(0);"" class=""grid-column-copyable text-muted"" data-content=""{content}"" title=""Copied!"" data-placement=""bottom"">
    <i class=""fa fa-copy""></i>
</a>&nbsp;{GetValue()}
";
        }

       

    }
}
