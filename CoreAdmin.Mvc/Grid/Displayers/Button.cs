using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class Button : AbstractDisplayer
    {
        //public Button(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}
        public string Display(IEnumerable<LabelColor> styles)
        {
            return $"<span class=\"btn {string.Join(" ", styles.Select(x => $"btn-{x.ToString().ToLower()}"))}\">{Value}</span>";
        }
        public string Display(LabelColor style)
        {
            return $"<span class=\"btn btn-{style.ToString().ToLower()}\">{Value}</span>";
        }

    }
}