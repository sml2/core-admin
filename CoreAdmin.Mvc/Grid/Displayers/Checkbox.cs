using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Checkbox : AbstractDisplayer
    {

        //public Checkbox(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}

        public dynamic Display(Array options)
        {
            return Admin.Component("admin::grid.inline-edit.checkbox",
                new Dictionary<string, dynamic> {
                    //{ "key", this.getKey() },
                    //{ "name", this.getPayloadName() },
                    //{ "resource", this.getResource() },
                    //{ "trigger", "ie-trigger-"+ this.getClassName() },
                    //{ "target", "ie-content-"+ this.getClassName() + "-" + this.getKey() },
                    //{ "value", json_encode(this.getValue()) },
                    //{ "display", implode(';', Arr::only(options, this.getValue())) },
                    { "options", options }
                }
            );
        }
    }
}
