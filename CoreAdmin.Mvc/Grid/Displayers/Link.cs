﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Link : AbstractDisplayer
    {
        //public Link(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}


        public string Display(Func<IIdexerModel, string> callback, string target = "_blank") => Display(callback(Row), target);
        public string Display(string href = "", string target = "_blank")
        {
            href ??= Value.ToString();
            return $"<a href='{href}' target='{target}'>{Value}</a>";
        }
    }
}
