﻿namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Limit : AbstractDisplayer
    {
        //public Limit(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        protected void AddScript()
        {
            var script = $@"
$('.limit-more').click(function () {{
    $(this).parent('.limit-text').toggleClass('hide').siblings().toggleClass('hide');
}});
";

            Admin.Script(script);
        }

        public string Display(int limit = 100, string end = "...")
        {
            AddScript();

            //var value = Str::limit($this->value, $limit, $end);
            var value = this.Value;


            var original = GetColumn().GetOriginal();

            if (value == original)
            {
                return value.ToString();
            }

            return $@"
<div class=""limit-text"">
    <span class=""text"">{value}</span>
    &nbsp;<a href=""javascript:void(0);"" class=""limit-more"">&nbsp;<i class=""fa fa-angle-double-down""></i></a>
</div>
<div class=""limit-text hide"">
    <span class=""text"">{original}</span>
    &nbsp;<a href=""javascript:void(0);"" class=""limit-more"">&nbsp;<i class=""fa fa-angle-double-up""></i></a>
</div>
";
        }
    }
}
