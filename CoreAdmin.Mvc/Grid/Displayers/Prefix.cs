﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Prefix : AbstractDisplayer
    {
        //public Prefix(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        public string Display<T>(Func<IIdexerModel, T, string> action, string delimiter = "&nbsp;")
        {
            var prefix = action(Row, (T)Value);

            return $@"
{prefix}{delimiter}{GetValue()}
";
        }

        public string Display(string prefix = null, string delimiter = "&nbsp;")
        {
            return $@"
{prefix}{delimiter}{GetValue()}
";
        }
    }
}
