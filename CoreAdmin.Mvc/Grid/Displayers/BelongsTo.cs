﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class BelongsTo : AbstractDisplayer
    {
        //public BelongsTo(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        protected string GetLoadUrl(string selectable, int multiple = 0)
        {
            selectable = selectable.Replace('\\', '_');
            //var args = [multiple];

            //return route('admin.handle-selectable', compact('selectable', 'args'));
            return "";
        }

        protected object GetOriginalData()
        {
            return GetColumn().GetOriginal();
        }

        public HtmlContent Display(Type selectable = null, string column = "")
        {
            if (!selectable.IsAssignableTo(typeof(GSelectable)))
            {
                throw new ArgumentException(
                    $"[Class [{selectable.FullName}] must be a sub class of {typeof(GSelectable).FullName}"
                );
            }

            return Admin.Component("admin::grid.inline-edit.belongsto", new()
            {
                { "modal", $"modal-grid-selector-{GetClassName()}" },
                { "key", GetKey() },
                { "original", GetOriginalData() },
                { "value", GetValue() },
                { "resource", GetResource() },
                { "name", column ?? GetName() },
                //{"relation"  , get_called_class()},
                //{ "url"       , GetLoadUrl(selectable, get_called_class() == BelongsToMany::class)},
            });
        }

    }
}
