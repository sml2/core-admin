using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Datetime : AbstractDisplayer
    {

        //public Datetime(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}

        public dynamic display(Array format)
        {
            return Admin.Component("admin::grid.inline-edit.datetime",
                new Dictionary<string, dynamic>
                {
                    //{ "key", this.getKey() },
                    //{ "value", this.getValue() },
                    //{ "display", this.getValue() },
                    //{ "name", this.getPayloadName() },
                    //{ "resource", this.getResource() },
                    //{ "trigger", "ie-trigger-" + this.getClassName() },
                    //{ "target", "ie-template-" + this.getClassName() },
                    { "format", format },
                    //{ "locale", config("app.locale") }
                });
        }

    }
}
