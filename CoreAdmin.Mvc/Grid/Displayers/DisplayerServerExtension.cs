﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public static class DisplayerServerExtension
    {
        internal static List<Type> BuiltinDisplayers = new List<Type>() {
            typeof(RowSelector),
            typeof(Actions)
        };

        /// <summary>
        ///  注入displayers
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        internal static IServiceCollection UseDisplayers(
             this IServiceCollection services)
        {
            foreach (var type in GColumn.displayers)
            {
                services.TryAddTransient(type);
            }

            foreach (var type in BuiltinDisplayers)
            {
                services.TryAddTransient(type);
            }
            
            services.TryAddTransient(typeof(SwitchDisplay));

            return services;
        }
    }
}
