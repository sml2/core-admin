﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Textarea : AbstractDisplayer
    {
        //public Textarea(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public HtmlContent Display(int rows = 5)
        {
            return Admin.Component("admin::grid.inline-edit.textarea", new()
            {
                { "key", GetKey() },
                { "value", GetValue() },
                { "display", GetValue() },
                { "name", GetPayloadName() },
                { "resource", GetResource() },
                { "trigger", $"ie-trigger-{GetClassName()}" },
                { "target", $"ie-template-{GetClassName()}" },
                { "rows", rows },
            });
        }
    }
}
