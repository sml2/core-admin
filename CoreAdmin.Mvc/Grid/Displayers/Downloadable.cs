using System;
using System.Collections.Generic;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Downloadable : AbstractDisplayer
    {
        //public Downloadable(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}

        public string Display(string server)
        {

            //if (this.value instanceof Arrayable) {
            //    this.value = this.value.toArray();
            //}
            string Func(string val)
            {
                if (string.IsNullOrEmpty(val)) return string.Empty;
                string src = default;
                if (val.IsValidUrl())
                {
                    src = val;
                } else if (!string.IsNullOrEmpty(server))
                {
                    src = server.TrimEnd('/') + '/' + val.TrimStart('/');
                } else
                {
                    //        $src = Storage::disk(config('admin.upload.disk'))->url($value);
                }
                var start = val.LastIndexOf("/") + 1;
                var name = val[start..];
                return $@"
<a href='{src}' download='{name}' target='_blank' class='text-muted'>
    <i class=""fa fa-download""></i> {name}
</a>
";
            }
            if (Value is string str)
            {
                return Func(str);
            } else if (Value is IEnumerable<string> list)
            {
                return string.Join("<br/>", list);
            }
            throw new ArgumentException(Value.ToString(), nameof(Value));
        }

    }
}
