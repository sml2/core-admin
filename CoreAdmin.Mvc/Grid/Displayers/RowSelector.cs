﻿namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class RowSelector : AbstractDisplayer
    {
        //public RowSelector(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}
        public override string Display()
        {
            Admin.Script(Script());

            return @"
    <input type=""checkbox"" class=""" + Grid.GetGridRowName() + @"-checkbox"" data-id=""" + GetKey() + @"""  autocomplete=""off""/>
";
        }

        protected string Script()
        {
            var all = Grid.GetSelectAllName();
            var row = Grid.GetGridRowName();

            var selected = Admin.Trans("grid_items_selected");

            return @"
$('." + row + @"-checkbox').iCheck({ checkboxClass: 'icheckbox_minimal-blue'}).on('ifChanged', function() {

            var id = $(this).data('id');

            if (this.checked) {
        $.admin.grid.select(id);
        $(this).closest('tr').css('background-color', '#ffffd5');
                } else
                {
        $.admin.grid.unselect(id);
        $(this).closest('tr').css('background-color', '');
                }
                }).on('ifClicked', function() {

                var id = $(this).data('id');

                if (this.checked) {
        $.admin.grid.unselect(id);
                    } else
                    {
        $.admin.grid.select(id);
                    }

                    var selected = $.admin.grid.selected().length;

                    if (selected > 0)
                    {
        $('." + all + @"-btn').show();
                    }
                    else
                    {
        $('." + all + @"-btn').hide();
                    }

    $('." + all + @"-btn .selected').html(""" + selected + @""".replace('{n}', selected));
                    });

                ";
        }
    }
}
