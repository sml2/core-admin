﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class Label : AbstractDisplayer
    {
        //public Label(object value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        private readonly ILogger<Label> _logger;
        public Label(ILogger<Label> logger)
        {
            _logger = logger;
        }

        private List<string> Prepare()
        {
            List<object> arr;
            if (Value is IEnumerable<object> en)
            {
                arr = en.ToList();
            }
            else
            {
                arr = new List<object>() {Value};
            }

            return arr.Cast<string>().ToList();
        }

        public string Display(LabelColor color = LabelColor.Success)
        {
            return Display(color.ToString().ToLower());
        }
        
        public string Display(string style)
        {
            var items = Prepare();

            return string.Join("&nbsp;", items.Select((item) =>
            {
                return $"<span class='label label-{style}'>{item}</span>";
            }));
        }

        public string Display(Dictionary<string, LabelColor> styles) => Display(styles.ToDictionary(x => x.Key, x => x.Value.ToString().ToLower()));
        public string Display(Dictionary<string, string> styles)
        {
            var items = Prepare();
            return string.Join("&nbsp;", items.Select((item) =>
            {
                var style = styles.TryGetValue(item.ToString(), out var v) ? v : "success";

                return $"<span class='label label-{style}'>{item}</span>";
            }));
        }
    }

    public enum LabelColor
    {
        Default,
        Primary,
        Info,
        Warning,
        Success,
        Danger,
    }
}
