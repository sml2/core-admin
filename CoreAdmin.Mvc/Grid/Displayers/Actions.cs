﻿using CoreAdmin.Mvc.Extensions;
using System;
using System.Collections.Generic;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class Actions : AbstractDisplayer
    {
        #region "list" //可优化项:此处可以用一个list对象代替
        protected List<object> appends { get; } = new();
        protected List<object> prepends { get; } = new();
        protected List<string> actions { get;  } = new() { "view", "edit", "delete" };
        #endregion
        protected string resource;

        protected bool disableAll = false;

        protected List<dynamic> trans;
        //public Actions(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //    appends = new();
        //    prepends = new();

        //}

        public Actions Append(object action)
        {
            appends.Add(action);

            return this;
        }

        public Actions Prepend(object action)
        {
            prepends.Insert(0, action);

            return this;
        }

        public int GetRouteKey()
        {
            return Row.GetKey();
        }
        /// <summary>
        /// 隐藏查看只读详情页面的按钮
        /// </summary>
        /// <param name="disable">是否隐藏</param>
        /// <returns></returns>
        public Actions DisableView(bool disable = true)
        {
            if (disable)
            {
                actions.Remove("view");//移除只读详情页面的按钮的标识
            }
            else if (!actions.Contains("view"))//判断是否存在只读详情页面的按钮的标识
            {
                actions.Add("view");//添加只读详情页面的按钮的标识
            }

            return this;
        }
        /// <summary>
        /// 隐藏表格列中的删除按钮
        /// </summary>
        /// <param name="disable">是否隐藏</param>
        /// <returns></returns>
        public Actions DisableDelete(bool disable = true)
        {
            if (disable)
            {
                actions.Remove("delete");//移除表格列中的删除按钮的标识
            }
            else if (!actions.Contains("delete"))//判断是否存在表格列中的删除按钮的标识
            {
                actions.Add("delete");//添加表格列中的删除按钮的标识
            }

            return this;
        }

        /// <summary>
        /// 隐藏表格列中的编辑按钮
        /// </summary>
        /// <param name="disable">是否隐藏</param>
        /// <returns></returns>
        public Actions DisableEdit(bool disable = true)
        {
            if (disable)
            {
                actions.Remove("edit");//移除表格列中的编辑按钮的标识
            }
            else if (!actions.Contains("edit"))//判断是否存在表格列中的编辑按钮的标识
            {
                actions.Add("edit");//添加表格列中的编辑按钮的标识
            }

            return this;
        }

        /// <summary>
        /// 隐藏表格列中的所有按钮
        /// </summary>
        /// <returns></returns>
        public Actions DisableAll()
        {
            disableAll = true;

            return this;
        }

        public Actions SetResource(string resource)
        {
            this.resource = resource.TrimEnd('/');
            return this;
        }

        public new string GetResource()
        {
            return resource ?? base.GetResource();
        }

        public override string Display(Action<Actions> callback)
        {
            if (callback != null)
            {
                callback.Invoke(this);
            }

            if (disableAll)
            {
                return "";
            }

            var actions = prepends;

            foreach (var action in this.actions)
            {
                var method = "Render" + action.UpperCaseFirst();
                var methodInfo = GetType().GetMethod(method);
                if (methodInfo == null)
                {
                    throw new ArgumentException($"[{method}] not exists");
                }
                actions.Add(methodInfo.Invoke(this, null));
            }
            if (appends != null)
            {
                actions.AddRange(appends);
            }

            return string.Join("", actions);
        }

        public string RenderView()
        {
            return $@"
    <a href=""{GetResource()}/{GetRouteKey()}"" class=""{Grid.GetGridRowName()}-view"">
            <i class=""fa fa-eye""></i>
        </a>
        ";
        }

        public string RenderEdit()
        {
            return $@"
    <a href=""{GetResource()}/edit/{GetRouteKey()}"" class=""{Grid.GetGridRowName()}-edit"">
        <i class=""fa fa-edit""></i>
    </a>
    ";
        }

        public string RenderDelete()
        {
            SetupDeleteScript();

            return $@"
        <a href=""javascript:void(0);"" data-id=""{GetKey()}"" class=""{Grid.GetGridRowName()}-delete"" >
           <i class=""fa fa-trash""></i>
        </a>
        ";
        }

        protected void SetupDeleteScript()
        {
            var trans = new Dictionary<string, string>{
                {"delete_confirm" ,Admin.Trans("delete_confirm") },
                {"confirm"        ,Admin.Trans("confirm") },
                { "cancel"        , Admin.Trans("cancel") },
            };

            //trans = array_merge($trans, $this->trans);

            var script = $@"

    $('.{Grid.GetGridRowName()}-delete').unbind('click').click(function() {{

                var id = $(this).data('id');

                swal({{
                title: ""{trans["delete_confirm"]}"",
            type: ""warning"",
            showCancelButton: true,
            confirmButtonColor: ""#DD6B55"",
            confirmButtonText: ""{trans["confirm"]}"",
            showLoaderOnConfirm: true,
            cancelButtonText: ""{trans["cancel"]}"",
            preConfirm: function() {{
                        return new Promise(function(resolve) {{
                    $.ajax({{
                        method: 'delete',
                        url: '{GetResource()}/' + id,
                        data:
    {{
    _method: 'delete',
                            _token: LA.token,
                        }},
                        success: function(data) {{
                            $.pjax.reload('#pjax-container');

                        resolve(data);
                    }}
                }});
            }});
        }}
    }}).then(function(result) {{
        var data = result.value;
        if (typeof data === 'object')
        {{
            if (data.status)
            {{
                swal(data.message, '', 'success');
            }}
            else
            {{
                swal(data.message, '', 'error');
            }}
        }}
    }});
    }});

";

            Admin.Script(script);
        }

        public void SetTrans(List<dynamic> tans)
        {
            trans = tans;
        }
    }
}
