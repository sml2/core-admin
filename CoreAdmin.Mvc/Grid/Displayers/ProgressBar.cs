﻿namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class ProgressBar : AbstractDisplayer
    {
        //public ProgressBar(object value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public string Display(LabelColor color = LabelColor.Primary, string size = "sm", int max = 100)
        {
            //$style = collect((array) $style)->map(function ($style) {
            //    return 'progress-bar-'.$style;
            //})->implode(' ');
            var style = "progress-bar-" + color.ToString().ToLower();

            return $@"
<div class=""row"" style=""min-width: 100px;"">
    <span class=""col-sm-3"" style=""color:#777;"">{Value}%</span>
    <div class=""progress progress-{size} col-sm-9"" style=""padding-left: 0;width: 100px;""> 
        <div class=""progress-bar {style}"" role=""progressbar"" aria-valuenow=""{Value}"" aria-valuemin=""0"" aria-valuemax=""{max}"" style=""width: {Value}%"">
        </div>
    </div>
</div>
";
        }
    }
}
