﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class SwitchDisplay : AbstractDisplayer
    {
        //public SwitchDisplay(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        protected Dictionary<string, SwitchState> states = new()
        {
            { "on", new SwitchState() { value = 1, text = "ON", color = "primary" } },
            { "off", new SwitchState { value = 0, text = "OFF", color = "default" } },
        };

        protected void OverrideStates(Dictionary<string, SwitchState> states)
        {
            if (states?.Count == 0)
            {
                return;
            }

            this.states = this.states.Merge(states);
        }

        public HtmlContent Display(Dictionary<string, SwitchState> states = null)
        {
            OverrideStates(states);

            return Admin.Component("~/Views/Grid/InlineEdit/Switch.cshtml", new()
            {
                { "class", "grid-switch-" + GetName().Replace(".", "-") },
                { "key", GetKey() },
                { "resource", GetResource() },
                { "name", GetPayloadName() },
                { "states", states },
                { "checked", states["on"].value.ToString() == GetValue() ? "checked" : "" },
            });
        }
    }

    public struct SwitchState
    {
        public int value;
        public string text;
        public string color;
    }
}
