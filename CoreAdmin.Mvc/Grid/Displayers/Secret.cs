﻿namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Secret : AbstractDisplayer
    {
        //public Secret(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public string Display(int dotCount = 6)
        {
            AddScript();

            var dots = new string('*', dotCount);

            return $@"
<span class=""secret-wrapper"">
    <i class=""fa fa-eye"" style=""cursor: pointer;""></i>
    &nbsp;
    <span class=""secret-placeholder"" style=""vertical-align: middle;"">{dots}</span>
    <span class=""secret-content"" style=""display: none;"">{GetValue()}</span>
</span>
";
        }
        

        protected void AddScript()
        {
            var script = $@"
$('.secret-wrapper i').click(function () {{
    $(this).toggleClass('fa-eye fa-eye-slash').parent().find('.secret-placeholder,.secret-content').toggle();
}});
";

            Admin.Script(script);
        }
    }
}
