﻿namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Orderable : AbstractDisplayer
    {
        //public Orderable(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public string Display()
        {
            //if (!trait_exists('\Spatie\EloquentSortable\SortableTrait')) {
            //    throw new \Exception('To use orderable grid, please install package [spatie/eloquent-sortable] first.');
            //}

            Admin.Script(Script());

            return $@"

<div class=""btn-group"">
    <button type=""button"" class=""btn btn-xs btn-info {Grid.GetGridRowName()}-orderable"" data-id=""{GetKey()}"" data-direction=""1"">
        <i class=""fa fa-caret-up fa-fw""></i>
    </button>
    <button type=""button"" class=""btn btn-xs btn-default {Grid.GetGridRowName()}-orderable"" data-id=""{GetKey()}"" data-direction=""0"">
        <i class=""fa fa-caret-down fa-fw""></i>
    </button>
</div>

";
        }

        protected string Script()
        {
            return $@"

$('.{Grid.GetGridRowName()}-orderable').on('click', function() {{

    var key = $(this).data('id');
    var direction = $(this).data('direction');

    $.post('{GetResource()}/' + key, {{_method:'PUT', _token:LA.token, _orderable:direction}}, function(data){{
        if (data.status) {{
            $.pjax.reload('#pjax-container');
            toastr.success(data.message);
        }}
    }});

}});
";
        }
    }
}
