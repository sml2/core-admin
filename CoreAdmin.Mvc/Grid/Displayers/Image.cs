﻿using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Image : AbstractDisplayer
    {
        //public Image(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public string Display(string server = "", int width = 200, int height = 200)
        {
            string func(string path)
            {
                var src = path;
                if (path.IsValidUrl() || path.Contains("data:image"))
                {
                    src = path;
                }
                else if (!string.IsNullOrEmpty(server))
                {
                    src = server.TrimEnd('/') + '/' + path.TrimStart('/');
                }
                else
                {
                    //src = Storage::disk(config('admin.upload.disk'))->url($path);
                }

                return $"<img src='{src}' style='max-width:{width}px;max-height:{height}px' class='img img-thumbnail' />";
            }

            return Value switch
            {
                IEnumerable<string> list => string.Join("%nbsp;", list.Where(v => !string.IsNullOrEmpty(v)).Select(func)),
                _ => func(Value.ToString())
            };
        }
    }
}
