﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class Table : AbstractDisplayer
    {
        //public Table(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public HtmlContent Display(List<string> titles)
    {
        if (string.IsNullOrEmpty(Value.ToString())) {
            return null;
        }

        //if (empty($titles)) {
        //    $titles = array_keys($this->value[0]);
        //}

        //if (Arr::isAssoc($titles)) {
        //    $columns = array_keys($titles);
        //} else {
        //    $titles = array_combine($titles, $titles);
        //    $columns = $titles;
        //}

        //$data = array_map(function ($item) use ($columns) {
        //    $sorted = [];

        //    $arr = Arr::only($item, $columns);

        //    foreach ($columns as $column) {
        //        if (array_key_exists($column, $arr)) {
        //            $sorted[$column] = $arr[$column];
        //        }
        //    }

        //    return $sorted;
        //}, $this->value);

        //$variables = [
        //    'titles' => $titles,
        //    'data'   => $data,
        //];

        return new()
        {
            Name = HtmlContentType.Partial,
            Value = "admin::grid.displayer.table",
            //Data = variables,
        };
    }
 
    }
}
