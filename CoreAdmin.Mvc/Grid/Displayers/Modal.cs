﻿using CoreAdmin.Mvc.Models;
using System;
using Microsoft.Extensions.Localization;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class Modal : AbstractDisplayer
    {
        private readonly IStringLocalizer<SharedResource> _stringLocalizer;

        public Modal(IStringLocalizer<SharedResource> stringLocalizer)
        {
            _stringLocalizer = stringLocalizer;
        }

        protected string Renderable;

        protected string GetLoadUrl()
        {
            var renderable = Renderable.Replace('\\', '_');

            //return route('admin.handle-renderable', compact('renderable'));
            return "";
        }

        public HtmlContent Display(Func<IIdexerModel, string> callback) => Display(_stringLocalizer["title"], callback);
        public HtmlContent Display(string title, Func<IIdexerModel, string> callback)
        {

            var html = "";
            var async = callback(Row);
            //if ($async = is_subclass_of($callback, Renderable::class)) {
            //    $this->renderable = $callback;
            //} else {
            //    $html = call_user_func_array($callback->bindTo($this->row), [$this->row]);
            //}

            return Admin.Component("~/Views/Components/ColumnModal", new()
            {
                { "url", GetLoadUrl() },
                { "async", async },
                //{"grid"    , is_subclass_of($callback, Simple::class)},
                { "title", title },
                { "html", html },
                { "key", GetKey() },
                { "value", Value },
                { "name", $"{GetKey()}-{GetColumn().GetName().Replace('.', '_')}" },
            });
        }
    }
}
