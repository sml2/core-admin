namespace CoreAdmin.Mvc.Grid.Displayers
{
    //class DropdownActions : Actions
    class DropdownActions
    {
        protected string view = "admin.grid.actions.dropdown";

        protected string[] custom = { };

        //protected string default = "";

        //protected List<string> defaultClass = new List<string>(Edit::class, Show::class, Delete::class);

        //public DropdownActions add(RowAction action)
        //{
        //    this.prepareAction(action);

        //    array_push(this.custom, action);

        //    return this;
        //}

        /**
         * Prepend default `edit` `view` `delete` actions.
         */
        //protected void prependDefaultActions()
        //{
        //    foreach (var class in this.defaultClass) {
        //        //action = new class();

        //        this.prepareAction(action);

        //        array_push(this.default, action);
        //    }
        //}

        /**
         * @param RowAction $action
         */
        //protected void prepareAction(RowAction action)
        //{
        //    action->setGrid(this.grid).setColumn(this.column).setRow(this.row);
        //}

        public DropdownActions disableView(bool disable = true)
        {
            if (disable)
            {
                //    //array_delete($this->defaultClass, Show::class);
            }
            //else if(!this.defaultClass.Contains(Show.class)) {
            //    array_push($this->defaultClass, Show::class);
            //}

            return this;
        }

        public DropdownActions disableDelete(bool disable = true)
        {
            //if (disable) {
            //    array_delete($this->defaultClass, Delete::class);
            //} elseif (!in_array(Delete::class, $this->defaultClass)) {
            //    array_push($this->defaultClass, Delete::class);
            //}

            return this;
        }

        public DropdownActions disableEdit(bool disable = true)
        {
            //if (disable) {
            //    array_delete(this.defaultClass, Edit.class);
            //} elseif (!in_array(Edit.class, this.defaultClass)) {
            //    array_push(this.defaultClass, Edit.class);
            //}

            return this;
        }

        public void display(string callback = null)
        {
            //if (callback instanceof \Closure) {
            //    callback.call(this, this);
            //}

            //if (this.disableAll) {
            //    return "";
            //}

            //this.prependDefaultActions();

            //Dictionary<string, dynamic> variables =
            //   new Dictionary<string, dynamic>
            //   {
            //       { "default" : this.default },
            //       { "custom" : this.custom }
            //    },

            //if (empty(variables['default']) && empty(variables['custom'])) {
            //    return;
            //}

            //return Admin.Component(this.view, variables);
        }
        //}
    }
}