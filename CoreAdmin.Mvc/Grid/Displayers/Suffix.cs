﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Suffix : AbstractDisplayer
    {
        //public Suffix(object value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        public string Display<T>(Func<IIdexerModel, T, string> suffix = null, string delimiter = "&nbsp;")
        {
            string suffixStr = suffix(Row, (T)GetValue());

            return $@"
{GetValue()}
{delimiter}
{suffixStr}
";
        }

    }
}
