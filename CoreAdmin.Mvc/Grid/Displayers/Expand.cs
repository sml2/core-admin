﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Expand : AbstractDisplayer
    {
        //public Expand(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        protected string renderable;

        public HtmlContent Display(Action callback = null, bool isExpand = false)
        {
            var html = "";
            var async = false;
            var loadGrid = false;

            //if (is_subclass_of($callback, Renderable::class)) {
            //    $this->renderable = $callback;
            //    $async = true;
            //    $loadGrid = is_subclass_of($callback, Simple::class);
            //} else {
            //    $html = call_user_func_array($callback->bindTo($this->row), [$this->row]);
            //}

            return Admin.Component("~/Views/Component/ColumnExpand", new()
            {
                { "key", GetKey() },
                { "url", GetLoadUrl() },
                { "name", $"{GetName()}-{GetKey()}".Replace('.', '-') },
                { "html", html },
                { "value", Value },
                { "async", async },
                { "expand", isExpand },
                { "loadGrid", loadGrid },
                { "elementClass", $"grid-expand-{Grid.GetGridRowName()}" },
            });
        }

        protected string GetLoadUrl()
        {
            //var renderable = renderable.Replace('\\', '_');

            //return route('admin.handle-renderable', compact('renderable'));
            return "";
        }

    }
}
