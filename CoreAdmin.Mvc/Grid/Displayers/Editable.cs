using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public class Editable : AbstractDisplayer
    {

        //public Editable(object value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{
        //}

        protected string[] arguments = { };

        protected string type = "";

        protected Dictionary<string, object> options = new Dictionary<string, object>{
                { "emptytext", "<i class=\"fa fa-pencil\"></i>" }
        };

        protected Dictionary<string, object> attributes = new();

        public void AddOptions(string key, object value)
        {
            this.options[key] = value;
        }
        public void AddOptions(Dictionary<string, object> options = null)
        {
            this.options = this.options.Merge(options);
        }
        public void AddAttributes(string key, object value)
        {
            this.attributes[key] = value;
        }
        public void AddAttributes(Dictionary<string, object> attributes)
        {
            this.attributes = this.attributes.Merge(attributes);
        }

        public void Text() { }
        public void Textarea() { }

        public struct ValueText
        {
            public string value { get; set; }
            public string text { get; set; }
            public bool hidden { get; set; }
            public bool disabled { get; set; }

        }

        public void Select(IEnumerable<ValueText> source)
        {
            var useClosure = false;

            // todo
            //if (options is Func<Editable, BaseModel, Enum> func) {
            //    useClosure = true;
            //    //options = func(this, row);
            //}
            

            if (useClosure)
            {
                AddAttributes("data-source", JsonSerializer.Serialize(source));
            }
            else
            {
                AddOptions("source", source);
            }
        }

        public void Date()
        {
            Combodate();
        }

        public void Datetime()
        {
            Combodate("YYYY-MM-DD HH:mm:ss");
        }

        public void Year()
        {
            Combodate("YYYY");
        }

        public void Month()
        {
            Combodate("MM");
        }

        public void Day()
        {
            Combodate("DD");
        }

        public void Time()
        {
            Combodate("HH:mm:ss");
        }

        public void Combodate(string format = "YYYY-MM-DD")
        {
            type = "combodate";

            this.AddOptions(
                new Dictionary<string, object> {
                    { "format" , format },
                    { "viewformat" , format },
                    { "template" , format },
                    { "combodate" ,
                        new Dictionary<string, int>{{ "maxYear", 2035}}
                    },
            });
        }

        protected void BuildEditableOptions(params object[] args)
        {
            type = args.Length > 0 ? args[0].ToString() : "text";
            var methodInfo = GetType().GetMethod(type.UpperCaseFirst());
            if (methodInfo != null)
            {
                methodInfo.Invoke(this, args[1..].Where(a => a != null).ToArray());
            } else
            {
                throw new ArgumentNullException($"未找到[{type}]方法");
            }

        }

        public string Display(string method, IEnumerable<ValueText> selectOptions = null)
        {
            // TODO: type添加枚举
            string column = GetName();
            options["name"] = column;
            var Class = "grid-editable-" + column.Replace(new[] { ".", "#", "[", "]" }, "-");

            BuildEditableOptions(method, selectOptions);

            var optionsJson = JsonSerializer.Serialize(options);

            optionsJson = optionsJson[..^1] + @"
        ,
        ""success"":function(response, newValue){
            if (response.status)
            {
                $.admin.toastr.success(response.message, '', { positionClass: ""toast-top-center""});
            }
            else
            {
                $.admin.toastr.error(response.message, '', { positionClass: ""toast-top-center""});
            }
        }
        }
        ";

            Admin.Script($"$('.{Class}').editable({optionsJson});");

            Value = WebUtility.HtmlEncode(Value?.ToString());

            var attributes = new Dictionary<string, object>() {
                { "href"       , "#" },
                {"class"      , Class},
                {"data-type"  , type},
                {"data-pk"    , $"{GetKey()}"},
                {"data-url"   , $"{GetResource()}/{GetKey()}".Replace("//","/")}, //防止出现"controll/",访问时出现"controll//id"的情况
                { "data-value" , $"{Value}"},
            };

            if (this.attributes.Count > 0)
            {
                attributes = attributes.Merge(this.attributes);
            }

            var attributesStr = string.Join(" ", attributes.Select((pair) => $"{pair.Key}='{pair.Value}'"));

            var html = type == "select" ? "" : Value;

            return $"<a {attributesStr}>{html}</a>";
        }
    }

}