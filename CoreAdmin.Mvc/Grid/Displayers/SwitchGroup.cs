﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class SwitchGroup : SwitchDisplay
    {
        //public SwitchGroup(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public string Display(List<string> columns, Dictionary<string, SwitchState> states = null)
        {
            OverrideStates(states);

            //if (!Arr::isAssoc($columns)) {
            //    $columns = collect($columns)->map(function ($column) {
            //        return [$column => ucfirst($column)];
            //    })->collapse();
            //}

            var html = new List<string>();

            //foreach (columns as $column => $label) {
            //    $html[] = BuildSwitch($column, $label);
            //}

            return "<table>" + string.Join("", html) + "</table>";
        }

        protected HtmlContent BuildSwitch(string name, string label = "")
        {
            return Admin.Component("admin::grid.inline-edit.switch-group", new()
            {
                { "class", "grid-switch-" + name.Replace(".", "-") },
                { "key", GetKey() },
                { "resource", GetResource() },
                { "name", GetPayloadName(name) },
                { "states", states },
                { "checked", states["on"].value == GetAttribute<int>(name) ? "checked" : "" },
                { "label", label },
            });
        }
    }
}
