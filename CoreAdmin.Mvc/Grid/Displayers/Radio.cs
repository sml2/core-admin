﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class Radio : AbstractDisplayer
    {
        //public Radio(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        public HtmlContent Display(Dictionary<string, dynamic> options)
        {
            return Admin.Component("admin::grid.inline-edit.radio", new()
            {
                { "key", GetKey() },
                { "name", GetPayloadName() },
                { "value", GetValue() },
                { "resource", GetResource() },
                { "trigger", $"ie-trigger-{GetClassName()}" },
                { "target", $"ie-template-{GetClassName()}" },
                { "display", options[GetValue().ToString()] ?? "" },
                { "options", options },
            });
        }
    }
}
