﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    class QRCode : AbstractDisplayer
    {
        //public QRCode(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}

        protected void AddScript()
        {
            var script = $@"
$('.grid-column-qrcode').popover({{
    html: true,
    container: 'body',
    trigger: 'focus'
}});
";

            Admin.Script(script);
        }

        public string Display(Func<object, IIdexerModel, string> formatter = null, int width = 150, int height = 150)
        {
            AddScript();

            var content = Value;

            if (formatter != null)
            {
                content = formatter(content, Row);
            }

            var img = $"<img src='https://api.qrserver.com/v1/create-qr-code/?size={width}x{height}&data={content}' style='height:{height}px;width:{width}px;'/>";

            return $@"
<a href=""javascript:void(0);"" class=""grid-column-qrcode text-muted"" data-content=""{img}"" data-toggle='popover' tabindex='0'>
    <i class=""fa fa-qrcode""></i>
</a>&nbsp;{GetValue()}
";
        }
    }
}
