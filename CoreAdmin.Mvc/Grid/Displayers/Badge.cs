﻿using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Grid.Displayers
{
    public enum BgColor
    {
        Red,
        Green,
        Yellow,
        Blue
    }
    class Badge : AbstractDisplayer
    {
        //public Badge(string value, BGrid grid, GColumn column, BaseModel row) : base(value, grid, column, row)
        //{

        //}
        
        public string Display(Dictionary<object, BgColor> dic)
        {
            string Func() => dic.TryGetValue(Value, out var v) ? v.ToString().ToLower() : BgColor.Red.ToString().ToLower();
            return Value switch
            {
                IEnumerable<object> list => string.Join("&nbsp;",
                    list.Select((name) => $"<span class='badge bg-{Func()}'>{name}</span>")),
                _ => $"<span class='badge bg-{Func()}'>{Value}</span>"
            };
        }

        public string Display(BgColor style = BgColor.Red)
        {
            var st = style.ToString().ToLower();
            return Value switch
            {
                IEnumerable<object> list => string.Join("&nbsp;",
                    list.Select((name) => $"<span class='badge bg-{st}'>{name}</span>")),
                _ => $"<span class='badge bg-{st}'>{Value}</span>"
            };

        }
        
    }
}
