﻿using CoreAdmin.Mvc.Grid.Filter;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Extensions;
using Microsoft.Extensions.Primitives;
using FLayout = CoreAdmin.Mvc.Grid.Filter.Layout.Layout;
using System.Linq.Dynamic.Core;
using CoreAdmin.Mvc.Grid.Column;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc.Grid
{
    public class BFilter : IRenderable
    {
        protected IQueryable<IIdexerModel> model;

        protected List<AbstractFilter> filters = new();

        protected static List<Type> Supports { get; } = new()
        {
            typeof(Equal),
            typeof(NotEqual),
            typeof(Ilike),
            typeof(Like),
            typeof(Gt),
            typeof(Lt),
            typeof(Between),
            typeof(Group),
            typeof(Where),
            typeof(In),
            typeof(NotIn),
            typeof(Date),
            typeof(Day),
            typeof(Month),
            typeof(Year),
            typeof(Hidden),
            typeof(Like),
            typeof(StartsWith),
            typeof(EndsWith),
        };


        protected bool useIdFilter = true;

        protected bool idFilterRemoved = false;

        protected string action;

        public string view = "~/Views/Filter/Container.cshtml";

        protected string filterID = "filter-box";

        protected string name;

        public bool expand = false;

        public List<Scope> Scopes { get; protected set; }

        protected FLayout layout;

        protected bool thisFilterLayoutOnly = false;

        protected List<string> layoutOnlyFilterColumns = new();

        protected string primaryKey;
        protected BGrid grid;

        private HttpContext Context { get; }

        public BFilter(IQueryable<IIdexerModel> model, HttpContext httpContext)
        {
            this.model = model;
            Context = httpContext;
            var primaryKey = model.GetKeyName();
            InitLayout();

            ResolveFilter<Equal>(primaryKey, primaryKey.ToUpper());
            Scopes = new();
        }

        protected void InitLayout() => layout = new FLayout(this);
        public void InitGrid(BGrid grid) => this.grid = grid;

        /// <summary>
        /// 设置请求url
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public BFilter SetAction(string action)
        {
            this.action = action;

            return this;
        }

        //    /**
        //     * Get grid model.
        //     *
        //     * @return Model
        //     */
        //    public function getModel()
        //    {
        //    $conditions = array_merge(
        //        $this->conditions(),
        //        $this->scopeConditions()
        //    );

        //        return $this->model->addConditions($conditions);
        //    }

        /// <summary>
        /// 设置UI fitler div id
        /// </summary>
        /// <param name="filterID"></param>
        /// <returns></returns>
        public BFilter SetFilterID(string filterID)
        {
            this.filterID = filterID;

            return this;
        }

        public string GetFilterID()
        {
            return filterID;
        }

        /// <summary>
        /// 设置UI div name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public BFilter SetName(string name)
        {
            this.name = name;

            SetFilterID($"{name}-{filterID}");

            return this;
        }

        public string GetName()
        {
            return name;
        }

        /// <summary>
        /// 禁用id过滤器
        /// </summary>
        /// <param name="disable"></param>
        /// <returns></returns>
        public BFilter DisableIdFilter(bool disable = true)
        {
            useIdFilter = !disable;

            return this;
        }


        public void RemoveIDFilterIfNeeded()
        {
            if (!useIdFilter && !idFilterRemoved)
            {
                RemoveDefaultIDFilter();

                layout.RemoveDefaultIDFilter();

                idFilterRemoved = true;
            }
        }

        protected void RemoveDefaultIDFilter()
        {
            filters.RemoveAt(0);
        }

        //    /**
        //     * Remove filter by filter id.
        //     *
        //     * @param mixed $id
        //     */
        //    public function removeFilterByID($id)
        //    {
        //    $this->filters = array_filter($this->filters, function(AbstractFilter $filter) use($id) {
        //            return $filter->getId() != $id;
        //        });
        //    }

        /// <summary>
        /// 搜索条件
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> Conditions()
        {
        // $inputs = Arr::dot(request()->all());
        var inputs = Context.Request.Query.Where(x => !string.IsNullOrEmpty( x.Value)).ToDictionary(x => x.Key, x => x.Value);


        SanitizeInputs(ref inputs);

            if (inputs.Count == 0)
            {
                return null;
            }

            var paramsList = new Dictionary<string, object>();

            foreach (var (key, value) in inputs) {
                paramsList[key] = value;
            }

            var conditions = new Dictionary<string, string>();

            RemoveIDFilterIfNeeded();

            foreach (var filter in filters)
            {
                string column;
                if (layoutOnlyFilterColumns.Contains((column = filter.GetColumn())))
                {
                    filter.Default(paramsList[column].ToString());
                }
                else
                {
                    
                    var pair = filter.Condition(paramsList);
                    if (pair is { } valuePair)
                    {
                        conditions.Add(valuePair.Key, valuePair.Value);
                    }

                }
            }
            if (conditions.Count > 0)
            {
                Expand();
            }
            return conditions;
        }
        
        protected void SanitizeInputs(ref Dictionary<string, StringValues> inputs)
        {
            if (string.IsNullOrEmpty(name))
            {
                return;
            }

            inputs = inputs.Where((pair) => pair.Value.ToString().StartsWith($"{name}_")).Select(pair => {
                var (s, value) = pair;
                var key = s.Replace($"{name}_", "");

                return KeyValuePair.Create(key, value);
            }).ToDictionary(x => x.Key, x => x.Value);
        }

        public BFilter LayoutOnly()
        {
            thisFilterLayoutOnly = true;

            return this;
        }

        protected AbstractFilter AddFilter(AbstractFilter filter)
        {
            layout.AddFilter(filter);

            filter.SetParent(this);

            if (thisFilterLayoutOnly)
            {
                thisFilterLayoutOnly = false;
                layoutOnlyFilterColumns.Add(filter.GetColumn());
            }

            filters.Add(filter);
            return filter;
        }

        public AbstractFilter Use(AbstractFilter filter)
        {
            return AddFilter(filter);
        }

        public List<AbstractFilter> Filters()
        {
            return filters;
        }

        public Scope Scope(string key, string label = "")
        {
            var scope = new Scope(key, label);
            Scopes.Add(scope);
            return scope;
        }

        public Scope ScopeSeparator()
        {
            return Scope(Filter.Scope.SEPARATOR);
        }

        public Scope GetCurrentScope()
        {
            var key = Context.Request.Get(Filter.Scope.QUERY_NAME);
            return Scopes.FirstOrDefault(scope => scope.key == key);
        }

        protected List<dynamic> ScopeConditions()
        {
            var scope = GetCurrentScope();
            return scope != null ? scope.Condition() : new List<dynamic>();
        }

        public BFilter Column(double width, Action<BFilter> closure)
        {
            width = width < 1 ? Math.Round(12 * width) : width;
            layout.Column((int) width, closure);
            return this;
        }

        public BFilter Expand()
        {
            expand = true;
            return this;
        }

        public Task<ListPaginator<IIdexerModel>> Execute(bool toArray = true)
        {
            //if (method_exists($this->model->eloquent(), 'paginate'))
            //{
            //    $this->model->usePaginate(true);

            //    return $this->model->buildData($toArray);
            //}
            var conditions = Conditions();
            //var scopeConditions = ScopeConditions();

            //$conditions = array_merge(
            //    $this->conditions(),
            //    $this->scopeConditions()
            //);
            if (conditions != null)
            {
                model = conditions.Aggregate(model, (m, pair) => pair.Key switch
                {
                    "where" => m.Where(pair.Value),
                    _ => throw new NotImplementedException(pair.Key)
                });
            }

            var sort = Sort();
            if (sort is { } s)
            {
                model =  model.OrderBy(s.Key + (s.Value == "asc" ? "" : " descending"));
            }
            var page = 1;
            var reqPage = Context.Request.Get(ListPaginator<IIdexerModel>.PageKeyName);
            if (!string.IsNullOrEmpty(reqPage) && int.TryParse(reqPage, out var v))
            {
                page = v;
            }

            //return $this->model->addConditions($conditions)->buildData($toArray);
            return ListPaginator.CreateAsync(model, page, grid.perPage, grid.Context);
        }

        private KeyValuePair<string, string>? Sort()
        {
            var sort = Context.Request.GetDic(Sorter.SortName);
            if (null != sort && sort.Any())
            {
                return KeyValuePair.Create(sort["column"], sort["type"]);
            }

            return null;
        }

        //    /**
        //     * @param callable $callback
        //     * @param int      $count
        //     *
        //     * @return bool
        //     */
        //    public function chunk(callable $callback, $count = 100)
        //{
        //    $conditions = array_merge(
        //        $this->conditions(),
        //        $this->scopeConditions()
        //    );

        //    return $this->model->addConditions($conditions)->chunk($callback, $count);
        //}
        public string Render() => "";

        public HtmlContent Render(bool t)
        {
            RemoveIDFilterIfNeeded();

            if (filters == null || filters.Count == 0)
            {
                return null;
            }

            return new()
            {
                Name = HtmlContentType.Partial,
                Value = view,
                Data = new()
                {
                    {"action", action ?? UrlWithoutFilters()},
                    {"layout", layout},
                    {"filterID", filterID},
                    {"expand", expand},
                }
            };
        }

        public string UrlWithoutFilters()
        {
            var columns = filters.Select(filter => filter.GetColumn());

            var pageKey = "page";
            //var gridName = gr
            //if ($gridName = $this->model->getGrid()->getName()) {
            //    $pageKey = "{$gridName}_{$pageKey}";
            //}

            columns = columns.Append(pageKey);

            var groupNames = filters.Where((filter) => filter is Group).Select((filter) => $"{filter.Id}_group");
            columns = columns.Concat(groupNames);

            return Context.Request.FullUrlWithoutQuery(
                columns.ToList()
            );
        }

        public string UrlWithoutScopes()
        {
            return grid.Context.Request.FullUrlWithoutQuery(Filter.Scope.QUERY_NAME);
        }

        ///**
        // * @param string $name
        // * @param string $filterClass
        // */
        //public static function extend($name, $filterClass)
        //{
        //    if (!is_subclass_of($filterClass, AbstractFilter::class)) {
        //        throw new \InvalidArgumentException("The class [$filterClass] must be a type of ".AbstractFilter::class.'.');
        //    }

        //    static::$supports[$name] = $filterClass;
        //}

        /// <summary>
        /// 生成过滤器
        /// </summary>
        /// <param name="arguments"></param>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public TModel ResolveFilter<TModel>(params object[] arguments) where TModel : AbstractFilter
        {
            if (!Supports.Contains(typeof(TModel))) throw new ArgumentException($"不支持{typeof(TModel).Name}过滤器");

            var filter = Activator.CreateInstance(typeof(TModel), arguments) as TModel;
            AddFilter(filter);
            return filter;
        }
    }
}