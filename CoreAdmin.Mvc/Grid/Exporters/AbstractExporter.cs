﻿using CoreAdmin.Mvc.Models;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.Grid.Exporters
{
    public abstract class AbstractExporter
    {

        protected BGrid grid;

        protected int page;

        public AbstractExporter(BGrid grid = null)
        {
            if (grid != null)
            {
                SetGrid(grid);
            }
        }

        public AbstractExporter SetGrid(BGrid grid)
        {
            this.grid = grid;

            return this;
        }

        //public string GetTable()
        //{
        //    return grid->model()->getOriginalModel()->getTable();
        //}

        public Task<ListPaginator<IIdexerModel>> GetData(bool toArray = true)
        {
            return grid.GetFilter().Execute(toArray);
        }

        /**
         * @param callable $callback
         * @param int      $count
         *
         * @return bool
         */
        //public function Chunk(callable $callback, $count = 100)
        //{
        //    grid.ApplyQuery();

        //    return grid.GetFilter().Chunk($callback, $count);
        //}

        public Task<ListPaginator<IIdexerModel>> GetCollection()
        {
            return GetData();
        }

        /**
         * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
         */
        //public function GetQuery()
        //{
        //    $model = $this->grid->getFilter()->getModel();

        //    $queryBuilder = $model->getQueryBuilder();

        //    // Export data of giving page number.
        //    if ($this->page) {
        //        $keyName = $this->grid->getKeyName();
        //        $perPage = request($model->getPerPageName(), $model->getPerPage());

        //        $scope = (clone $queryBuilder)
        //            ->select([$keyName])
        //            ->setEagerLoads([])
        //            ->forPage($this->page, $perPage)->get();
        //        // If $querybuilder is a Model, it must be reassigned, unless it is a eloquent/query builder.
        //        $queryBuilder = $queryBuilder->whereIn($keyName, $scope->pluck($keyName));
        //    }

        //    return $queryBuilder;
        //}

        /**
         * Export data with scope.
         *
         * @param string $scope
         *
         * @return $this
         */
        //public function withScope($scope)
        //{
        //    if ($scope == Grid\Exporter::SCOPE_ALL) {
        //        return $this;
        //    }

        //    list($scope, $args) = explode(':', $scope);

        //    if ($scope == Grid\Exporter::SCOPE_CURRENT_PAGE) {
        //        $this->grid->model()->usePaginate(true);
        //        $this->page = $args ?: 1;
        //    }

        //    if ($scope == Grid\Exporter::SCOPE_SELECTED_ROWS) {
        //        $selected = explode(',', $args);
        //        $this->grid->model()->whereIn($this->grid->getKeyName(), $selected);
        //    }

        //    return $this;
        //}

        abstract public void Export();
    }
}
