﻿using CoreAdmin.Mvc.Models;

namespace CoreAdmin.Mvc.Show
{
    public abstract class AbstractField
    {
        protected string value;

        protected BaseModel model;

        public bool border = true;

        public bool escape = true;

        public AbstractField SetValue(string value)
        {
            this.value = value;

            return this;
        }

        public AbstractField setModel(BaseModel model)
        {
            this.model = model;

            return this;
        }
        abstract public string Render();
    }
}
