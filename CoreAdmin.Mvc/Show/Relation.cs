﻿using CoreAdmin.Mvc.Models;
using System;

namespace CoreAdmin.Mvc.Show
{
    public class Relation : Field
    {

        protected string name;

        protected Action builder;

        protected string title;

        protected IIdexerModel model;

        public Relation(string name, Action builder, string title = "")
        {
            this.name = name;
            this.builder = builder;
            this.title = FormatLabel(title);
        }

        public Relation SetModel(IIdexerModel model)
        {
            this.model = model;

            return this;
        }

        //protected function GetNullRenderable()
        //{
        //    return new class() implements Renderable {
        //        public function render()
        //        {
        //        }
        //    };
        //}

        public string Render()
        {
            //relation = model->{this->name}();

            //$renderable = $this->getNullRenderable();

            //if ($relation    instanceof HasOne
            //    || $relation instanceof BelongsTo
            //    || $relation instanceof MorphOne
            //) {
            //    $model = $this->model->{$this->name};

            //    if (!$model instanceof Model) {
            //        $model = $relation->getRelated();
            //    }

            //    $renderable = new Show($model, $this->builder);

            //    $renderable->panel()->title($this->title);
            //}

            //if ($relation    instanceof HasMany
            //    || $relation instanceof MorphMany
            //    || $relation instanceof BelongsToMany
            //    || $relation instanceof HasManyThrough
            //) {
            //    $renderable = new Grid($relation->getRelated(), $this->builder);

            //    $renderable->setName($this->name)
            //        ->setTitle($this->title)
            //        ->setRelation($relation);
            //}

            //return $renderable->render();
            return "";
        }
    }
}
