﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Show;
using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Show
{
    public class Panel
    {
        protected string view = "~/Views/Show/Panel.cshtml";

        protected List<Field> fields;

        protected ViewShowPanel data;

        protected BShow parent;

        public Panel(BShow show)
        {
            parent = show;
            InitData();
        }

        protected void InitData()
        {
            data = new () {
            Fields = new List<Field>(),
            Tools = new Tools(this),
            Style = "info",
            Title = Admin.Trans("detail"),
        };
        }

        public Panel SetParent(BShow show)
        {
            parent = show;
            return this;
        }

        public BShow GetParent()
        {
            return parent;
        }

        public Panel Style(string style = "info")
        {
            data.Style = style;
            return this;
        }

        public Panel Title(string title)
        {
            data.Title = title;
            return this;
        }

        public Panel View(string view)
        {
            this.view = view;
            return this;
        }

        public void Tools(Action<Tools> callable)
        {
            callable.Invoke(data.Tools);
        }

        public Panel Fill(List<Field> fields)
        {
            data.Fields = fields;
            return this;
        }

        public HtmlContent Render()
        {
            return new()
            {
                Name = HtmlContentType.Partial,
                Value = view,
                Form = data,
            };
        }
    }
}
