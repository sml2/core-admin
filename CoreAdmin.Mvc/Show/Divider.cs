﻿namespace CoreAdmin.Mvc.Show
{
    class Divider : Field
    {
        public string Render()
        {
            return "<hr>";
        }
    }
}
