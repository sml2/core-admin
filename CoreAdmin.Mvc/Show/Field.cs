﻿using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Struct.Show;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Show
{
    public class Field
    {
        //     use Macroable {
        //    __call as macroCall;
        //}

        protected string view = "~/Views/Show/Field.cshtml";

        protected string name;

        protected string label;

        protected Dictionary<string, int> width = new()
        {
            { "label", 2 },
            { "field", 8 },
        };

        protected bool escape = true;

        protected object value;

        public delegate object ShowAsAction(IIdexerModel model, object value = null);
        protected List<ShowAsAction> showAs;

        protected BShow parent;

        protected string relation;

        public bool border = true;

        protected Dictionary<string, string> fileTypes = new()
        {
            { "image", "png|jpg|jpeg|tmp|gif" },
            { "word", "doc|docx" },
            { "excel", "xls|xlsx|csv" },
            { "powerpoint", "ppt|pptx" },
            { "pdf", "pdf" },
            { "code", "php|js|java|python|ruby|go|c|cpp|sql|m|h|json|html|aspx" },
            { "archive", "zip|tar\\.gz|rar|rpm" },
            { "txt", "txt|pac|log|md" },
            { "audio", "mp3|wav|flac|3pg|aa|aac|ape|au|m4a|mpc|ogg" },
            { "video", "mkv|rmvb|flv|mp4|avi|wmv|rm|asf|mpeg" },
        };

        public Field(string name = "", string label = "")
        {
            this.name = name;

            this.label = FormatLabel(label);

            showAs = new();
        }

        public Field SetParent(BShow show)
        {
            parent = show;

            return this;
        }

        public string GetName()
        {
            return name;
        }

        protected string FormatLabel(string label)
        {
            label ??= name.UpperCaseFirst();
            return label.Replace(".", " ").Replace("_", " ");
        }

        public string GetLabel()
        {
            return label;
        }

        public Field As<T>(Func<T, object> callable)
        {
            showAs.Add((model, val) => callable.Invoke((T)val));
            return this;
        }

        public Field Using(Dictionary<string, dynamic> values, dynamic _default = null)
        {
            return As<object>((value) =>
            {
                if (value == null)
                {
                    return _default;
                }

                return values.ContainsKey((string)value) ? values["value"] : _default;
            });
        }

        //public Field Image(string server = "", int width = 200, int height = 200)
        //{
        //    return Unescape().As((images) => {
        //        return string.Join("&nbsp;", images.Select((path) => {
        //            if (string.IsNullOrEmpty(path)) {
        //                return "";
        //            }

        //            var src = "";
        //            //if (url()->isValidUrl($path)) {
        //            if (path != null) {
        //                src = path;
        //            } else if (server) {
        //                //src = server.path;
        //            } else {
        //                //disk = config('admin.upload.disk');

        //                //if (config("filesystems.disks.{disk}")) {
        //                //    src = Storage::disk(disk)->url(path);
        //                //} else {
        //                //    return '';
        //                //}
        //            }

        //            return $"<img src='src' style='max-width:{width}px;max-height:{height}px' class='img' />";
        //        }));
        //    });
        //}

        /**
         * Show field as a carousel.
         *
         * @param int    $width
         * @param int    $height
         * @param string $server
         *
         * @return Field
         */
        //public Field Carousel(int width = 300, int height = 200, string server = "")
        //{
        //    return Unescape().As((images) => {
        //        var items = images.Select((path) => {
        //            if (string.IsNullOrEmpty(path)) {
        //                return "";
        //            }

        //            var image = "";
        //            //if (url()->isValidUrl($path)) {
        //            if (true) {
        //                image = path;
        //            }
        //            //} elseif ($server) {
        //            //    $image = $server.$path;
        //            //} else {
        //            //    $disk = config("admin.upload.disk");

        //            //    if (config("filesystems.disks.{$disk}")) {
        //            //        $image = Storage::disk($disk)->url($path);
        //            //    } else {
        //            //        $image = '';
        //            //    }
        //            //}
        //            return new Dictionary<string, string> {
        //                { "image", image },
        //                {"caption", "" },
        //            };
        //        });

        //        return (new Carousel($items))->width($width)->height($height);
        //    });
        //}

        //    /**
        //     * Show field as a file.
        //     *
        //     * @param string $server
        //     * @param bool   $download
        //     *
        //     * @return Field
        //     */
        //    public function file($server = '', $download = true)
        //    {
        //        $field = $this;

        //        return $this->unescape()->as(function ($path) use ($server, $download, $field) {
        //            $name = basename($path);

        //            $field->border = false;

        //            $size = $url = '';

        //            if (url()->isValidUrl($path)) {
        //                $url = $path;
        //            } elseif ($server) {
        //                $url = $server.$path;
        //            } else {
        //                $storage = Storage::disk(config('admin.upload.disk'));
        //                if ($storage->exists($path)) {
        //                    $url = $storage->url($path);
        //                    $size = ($storage->size($path) / 1000).'KB';
        //                }
        //            }

        //            if (!$url) {
        //                return '';
        //            }

        //            $download = $download ? "download='$name'" : '';

        //            return <<<HTML
        //<ul class="mailbox-attachments clearfix">
        //    <li style="margin-bottom: 0;">
        //      <span class="mailbox-attachment-icon"><i class="fa {$field->getFileIcon($name)}"></i></span>
        //      <div class="mailbox-attachment-info">
        //        <div class="mailbox-attachment-name">
        //            <i class="fa fa-paperclip"></i> {$name}
        //            </div>
        //            <span class="mailbox-attachment-size">
        //              {$size}&nbsp;
        //              <a href="{$url}" class="btn btn-default btn-xs pull-right" target="_blank" $download><i class="fa fa-cloud-download"></i></a>
        //            </span>
        //      </div>
        //    </li>
        //  </ul>
        //HTML;
        //        });
        //    }

        //    /**
        //     * Show field as a link.
        //     *
        //     * @param string $href
        //     * @param string $target
        //     *
        //     * @return Field
        //     */
        //    public function link($href = '', $target = '_blank')
        //    {
        //        return $this->unescape()->as(function ($link) use ($href, $target) {
        //            $href = $href ?: $link;

        //            return "<a href='$href' target='{$target}'>{$link}</a>";
        //        });
        //    }

        public Field Label(string style = "success")
        {
            return Unescape().As<object>((value) => {
                List<string> arr;
                if (value is string str) {
                    arr = new List<string>() { str };
                } else
                {
                    arr = (List<string>)value;
                }

                return string.Join("&nbsp;", arr.Select((name) =>  $"<span class='label label-{style}'>{name}</span>"));
            });
        }

        //    /**
        //     * Show field as badges.
        //     *
        //     * @param string $style
        //     *
        //     * @return Field
        //     */
        //    public function badge($style = 'blue')
        //    {
        //        return $this->unescape()->as(function ($value) use ($style) {
        //            if ($value instanceof Arrayable) {
        //                $value = $value->toArray();
        //            }

        //            return collect((array) $value)->map(function ($name) use ($style) {
        //                return "<span class='badge bg-{$style}'>$name</span>";
        //            })->implode('&nbsp;');
        //        });
        //    }

        //    /**
        //     * Show field as json code.
        //     *
        //     * @return Field
        //     */
        //    public function json()
        //    {
        //        $field = $this;

        //        return $this->unescape()->as(function ($value) use ($field) {
        //            if (is_string($value)) {
        //                $content = json_decode($value, true);
        //            } else {
        //                $content = $value;
        //            }

        //            if (json_last_error() == 0) {
        //                $field->border = false;

        //                return '<pre><code>'.json_encode($content, JSON_PRETTY_PRINT).'</code></pre>';
        //            }

        //            return $value;
        //        });
        //    }

        //    /**
        //     * Show readable filesize for giving integer size.
        //     *
        //     * @return Field
        //     */
        //    public function filesize()
        //    {
        //        return $this->as(function ($value) {
        //            return file_size($value);
        //        });
        //    }

        //    /**
        //     * Get file icon.
        //     *
        //     * @param string $file
        //     *
        //     * @return string
        //     */
        //    public function getFileIcon($file = '')
        //    {
        //        $extension = File::extension($file);

        //        foreach ($this->fileTypes as $type => $regex) {
        //            if (preg_match("/^($regex)$/i", $extension) !== 0) {
        //                return "fa-file-{$type}-o";
        //            }
        //        }

        //        return 'fa-file-o';
        //    }

        public Field SetEscape(bool escape = true)
        {
            this.escape = escape;
            return this;
        }


        public Field Unescape()
        {
            return SetEscape(false);
        }

        public Field SetValue(IIdexerModel model)
        {
            //if ($this->relation) {
            //    if (!$relationValue = $model->{$this->relation}) {
            //        return $this;
            //    }

            //        $this->value = $relationValue;
            //} else
            //{
                if (name.Contains('.'))
                {
                        value = model.Get(name);
                }
                else
                {
                        value = model.Get(name);
                }
            //}

            return this;
        }

        //    /**
        //     * Set relation name for this field.
        //     *
        //     * @param string $relation
        //     *
        //     * @return $this
        //     */
        //    public function setRelation($relation)
        //    {
        //        $this->relation = $relation;

        //        return $this;
        //    }

        //    /**
        //     * @param Model  $model
        //     * @param string $name
        //     *
        //     * @return mixed
        //     */
        //    protected function getRelationValue($model, $name)
        //    {
        //        list($relation, $key) = explode('.', $name);

        //        if ($related = $model->getRelationValue($relation)) {
        //            return $related->getAttribute($key);
        //        }
        //    }

        //    /**
        //     * Set width for field and label.
        //     *
        //     * @param int $field
        //     * @param int $label
        //     *
        //     * @return $this
        //     */
        //    public function setWidth($field = 8, $label = 2)
        //    {
        //        $this->width = [
        //            'label' => $label,
        //            'field' => $field,
        //        ];

        //        return $this;
        //    }

        //    /**
        //     * Call extended field.
        //     *
        //     * @param string|AbstractField|\Closure $abstract
        //     * @param array                         $arguments
        //     *
        //     * @return Field
        //     */
        //    protected function callExtendedField($abstract, $arguments = [])
        //    {
        //        if ($abstract instanceof \Closure) {
        //            return $this->as($abstract);
        //        }

        //        if (is_string($abstract) && class_exists($abstract)) {
        //            /** @var AbstractField $extend */
        //            $extend = new $abstract();
        //        }

        //        if ($abstract instanceof AbstractField) {
        //            /** @var AbstractField $extend */
        //            $extend = $abstract;
        //        }

        //        if (!isset($extend)) {
        //            admin_warning("[$abstract] is not a valid Show field.");

        //            return $this;
        //        }

        //        if (!$extend->escape) {
        //            $this->unescape();
        //        }

        //        $field = $this;

        //        return $this->as(function ($value) use ($extend, $field, $arguments) {
        //            if (!$extend->border) {
        //                $field->border = false;
        //            }

        //            $extend->setValue($value)->setModel($this);

        //            return $extend->render(...$arguments);
        //        });
        //    }

        //    /**
        //     * @param string $method
        //     * @param array  $arguments
        //     *
        //     * @return $this
        //     */
        //    public function __call($method, $arguments = [])
        //    {
        //        if ($class = Arr::get(Show::$extendedFields, $method)) {
        //            return $this->callExtendedField($class, $arguments);
        //        }

        //        if (static::hasMacro($method)) {
        //            return $this->macroCall($method, $arguments);
        //        }

        //        if ($this->relation) {
        //            $this->name = $method;
        //            $this->label = $this->formatLabel(Arr::get($arguments, 0));
        //        }

        //        return $this;
        //    }

            protected ViewShowField Variables()
            {
                return new() {
                    Content = value,
                    Escape = escape,
                    Label = GetLabel(),
                    Wrapped = border,
                    Width = width,
                };
            }

        public HtmlContent Render()
        {
            if (showAs.Count > 0) {
                    showAs.ForEach((callable) => {
                        value = callable.Invoke(
                            parent.GetModel(),
                            value
                        );
                });
            }

            return new() {
                Value = view,
                Form = Variables()
            };
        }
    }
}
