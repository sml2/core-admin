﻿using CoreAdmin.Lib;
using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Show
{
    public class Tools
    {
        protected Panel panel;

        protected string resource;

        protected List<string> tools = new() { "delete", "edit", "list" };

        protected List<string> appends;

        protected List<string> prepends;

        public Tools(Panel panel)
        {
            this.panel = panel;

            this.appends = new();
            prepends = new();
        }

        public Tools Append(string tool)
        {
            appends.Add(tool);
            return this;
        }

        public Tools prepend(string tool)
        {
            prepends.Add(tool);
            return this;
        }


        public string GetResource()
        {
            if (string.IsNullOrEmpty(resource))
            {
                resource = panel.GetParent().GetResourcePath();
            }

            return resource;
        }

        protected Tools DisableActions(string action, bool disable)
        {
            if (disable)
            {
                tools.Remove(action);
            }
            else if (!tools.Contains(action))
            {
                tools.Add(action);
            }

            return this;
        }

        public Tools DisableList(bool disable = true) => DisableActions("list", disable);

        public Tools DisableDelete(bool disable = true) => DisableActions("delete", disable);

        public Tools DisableEdit(bool disable = true) => DisableActions("edit", disable);

        protected string GetListPath() => GetResource().TrimEnd('/');

        protected string GetEditPath() => $"{GetListPath()}/edit/{panel.GetParent().GetModel().GetKey()}";

        protected string GetDeletePath() => $"{GetListPath()}/{panel.GetParent().GetModel().GetKey()}";

        protected string RenderList()
        {
            var list = Admin.Trans("list");

            return $@"
        <div class=""btn-group pull-right"" style=""margin-right: 5px"">
            <a href=""{GetListPath()}"" class=""btn btn-sm btn-default"" title=""{list}"">
                <i class=""fa fa-list""></i><span class=""hidden-xs""> {list}</span>
            </a>
        </div>
        ";
        }

        protected string RenderEdit()
        {
            var edit = Admin.Trans("edit");

            return $@"
        <div class=""btn-group pull-right"" style=""margin-right: 5px"">
            <a href=""{GetEditPath()}"" class=""btn btn-sm btn-primary"" title=""{edit}"">
                <i class=""fa fa-edit""></i><span class=""hidden-xs""> {edit}</span>
            </a>
        </div>
        ";
        }

        protected string RenderDelete()
        {
            var trans = new Dictionary<string, string> {
                    { "delete_confirm" , Admin.Trans("delete_confirm")},
                    {"confirm"        , Admin.Trans("confirm")},
                    {"cancel"         , Admin.Trans("cancel")},
                    { "delete"         , Admin.Trans("delete")},
                };

            var Class = Str.Uniqid();

            var script = $@"

        $('.{Class}-delete').unbind('click').click(function() {{

            swal({{
                title: ""{trans["delete_confirm"]}"",
                type: ""warning"",
                showCancelButton: true,
                confirmButtonColor: ""#DD6B55"",
                confirmButtonText: ""{trans["confirm"]}"",
                showLoaderOnConfirm: true,
                cancelButtonText: ""{trans["cancel"]}"",
                preConfirm: function() {{
                    return new Promise(function(resolve) {{
                        $.ajax({{
                            method: 'delete',
                            url: '{GetDeletePath()}',
                            data: {{
                                _method:'delete',
                                _token:LA.token,
                            }},
                            success: function (data) {{
                                $.pjax({{container:'#pjax-container', url: '{GetListPath()}' }});

                                resolve(data);
                            }}
                        }});
                    }});
                }}
            }}).then(function(result) {{
                var data = result.value;
                if (typeof data === 'object') {{
                    if (data.status) {{
                        swal(data.message, '', 'success');
                    }} else {{
                        swal(data.message, '', 'error');
                    }}
                }}
            }});
        }});

        ";
            Admin.Script(script);

            return $@"
        <div class=""btn-group pull-right"" style=""margin-right: 5px"">
            <a href=""javascript:void(0);"" class=""btn btn-sm btn-danger {Class}-delete"" title=""{trans["delete"]}"">
                <i class=""fa fa-trash""></i><span class=""hidden-xs"">  {trans["delete"]}</span>
            </a>
        </div>
        ";
        }

        protected string RenderCustomTools(List<object> tools)
        {
            return string.Join(" ", tools.Select((tool) =>
            {
                if (tool is IRenderable render)
                {
                    // TODO:
                    return render.Render().ToString();
                }

                if (tool is IHtmlable html)
                {
                    return html.ToHtml();
                }

                return (string)tool;
            }));
        }


        public string Render()
        {
            var output = new StringBuilder();
            output.Append(RenderCustomTools(prepends.Cast<object>().ToList()));

            foreach (var tool in tools)
            {
                var renderMethod = "Render" + tool.UpperCaseFirst();

                output.Append(GetType().GetMethod(renderMethod, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, null).ToString());
            }

            return output.Append(RenderCustomTools(appends.Cast<object>().ToList())).ToString();
        }

        public override string ToString()
        {
            return Render();
        }
    }
}
