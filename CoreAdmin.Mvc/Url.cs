﻿using System;

namespace CoreAdmin.Mvc
{
    public class Url
    {
        string url;
        public Url(string url) => this.url = url;
        // '~^(#|//|https?://|(mailto|tel|sms):)~'
        [Obsolete]
        public bool IsValidUrl() => url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("mailto:") || url.StartsWith("tel:") || url.StartsWith("sms:");

        public override string ToString() => url;
    }
}
