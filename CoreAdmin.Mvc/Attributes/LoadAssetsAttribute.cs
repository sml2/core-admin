﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LoadAssetsAttribute : Attribute
    {
        public string Css { get; set; }
        public string Js { get; set; }

        public IEnumerable<string> CssList =>
            Css?.Split(',', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        public IEnumerable<string> JsList =>
            Js?.Split(',', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
    }
}