﻿namespace CoreAdmin.Mvc.Actions
{
    abstract public class GridAction : Action
    {
        protected BGrid Parent;

        /// <summary>
        /// 赋值 用于调用
        /// </summary>
        public GridAction()
        {
            SelectorPrefix = ".grid-action-";
        }
        /// <summary>
        /// 用于调用
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public GridAction SetGrid(BGrid grid)
        {
            Parent = grid;
            return this;
        }
        /// <summary>
        /// 用于调用链接判断
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public override string GetResource()
        {
            return Parent.Resource();
        }

        //    /**
        //     * @return mixed
        //     */
        //    protected function getModelClass()
        //    {
        //    $model = $this->parent->model()->getOriginalModel();

        //        return str_replace('\\', '_', get_class($model));
        //    }

        //    /**
        //     * @return array
        //     */
        //    public function parameters()
        //    {
        //        return ['_model' => $this->getModelClass()];
        //    }

        //    /**
        //     * Indicates if model uses soft-deletes.
        //     *
        //     * @param $modelClass
        //     *
        //     * @return bool
        //     */
        //    protected function modelUseSoftDeletes($modelClass)
        //    {
        //        return in_array(SoftDeletes::class, class_uses_deep($modelClass));
        //}
    }
}
