﻿using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc.Actions.Interactor
{
    public class Dialog : Interactor
    {
        protected bool UploadFile = false;

        protected Dictionary<string, object> Settings;

        public Dialog(Action action) : base(action)
        {
            Settings = new();
        }

        public Dialog Success(string title, string text = "", Dictionary<string, string> options = null)
        {
            return AddSettings(title, "success", text, options);
        }

        public Dialog Error(string title, string text = "", Dictionary<string, string> options = null)
        {
            return AddSettings(title, "error", text, options);
        }

        public Dialog Warning(string title, string text = "", Dictionary<string, string> options = null)
        {
            return AddSettings(title, "warning", text, options);
        }

        public Dialog Info(string title, string text = "", Dictionary<string, string> options = null)
        {
            return AddSettings(title, "info", text, options);
        }

        public Dialog Question(string title, string text = "", Dictionary<string, string> options = null)
        {
            return AddSettings(title, "question", text, options);
        }

        public Dialog Confirm(string title, string text = "", Dictionary<string, string> options = null)
        {
            return AddSettings(title, "question", text, options);
        }
        protected Dialog AddSettings(string title, string type, string text = "", Dictionary<string, string> options = null)
        {
            Settings = new()
            {
                { "title", title },
                { "text", text },
                { "type", type },
                { "options", options },

            };

            return this;
        }

       /// <summary>
       /// 获取数据
       /// </summary>
        protected Dictionary<string, object> DefaultSettings()
        {
            var trans = new Dictionary<string, string> {
           {"cancel" , Admin.Trans("cancel") },
           { "submit" , Admin.Trans("submit")},
        };

            return new()
            {
                { "type", "question" },
                { "showCancelButton", true },
                { "showLoaderOnConfirm", true },
                { "confirmButtonText", trans["submit"] },
                { "cancelButtonText", trans["cancel"] },

            };
        }

       /// <summary>
       /// 判断返回数据 
       /// </summary>
        protected string FormatSettings()
        {
            if (this.Settings.Count == 0)
            {
                return "";
            }

            var settings = DefaultSettings().Merge(this.Settings);

            //return trim(substr(json_encode($settings, JSON_PRETTY_PRINT), 1, -1));
            return JsonSerializer.Serialize(settings);
        }

       /// <summary>
       /// 显示数据
       /// </summary>
        public override void AddScript()
        {
            var parameters = JsonSerializer.Serialize(action.Parameters());

            var script = $@"

(function($) {{
    $('{action.Selector(action.SelectorPrefix)}').off('{action.Event}').on('{action.Event}', function() {{
                    var data = $(this).data();
                    var target = $(this);
                    Object.assign(data, {parameters});
                    {action.ActionScript()}
                    {BuildActionPromise()}
                    {action.HandleActionPromise()}
                }});
            }})(jQuery);

            ";

            Admin.Script(script);
        }

        /**
         * @return string
         */

       /// <summary>
       /// 添加数据
       /// </summary>
        protected string BuildActionPromise()
        {
            //call_user_func([$this->action, 'dialog']);

            var route = action.GetHandleRoute();
            var settings = FormatSettings();
            var calledClass = action.GetCalledClass();

            if (UploadFile)
            {
                return BuildUploadFileActionPromise(settings, calledClass.Name, route);
            }

            return $@"
            var process = $.admin.swal({{
                {settings},
            preConfirm: function(input) {{
                    return new Promise(function(resolve, reject) {{
                    Object.assign(data, {{
                        _token: $.admin.token,
                        _action: '{calledClass}',
                        _input: input,
                    }});

                    $.ajax({{
                    method: '{action.GetMethod()}',
                        url: '{route}',
                        data: data,
                        success: function(data) {{
                            resolve(data);
                        }},
                        error: function(request){{
                            reject(request);
                        }}
                    }});
                }});
            }}
        }}).then(function(result) {{
            if (typeof result.dismiss !== 'undefined')
            {{
                return Promise.reject();
            }}

            if (typeof result.status === ""boolean"")
            {{
                var response = result;
            }}
            else
            {{
                var response = result.value;
            }}

            return [response, target];
        }});
";
        }

       /// <summary>
       /// 添加数据  Ajax传值本类方法传参
       /// </summary>
        protected string BuildUploadFileActionPromise(string settings, string calledClass, string route)
        {
            return $@"
var process = $.admin.swal({{
            {settings}
        }}).then(function(file) {{
            return new Promise(function(resolve) {{
        var data = {{
            _token: $.admin.token,
            _action: '{calledClass}',
        }};

            var formData = new FormData();
        for (var key in data)
            {{
                formData.append(key, data[key]);
            }}

            formData.append('_input', file.value, file.value.name);

        $.ajax({{
            url: '{route}',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            success: function(data) {{
                    resolve([response, target]);
                }},
            error: function(request){{
                    reject(request);
                }}
            }});
        }});
    }})
";
        }
    }
}
