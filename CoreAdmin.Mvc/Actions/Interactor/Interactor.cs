﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Actions.Interactor
{
    abstract public class Interactor
    {
        protected Action action;

        public static List<string> Elements = new()
        {
            "success",
            "error",
            "warning",
            "info",
            "question",
            "confirm",
            "text",
            "email",
            "integer",
            "ip",
            "url",
            "password",
            "mobile",
            "textarea",
            "select",
            "multipleSelect",
            "checkbox",
            "radio",
            "file",
            "image",
            "date",
            "datetime",
            "time",
            "hidden",
            "multipleImage",
            "multipleFile",
            "modalLarge",
            "modalSmall",
        };

        public Interactor(Action action)
        {
            this.action = action;
        }

        abstract public void AddScript();

    }
}
