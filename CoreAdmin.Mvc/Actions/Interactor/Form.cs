﻿using System.Collections.Generic;
using System.Text.Json;
using CoreAdmin.Lib;

namespace CoreAdmin.Mvc.Actions.Interactor
{
    public class Form : Interactor
    {
        protected List<string> Fields;

        protected string ModalId;

        protected string ModalSize;

        protected string Confirm;

        public Form(Action action) : base(action) { }

        //    /**
        //     * @param string $label
        //     *
        //     * @return array
        //     */
        //    protected function formatLabel($label)
        //        {
        //            return array_filter((array) $label);
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function text($column, $label = '')
        //        {
        //        $field = new Field\Text($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param $column
        //         * @param string   $label
        //         * @param \Closure $builder
        //         *
        //         * @return Field\Table
        //         */
        //        public function table($column, $label = '', $builder = null)
        //        {
        //        $field = new Field\Table($column, [$label, $builder]);

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function email($column, $label = '')
        //        {
        //        $field = new Field\Email($column, $this->formatLabel($label));

        //        $this->addField($field)->setView('admin::actions.form.text');

        //            return $field->inputmask(['alias' => 'email']);
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function integer($column, $label = '')
        //        {
        //            return $this->text($column, $label)
        //                ->width('200px')
        //                ->inputmask(['alias' => 'integer']);
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function ip($column, $label = '')
        //        {
        //            return $this->text($column, $label)
        //                ->width('200px')
        //                ->inputmask(['alias' => 'ip']);
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function url($column, $label = '')
        //        {
        //            return $this->text($column, $label)
        //                ->inputmask(['alias' => 'url'])
        //            ->width('200px');
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function password($column, $label = '')
        //        {
        //            return $this->text($column, $label)
        //                ->attribute('type', 'password');
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Text
        //         */
        //        public function mobile($column, $label = '')
        //        {
        //            return $this->text($column, $label)
        //                ->inputmask(['mask' => '99999999999'])
        //            ->width('100px');
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Textarea
        //         */
        //        public function textarea($column, $label = '')
        //        {
        //        $field = new Field\Textarea($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Select
        //         */
        //        public function select($column, $label = '')
        //        {
        //        $field = new Field\Select($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\MultipleSelect
        //         */
        //        public function multipleSelect($column, $label = '')
        //        {
        //        $field = new Field\MultipleSelect($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Checkbox
        //         */
        //        public function checkbox($column, $label = '')
        //        {
        //        $field = new Field\Checkbox($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Radio
        //         */
        //        public function radio($column, $label = '')
        //        {
        //        $field = new Field\Radio($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\File
        //         */
        //        public function file($column, $label = '')
        //        {
        //        $field = new Field\File($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\MultipleFile
        //         */
        //        public function multipleFile($column, $label = '')
        //        {
        //        $field = new Field\MultipleFile($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Image
        //         */
        //        public function image($column, $label = '')
        //        {
        //        $field = new Field\Image($column, $this->formatLabel($label));

        //        $this->addField($field)->setView('admin::actions.form.file');

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\MultipleImage
        //         */
        //        public function multipleImage($column, $label = '')
        //        {
        //        $field = new Field\MultipleImage($column, $this->formatLabel($label));

        //        $this->addField($field)->setView('admin::actions.form.muitplefile');

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Date
        //         */
        //        public function date($column, $label = '')
        //        {
        //        $field = new Field\Date($column, $this->formatLabel($label));

        //        $this->addField($field);

        //            return $field;
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Date
        //         */
        //        public function datetime($column, $label = '')
        //        {
        //            return $this->date($column, $label)->format('YYYY-MM-DD HH:mm:ss');
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Date
        //         */
        //        public function time($column, $label = '')
        //        {
        //            return $this->date($column, $label)->format('HH:mm:ss');
        //        }

        //        /**
        //         * @param string $column
        //         * @param string $label
        //         *
        //         * @return Field\Hidden
        //         */
        //        public function hidden($column, $label = '')
        //        {
        //        $field = new Field\Hidden($column, $this->formatLabel($label));

        //        $this->addField($field);

        //        return $field;
        //    }

        //    /**
        //     * @param $message
        //     *
        //     * @return $this
        //     */
        //    public function confirm($message)
        //    {
        //        $this->confirm = $message;

        //        return $this;
        //    }

        //    /**
        //     * @return $this
        //     */
        //    public function modalLarge()
        //    {
        //        $this->modalSize = 'modal-lg';

        //        return $this;
        //    }

        //    /**
        //     * @return $this
        //     */
        //    public function modalSmall()
        //    {
        //        $this->modalSize = 'modal-sm';

        //        return $this;
        //    }

        /**
         * @param string $content
         * @param string $selector
         *
         * @return string
         */
        public string AddElementAttr(string content, string selector)
        {
            // TODO Crawler
            //var crawler = new Crawler(content);

            //    var node = crawler.Filter(selector).GetNode(0);
            //    node.SetAttribute("modal", GetModalId());

            //return crawler.Children().Html();
            return "";
        }

        //    /**
        //     * @param Field $field
        //     *
        //     * @return Field
        //     */
        //    protected function addField(Field $field)
        //    {
        //        $elementClass = array_merge(['action'], $field->getElementClass());

        //        $field->addElementClass($elementClass);

        //        $field->setView($this->resolveView(get_class($field)));

        //        array_push($this->fields, $field);

        //        return $field;
        //    }

        //    /**
        //     * @param Request $request
        //     *
        //     * @throws ValidationException
        //     * @throws \Exception
        //     *
        //     * @return void
        //     */
        //    public function validate(Request $request)
        //    {
        //        if ($this->action instanceof RowAction) {
        //            call_user_func([$this->action, 'form'], $this->action->getRow());
        //        } else {
        //            call_user_func([$this->action, 'form']);
        //        }

        //        $failedValidators = [];

        //        /** @var Field $field */
        //        foreach ($this->fields as $field) {
        //            if (!$validator = $field->getValidator($request->all())) {
        //                continue;
        //            }

        //            if (($validator instanceof Validator) && !$validator->passes()) {
        //                $failedValidators[] = $validator;
        //            }
        //        }

        //        $message = $this->mergeValidationMessages($failedValidators);

        //        if ($message->any()) {
        //            throw ValidationException::withMessages($message->toArray());
        //        }
        //    }

        //    /**
        //     * Merge validation messages from input validators.
        //     *
        //     * @param \Illuminate\Validation\Validator[] $validators
        //     *
        //     * @return MessageBag
        //     */
        //    protected function mergeValidationMessages($validators)
        //    {
        //        $messageBag = new MessageBag();

        //        foreach ($validators as $validator) {
        //            $messageBag = $messageBag->merge($validator->messages());
        //        }

        //        return $messageBag;
        //    }

        //    /**
        //     * @param string $class
        //     *
        //     * @return string
        //     */
        //    protected function resolveView($class)
        //    {
        //        $path = explode('\\', $class);

        //        $name = strtolower(array_pop($path));

        //        return "admin::actions.form.{$name}";
        //    }


       /// <summary>
       /// 添加数据
       /// </summary>
        public void AddModalHtml()
        {
            var data = new Dictionary<string, dynamic> {
                    {"fields"     ,Fields },
                    {"title"      ,action.Name },
                    {"modal_id"   ,GetModalId() },
                    { "modal_size",ModalSize },
                 };
            // TODO view
            //var modal = view('admin::actions.form.modal', $data)->render();
            var modal = "";
            Admin.Html(modal);
        }

       /// <summary>
       /// 判断获取数据的方法
       /// </summary>
        public string GetModalId()
        {
            if (!string.IsNullOrEmpty(ModalId)) return ModalId;
            if (action is RowAction)
            {
                ModalId = Str.Uniqid("row-action-modal-");
            }
            else
            {
                var fullName = action.GetType().FullName;
                if (fullName != null) ModalId = fullName.ToLower();
            }

            return ModalId;
        }
        

       /// <summary>
       /// 添加数据的方法
       /// </summary>
        public override void AddScript()
        {
            action.Attribute("modal", GetModalId());
            var parameters = JsonSerializer.Serialize(action.Parameters());

            var script = @"

        (function($) {
            $('" + action.Selector(action.SelectorPrefix) + @"').off('" + action.Event + @"').on('" + action.Event + @"', function() {
                    var data = $(this).data();
                    var target = $(this);
                    var modalId = $(this).attr('modal');
                    Object.assign(data, " + parameters + @");
                    " + action.ActionScript() + @"
                $('#' + modalId).modal('show');
                $('#' + modalId + ' form').off('submit').on('submit', function(e) {
                        e.preventDefault();
                        var form = this;
                        " + BuildActionPromise() + @"
                        " + action.HandleActionPromise() + @"
                    });
                });
            })(jQuery);

            ";

            Admin.Script(script);
        }

        /**
         * @return string
         */

       /// <summary>
       /// 获取数据的方法
       /// </summary>
        protected string BuildConfirmActionPromise()
        {
            var trans = new Dictionary<string, string>{
                    {"cancel" ,Admin.Trans("cancel") },
                    { "submit", Admin.Trans("submit") },
                };

            var settings = new Dictionary<string, dynamic>{
                    {"type"                ,"question" },
                    {"showCancelButton"    ,true },
                    {"showLoaderOnConfirm" ,true },
                    {"confirmButtonText"   ,trans["submit"] },
                    {"cancelButtonText"    ,trans["cancel"] },
                    {"title"               ,Confirm },
                    { "text"               , ""},
                };
            var settingsStr = JsonSerializer.Serialize(settings);
            settingsStr = settingsStr.Substring(1, settingsStr.Length - 2).Trim();
            return @"
                var process = $.admin.swal({
                " + settingsStr + @",
                    preConfirm: function() {
                    " + BuildGeneralActionPromise() + @"

                    return process;
                }
            }).then(function(result) {

                if (typeof result.dismiss !== 'undefined')
                {
                    return Promise.reject();
                }

                var result = result.value[0];

                if (typeof result.status === ""boolean"")
                {
                    var response = result;
                }
                else
                {
                    var response = result.value;
                }

                return [response, target];
            });
            ";
        }

        /// <summary>
        /// 显示   调用Action的GetCalledClass GetMethod GetHandleRoute 方法  
        /// </summary>
        protected string BuildGeneralActionPromise()
        {
            return @"
                var process = new Promise(function(resolve, reject) {
                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: '" + action.GetCalledClass() + @"',
                    });

                    var formData = new FormData(form);
                    for (var key in data) {
                        formData.append(key, data[key]);
                    }

                    $.ajax({
    method: '" + action.GetMethod() + @"',
                        url: '" + action.GetHandleRoute() + @"',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
            resolve([data, target]);
            if (data.status === true)
            {
                                $('#' + modalId).modal('hide');
            }
        },
                        error: function(request){
            reject(request);
        }
    });
                });
";
        }

        /// <summary>
        /// 调用本类AddModalHtml方法   判断非空返回本类显示方法
        /// </summary>
        protected string BuildActionPromise()
        {
            if (action is RowAction)
            {
                // TODO reflect
                //call_user_func([$this->action, 'form'], $this->action->getRow());
            }
            else
            {
                //call_user_func([$this->action, 'form']);
            }

            AddModalHtml();

            if (!string.IsNullOrEmpty(Confirm))
            {
                return BuildConfirmActionPromise();
            }

            return BuildGeneralActionPromise();
        }
    }
}
