﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Actions
{
    public class SweatAlert2
    {
        protected string Type;

        protected string Title;

        public SweatAlert2 Show(string type, string title = "")
        {
            this.Type = type;
            this.Title = title;

            return this;
        }

        public Dictionary<string, dynamic> GetOptions()
        {
            return new()
            {
                {
                    "swal",
                    new Dictionary<string, string> { { "type", Type }, { "title", Title }, }
                },
            };
        }
    }
}
