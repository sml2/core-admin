﻿using System.Collections.Generic;

namespace CoreAdmin.Mvc.Actions
{
    public class Toastr
    {
        protected string Type;

        protected string Content;

        protected Dictionary<string, string> options = new();

        public Toastr Show(string type, string content = "")
        {
            this.Type = type;
            this.Content = content;

            return this;
        }

        protected Toastr Options(string option, string value)
        {
            options[option] = value;

            return this;
        }

        protected Toastr Position(string position)
        {
            return Options("positionClass", position);
        }

        public Toastr TopCenter()
        {
            return Position("toast-top-center");
        }

        public Toastr TopLeft()
        {
            return Position("toast-top-left");
        }

        public Toastr TopRight()
        {
            return Position("toast-top-right");
        }

        public Toastr BottomLeft()
        {
            return Position("toast-bottom-left");
        }

        public Toastr BottomCenter()
        {
            return Position("toast-bottom-center");
        }

        public Toastr BottomRight()
        {
            return Position("toast-bottom-right");
        }

        public Toastr TopFullWidth()
        {
            return Position("toast-top-full-width");
        }

        public Toastr BottomFullWidth()
        {
            return Position("toast-bottom-full-width");
        }

        public Toastr Timeout(int timeout = 5000)
        {
            return Options("timeOut", timeout.ToString());
        }

        /**
         * @return array
         */
        public Dictionary<string, dynamic> GetOptions()
        {
            if (!options.ContainsKey("positionClass"))
            {
                TopCenter();
            }

            return new()
            {
                {
                    "toastr",
                    new Dictionary<string, dynamic>() {
                        {"type"    , Type },
                        {"content" , Content},
                        { "options" , options},
                    }

                },
            };
        }
    }
}
