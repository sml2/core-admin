﻿using System;
using BInteractor = CoreAdmin.Mvc.Actions.Interactor.Interactor;
using System.Collections.Generic;
using System.Diagnostics;
using CoreAdmin.Lib;
using System.Text.Json;
using CoreAdmin.Mvc.Widgets;

namespace CoreAdmin.Mvc.Actions
{
    public abstract class Action : IRenderable
    {
        //        use Authorizable;

        /**
         * @var Response
         */
        //protected $response;

        protected string selector;

        public string Event = "click";

        protected string Method = "POST";

        protected Dictionary<string, object> attributes;

        public string SelectorPrefix = ".action-";

        protected BInteractor Interactor;

        protected static Dictionary<string, string> Selectors;

        public virtual string Name { get; set; }

        public Action()
        {
            InitInteractor();
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        protected void InitInteractor()
        {
            var hasForm = GetType().GetMethod("Form");
            if (hasForm != null)
            {
                Interactor = new Interactor.Form(this);
            }
            var hasDialog = GetType().GetMethod("Dialog");
            if (hasDialog != null)
            {
                Interactor = new Interactor.Dialog(this);
            }

            if (hasForm != null && hasDialog != null)
            {
                throw new ArgumentException("Can only define one of the methods in `form` and `dialog`");
            }
        }

        /// <summary>
        ///   先跳过框架1帧、帧在其中获得的方法 获取Type类的名字  然后返回随机数  1000 -  9000
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string Selector(string prefix)
        {
            if (string.IsNullOrEmpty(selector))
            {
                var name = new StackFrame(1).GetMethod().DeclaringType.Name;
                return MakeSelector(name + GetHashCode(), prefix);
            }

            return selector;
        }
        /// <summary>
        /// 判断获取返回随机数
        /// </summary>
        /// <param name="className"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string MakeSelector(string className, string prefix)
        {
            if (!Selectors.ContainsKey(className))
            {
                Selectors[className] = Str.Uniqid(prefix) + new Random().Next(1000, 9999);
            }

            return Selectors[className];
        }

        /// <summary>
       /// 获取数据
       /// </summary>
        public Action Attribute(string name, dynamic value)
        {
            attributes[name] = value;

            return this;
        }

        /// <summary>
        /// 循环添加html键值的方法
        /// </summary>
        /// <returns></returns>
        protected string FormatAttributes()
        {
            var html = new List<string>();

            foreach (var pair in attributes)
            {
                html.Add(pair.Key + "=\"" + pair.Value + "\"");
            }

            return string.Join(" ", html);
        }

        public virtual string GetResource() => "";

        /// <summary>
        /// 返回清空 .
        /// </summary>
        /// <returns></returns>
        protected string GetElementClass()
        {
            return Selector(SelectorPrefix).Trim('.');
        }

        ///**
        // * @return Response
        // */
        //public function response()
        //{
        //    if (is_null($this->response))
        //    {
        //            $this->response = new Response();
        //    }

        //    if (method_exists($this, 'dialog'))
        //    {
        //            $this->response->swal();
        //    }
        //    else
        //    {
        //            $this->response->toastr();
        //    }

        //    return $this->response;
        //}

        /// <summary>
        /// 显示方法    
        /// </summary>
        public string GetMethod()
        {
            return Method;
        }

        /// <summary>
        /// 显示方法  用于调用 GetCalledClass  
        /// </summary>
        public Type GetCalledClass()
        {
            return new StackFrame(1).GetMethod()?.DeclaringType;
        }

       /// <summary>
       /// URL调用
       /// </summary>
        public string GetHandleRoute()
        {
            return Admin.Url("_handle_action_");
        }
      
        /// <summary>
       /// 获取数据
       /// </summary>
        protected string GetModelClass()
        {
            return "";
        }

        /// <summary>
       /// 获取数据
       /// </summary>
        public List<string> Parameters()
        {
            return new();
        }

        ///**
        // * @param Request $request
        // *
        // * @return $this
        // */
        //public function validate(Request $request)
        //{
        //    if ($this->interactor instanceof Interactor\Form) {
        //            $this->interactor->validate($request);
        //}

        //return $this;
        //    }

       /// <summary>
       /// 添加
       /// </summary>
        protected void AddScript()
        {
            if (null != Interactor)
            {
                Interactor.AddScript();
                return;
            }

            var parameters = JsonSerializer.Serialize(Parameters());

            var script = @"

        (function($) {
            $('" + Selector(SelectorPrefix) + @"').off('" + Event + @"').on('" + Event + @"', function() {
                    var data = $(this).data();
                    var target = $(this);
                    Object.assign(data, " + parameters + @");
                    " + ActionScript() + @"
                    " + BuildActionPromise() + @"
                    " + HandleActionPromise() + @"
                });
            })(jQuery);

            ";

            Admin.Script(script);
        }

        public virtual string ActionScript() => "";

       /// <summary>
       /// 获取数据
       /// </summary>
        protected string BuildActionPromise()
        {
            return @"
                var process = new Promise(function(resolve, reject) {

                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: '" + GetCalledClass() + @"',
                    });

                    $.ajax({
                        method: '" + Method + @"',
                        url: '" + GetHandleRoute() + @"',
                        data: data,
                        success: function(data) {
                resolve([data, target]);
            },
                        error: function(request){
                reject(request);
            }
        });
                });

        ";
        }


        /// <summary>
       /// 判断获取数据
       /// </summary>
        public string HandleActionPromise()
        {
            var resolve = @"
    var actionResolver = function(data) {

                var response = data[0];
                var target = data[1];

                if (typeof response !== 'object')
                {
                    return $.admin.swal({ type: 'error', title: 'Oops!'});
                }

                var then = function(then) {
                    if (then.action == 'refresh')
                    {
                    $.admin.reload();
                    }

                    if (then.action == 'download')
                    {
                        window.open(then.value, '_blank');
                    }

                    if (then.action == 'redirect')
                    {
                    $.admin.redirect(then.value);
                    }

                    if (then.action == 'location')
                    {
                        window.location = then.value;
                    }

                    if (then.action == 'open')
                    {
                        window.open(then.value, '_blank');
                    }
                };

                if (typeof response.html === 'string')
                {
                    target.html(response.html);
                }

                if (typeof response.swal === 'object')
                {
                $.admin.swal(response.swal);
                }

                if (typeof response.toastr === 'object' && response.toastr.type)
                {
                $.admin.toastr[response.toastr.type](response.toastr.content, '', response.toastr.options);
                }

                if (response.then)
                {
                    then(response.then);
                }
            };

            var actionCatcher = function(request) {
                if (request && typeof request.responseJSON === 'object')
                {
                $.admin.toastr.error(request.responseJSON.message, '', { positionClass: ""toast-bottom-center"", timeOut: 10000}).css(""width"", ""500px"")
                }
            };
            ";

            Admin.Script(resolve);

            return @"
    process.then(actionResolver).catch (actionCatcher);
            ";
        }

        /// <summary>
       /// 判断获取数据的方法
       /// </summary>
        public dynamic Call(string method, params object[] arguments)
        {
            if (BInteractor.Elements.Contains(method))
            {
                //return interactor.{method} (...$arguments);
            }

            throw new ArgumentException($"Method {method} does not exist.");
        }

        public string Html()
        {
            return "";
        }
        /// <summary>
        /// 判断返回数据
        /// </summary>
        /// <returns></returns>
        public virtual string Render()
        {
            AddScript();

            var content = Html();

            if (!string.IsNullOrEmpty(content) && Interactor is Interactor.Form form)
            {
                return form.AddElementAttr(content, selector);
            }

            return Html();
        }
    }
}
