﻿using System;
using System.Collections.Generic;

namespace CoreAdmin.Mvc.Actions
{
    public class Response
    {
        public bool Status = true;

        public Exception exception;
  
        public List<string> ToastrMethods = new()
        {
            "topCenter",
            "topLeft",
            "topRight",
            "bottomLeft",
            "bottomCenter",
            "bottomRight",
            "topFullWidth",
            "bottomFullWidth",
            "timeout",
        };

        protected dynamic Plugin;

        protected Dictionary<string, string> Then;

        protected string html = "";

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Response Toastr()
        {
            if (!Plugin is Toastr)
            {
                Plugin = new Toastr();
            }

            return this;
        }

        /// <summary>
        /// 判断返回获取数据
        /// </summary>
        /// <returns></returns>
        public Response Swal()
        {
            if (!Plugin is SweatAlert2)
            {
                Plugin = new SweatAlert2();
            }

            return this;
        }

        public dynamic GetPlugin()
        {
            return Plugin;
        }

        public Response Success(string message = "")
        {
            return Show("success", message);
        }

        public Response Info(string message = "")
        {
            return Show("info", message);
        }

        public Response Warning(string message = "")
        {
            return Show("warning", message);
        }

        public Response Error(string message = "")
        {
            return Show("error", message);
        }

        protected Response Show(string type, string title = "")
        {
            GetPlugin().Show(type, title);
            return this;
        }

        public Response Redirect(string url)
        {
            Then = new() { { "action", "redirect" }, { "value", url } };

            return this;
        }

        public Response Open(string url)
        {
            Then = new() { { "action", "open" }, { "value", url } };

            return this;
        }

        public Response Location(string location)
        {
            Then = new() { { "action", "location" }, { "value", location } };
            return this;
        }

        public Response Download(string url)
        {
            Then = new() { { "action", "download" }, { "value", url } };
            return this;
        }

        public Response Refresh()
        {
            Then = new() { { "action", "refresh" }, { "value", "true" } };

            return this;
        }

        public Response Html(string html = "")
        {
            this.html = html;

            return this;
        }

        /**
         * @param \Exception $exception
         *
         * @return mixed
         */
        //public static function withException(\Exception $exception)
        //{
        //$response = new static ();

        //$response->status = false;

        //    if ($exception instanceof ValidationException) {
        //    $message = collect($exception->errors())->flatten()->implode("\n");
        //    } else
        //    {
        //    $message = $exception->getMessage();
        //    }

        //    return $response->toastr()->topCenter()->error($message);
        //}

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        //public function Send()
        //{
        //$data = array_merge(
        //    ["status" => $this->status, "then" => $this->then],
        //    $this->getPlugin()->getOptions()
        //    );

        //    if ($this->html) {
        //    $data["html"] = $this->html;
        //    }

        //    return response()->json($data);
        //}

        /**
         * @param string $method
         * @param array  $arguments
         *
         * @return $this
         */
        //    public function __call($method, $arguments)
        //    {
        //        if (in_array($method, $this->toastrMethods))
        //        {
        //        $this->toastr();
        //        }

        //    $this->getPlugin()->{$method} (...$arguments);

        //        return $this;
        //    }
    }
}
