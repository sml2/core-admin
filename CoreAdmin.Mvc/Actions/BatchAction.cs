﻿namespace CoreAdmin.Mvc.Actions
{
    public abstract class BatchAction : GridAction
    {
        public BatchAction()
        {
            SelectorPrefix = ".grid-batch-action-";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ActionScript()
        {
            var warning = Admin.Trans("No data selected!");

            return $@"
        var key = $.admin.grid.selected();

            if (key.length === 0)
            {{
            $.admin.toastr.warning('{warning}', '', {{ positionClass: 'toast-top-center'}});
                return;
            }}

            Object.assign(data, {{ _key: key}});
            ";
        }

        /**
         * @param Request $request
         *
         * @return mixed
         */
        //public function RetrieveModel(Request $request)
        //{
        //    if (!$key = $request->get('_key')) {
        //        return false;
        //    }

        //$modelClass = str_replace('_', '\\', $request->get('_model'));

        //    if (is_string($key))
        //    {
        //    $key = explode(',', $key);
        //    }

        //    if ($this->modelUseSoftDeletes($modelClass)) {
        //        return $modelClass::withTrashed()->findOrFail($key);
        //    }

        //    return $modelClass::findOrFail($key);
        //}

        public override string Render()
        {
            AddScript();

            var modalId = "";

            if (Interactor is Interactor.Form form)
            {
                modalId = form.GetModalId();
                var content = Html();
                if (!string.IsNullOrEmpty(content))
                {
                    return form.AddElementAttr(content, selector);
                }
            }

            return $"<a href='javascript:void(0);' class='{GetElementClass()}' {(!string.IsNullOrEmpty(modalId) ? $"modal='{modalId}'" : "")}>{Name}</a>";
        }
    }
}
