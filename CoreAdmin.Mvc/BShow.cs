﻿using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Show;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Extensions;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using CoreAdmin.Extensions;

namespace CoreAdmin.Mvc
{
    /// <summary>
    /// 详情页面的数据类和方法
    /// </summary>
    public class BShow
    {
        protected IIdexerModel model;

        protected Action<BShow> builder { get; }
        private HttpContext _context { get;  }

        protected string resource;

        protected List<Field> fields;

        protected List<Relation> relations;

        public Panel Panel { get; private set; }

        public static Dictionary<string, Type> extendedFields = new();

        protected static Action<BShow> initCallback;

        public BShow(IIdexerModel model, Action<BShow> builder = null, HttpContext context = null)
        {
            this.model = model;
            this.builder = builder;
            _context = context;
            relations = new List<Relation>();
            InitPanel();
            InitContents();

            initCallback?.Invoke(this);
        }

        public static void Init(Action<BShow> callback)
        {
            initCallback = callback;
        }

        public static void Extend(string abs, Type classType)
        {
            extendedFields[abs] = classType;
        }

        protected void InitContents()
        {
            fields = new();
            //this.relations = new ();
        }

        protected void InitPanel()
        {
            Panel = new Panel(this);
        }

        public Field Field(string name, string label)
        {
            return AddField(name, label);
        }

        public Field Field(string name)
        {
            var lable = model.GetDisplayName(name);
            return AddField(name, lable);
        }
        public BShow Fields(Dictionary<string, string> fields)
        {
            foreach (var (key, value) in fields) {
                Field(key, value);
            }

            return this;
        }
        public BShow Fields(List<string> fields)
        {
            foreach (var name in fields)
            {
                Field(name);
            }

            return this;
        }

        public BShow All()
        {
            //$fields = array_keys($this->model->getAttributes());

            //return $this->fields($fields);
            return this;
        }

        ///**
        // * Add a relation to show.
        // *
        // * @param string          $name
        // * @param string|\Closure $label
        // * @param null|\Closure   $builder
        // *
        // * @return Relation
        // */
        //public function relation($name, $label, $builder = null)
        //{
        //    if (is_null($builder)) {
        //        $builder = $label;
        //        $label = '';
        //    }

        //    return $this->addRelation($name, $builder, $label);
        //}
        
        protected Field AddField(string name, string label = "")
        {
            var field = new Field(name, label);

            field.SetParent(this);

            OverwriteExistingField(name);
            fields.Add(field);
            return field;
        }

        ///**
        // * Add a relation panel to show.
        // *
        // * @param string   $name
        // * @param \Closure $builder
        // * @param string   $label
        // *
        // * @return Relation
        // */
        //protected function addRelation($name, $builder, $label = '')
        //{
        //    $relation = new Relation($name, $builder, $label);

        //    $relation->setParent($this);

        //    $this->overwriteExistingRelation($name);

        //    return tap($relation, function ($relation) {
        //        $this->relations->push($relation);
        //    });
        //}

        protected void OverwriteExistingField(string name)
        {
            if (fields == null || fields.Count == 0) {
                return;
            }

            fields = fields.Where(field => field.GetName() != name).ToList();
        }

        ///**
        // * Overwrite existing relation.
        // *
        // * @param string $name
        // */
        //protected function overwriteExistingRelation($name)
        //{
        //    if ($this->relations->isEmpty()) {
        //        return;
        //    }

        //    $this->relations = $this->relations->filter(
        //        function (Relation $relation) use ($name) {
        //            return $relation->getName() != $name;
        //        }
        //    );
        //}

        ///**
        // * Show a divider.
        // */
        //public function divider()
        //{
        //    $this->fields->push(new Divider());
        //}
        
        public BShow SetResource(string resource)
        {
            this.resource = resource;

            return this;
        }

        public string GetResourcePath()
        {
            if (string.IsNullOrEmpty(resource))
            {
                var segments = _context.Request.Segments();
                resource = string.Join('/', segments.Take(segments.Length - 1));
            }

            return Admin.Url('/'+resource);
        }

        ///**
        // * Set field and label width in fields.
        // *
        // * @param int $fieldWidth
        // * @param int $labelWidth
        // *
        // * @return $this
        // */
        //public function setWidth($fieldWidth = 8, $labelWidth = 2)
        //{
        //    collect($this->fields)->each(function ($field) use ($fieldWidth, $labelWidth) {
        //        $field->setWidth($fieldWidth, $labelWidth);
        //    });

        //    return $this;
        //}

        ///**
        // * Set the model instance.
        // *
        // * @param Model $model
        // *
        // * @return $this
        // */
        //public function setModel($model)
        //{
        //    $this->model = $model;

        //    return $this;
        //}

        public IIdexerModel GetModel()
        {
            return model;
        }

        ///**
        // * Add field and relation dynamically.
        // *
        // * @param string $method
        // * @param array  $arguments
        // *
        // * @return bool|mixed
        // */
        //public function __call($method, $arguments = [])
        //{
        //    $label = isset($arguments[0]) ? $arguments[0] : ucfirst($method);

        //    if ($field = $this->handleGetMutatorField($method, $label)) {
        //        return $field;
        //    }

        //    if ($field = $this->handleRelationField($method, $arguments)) {
        //        return $field;
        //    }

        //    return $this->addField($method, $label);
        //}

        ///**
        // * Handle the get mutator field.
        // *
        // * @param string $method
        // * @param string $label
        // *
        // * @return bool|Field
        // */
        //protected function handleGetMutatorField($method, $label)
        //{
        //    if (is_null($this->model)) {
        //        return false;
        //    }

        //    if ($this->model->hasGetMutator($method)) {
        //        return $this->addField($method, $label);
        //    }

        //    return false;
        //}

        ///**
        // * Handle relation field.
        // *
        // * @param string $method
        // * @param array  $arguments
        // *
        // * @return $this|bool|Relation|Field
        // */
        //protected function handleRelationField($method, $arguments)
        //{
        //    if (!method_exists($this->model, $method)) {
        //        return false;
        //    }

        //    if (!($relation = $this->model->$method()) instanceof EloquentRelation) {
        //        return false;
        //    }

        //    if ($relation    instanceof HasOne
        //        || $relation instanceof BelongsTo
        //        || $relation instanceof MorphOne
        //    ) {
        //        $this->model->with($method);

        //        if (count($arguments) == 1 && $arguments[0] instanceof \Closure) {
        //            return $this->addRelation($method, $arguments[0]);
        //        }

        //        if (count($arguments) == 2 && $arguments[1] instanceof \Closure) {
        //            return $this->addRelation($method, $arguments[1], $arguments[0]);
        //        }

        //        return $this->addField($method, Arr::get($arguments, 0))->setRelation(
        //            $this->shouldSnakeAttributes() ? Str::snake($method) : $method
        //        );
        //    }

        //    if ($relation    instanceof HasMany
        //        || $relation instanceof MorphMany
        //        || $relation instanceof BelongsToMany
        //        || $relation instanceof HasManyThrough
        //    ) {
        //        if (empty($arguments) || (count($arguments) == 1 && is_string($arguments[0]))) {
        //            return $this->showRelationAsField($method, $arguments[0] ?? '');
        //        }

        //        $this->model->with($method);

        //        if (count($arguments) == 1 && is_callable($arguments[0])) {
        //            return $this->addRelation($method, $arguments[0]);
        //        } elseif (count($arguments) == 2 && is_callable($arguments[1])) {
        //            return $this->addRelation($method, $arguments[1], $arguments[0]);
        //        }

        //        throw new \InvalidArgumentException('Invalid eloquent relation');
        //    }

        //    return false;
        //}

        ///**
        // * @param string $relation
        // * @param string $label
        // *
        // * @return Field
        // */
        //protected function showRelationAsField($relation = '', $label = '')
        //{
        //    return $this->addField($relation, $label);
        //}

        ///**
        // * Handle model field.
        // *
        // * @param string $method
        // * @param string $label
        // *
        // * @return bool|Field
        // */
        //protected function handleModelField($method, $label)
        //{
        //    if (in_array($method, $this->model->getAttributes())) {
        //        return $this->addField($method, $label);
        //    }

        //    return false;
        //}


        /// <summary>
        /// 
        /// </summary>
        public HtmlContent Render()
        {
            builder?.Invoke(this);

            if (fields == null || fields.Count == 0) {
                All();
            }

            //if (is_array($this->builder))
            //{
            //    $this->fields($this->builder);
            //}

            fields.ForEach(field => field.SetValue(model));
            relations.ForEach(relation => relation.SetModel(model));

            var data = new Dictionary<string, dynamic> {
                {"panel"     , Panel.Fill(fields) },
                { "relations" , relations },
            };
            return new HtmlContent()
            {
                Name = HtmlContentType.Component,
                Value = nameof(ViewComponents.Show),
                Data = data
            };
        }
    }
}
