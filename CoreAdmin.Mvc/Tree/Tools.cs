﻿using CoreAdmin.Mvc.Models;
using System.Collections.Generic;
using System.Linq;

namespace CoreAdmin.Mvc.Tree
{
    public class Tools<T> where T:ModelTree<T>
    {
        protected BTree<T> tree;

        protected List<string> tools;

        public Tools(BTree<T> tree)
        {
            this.tree = tree;
            this.tools = new();
        }

        public Tools<T> Add(string tool)
        {
            this.tools.Add(tool);

            return this;
        }

        public string Render()
        {
            return string.Join(' ', tools.Select((tool) =>
            {
                //if (tool is IRenderable) {
                //    return tool.Render();
                //}

                //if (tool is IHtmlable) {
                //    return tool.ToHtml();
                //}

                return tool;
            }));
        }
    }
}
