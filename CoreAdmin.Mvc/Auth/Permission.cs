﻿using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Layout;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAdmin.Mvc.Auth
{
    public class Permission
    {
        //public static bool IsAdministrator()
        //{
        //    return Admin.User.IsAdministrator();
        //}

        public static async Task Error(HttpContext httpContext)
        {
            var content = httpContext.RequestServices.GetRequiredService<Content>();
            var viewResult = await content.WithError("无权访问", "无权访问").Render();
            var executor = httpContext.RequestServices
            .GetRequiredService<IActionResultExecutor<ViewResult>>();
            var routeData = httpContext.GetRouteData() ?? new RouteData();
            var actionContext = new ActionContext(httpContext, routeData,
            new Microsoft.AspNetCore.Mvc.Abstractions.ActionDescriptor());

            await executor.ExecuteAsync(actionContext, viewResult as ViewResult);
        }

        /// <summary>
        /// 返回Url路径
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        public static string AdminBasePath(string Path)
        {
            //$prefix = '/'.trim(config('admin.route.prefix'), '/');
            var Prefix = "/admin";

            Prefix = (Prefix == "/") ? "" : Prefix;

            Path = Path.Trim('/');

            if(string.IsNullOrEmpty(Path) || Path.Length == 0)
            {
                return !string.IsNullOrEmpty(Prefix) ? Prefix : "/";
            }

            return Prefix + Path;
        }

        public static bool ShouldPassThrough(string Route)
        {
            List<string> Excepts = new List<string>() { "login", "logout" };

            Excepts.ForEach((item) => { item = AdminBasePath(item); });

            if (Excepts.Contains(Route.ToLower().Trim('/')))
            {
                return true;
            }
            return false;
        }

        //public static bool CheckMenu(string Path)
        //{
        //    Path = Permission.AdminBasePath(Path);
        //    var Permissions = Admin.User.allPermissions();
        //    foreach (var P in Permissions)
        //    {
        //       if(P.ShouldPassThrough(Path, "GET"))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static IEnumerable<MenuModel> Menu()
        //{
            
        //    List<MenuModel> Menu = new List<MenuModel>();
        //    if (Admin.User.IsAdministrator())
        //    {
        //        Menu = Admin.Instance.DefaultDbContext.Menu.ToList();
        //    }
        //    else
        //    {
        //        foreach (var collect in Admin.User.Roles.Pluck<Roles, ICollection<MenuModel>>(nameof(Roles.Menu)))
        //        {
        //            Menu.AddRange(collect);
        //        }
               
        //    }

        //    return Menu;
        //}

    }
}
