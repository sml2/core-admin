﻿using CoreAdmin.Mvc.Controllers;
using CoreAdmin.Mvc.Form;
using FBuilder = CoreAdmin.Mvc.Form.Builder;
using CoreAdmin.Mvc.Form.Layout;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Mvc.Interface;
using System.Diagnostics;
using Microsoft.Extensions.Primitives;
using CoreAdmin.Extensions;
using Microsoft.AspNetCore.Http;

namespace CoreAdmin.Mvc
{
    /// <summary>
    /// 添加时需要的数据类型和方法
    /// </summary>
    public partial class BForm : IForm
    {
        public const string RemoveFlagName = "_remove_";

        private IQueryable<IIdexerModel> ModelInstance { get; set; }

        private readonly FBuilder _builder;

        protected Dictionary<string, string> updates;

        //protected $relations = [];

        protected Dictionary<string, string> inputs;

        protected FLayout layout;

        protected List<string> ignored;

        protected static Dictionary<string, List<string>> CollectedAssets { get; } = new();

        protected Tab tab;

        public List<Row> rows;

        protected bool isSoftDeletes = false;

        private IAdminController _controller;

        public HttpRequest Request => _controller?.Request;


        public Type ModelType { get; set; }

        public IIdexerModel Original { get; private set; }

        /// <summary>
        /// 添加数据时调用的方法
        /// </summary>
        /// <param name="model"></param>
        /// <param name="callback"></param>
        public BForm(IQueryable<IIdexerModel> model, Action<BForm> callback = null)
        {
            this.ModelInstance = model;
            ModelType = model.GetType().GetGenericArguments()[0];
            _builder = new FBuilder(this);

            InitLayout();

            callback?.Invoke(this);

            //isSoftDeletes = in_array(SoftDeletes::class, class_uses_deep(this.model), true);

            ignored = new();
            rows = new();
            CallInitCallbacks?.Invoke(this);
        }

        public BForm PushField(FField field)
        {
            field.SetForm(this);

            var width = _builder.GetWidth();
            field.SetWidth(width["field"], width["label"]);

            Fields().Add(field);
            layout.AddField(field);

            return this;
        }

        public IQueryable<IIdexerModel> Model()
        {
            return ModelInstance;
        }

        public FBuilder Builder()
        {
            return _builder;
        }

        public List<FField> Fields()
        {
            return Builder().Fields();
        }

        public BForm Edit(int id)
        {
            _builder.SetMode(FBuilder.MODE_EDIT);
            _builder.SetResourceId(id);
            SetFieldValue(id);

            return this;
        }

        public BForm Tab(string title, Action<BForm> content, bool active = false)
        {
            SetTab().Append(title, content, active);

            return this;
        }

        public Tab GetTab()
        {
            return tab;
        }

        public Tab SetTab()
        {
            return tab ??= new Tab(this);
        }

        public BForm WithController(IAdminController controller)
        {
            _controller = controller;
            return this;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<IActionResult> Destroy(IEnumerable<int> ids)
        {
            object response;
            try
            {
                CallDeleting?.Invoke(ids);
                //if (($ret = $this->callDeleting($id)) instanceof Response) {
                //    return $ret;
                //}

                //collect(explode(',', $id))->filter()->each(function($id) {
                //    $builder = $this->model()->newQuery();

                //    if ($this->isSoftDeletes) {
                //        $builder = $builder->withTrashed();
                //    }

                //    $model = $builder->with($this->getRelations())->findOrFail($id);

                //    if ($this->isSoftDeletes && $model->trashed()) {
                //        $this->deleteFiles($model, true);
                //        $model->forceDelete();

                //        return;
                //    }

                //    $this->deleteFiles($model);
                //    $model->delete();
                //});
                var keyName = ModelInstance.GetKeyName();
                var data = await ModelInstance.Where($"@0.Contains({keyName})", ids).ToListAsync();

                _controller.DbContext.RemoveRange(data);
                await _controller.DbContext.SaveChangesAsync();
                CallDeleted?.Invoke(ids);
                //if (($ret = $this->callDeleted()) instanceof Response) {
                //    return $ret;
                //}

                response = new
                {
                    status = true,
                    message = Admin.Trans("delete_succeeded"),
                };
            }
            catch (Exception exception)
            {
                response = new
                {
                    status = false,
                    message = exception.Message ?? Admin.Trans("delete_failed"),
                };
            }

            return _controller.Json(response);
        }

        //    /**
        //     * Remove files in record.
        //     *
        //     * @param Model $model
        //     * @param bool  $forceDelete
        //     */
        //    protected function deleteFiles(Model $model, $forceDelete = false)
        //    {
        //        // If it's a soft delete, the files in the data will not be deleted.
        //        if (!$forceDelete && $this->isSoftDeletes) {
        //        return;
        //    }

        //    $data = $model->toArray();

        //    $this->fields()->filter(function($field) {
        //        return $field instanceof Field\File;
        //    })->each(function(Field\File $file) use($data) {
        //        $file->setOriginal($data);

        //        $file->destroy();
        //    });
        //}

        /// <summary>
        /// 清空数据表
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Truncate()
        {
            var tableName = _controller.DbContext.Model.FindEntityType(ModelType).GetTableName();
            // await _controller.DbContext.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE {tableName}");
            await _controller.DbContext.Database.ExecuteSqlRawAsync(
                $"DELETE FROM {tableName};DELETE FROM sqlite_sequence WHERE name = '{tableName}';VACUUM;");
            await _controller.DbContext.SaveChangesAsync();
            return _controller.Redirect("/");
        }

        public Task<IActionResult> Store<TModel>(TModel model) where TModel : IIdexerModel
        {
            return Store(model, _controller.ModelState);
        }

        public async Task<IActionResult> Store<TModel>(TModel model,
            Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary modelState) where TModel : IIdexerModel
        {
            if (!modelState.IsValid)
            {
                var errorMessages = new Dictionary<string, string>();
                foreach (var (key, value) in modelState)
                {
                    if (value.Errors.Count > 0)
                    {
                        errorMessages.Add(key, string.Join(",", value.Errors.Select(e => e.ErrorMessage)));
                    }
                }

                AddErrorMessage(errorMessages);
                return await _controller.Store(this);
            }

            if (Prepare(model) is IActionResult response1) {
                return response1;
            }

            _controller.DbContext.Add(model);
            _controller.DbContext.SaveChanges();
            //DB::transaction(function() {
            //    $inserts = $this->prepareInsert($this->updates);

            //    foreach ($inserts as $column => $value) {
            //        $this->model->setAttribute($column, $value);
            //    }

            //    $this->model->save();

            //    $this->updateRelation($this->relations);
            //});

            var response = CallSaved?.Invoke(this,Original);
            if (response is IActionResult result)
            {
                return result;
            }

            response = AjaxResponse(Admin.Trans("save_succeeded"));
            if (response is IActionResult result1)
            {
                return result1;
            }

            return RedirectAfterStore();
        }

        public BForm AddErrorMessage(Dictionary<string, string> error)
        {
            _builder.AddErrorMessage(error);
            return this;
        }

        protected IActionResult AjaxResponse(string message)
        {
            var request = _controller.Request;

            // ajax but not pjax
            if (request.IsAjax() && !request.IsPjax())
            {
                return new JsonResult(new
                {
                    status = true,
                    message,
                    //display   = ApplayFieldDisplay(),
                });
            }

            return null;
        }

        ///**
        // * @return array
        // */
        //protected function ApplayFieldDisplay()
        //{
        //    var editable = new() { };

        //    /** @var Field $field */
        //    foreach (var field in Fields()) {
        //        if (!Request.Instance.HttpRequest.Has(field.Column())) {
        //            continue;
        //        }

        //        var newValue = model->fresh()->getAttribute($field->column());

        //        if ($newValue instanceof Arrayable) {
        //            $newValue = $newValue->toArray();
        //        }

        //        if ($field instanceof Field\BelongsTo || $field instanceof Field\BelongsToMany) {
        //            $selectable = $field->getSelectable();

        //            if (method_exists($selectable, 'display'))
        //            {
        //                $display = $selectable::display();

        //                $editable[$field->column()] = $display->call($this->model, $newValue);
        //            }
        //        }
        //    }

        //    return $editable;
        //}

        protected IActionResult Prepare<TModel>(TModel data) where TModel : IIdexerModel
        {
            if (CallSubmitted?.Invoke() is IActionResult result)
            {
                return result;
            }

            Original = data;
            if (CallSaving?.Invoke(this, Original) is IActionResult result1)
            {
                return result1;
            }

            return null;
        }
        protected IActionResult Prepare(Dictionary<string, StringValues> data)
        {
            if (CallSubmitted?.Invoke() is IActionResult result)
            {
                return result;
            }

            InputData = RemoveIgnoredFields(data)
                .Merge(this.inputs?.ToDictionary(x => x.Key, x => new StringValues(x.Value)));

            if (CallSaving?.Invoke(this,Original) is IActionResult result1)
            {
                return result1;
            }

            //this.relations = GetRelationInputs($this->inputs);

            //$this->updates = Arr::except($this->inputs, array_keys($this->relations));
            return null;
        }

        public Dictionary<string,StringValues> InputData { get; set; }

        protected Dictionary<string, TValue> RemoveIgnoredFields<TValue>(Dictionary<string, TValue> data)
        {
            foreach (var ignore in ignored)
            {
                data.Remove(ignore);
            }

            return data;
        }

        ///**
        // * Get inputs for relations.
        // *
        // * @param array $inputs
        // *
        // * @return array
        // */
        //protected function getRelationInputs($inputs = []): array
        //{
        //    $relations = [];

        //    foreach ($inputs as $column => $value) {
        //        if ((method_exists($this->model, $column) ||
        //            method_exists($this->model, $column = Str::camel($column))) &&
        //            !method_exists(Model::class, $column)
        //        ) {
        //            $relation = call_user_func([$this->model, $column]);

        //            if ($relation instanceof Relations\Relation) {
        //                $relations[$column] = $value;
        //            }
        //        }
        //    }

        //    return $relations;
        //}
        public IActionResult UpdateInlineEdit<TModel>(int id) where TModel : class, IIdexerModel, new()
        {
            var data = _controller.Request.Form.ToDictionary(x => x.Key, x => x.Value);

            var isEditable = IsEditable(data.ToDictionary(x => x.Key, x => x.Value.ToString()));

            //if (HandleColumnUpdates(id, ref data) is IActionResult result1)
            //{
            //    return result1;
            //}

            //var builder = Model();

            //if (isSoftDeletes)
            //{
            //    //builder = $builder->withTrashed();
            //}

            //Original = builder.Include(GetRelations()).Single($"{model.GetKeyName()}={id}");

            //SetFieldOriginalValue();

            if ((Prepare(data)) is IActionResult result2)
            {
                return result2;
            }

            var name = data["name"].ToString();
            object value = data["value"];

            using var transaction = _controller.DbContext.Database.BeginTransaction();
            try
            {
                var entity = new TModel();
                var keyProp = entity.GetKeyNameProperty();
                keyProp.SetValue(entity, id);

                var nameProp = typeof(TModel).GetProperty(name);
                if (nameProp == null) throw new ArgumentNullException($"{typeof(TModel).Name}不存在属性[{name}]");

                // 处理数据转换
                if (nameProp.PropertyType == typeof(string))
                {
                    nameProp.SetValue(entity, value.ToString());
                }
                else if (nameProp.PropertyType.IsEnum)
                {
                    nameProp.SetValue(entity, Enum.Parse(nameProp.PropertyType, value.ToString()));
                }
                else
                {
                    throw new NotImplementedException();
                }


                var entry = _controller.DbContext.Entry(entity);
                entry.State = EntityState.Unchanged;
                entry.Property(name).IsModified = true;
                _controller.DbContext.SaveChanges();
                transaction.Commit();
            }
            catch (Exception)
            {
                throw;
            }
            //    DB::transaction(function() {
            //    $updates = $this->prepareUpdate($this->updates);

            //        foreach ($updates as $column => $value) {
            //        /* @var Model $this ->model */
            //        $this->model->setAttribute($column, $value);
            //        }

            //    $this->model->save();

            //    $this->updateRelation($this->relations);
            //    });
            var response = CallSaved?.Invoke(this, Original);
            if (response is IActionResult result3)
            {
                return result3;
            }

            response = AjaxResponse(Admin.Trans("update_succeeded"));
            if (response is IActionResult result4)
            {
                return result4;
            }

            return RedirectAfterUpdate(id);
        }

        public Task<IActionResult> Update(IIdexerModel model)
        {
            //$data = ($data) ?: request()->all();
            if (!_controller.ModelState.IsValid)
            {
                var errorMessages = new Dictionary<string, string>();
                foreach (var (key, value) in _controller.ModelState)
                {
                    if (value.Errors.Count > 0)
                    {
                        errorMessages.Add(key, string.Join(",", value.Errors.Select(e => e.ErrorMessage)));
                    }
                }

                AddErrorMessage(errorMessages);
                return _controller.Update(this, model.GetKey());
            }

            _controller.DbContext.SaveChanges();
            //$isEditable = $this->isEditable($data);

            //    if (($data = $this->handleColumnUpdates($id, $data)) instanceof Response) {
            //        return $data;
            //    }

            ///* @var Model $this ->model */
            //$builder = $this->model();

            //    if ($this->isSoftDeletes) {
            //    $builder = $builder->withTrashed();
            //    }

            //$this->model = $builder->with($this->getRelations())->findOrFail($id);

            //$this->setFieldOriginalValue();

            // if ((Prepare(this)) is IActionResult result2) {
            //     return Task.Run(() => result2);
            // }

            //    DB::transaction(function() {
            //    $updates = $this->prepareUpdate($this->updates);

            //        foreach ($updates as $column => $value) {
            //        /* @var Model $this ->model */
            //        $this->model->setAttribute($column, $value);
            //        }

            //    $this->model->save();

            //    $this->updateRelation($this->relations);
            //    });

            var response = CallSaved?.Invoke(this, Original);
            if (response is IActionResult result)
            {
                return Task.Run(() => result);
            }

            response = AjaxResponse(Admin.Trans("update_succeeded"));
            if (response is IActionResult result1)
            {
                return Task.Run(() => result1);
            }

            return Task.Run(() => RedirectAfterUpdate(model.GetKey()));
        }

        protected IActionResult RedirectAfterStore()
        {
            var resourcesPath = Resource(0);

            //var key = Admin.GetKey(model.GetType().GetGenericArguments()[0]);

            return RedirectAfterSaving(resourcesPath);
        }

        protected IActionResult RedirectAfterUpdate(int key)
        {
            var resourcesPath = Resource(-1);

            return RedirectAfterSaving(resourcesPath, key.ToString());
        }

        protected IActionResult RedirectAfterSaving(string resourcesPath, string key = "")
        {
            var afterSave = 0;
            if (_controller.Request.Form.ContainsKey("after-save"))
            {
                afterSave = Convert.ToInt32(_controller.Request.Form["after-save"][0]);
            }

            var url = afterSave switch
            {
                //新增时需不需要显示继续编辑按钮
                //1 =>resourcesPath.TrimStart('/') + $"/{key}/edit",// continue editing
                1 => $"{resourcesPath}/edit/{key}",// continue editing
                2 => $"{resourcesPath}/create",// continue creating
                3 => $"{resourcesPath}/{key}",// view resource
                _ => _controller.Request.Form.ContainsKey(FBuilder.PREVIOUS_URL_KEY)
                    ? _controller.Request.Form[FBuilder.PREVIOUS_URL_KEY][0]
                    : resourcesPath
            };

            Admin.Toastr(Admin.Trans("save_succeeded"));

            return _controller.Redirect(url);
        }

        protected static bool IsEditable(Dictionary<string, string> input)
        {
            return input.ContainsKey("_editable") || input.ContainsKey("_edit_inline");
        }

        protected static IActionResult HandleColumnUpdates(int id, ref Dictionary<string, StringValues> data)
        {
            data = HandleEditable(data);

            data = HandleFileDelete(data);

            data = HandleFileSort(data);

            if (HandleOrderable(id, data))
            {
                return new JsonResult(new
                {
                    status = true,
                    message = Admin.Trans("update_succeeded"),
                });
            }

            return null;
        }

        protected static Dictionary<string, StringValues> HandleEditable(Dictionary<string, StringValues> input)
        {
            if (input.ContainsKey("_editable"))
            {
                var name = input["name"];
                var value = input["value"];
                input.Remove("pk");
                input.Remove("value");
                input.Remove("name");

                input[name] = value;
            }

            return input;
        }

        protected static Dictionary<string, StringValues> HandleFileDelete(Dictionary<string, StringValues> input)
        {
            if (input.ContainsKey(FField.FILE_DELETE_FLAG))
            {
                input[FField.FILE_DELETE_FLAG] = input["key"];
                input.Remove("key");
            }

            return input;
        }

        protected static Dictionary<string, StringValues> HandleFileSort(Dictionary<string, StringValues> input)
        {
            if (!input.ContainsKey(FField.FILE_SORT_FLAG))
            {
                return input;
            }

            //$sorts = array_filter($input[Field::FILE_SORT_FLAG]);

            //if (empty($sorts)) {
            //    return $input;
            //}

            //foreach ($sorts as $column => $order) {
            //    $input[$column] = $order;
            //}

            //request()->replace($input);

            return input;
        }

        protected static bool HandleOrderable(int id, Dictionary<string, StringValues> input)
        {
            var _ = $"{id}"; 
            if (input.ContainsKey("_orderable"))
            {
                //var model = Model().SingleOrDefault($"{this.model.GetKeyName()}={id}");

                //if ($model instanceof Sortable) {
                //    $input['_orderable'] == 1 ? $model->moveOrderUp() : $model->moveOrderDown();

                //    return true;
                //}
            }

            return false;
        }

        ///**
        // * Update relation data.
        // *
        // * @param array $relationsData
        // *
        // * @return void
        // */
        //protected function updateRelation($relationsData)
        //{
        //    foreach ($relationsData as $name => $values) {
        //        if (!method_exists($this->model, $name)) {
        //            continue;
        //        }

        //        $relation = $this->model->$name();

        //        $oneToOneRelation = $relation instanceof Relations\HasOne
        //            || $relation instanceof Relations\MorphOne
        //            || $relation instanceof Relations\BelongsTo;

        //        $prepared = $this->prepareUpdate([$name => $values], $oneToOneRelation);

        //        if (empty($prepared)) {
        //            continue;
        //        }

        //        switch (true) {
        //            case $relation instanceof Relations\BelongsToMany:
        //            case $relation instanceof Relations\MorphToMany:
        //                if (isset($prepared[$name])) {
        //                    $relation->sync($prepared[$name]);
        //                }
        //                break;
        //            case $relation instanceof Relations\HasOne:
        //            case $relation instanceof Relations\MorphOne:
        //                $related = $this->model->getRelationValue($name) ?: $relation->getRelated();

        //                foreach ($prepared[$name] as $column => $value) {
        //                    $related->setAttribute($column, $value);
        //                }

        //                // save child
        //                $relation->save($related);
        //                break;
        //            case $relation instanceof Relations\BelongsTo:
        //            case $relation instanceof Relations\MorphTo:
        //                $related = $this->model->getRelationValue($name) ?: $relation->getRelated();

        //                foreach ($prepared[$name] as $column => $value) {
        //                    $related->setAttribute($column, $value);
        //                }

        //                // save parent
        //                $related->save();

        //                // save child (self)
        //                $relation->associate($related)->save();
        //                break;
        //            case $relation instanceof Relations\HasMany:
        //            case $relation instanceof Relations\MorphMany:
        //                foreach ($prepared[$name] as $related) {
        //                    /** @var Relations\HasOneOrMany $relation */
        //                    $relation = $this->model->$name();

        //                    $keyName = $relation->getRelated()->getKeyName();

        //                    /** @var Model $child */
        //                    $child = $relation->findOrNew(Arr::get($related, $keyName));

        //                    if (Arr::get($related, static::REMOVE_FLAG_NAME) == 1) {
        //                        $child->delete();
        //                        continue;
        //                    }

        //                    Arr::forget($related, static::REMOVE_FLAG_NAME);

        //                    $child->fill($related);

        //                    $child->save();
        //                }
        //                break;
        //        }
        //    }
        //}

        ///**
        // * Prepare input data for update.
        // *
        // * @param array $updates
        // * @param bool  $oneToOneRelation If column is one-to-one relation.
        // *
        // * @return array
        // */
        //protected function prepareUpdate(array $updates, $oneToOneRelation = false): array
        //{
        //    $prepared = [];

        //    /** @var Field $field */
        //    foreach ($this->fields() as $field) {
        //        $columns = $field->column();

        //        // If column not in input array data, then continue.
        //        if (!Arr::has($updates, $columns)) {
        //            continue;
        //        }

        //        if ($this->isInvalidColumn($columns, $oneToOneRelation || $field->isJsonType)) {
        //            continue;
        //        }

        //        $value = $this->getDataByColumn($updates, $columns);

        //        $value = $field->prepare($value);

        //        if (is_array($columns)) {
        //            foreach ($columns as $name => $column) {
        //                Arr::set($prepared, $column, $value[$name]);
        //            }
        //        } elseif (is_string($columns)) {
        //            Arr::set($prepared, $columns, $value);
        //        }
        //    }

        //    return $prepared;
        //}

        ///**
        // * @param string|array $columns
        // * @param bool         $containsDot
        // *
        // * @return bool
        // */
        //protected function isInvalidColumn($columns, $containsDot = false): bool
        //{
        //    foreach ((array) $columns as $column) {
        //        if ((!$containsDot && Str::contains($column, '.')) ||
        //            ($containsDot && !Str::contains($column, '.'))) {
        //            return true;
        //        }
        //    }

        //    return false;
        //}

        ///**
        // * Prepare input data for insert.
        // *
        // * @param $inserts
        // *
        // * @return array
        // */
        //protected function prepareInsert($inserts): array
        //{
        //    if ($this->isHasOneRelation($inserts)) {
        //        $inserts = Arr::dot($inserts);
        //    }

        //    foreach ($inserts as $column => $value) {
        //        if (($field = $this->getFieldByColumn($column)) === null) {
        //            unset($inserts[$column]);
        //            continue;
        //        }

        //        $inserts[$column] = $field->prepare($value);
        //    }

        //    $prepared = [];

        //    foreach ($inserts as $key => $value) {
        //        Arr::set($prepared, $key, $value);
        //    }

        //    return $prepared;
        //}

        ///**
        // * Is input data is has-one relation.
        // *
        // * @param array $inserts
        // *
        // * @return bool
        // */
        //protected function isHasOneRelation($inserts): bool
        //{
        //    $first = current($inserts);

        //    if (!is_array($first)) {
        //        return false;
        //    }

        //    if (is_array(current($first))) {
        //        return false;
        //    }

        //    return Arr::isAssoc($first);
        //}
        public BForm Ignore(List<string> fields)
        {
            ignored = ignored.Distinct().ToList();
            return this;
        }

        ///**
        // * @param array        $data
        // * @param string|array $columns
        // *
        // * @return array|mixed
        // */
        //protected function getDataByColumn($data, $columns)
        //{
        //    if (is_string($columns)) {
        //        return Arr::get($data, $columns);
        //    }

        //    if (is_array($columns)) {
        //        $value = [];
        //        foreach ($columns as $name => $column) {
        //            if (!Arr::has($data, $column)) {
        //                continue;
        //            }
        //            $value[$name] = Arr::get($data, $column);
        //        }

        //        return $value;
        //    }
        //}

        ///**
        // * Find field object by column.
        // *
        // * @param $column
        // *
        // * @return mixed
        // */
        //protected function getFieldByColumn($column)
        //{
        //    return $this->fields()->first(
        //        function (Field $field) use ($column) {
        //            if (is_array($field->column())) {
        //                return in_array($column, $field->column());
        //            }

        //            return $field->column() == $column;
        //        }
        //    );
        //}
        protected void SetFieldOriginalValue()
        {
            Fields().ForEach((field) => { field.SetOriginal(Original); });
        }

        protected void SetFieldValue(int id)
        {
            var relations = GetRelations();

            var builder = Model();

            //if ($this->isSoftDeletes) {
            //    $builder = $builder->withTrashed();
            //}

            //$this->model = $builder->with($relations)->findOrFail($id);
            var keyName = builder.GetKeyName();

            var data = builder.Where($"{keyName}={id}").FirstOrDefault();

            Original = data ?? throw new ArgumentNullException($"未查询到{keyName}为[{id}]的数据");
            CallEditing?.Invoke(this);

            Fields().ForEach((field) =>
            {
                if (!ignored.Contains(field.Column()))
                {
                    field.Fill(data);
                }
            });
        }

        ///**
        // * Add a fieldset to form.
        // *
        // * @param string  $title
        // * @param Closure $setCallback
        // *
        // * @return Field\Fieldset
        // */
        //public function fieldset(string $title, Closure $setCallback)
        //{
        //    $fieldset = new Field\Fieldset();

        //    $this->html($fieldset->start($title))->plain();

        //    $setCallback($this);

        //    $this->html($fieldset->end())->plain();

        //    return $fieldset;
        //}

        ///**
        // * Get validation messages.
        // *
        // * @param array $input
        // *
        // * @return MessageBag|bool
        // */
        //public function validationMessages($input)
        //{
        //    $failedValidators = [];

        //    /** @var Field $field */
        //    foreach ($this->fields() as $field) {
        //        if (!$validator = $field->getValidator($input)) {
        //            continue;
        //        }

        //        if (($validator instanceof Validator) && !$validator->passes()) {
        //            $failedValidators[] = $validator;
        //        }
        //    }

        //    $message = $this->mergeValidationMessages($failedValidators);

        //    return $message->any() ? $message : false;
        //}

        ///**
        // * Merge validation messages from input validators.
        // *
        // * @param \Illuminate\Validation\Validator[] $validators
        // *
        // * @return MessageBag
        // */
        //protected function mergeValidationMessages($validators): MessageBag
        //{
        //    $messageBag = new MessageBag();

        //    foreach ($validators as $validator) {
        //        $messageBag = $messageBag->merge($validator->messages());
        //    }

        //    return $messageBag;
        //}

        /**
         * Get all relations of model from callable.
         *
         * @return array
         */
        public static string GetRelations()
        {
            return "";
            //        $relations = $columns = [];

            //        /** @var Field $field */
            //        foreach ($this->fields() as $field) {
            //            $columns[] = $field->column();
            //}

            //        foreach (Arr::flatten($columns) as $column) {
            //            if (Str::contains($column, '.')) {
            //                list($relation) = explode('.', $column);

            //                if (method_exists($this->model, $relation) &&
            //                    !method_exists(Model::class, $relation) &&
            //                    $this->model->$relation() instanceof Relations\Relation
            //                ) {
            //                    $relations[] = $relation;
            //}
            //}
            //elseif(method_exists($this->model, $column) &&
            //                !method_exists(Model::class, $column)
            //            ) {
            //                $relations[] = $column;
            //            }
        }

        //    return array_unique($relations);
        //}

        public BForm SetAction(string action)
        {
            Builder().SetAction(action);

            return this;
        }

        public BForm SetWidth(int fieldWidth = 8, int labelWidth = 2)
        {
            Fields().ForEach((field) => { field.SetWidth(fieldWidth, labelWidth); });

            Builder().SetWidth(fieldWidth, labelWidth);

            return this;
        }

        public BForm SetView(string view)
        {
            Builder().SetView(view);
            return this;
        }

        public BForm SetTitle(string title = "")
        {
            Builder().SetTitle(title);

            return this;
        }

        ///**
        // * Set a submit confirm.
        // *
        // * @param string $message
        // * @param string $on
        // *
        // * @return $this
        // */
        //public function confirm(string $message, $on = null)
        //{
        //    if ($on && !in_array($on, ['create', 'edit'])) {
        //        throw new \InvalidArgumentException("The second paramater `\$on` must be one of ['create', 'edit']");
        //    }

        //    if ($on == 'create' && !$this->isCreating()) {
        //        return;
        //    }

        //    if ($on == 'edit' && !$this->isEditing()) {
        //        return;
        //    }

        //    $this->builder()->confirm($message);

        //    return $this;
        //}
        public BForm Row(Action<Row> callback)
        {
            rows.Add(new Row(callback, this));

            return this;
        }

        public void Tools(Action<Tools> callback)
        {
            callback.Invoke(_builder.GetTools());
        }


        public Tools Header() => _builder.GetTools();

        public void Header(Action<Tools> callback)
        {
            callback.Invoke(_builder.GetTools());
        }

        //    public bool IsCreating()
        //    {
        //        return Str::endsWith(\request()->route()->getName(), ['.create', '.store']);
        //    }

        //public bool IsEditing()
        //    {
        //        return Str::endsWith(\request()->route()->getName(), ['.edit', '.update']);
        //    }

        public BForm DisableSubmit(bool disable = true)
        {
            Builder().GetFooter().DisableSubmit(disable);
            return this;
        }


        public BForm DisableReset(bool disable = true)
        {
            Builder().GetFooter().DisableReset(disable);
            return this;
        }

        public BForm DisableViewCheck(bool disable = true)
        {
            Builder().GetFooter().DisableViewCheck(disable);
            return this;
        }

        public BForm DisableEditingCheck(bool disable = true)
        {
            Builder().GetFooter().DisableEditingCheck(disable);
            return this;
        }

        public BForm DisableCreatingCheck(bool disable = true)
        {
            Builder().GetFooter().DisableCreatingCheck(disable);
            return this;
        }

        public Footer Footer() => Builder().GetFooter();

        public void Footer(Action<Footer> callback)
        {
            callback.Invoke(Builder().GetFooter());
        }

        public string Resource(int slice = -2)
        {
            var segments = _controller.Request.Segments();

            if (slice == -1)
            {
                segments = segments.Take(segments.Length == 1 ? 1 : segments.Length - 1).ToArray();
            }
            else if (slice != 0)
            {
                segments = segments.Take(segments.Length + slice).ToArray();
            }

            return "/" + string.Join("/", segments);
        }

        public string Render()
        {
            try
            {
                return _builder.Render();
            }
            catch
            {
                Debug.Assert(false);
                //return Handler::renderException(e);
                return "";
            }
        }

        public HtmlContent Render(bool t)
        {
            return _builder.Render(t);
        }

        ///**
        // * Get or set input data.
        // *
        // * @param string $key
        // * @param null   $value
        // *
        // * @return array|mixed
        // */
        //public function input($key, $value = null)
        //{
        //    if ($value === null) {
        //        return Arr::get($this->inputs, $key);
        //    }

        //    return Arr::set($this->inputs, $key, $value);
        //}
        public BForm Column(double width, Action<BForm> closure)
        {
            width = width < 1 ? Math.Round(12 * width) : width;

            layout.Column((int) width, closure);

            return this;
        }

        protected void InitLayout()
        {
            layout = new FLayout(this);
        }

        public FLayout GetLayout()
        {
            return layout;
        }
    }
}