﻿using CoreAdmin.EF;
using CoreAdmin.Mvc.Models;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using CoreAdmin.Mvc.Widgets;
using CoreAdmin.Mvc.Middleware;
using System.Text.Json;
using CoreAdmin.Mvc.Struct;
using CoreAdmin.Mvc.Extensions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace CoreAdmin.Mvc
{
    public partial class Admin
    {
        private static IStringLocalizer<SharedResource> _stringLocalizer;
        public AdminDbContext DefaultDbContext { get; private set; }

        public static ILoggerFactory LoggerFactory { get; set; }


        public static Admin Instance { get; private set; }

        public Admin(IStringLocalizer<SharedResource> stringLocalizer, AdminDbContext context)
        {
            _stringLocalizer = stringLocalizer;
            DefaultDbContext = context;
            Instance = this;
        }
        static Admin()
        {
            Bootstrap();
        }
        /// <summary>
       /// 获取数据
       /// </summary>
        public static string Trans(string str, params object[] param) => _stringLocalizer != null ? (param.Length > 0 ? _stringLocalizer[str, param] : _stringLocalizer[str]) : str;
        /// <summary>
        /// 判断返回字符串以指定前缀开始的方法
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Url(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            return str.StartsWith("/") ? str : "/" + str;
        }

        public static string Assets(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            return str.StartsWith("/") ? "/_content/CoreAdmin.RCL" + str : str;
        }

        public Task<List<MenuModel>> Menu()
        {
            return DefaultDbContext.Menu.ToTree();
        }

        //public static UserModel User => Request.Instance?.HttpContext?.Session?.Get<UserModel>("userInfo");
        //[Obsolete("不能使用静态")]
        //public static UserModel User => new() { Avatar = Assets("/assets/AdminLTE/dist/img/user2-160x160.jpg"), UserName = "Administrator", Name = "Administrator", CreatedAt = DateTime.Now, Id = 1 };

        public const string Version = "1.8.11";

        protected BNavbar navbar;

        //    protected $menu = [];
        //public static string Title {get => _metaTitle ?? CoreAdminConfigure('admin.title') }

        public static string Favicon;

        protected static List<Action> BootingCallbacks = new();

        protected static List<Action> BootedCallbacks = new();

        public static string GetLongVersion()
        {
            return $"Laravel-admin <comment>version</comment> <info>{Version}</info>";
        }


        //        /**
        //         * Left sider-bar menu.
        //         *
        //         * @return array
        //         */
        //        public function menu()
        //        {
        //            if (!
        //            ($this->menu))
        //            {
        //                return $this->menu;
        //            }

        //        $menuClass = config('admin.database.menu_model');

        //        /** @var Menu $menuModel */
        //        $menuModel = new $menuClass();

        //            return $this->menu = $menuModel->toTree();
        //        }

        //        /**
        //         * @param array $menu
        //         *
        //         * @return array
        //         */
        //        public function menuLinks($menu = [])
        //        {
        //            if (empty($menu))
        //            {
        //            $menu = $this->menu();
        //            }

        //        $links = [];

        //            foreach ($menu as $item) {
        //                if (!empty($item['children']))
        //                {
        //                $links = array_merge($links, $this->menuLinks($item['children']));
        //                }
        //                else
        //                {
        //                $links[] = Arr::only($item, ['title', 'uri', 'icon']);
        //                }
        //            }

        //            return $links;
        //        }

        //        /**
        //         * Set admin title.
        //         *
        //         * @param string $title
        //         *
        //         * @return void
        //         */
        //        public static function setTitle($title)
        //        {
        //            self::$metaTitle = $title;
        //        }


        //        /**
        //         * Get the currently authenticated user.
        //         *
        //         * @return \Illuminate\Contracts\Auth\Authenticatable|null
        //         */
        //        public function user()
        //        {
        //            return $this->guard()->user();
        //        }


        public void Navbar(Action<BNavbar> builder) => builder.Invoke(GetNavbar());
        public BNavbar Navbar() => GetNavbar();

        public BNavbar GetNavbar()
        {
            if (null == navbar)
            {
                navbar = new BNavbar();
            }

            return navbar;
        }

        public static void Booting(Action callback)
        {
            BootingCallbacks.Add(callback);
        }

        ///**
        // * @param callable $callback
        // */
        //public static function booted(callable $callback)
        //{
        //    static::$bootedCallbacks[] = $callback;
        //}

        public static void Bootstrap()
        {
            FireBootingCallbacks();

            AddAdminAssets();

            FireBootedCallbacks();
        }

        /// <summary>
        /// 加载静态资源文件
        /// </summary>
        protected static void AddAdminAssets()
        {
            var assets = BForm.CollectFieldAssets();

            Css(assets["css"]);
            Js(assets["js"]);
        }

        protected static void FireBootingCallbacks()
        {
            foreach (var callable in BootingCallbacks)
            {
                callable.Invoke();
            }
        }

        protected static void FireBootedCallbacks()
        {
            foreach (var callable in BootedCallbacks)
            {
                callable.Invoke();
            }
        }

        public static void Toastr(string message, MessageType type = MessageType.Success)
        {
            FlashSessionMiddleware.Flash("toastr", JsonSerializer.Serialize(new MessageBag
            {
                Message = message,
                Type = type,
            }));
        }

        public static void Info(string title, string message, MessageType type = MessageType.Error)
        {
            FlashSessionMiddleware.Flash(type.ToString(), JsonSerializer.Serialize(new MessageBag
            {
                Title = title,
                Message = message,
                Type = type,
            }));
        }

        public static void Error(string title, string message) => Info(title, message);
        public static void Warning(string title, string message) => Info(title, message, MessageType.Warning);
        public static void Success(string title, string message) => Info(title, message, MessageType.Success);

    }

    public enum MessageType
    {
        Success,
        Error,
        Warning
    }
}
