﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using testDemo.Model;

namespace testDemo.Controllers;
public class HomeController : Controller
{
    public IActionResult Index()
    {
        List<Product> productList = new List<Product>()
        {
            new Product()
                {
                    ID = 5,
                    Name = "酸奶5",
                    Price = 7
                },
            new Product()
                {
                    ID = 6,
                    Name = "薯片6",
                    Price =8
                },
            new Product()
                {
                    ID = 7,
                    Name = "卤蛋7",
                    Price =2
                },
            new Product()
                {
                    ID = 8,
                    Name = "火腿肠8",
                    Price = 10
                }
    };
        try
        {
            using (var db = new ProductDBContext())
            {
                db.Products.AddRange(productList.ToArray());
                db.SaveChanges();
                ViewData["allProducts"] = db.Products.ToList();
                //foreach (var p in lists)
                //{
                //    Console.WriteLine(p.Name);
                //}

                //,NAME,PRICE
                string selectSql = "SELECT ID,NAME,PRICE FROM PRODUCTS  WHERE cast(ID as int)>2 ORDER BY RANDOM()";

                //var query = 

                

                ViewData["newProducts"]= db.Products.FromSqlRaw<Product>(selectSql).ToList();


                //foreach (var p in query)
                //{
                //    Console.WriteLine(p.ID);
                //}                             
                return View();
            }
        }
        catch (Exception)
        {

            throw;
        }       
    }
}
