﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Debug;

namespace testDemo.Model;
public class ProductDBContext:DbContext
{
    public static readonly ILoggerFactory MyLoggerFactory
     = LoggerFactory.Create(builder => { builder.AddConsole(); });




    public DbSet<Product> Products { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        optionsBuilder.UseSqlite("Data Source=products.db");
    }
}
