﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using testDemo.Model;
using System.Linq.Dynamic;

namespace testDemo.Pages;
public class IndexModel : PageModel
{
    private readonly ILogger<IndexModel> _logger;

    public IndexModel(ILogger<IndexModel> logger)
    {
        _logger = logger;
    }

    public void OnGet()
    {
        using (var db = new ProductDBContext())
        {
            ViewData["allProducts"] = db.Products.OrderBy(x=>x.ID).ToList();

            //ViewData["newProducts"] = (from t in db.Products where t.ID > 2 orderby Guid.NewGuid() select t).ToList();
            
            ViewData["newProducts"] = db.Products.Select(o=>new {o.ID}).Where(x => x.ID > 2).OrderBy(d=>EF.Functions.Random()).ToList();
        }
    }
}


public static class HelpClass
{
    //[System.Data.Objects.DataClasses.EdmFunction("Sqlite", "RANDOM()")]
    public static Guid NewId()
    {
        return Guid.NewGuid();
    }

    public static IQueryable<T> OrderByNewId<T>(this IEnumerable<T> source)
    {
        return source.AsQueryable().OrderBy(d => NewId());
    }
}