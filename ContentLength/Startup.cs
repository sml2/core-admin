using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ContentLengthTDC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //此方法由运行时调用。使用此方法将服务添加到容器
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //注册MVC服务，启用MVC应用程序模型
            //services.AddControllersWithViews(options =>
            //{
            //    //添加ActionFilter筛选器
            //    options.Filters.Add<ResponseContentLengthActionFilter>();

            //    //获取或设置一个值，该值确定路由是否应在内部使用终结点，或者是否应使用旧的路由逻辑。 终结点路由用于将 HTTP 请求与 MVC 操作进行匹配，并使用生成 Url IUrlHelper 。
            //    options.EnableEndpointRouting = false;
            //});

            //AddMvcCore()方法并没有象AddMvc()方法调用了支持MVC应用程序模型的视图引擎等相关的方法。
            //services.AddMvcCore().AddViews().AddRazorViewEngine();

            //注册MVC服务，启用MVC应用程序模型
            services.AddMvc(options =>
            {
                //添加ActionFilter筛选器
                options.Filters.Add<ResponseContentLengthActionFilter>();

                //获取或设置一个值，该值确定路由是否应在内部使用终结点，或者是否应使用旧的路由逻辑。 终结点路由用于将 HTTP 请求与 MVC 操作进行匹配，并使用生成 Url IUrlHelper 。
                options.EnableEndpointRouting = false;
            });
        }

        //此方法由运行时调用。使用此方法配置HTTP请求管道。
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //注册中间件
            //将它放置在项目中间件管道的头部。因为根据ASP.NET Core的中间件管道设计，只有第一个中间件才能获取到原始的请求信息和最终的响应信息
            app.UseResponseContentLength();

            //使用Use方法注册中间件类 中间件本质就是一个方法,这个方法接收一个HttpContext参数,返回Task
            //app.Use(next => new ResponseContentLengthMiddleware(next).Invoke);

            app.UseResponseCaching();          

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            //配置路由
            app.UseMvc(routes =>

               //使用指定的名称和模板将路由添加到IRouteBuilder。
               routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}"));

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllerRoute(
            //        name: "default",
            //        pattern: "{controller=Home}/{action=Index}/{id?}");
            //});
        }
    }
}
