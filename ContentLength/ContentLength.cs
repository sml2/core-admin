﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ContentLengthTDC
{
    /// <summary>
    /// 测试获取返回长度的中间件
    /// </summary>
    public class ResponseContentLengthMiddleware
    {
        //保存下一个中间件
        private readonly RequestDelegate _next;

        //构造函数中传入下一个中间件
        public ResponseContentLengthMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        //中间件方法,接收HttpContext参数返回Task
        public async Task Invoke(HttpContext context)
        {
            var Request = context.Request;

            var Response = context.Response;

            var originalResponseStream = Response.Body;

            var buffer = new MemoryStream();

            Response.Body = buffer;

            //调用下一个中间件
            await _next(context);

            Debug(Request, Response, "UseMiddleware Begin");

            #region
            //Response.OnStarting(() =>
            //{
            //    Debug(Request, Response, "Response.OnStarting");

            //    return Task.CompletedTask;
            //});

            //Response.OnStarting((state) =>
            //{
            //    var info = state as string;

            //    Debug(Request, Response, info);

            //    return Task.CompletedTask;

            //}, "Response.OnStarting(State)");

            //Response.OnCompleted(() =>
            //{
            //    Debug(Request, Response, "Response.OnCompleted");

            //    return Task.CompletedTask;
            //});

            //Response.OnCompleted((state) =>
            //{
            //    var info = state as string;

            //    Debug(Request, Response, info);

            //    return Task.CompletedTask;

            //}, "Response.OnCompleted(State)");

            //Response.RegisterForDispose(new Disposable(Request, Response));

            //Response.RegisterForDisposeAsync(new Disposable(Request, Response));
            #endregion

            //Debug(Request, Response, "UseMiddleware End");

            buffer.Position = 0;

            await buffer.CopyToAsync(originalResponseStream);

            Response.Body = originalResponseStream;
        }

        const string OrderKeyName = "RequestOrderKeyName";
        public static void Debug(HttpRequest Request, HttpResponse Response, string TimeInfo)
        {
            //存储响应体的内容
            long Length = Math.Max(Response.ContentLength.GetValueOrDefault(), Response.Body.Length);

            Request.HttpContext.Items.TryGetValue(OrderKeyName, out var o);

            var i = (o as int?).GetValueOrDefault();

            i++;

            var sb = new StringBuilder();

            sb.AppendLine($"----------------------------------------------------------------");

            sb.AppendLine($"-InjectPoint[{i}]：{TimeInfo}");

            sb.AppendLine($"-Request.Path:{Request.Path}");

            sb.AppendLine($"-Response.ContentLength:{Length}");

            Console.WriteLine(sb.ToString());

            Request.HttpContext.Items[OrderKeyName] = i;
        }
        public class Disposable : IDisposable, IAsyncDisposable
        {
            private HttpRequest Request;

            private HttpResponse Response;

            public Disposable(HttpRequest request, HttpResponse response)
            {
                Request = request;

                Response = response;
            }

            public void Dispose()
            {
                Debug(Request, Response, "IDisposable.Dispose");
            }

            public ValueTask DisposeAsync()
            {
                Debug(Request, Response, "IAsyncDisposable.DisposeAsync");

                return ValueTask.CompletedTask;
            }
        }
    }
    public static class ResponseContentLengthExtensions
    {
        public static IApplicationBuilder UseResponseContentLength(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ResponseContentLengthMiddleware>();
        }
    }
}
