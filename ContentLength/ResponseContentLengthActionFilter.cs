﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ContentLengthTDC
{
    public class ResponseContentLengthActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            Debug(context, "ActionFilter.OnActionExecuted");
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            //context.HttpContext

            //var i = context.HttpContext.RequestServices.GetService<Ic>();

            //Console.WriteLine(i);

            var HttpResetFeature = context.HttpContext.Features.Get<IHttpResetFeature>();

            //HttpResetFeature.Reset 

            Debug(context, "ActionFilter.OnActionExecuting");
        }
        private void Debug(ActionContext context, string Timeinfo)
        {
            var HttpContext = context.HttpContext;

            ResponseContentLengthMiddleware.Debug(HttpContext.Request, HttpContext.Response, Timeinfo);

            //ContentLengthTDC.ResponseContentLengthMiddleware.Debug(HttpContext.Request, HttpContext.Response, Timeinfo);
        }
    }
}
