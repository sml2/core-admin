import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Register from '@/components/Register'
import ManagerUser from '@/components/ManagerUser'
import Test from '@/components/Test'
import SwitchTest from '@/components/switchTest'

Vue.use(Router)

var router= new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register,
      meta:{
        requireLogin:true
           }
    },
    {
      path: '/ManagerUser',
      name: 'ManagerUser',
      component: ManagerUser,
      meta:{
        requireLogin:true
           }
    },
    {
      path: '/Test',
      name: 'Test',
      component: Test,
      meta:{
        requireLogin:true
           }
    },
    {
      path:'/SwitchTest',
      name: 'SwitchTest',
      component: SwitchTest,
      meta:{
        requireLogin:true
           }
    }
  ]
})

router.beforeEach((to,from,next)=>{  
  let islogin = localStorage.getItem("islogin");
    if(to.meta.requireLogin && !islogin)
    {
      next({name:'Login'});
    }
    else{
      next();
    }
}) 

export default router