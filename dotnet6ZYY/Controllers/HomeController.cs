﻿using Microsoft.AspNetCore.Mvc;

namespace dotnet6ZYY.Controllers;
public class HomeController : Controller
{
    public IActionResult Index()
    {
        //简单传值
        ViewBag.Title = "this is homeView";
        ViewData["Content"] = "";
        return View();
    }
}
