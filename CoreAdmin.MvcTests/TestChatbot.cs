﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoreAdmin.MvcTests
{
    [TestClass]
    public class TestChatbot
    {
        [TestMethod]
        public void Test()
        {
            ChatTest();
            Console.WriteLine("x");
        }
        [TestMethod]
        public void ChatTest() {
            //智障机器人的webhook
            string Url = @"https://open.feishu.cn/open-apis/bot/v2/hook/67df5485-d321-4dd2-831d-a623f9ef4992";
            
            string postDataStr = string.Empty;

            //智障机器人的webhook的参数

            var param = new
            {
                msg_type = "text",
                content = new
                {
                    text = "新更新提醒"
                }
            };

            string postData = JsonSerializer.Serialize(param);



            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == string.Empty ? string.Empty : "?") + postDataStr);
            request.Method = "POST";
            request.ContentType = "application/json;charset=utf-8";
            //request.Headers
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;

            using (Stream os = request.GetRequestStream())
            {
                os.Write(bytes, 0, bytes.Length);
            }

            using System.Net.WebResponse resp = request.GetResponse();
            if (resp == null) return;

            using System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            var @out = sr.ReadToEnd().Trim();
            Console.WriteLine(@out);

        }
    }
}
