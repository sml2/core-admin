﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreAdmin.MvcTests
{
    [TestClass]
    public class TestClass
    {
        [TestMethod()]
        public void Fun()
        {
            string s = "abc";
            string x = s[1..];
            string r = s[..1];
            Console.WriteLine($"x:{x}  r:{r}");//x:bc  r:a
        }
        public IEnumerable<int> GetIndexes(string str)
        {
            for (int CurIndex = 0; CurIndex  < str.Length; CurIndex++)
            {
                CurIndex = str.IndexOf("\r\n", CurIndex);
                if (CurIndex == -1)
                {
                    yield break;
                }
                else
                {
                    yield return CurIndex;
                }
            }
        }
        [TestMethod()]
        public void TestGetIndexes() {
            foreach (var item in GetIndexes("\r\nqwe\r\nrrrr\r\nasdasdasd\r\n\r\nasdsadasdadsa\r\n"))
            {
                Console.WriteLine(item);
            }
        }
    }
}
