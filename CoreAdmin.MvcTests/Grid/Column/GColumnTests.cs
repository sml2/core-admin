﻿using CoreAdmin.Mvc.Grid;
using CoreAdmin.Mvc.Grid.Displayers;
using CoreAdmin.Mvc.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using CoreAdmin.EF;
using CoreAdmin.Mvc;
using Microsoft.AspNetCore.Mvc;

namespace CoreAdmin.MvcTests.Grid.Column.Tests
{
    [TestClass()]
    public class GColumnTests
    {
        private static readonly UserModel user = new() { Name = "ryu" };
        private static string Mock(GColumn col)
        {
            var data = new List<UserModel>() { user };
            col.SetOriginalGridModels(data);
            var uiData = new List<Dictionary<string, object>>(new Dictionary<string, object>[data.Count]);

            col.Fill(data, ref uiData);
            return uiData[0][nameof(UserModel.Name)].ToString();
        }
       [TestMethod]
       public void LabelTest()
        {
            var col = new GColumn(nameof(UserModel.Name), "");

            // 单个默认sucess
            col.Label();
            Assert.AreEqual($"<span class='label label-success'>{user.Name}</span>", Mock(col));

            // 单个error
            col = new GColumn(nameof(UserModel.Name), "");
            col.Label(LabelColor.Danger);
            Assert.AreEqual($"<span class='label label-danger'>{user.Name}</span>", Mock(col));

            // 根据条件展示label
            col = new GColumn(nameof(UserModel.Name), "");
            var condition = new Dictionary<string, LabelColor>()
            {
                { "ryu", LabelColor.Success },
                {"ryu1", LabelColor.Danger }
            };
            col.Label(condition);
            Assert.AreEqual($"<span class='label label-success'>{user.Name}</span>", Mock(col));
        }

        [TestMethod]
        public void ImageTest()
        {
            var col = new GColumn(nameof(UserModel.Name), "");

            // 默认
            col.Image();
            Assert.AreEqual($"<img src='{user.Name}' style='max-width:200px;max-height:200px' class='img img-thumbnail' />", Mock(col));

            // 添加域名/前缀， 图片宽高
            col = new GColumn(nameof(UserModel.Name), "");
            col.Image("http://127.0.0.1/", 100, 100);
            Assert.AreEqual($"<img src='http://127.0.0.1/{user.Name}' style='max-width:100px;max-height:100px' class='img img-thumbnail' />", Mock(col));
        }

        [TestMethod]
        public void ButtonTest()
        {
            var col = new GColumn(nameof(UserModel.Name), "");

            // 默认按钮
            col.Button();
            Assert.AreEqual($"<span class=\"btn btn-success\">{user.Name}</span>", Mock(col));

            // 多个class的按钮, 貌似没什么用，后面可能移除
            col = new GColumn(nameof(UserModel.Name), "");
            col.Button(LabelColor.Success, LabelColor.Danger);
            Assert.AreEqual($"<span class=\"btn btn-success btn-danger\">{user.Name}</span>", Mock(col));
        }

        [TestMethod]
        public void LinkTest()
        {
            var col = new GColumn(nameof(UserModel.Name), "");

            // 默认链接
            col.Link("http://localhost");
            Assert.AreEqual($"<a href='http://localhost' target='_blank'>{user.Name}</a>", Mock(col));

            // 多个class的按钮, 貌似没什么用，后面可能移除
            col = new GColumn(nameof(UserModel.Name), "");
            col.Link(model => (model as UserModel).Name);
            Assert.AreEqual($"<a href='{user.Name}' target='_blank'>{user.Name}</a>", Mock(col));
        }

        [TestMethod]
        public void BadgeTest()
        {
            var col = new GColumn(nameof(UserModel.Name), "");
            // 默认badge
            col.Badge(BgColor.Green);
            Assert.AreEqual($"<span class='badge bg-green'>{user.Name}</span>", Mock(col));

            // 条件展示
            col = new GColumn(nameof(UserModel.Name), "");
            col.Badge(new Dictionary<object, BgColor> {
                {"ryu", BgColor.Green },
                {"ryu1", BgColor.Red },

            });
            Assert.AreEqual($"<span class='badge bg-green'>{user.Name}</span>", Mock(col));
        }
    }
}
