﻿using Microsoft.EntityFrameworkCore;
using EFDemo.Data;

var builder = WebApplication.CreateBuilder(args);

//builder.Services.AddDbContext<EFDemoContext>(options =>
//    options.UseSqlite(builder.Configuration.GetConnectionString("EFDemoContext")));

// sqlite数据库连接字符串
//string connectText = builder.Configuration.GetConnectionString("EFDemoContext");
builder.Services.AddDbContext<EFDemoContext>(options =>
    options.UseSqlite("Data Source=Data/EFDemo.db"));

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
