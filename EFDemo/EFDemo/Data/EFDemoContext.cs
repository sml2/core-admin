﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EFDemo.Models;
using EFDemo_XQJ.Models;

namespace EFDemo.Data
{
    public class EFDemoContext : DbContext //表示该类为上下文类，数据库名称为 XXEFDemo，类名称为什么，数据库名就为什么
    {
        /// <summary>
        /// 这个构造函数能够配置相关设置
        /// </summary>
        /// <param name="options"></param>
        public EFDemoContext (DbContextOptions<EFDemoContext> options)
            : base(options)
        {
        }
        //Dbset  映射成一个表
        //Dbset<User>   里面的User即为使用的模型类
        //User  User 类在数据库生成的名称
        public DbSet<Movie> Movie { get; set; }

        public DbSet<User> User { get; set; }
    }
}
