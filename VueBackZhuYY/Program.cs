
using VueCliMiddleware;

var builder = WebApplication.CreateBuilder(args);

var VueRoot = builder.Configuration.GetSection("Spa:SourcePath").Value;
//配置Vue项目的静态路径
builder.Services.AddSpaStaticFiles(c =>
{
    c.RootPath = VueRoot;
});
// Add services to the container.


builder.Services.AddLogging(log => {
    log.AddLog4Net("Cfg/log4net.Config");
});


builder.Services.AddCors(options => {
    options.AddPolicy("CorsPolicy",
               builder => builder.WithOrigins("http://localhost:8080") //前端地址
               .AllowAnyMethod()
               .AllowAnyHeader());
});



builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseAuthorization();
app.UseCors("CorsPolicy");

app.UseSpa(spa =>
{
    // To learn more about options for serving an Angular SPA from ASP.NET Core,
    // see https://go.microsoft.com/fwlink/?linkid=864501

    spa.Options.SourcePath = VueRoot;

    if (!app.Environment.IsProduction())
    {
        var mode = "start";//start,attach
        var port = 8080;

        if (mode == "start")
            // run npm process with client app
            spa.UseVueCli(npmScript: "serve", port: port, forceKill: true);

        //
        spa.UseProxyToSpaDevelopmentServer($"http://localhost:{port}");
    }
});


app.MapControllerRoute(
    name: "default",
    pattern: "{controller=User}/{action=Index}/{id?}");


app.MapControllers();

app.Run();
