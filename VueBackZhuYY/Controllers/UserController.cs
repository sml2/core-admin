﻿using dotnet6DemoZyy.Models;
using Microsoft.AspNetCore.Mvc;

namespace VueBackZhuYY.Controllers;
[Route("api/[controller]/[action]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> logger;  
    public UserController(ILogger<UserController> _logger)
    {
        logger = _logger;        
    }

   
    public bool Login()
    {
        if (HttpContext.Session.GetString("Admin") == null)
        {
            HttpContext.Session.SetString("Admin", "ZYY");
            logger.LogInformation("ZYY登录成功！");
        }
        return true;
    }


    public string getloginUser([FromBody] User u)
    {
        try
        {
            using (var db = new UserDBContext())
            {
                var sameName = db.User.Where(x => x.NAME == u.NAME && x.PASSWORD == u.PASSWORD).ToList();
                if (sameName.Count() > 0)
                {
                    return "1";
                }
                else
                {
                    return "2";
                }
            }
        }
        catch (Exception)
        {
            throw;
        }

    }


    private static int num = 3;
    [HttpPost]
    public string PostRegisterUser([FromBody] User user)
    {
        try
        {
            using (var db = new UserDBContext())
            {
                //先查一下数据库中有没有同名的
                var sameName = db.User.Where(x => x.NAME == user.NAME).ToList();
                if (sameName.Count() > 0)
                {
                    return "2";
                }
                else
                {
                    user.ID = num + 1;
                    db.User.Add(user);
                    db.SaveChanges();
                    return "1";
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

 
    public ActionResult GetUsers()
    {
        try
        {
            using (var db = new UserDBContext())
            {
                return Ok(db.User.Where(x => x.UserType == EnumUserType.User).ToList());
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    [HttpGet]
    public string ChangeStatue(int id)
    {
        try
        {
            using (var db = new UserDBContext())
            {
                //先查一下数据库中有没有同名的
                var sameName = db.User.Find(id);
                if (sameName == null)
                {
                    return "2";
                }
                else
                {
                    sameName.Statue = !sameName.Statue;
                    db.User.Update(sameName);
                    db.SaveChanges();
                    return "1";
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

}
