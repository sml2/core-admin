﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotnet6DemoZyy.Models;
public class User
{
    [Key]
    public int ID { get; set; }
    public string NAME { get; set; }
    public string PASSWORD { get; set; }

    [DefaultValue(EnumUserType.User)]
    public EnumUserType UserType { get; set; }

    [DefaultValue(false)]
    public Boolean Statue { get; set; }    
}


public enum EnumUserType
{
    Admin = 0,
    User = 1
}