﻿
using Microsoft.EntityFrameworkCore;

namespace dotnet6DemoZyy.Models;
public class UserDBContext : DbContext
{
    public DbSet<User> User { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=User.db");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //添加种子数据
        modelBuilder.Entity<User>().HasData(
            new User()
            {
                ID = 1,
                NAME = "admin",
                PASSWORD = "123",
                Statue = true,
                UserType = EnumUserType.Admin
            });
    }
}
