
using dotnet6DemoZyy.Models;
using Microsoft.AspNetCore.Http.Connections;
using System.Configuration;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddLogging(log => {
    log.AddFilter("System", LogLevel.Warning);  // 忽略系统的其他日志
    log.AddFilter("Microsoft", LogLevel.Warning);
    log.AddLog4Net("Cfg/log4net.Config");
});


// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddSession();



//注入DBContext
builder.Services.AddDbContext<ProductDbContext>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseSession();

app.UseAuthorization();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Simple}/{action=UseSqliteDataBaseAdd}/{id?}");



app.Run();
