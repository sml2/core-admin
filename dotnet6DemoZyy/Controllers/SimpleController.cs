﻿using dotnet6DemoZyy.Models;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Specialized;
using System.Configuration;


namespace dotnet6DemoZyy.Controllers;
public class SimpleController : Controller
{
    private readonly ILogger<SimpleController> logger;

  //  private readonly ConnectionOptions dbConnectionOptions;

    private readonly IConfiguration configuration;



    

    // ConnectionOptions _dbConnectionOptions, 
    public SimpleController(ILogger<SimpleController> _logger,IConfiguration _configuration)
    {
        logger = _logger;
       // dbConnectionOptions = _dbConnectionOptions;
        configuration = _configuration;
    }

    /// <summary>
    /// 简单controller：
    /// 1.使用log4net
    /// 2.session
    /// 3.传值
    /// 4.读取配置
    /// 5.视图：母版视图，局部视图，视图扩展
    /// 6.内置容器
    /// 7.ef的增删改查
    /// 8.过滤器
    /// 9.中间件
    /// </summary>
    /// <returns></returns>
    public IActionResult Index()
    { 
        logger.LogInformation("这是Index方法");

        ViewBag.Title = "这是Simple的Index标题";
        ViewData["Content"] = "内容";
        TempData["Author"] = "zyy写的";

        //session
       if(HttpContext.Session.GetString("Admin")==null)
        {
            ViewBag.Tip = "请登录再使用本系统";
        }
       else
        {
            ViewBag.Tip=$"欢迎"+HttpContext.Session.GetString("Admin")+"使用本系统";
        };

        //读取配置


       
        //string configPath = "/TestConfig";

        //// Get the Web application configuration object.
        //Configuration config =
        //  WebConfigurationManager.OpenWebConfiguration(configPath);

        //// Get the conectionStrings section.
        //ConnectionStringsSection csSection =
        //    config.ConnectionStrings;

        //Console.WriteLine("Display configuration strings.");

        //for (int i = 0; i <
        //    ConfigurationManager.ConnectionStrings.Count; i++)
        //{
        //    ConnectionStringSettings cs =
        //        csSection.ConnectionStrings[i];

        //    Console.WriteLine("  Connection String: \"{0}\"",
        //        cs.ConnectionString);

        //    Console.WriteLine("#{0}", i);
        //    Console.WriteLine("  Name: {0}", cs.Name);

        //    Console.WriteLine("  Provider Name: {0}",
        //        cs.ProviderName);
        //}

        //ViewBag.Port = configuration["port"];
        //ViewBag.Id = configuration["Id"];

        //ViewBag.C1 = config.GetSection("WriteConnection");
        //ConfigurationSectionGroup sectionGroup = config.GetSectionGroup("ReadConnectionList");
        //ViewBag.C2 = sectionGroup.SectionGroups[0];       
        //ViewBag.C3 = sectionGroup.SectionGroups[1];
        //ViewBag.C4 = sectionGroup.SectionGroups[2];
      
       
        return View();
    }


  



    public bool Login()
    {
        if(HttpContext.Session.GetString("Admin")==null)
        {
            HttpContext.Session.SetString("Admin", "ZYY");
            logger.LogInformation("ZYY登录成功！");
        }
        return true;
    }


    public IActionResult UseSqliteDataBaseAdd()
    {
        try
        {
            using(var db=new ProductDbContext())
            {
                //add
                Product p = new Product() { ID ="1", Name = "酸奶", Price = 8 };
                db.Products.Add(p);
                db.SaveChanges();
                var p1 = db.Products.FirstOrDefault();
                return View(p1);
            }           
        }
        catch (Exception)
        {

            throw;
        }
    }

    public IActionResult UseSqliteDataBaseUpdate()
    {
        try
        {
            using (var db = new ProductDbContext())
            {
                //add
                Product p = new Product() { ID = "1", Name = "酸奶new", Price = 10 };
                db.Products.Add(p);
                db.SaveChanges();
                var p1 = db.Products.FirstOrDefault();
                return View(p1);
            }
        }
        catch (Exception)
        {

            throw;
        }
    }
}
