﻿using Microsoft.EntityFrameworkCore;

namespace dotnet6DemoZyy.Models;
public class ProductDbContext:DbContext
{
    public DbSet<Product> Products { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {      
        optionsBuilder.UseSqlite("Data Source=products.db");
    }
}
