﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PracticeADY.Context.Migrations_Commodity
{
    public partial class Shop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Shops",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShopKeeper = table.Column<string>(type: "TEXT", nullable: true),
                    ShopName = table.Column<string>(type: "TEXT", nullable: true),
                    StartTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Shoplocation = table.Column<string>(type: "TEXT", nullable: true),
                    IsPutaway = table.Column<bool>(type: "INTEGER", nullable: false),
                    GetGoodsId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shops", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Shops_Good_GetGoodsId",
                        column: x => x.GetGoodsId,
                        principalTable: "Good",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shops_GetGoodsId",
                table: "Shops",
                column: "GetGoodsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Shops");
        }
    }
}
