﻿using PracticeADY.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.EF;
using CoreAdmin.Mvc;

namespace PracticeADY.Context
{
    public class Commodity: BaseDbContext, IAdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Commodity -OutputDir Context\Migrations_Commodity [name]
        //回滚迁移
        //Remove-Migration  -Context Demands
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Commodity
        //删库
        //Drop-Database  -Context Demands
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Demands
        //获取帮助
        //get-help entityframework
        //20210720034538_DepartmentsData
        public DbSet<Goods> Good { get; set; }
        public DbSet<Shops> Shops { get; set; }
        public Commodity (DbContextOptions<Commodity> options) :base(options)
        {

        }
    }
}
