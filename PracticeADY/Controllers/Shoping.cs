﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Web.CodeGeneration;
using PracticeADY.Context;
using PracticeADY.Context.Migrations_Commodity;
using PracticeADY.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PracticeADY.Controllers
{
    public class Shoping : Controller
    {
        private readonly ILogger<Shoping> Logger;
        private readonly Commodity Commodity;
        public Shoping(ILogger<Shoping> logger, Commodity _Commodity)
        {
            Commodity= _Commodity;
            Logger = logger;
            Logger.LogInformation("ShopingController Be Create");
        }

        public async Task<IActionResult> Index()
        {
            var Shop = await Commodity.Shops.ToListAsync();
            return View(Shop); 
        }
        [HttpPost]
        public async Task<string> CreateShop(Shops shops)
        {
          
           await Commodity.Shops.AddAsync(shops);
            Commodity.SaveChanges();
            return "";
        }
        [HttpPost]
        public async Task<string> CreateShp(Shops shops)
        {

            await Commodity.Shops.AddAsync(shops);
            Commodity.SaveChanges();
            return "";
        }

    }
}
