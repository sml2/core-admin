﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PracticeADY.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> Loger;

        public HomeController(ILogger<HomeController> logger)
        {
            Loger = logger;
            Loger.LogInformation("HomeController Be Create");
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
