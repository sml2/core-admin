﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PracticeADY.Context;
using PracticeADY.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;
using Rong.EasyExcel;
using Npoi.Mapper;
using Microsoft.AspNetCore.Http;

namespace PracticeADY.Controllers
{
    [Route("[controller]")]
    public class GoodsController : Controller
    {
        private readonly ILogger<GoodsController> Loger;

        private readonly Commodity DbGoods;
        public GoodsController(ILogger<GoodsController> logger, Commodity commodity)
        {
            DbGoods = commodity;
            Loger = logger;
            Loger.LogInformation("GoodsController Be Create");
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(string searchKey, int page = 1, int pageSize = 10)
        {
            ViewData["PageSize"] = pageSize;
            ViewData["SearchKey"] = searchKey;
            List<Goods> result = DbGoods.Good.ToList();  //可以把参数带到后台，数据量小，直接全部全取在客户端分页的
            if (!string.IsNullOrEmpty(searchKey))
            {
                result = result.Where(s => (!string.IsNullOrEmpty(s.GoodsName) && s.GoodsName.Contains(searchKey))
                                           || (!string.IsNullOrEmpty(s.GoodsAddress) && s.GoodsAddress.Contains(searchKey))).ToList();
            }
            return View(result.ToPagedList(page, pageSize));

            //var totalRows = DbGoods.Good.Count();
            ////计算总页数
            //var totalPages = Math.Ceiling(totalRows * 1.00 / pageSize);
            //ViewBag.totalPages = totalPages;
            //var Index = await DbGoods.Good
            //      .ToListAsync();
            //
            //
            //   .OrderBy(p => p.Id)  //排序
            //   .Skip((pageIndex - 1) * pageSize)//跳过
            //   .Take(pageSize)   //取
            //   .ToListAsync();
            // return View(Index);
        }

        [HttpPost]
        public IEnumerable<Goods> UploadFile(IFormFile formFile)
        {
            //通过上传文件流初始化Mapper
            var mapper = new Mapper(formFile.OpenReadStream());
            //读取sheet1的数据
            return mapper.Take<Goods>("sheet1").Select(i => i.Value);
        }


        // 获取excel文件
        [HttpGet]
        public ActionResult DownLoadFile()
        {
            
            var goods= DbGoods.Good;

            var mapper = new Mapper();
            MemoryStream stream = new MemoryStream();
            //将students集合生成的Excel直接放置到Stream中
            mapper.Save(stream, goods, "sheet1", overwrite: true, xlsx: true);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Goods.xls");
        }

        [HttpPost]
        public async Task CreateGoods(Goods goods)
        {
            await DbGoods.Good.AddAsync(goods);
            DbGoods.SaveChanges();

        }

    }

}
