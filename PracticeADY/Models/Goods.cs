﻿using CoreAdmin.Mvc.Form.Field;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PracticeADY.Models
{
    public  class Goods
    {
        public int Id { get; set; }
        public string GoodsName { get; set; }
        public string img { get; set; }
        public decimal Price { get; set; }
        public DateTime ProductionTime { get; set; }  //生产日期
        public string Expiration { get; set; }  //保质期
        public string GoodsAccessory { get; set; }  //配料
        public string GoodsAddress { get; set; }  //产地
        public string GoodsStandard { get; set; }  //标准
       
    }
}
