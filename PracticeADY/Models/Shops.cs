﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PracticeADY.Models
{
    public  class Shops
    {
        public int Id { get; set; }
        public string ShopKeeper { get; set; }  //店主
        public string ShopName { get; set; }     
        public DateTime StartTime { get; set; }  //开业时间
        public string Shoplocation { get; set; } //地址
        public Boolean IsPutaway { get; set; } //上下架
        public Goods GetGoods{ get; set; }
    }
}
