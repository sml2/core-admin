using CoreAdmin.EF;
using CoreAdminTest.EF;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PracticeADY.Context;
using Rong.EasyExcel.EpPlus;
using Rong.EasyExcel.Npoi;

namespace PracticeADY
{
    public class Startup
    {
 
        
        
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        private string DatabaseDriver => Configuration["DatabaseDriver"].ToUpper() ?? "SQLITE";
        public void ConfigureServices(IServiceCollection services)
        {
            #region 数据库
             var CommoditysConnectionString = Configuration.GetConnectionString($"{DatabaseDriver}_Commoditys");

            //services.AddDbContext<Context>(optionsAction => _ = DatabaseDriver switch
            //{
            //    "SQLITE" => optionsAction.UseSqlite(ConnectionString),
            //    "MYSQL" => ((dynamic)optionsAction).UseMySQL(ConnectionString),
            //    "SQLSERVER" => ((dynamic)optionsAction).UseSqlServer(ConnectionString),
            //    _ => throw new System.ArgumentException($"Unsupported DatabaseDriver: {DatabaseDriver}")
            //});
            //使用ef core sqlite 连接
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new EFLoggerProvider());
            //需要指定AdminDbContext,要不会注入失败 
        
            services.AddDbContext<Commodity>(r => r.UseSqlite(CommoditysConnectionString).UseLoggerFactory(loggerFactory));
            #endregion
            services.AddEpPlusExcel();
            services.AddNpoiExcel();


            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();


            app.UseAuthentication();
            app.UseAuthorization();
                     
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                

            });

           //app.UseEndpoints(endpoints =>
           //{
           //    endpoints.MapRazorPages();
           //});
        }
    }
}
