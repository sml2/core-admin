﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace vuedemobackstageZhuYY.Model;
public class User
{
    [Key]
    public int ID { get; set; }
    public string NAME  { get; set; }
    public string PASSWORD { get; set; }
    [DefaultValue(UserType.User)]
    public UserType UserType { get; set; }
    [DefaultValue(false)]
    public bool Statue { get; set; }
}

public enum UserType
{
    Admin = 0,
    User = 1
}
