﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using vuedemobackstageZhuYY.Model;

namespace vuedemobackstageZhuYY.Controllers;
[Route("api/[controller]/[action]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;

    public UserController(ILogger<UserController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public string GetString()
    {
        return "STRING";
    }


    [HttpGet]
    public IActionResult getAllUser()
    {
        try
        {           
            using (var db = new UserDBContext())
            {
              return  Ok(db.User.ToList());               
            }
        }
        catch (Exception)
        {
            throw;
        }
    }




    [HttpPost]
    public string getloginUser([FromBody] User u)
    {               
        try
        {                      
            using (var db = new UserDBContext())
            {                
                var sameName = db.User.Where(x => x.NAME == u.NAME && x.PASSWORD ==u.PASSWORD).ToList();
                if (sameName.Count() > 0)
                {
                    return "1";
                }
                else
                {                   
                    return "2";
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
     
    }

    private int num = 3;

    [HttpPost]
    public string PostRegisterUser([FromBody] User user)
    {
        try
        {
            using (var db = new UserDBContext())
            {
                //先查一下数据库中有没有同名的
                var sameName = db.User.Where(x => x.NAME == user.NAME).ToList();
                if (sameName.Count() > 0)
                {                    
                    return "2";
                }
                else
                {
                    user.ID = num+1;
                    db.User.Add(user);
                    db.SaveChanges();
                    return "1";
                }
            }
        }
        catch (Exception)
        {
            throw;
        }       
    }
}
