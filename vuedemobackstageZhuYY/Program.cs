
using vuedemobackstageZhuYY;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddCors(options => {
    options.AddPolicy("CorsPolicy",
               builder => builder.WithOrigins("http://localhost:8084") //ǰ�˵�ַ
               .AllowAnyMethod()
               .AllowAnyHeader());
});

builder.Services.AddLogging(log => {
    log.AddLog4Net("Cfg/log4net.Config");
});

//ע��DBContext
builder.Services.AddDbContext<UserDBContext>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseAuthorization();
app.UseCors("CorsPolicy");
app.MapControllers();
app.Run();
