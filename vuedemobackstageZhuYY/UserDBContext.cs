﻿
using Microsoft.EntityFrameworkCore;

namespace vuedemobackstageZhuYY;

using vuedemobackstageZhuYY.Model;
public class UserDBContext:DbContext
{
    public DbSet<User> User { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=User.db");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>().HasData(
         new User()
         {
             ID = 1,
             NAME = "admin",
             PASSWORD = "123",
             Statue = true,
             UserType = UserType.admin            
         });
    }
}
