﻿using Microsoft.AspNetCore.Mvc;
using VueDemoZhuyy.Models;
using VueDemoZhuyy.Extensions;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;

namespace VueDemoZhuyy.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> logger;
    private readonly UserDBContext DBContext;

    public UserController(ILogger<UserController> _logger, UserDBContext _userDBContext)
    {
        logger = _logger;
        DBContext = _userDBContext;
        logger.LogWarning("测试信息！");

    }
    State Format(State State)
    {
        //方便调试，header头追加Message信息
        var desc = State.GetDescription();
        //System.Web.HttpUtility.UrlEncode(desc,System.Text.Encoding.UTF8).ToUpper();
        //Uri.EscapeUriString()
        Response.Headers.Add("Message", Uri.EscapeDataString(desc));
        return State;
    }

    public bool CheckSeesion()
    {
       var flag= HttpContext.Session.GetString("SessionLogin");      
        if (flag!="true")
        {
            return false;
        }
        return true;
    }

    [HttpPost]
    [Route("/User/getLogin")]
    [Route("/User/softlogin")]
    public State getLogin([FromBody] User u)
    {
        try
        {
            logger.LogInformation("登录方法！");
            var sameName = DBContext.User.Where(x => x.NAME == u.NAME).FirstOrDefault();
            if (sameName is null)
            {
                return Format(State.UserNone);
            }
            else
            {
                if (sameName.PASSWORD == u.PASSWORD)
                {
                    if (!sameName.State)
                    {
                        return Format(State.LoginUserStatue);
                    }
                    if (sameName.Role == Role.Admin)
                    {
                        HttpContext.Session.SetString("SessionLogin", "true");                                                                                         
                        return Format(State.Success);
                    }
                    else
                    {
                        return Format(State.LoginNOTAdmin);
                    }
                }
                else
                {
                    return Format(State.PasswordError);
                }
            }
        }
        catch (Exception ex)
        {
            logger.LogError("getLogin方法出错！" + ex.Message);
            return Format(State.Exception);           
            throw;
        }
      }


   
    [HttpPost]
    [Route("/User/PostRegisterUser")]
    [Route("/User/softreg")]
    public State softreg([FromBody] User user)
    {
        try
        {
            if(!CheckSeesion())
            {
                return Format(State.NOLOGIN);
            }
            //先查一下数据库中有没有同名的
            var sameName = DBContext.User.Where(x => x.NAME == user.NAME).Any();
            if (sameName)
            {
                return Format(State.SameName);
            }
            else
            {
                DBContext.User.Add(user);
                DBContext.SaveChanges();
                return Format(State.Success);
            }
        }
        catch (Exception ex)
        {
            logger.LogError("softreg方法出错！" + ex.Message);
            return Format(State.Exception);
            throw;           
        }
    }


    
    [HttpPost]
    public State UpdateUser([FromBody] User user)
    {
        try
        {
            if (!CheckSeesion())
            {
                return Format(State.NOLOGIN);
            }
            //先查一下数据库中有没有同名的
            var u = DBContext.User.Where(x => x.ID == user.ID).FirstOrDefault();
                if (u!=null)
                {
                    var sameName = DBContext.User.Where(x => x.ID != user.ID && x.NAME == user.NAME).FirstOrDefault();
                    if (sameName!=null)
                    {
                        return State.SameName;
                    }
                    else
                    {
                        u.NAME = user.NAME;
                        u.Phone = user.Phone;
                        u.Mailbox = user.Mailbox;
                        u.Remark = user.Remark;                       
                        DBContext.User.Update(u);
                        DBContext.SaveChanges();
                        return State.Success;
                    }
                }
                else
                {
                    return State.UserNone;
                }
        }
        catch (Exception ex)
        {
            logger.LogError("UpdateUser方法出错！" + ex.Message);
            return State.Exception;
            throw;
        }
    }

  
    [HttpPost]
    public State InitPwd([FromBody] User user)
    {
        try
        {
            if (!CheckSeesion())
            {
                return Format(State.NOLOGIN);
            }
            var u = DBContext.User.Where(x => x.ID == user.ID).FirstOrDefault();
                if (u is not null)
                {                                           
                        u.PASSWORD = user.PASSWORD;
                        DBContext.User.Update(u);
                        DBContext.SaveChanges();
                        return State.Success;                    
                }
                else
                {
                    return State.UserNone;
                }
        }
        catch (Exception ex)
        {
            logger.LogError("InitPwd方法出错！" + ex.Message);
            return State.Exception;
            throw;
        }
    }


  
    [HttpPost]
    public State DeleteUser([FromBody] User user)
    {
        try
        {
            if (!CheckSeesion())
            {
                return Format(State.NOLOGIN);
            }
            var sameName = DBContext.User.Where(x => x.ID == user.ID).FirstOrDefault();
                if (sameName==null || sameName.ID<=0)
                {
                    return State.UserNone;
                }
                else
                {
                    DBContext.User.Remove(sameName);
                    DBContext.SaveChanges();
                    return State.Success;
                }
        }
        catch (Exception ex)
        {
            logger.LogError("DeleteUser方法出错！" + ex.Message);
            return State.Exception;
            throw;
        }
    }

   
    [HttpGet]
    public State ChangeStatue(int id)
    {
        try
        {
            if (!CheckSeesion())
            {
                return Format(State.NOLOGIN);
            }
            var sameName = DBContext.User.Find(id);
                if (sameName == null)
                {
                    return State.UserNone;
                }
                else
                {
                    sameName.State = !sameName.State;
                    DBContext.User.Update(sameName);
                    DBContext.SaveChanges();
                    return State.Success;
                }
        }
        catch (Exception ex)
        {
            logger.LogError("ChangeStatue方法出错！" + ex.Message);
            return State.Exception;
            throw;
        }
    }

    [HttpGet]
    public ActionResult GetPagesUsers(int currentPage = 0,int pagesize = 5)
    {       
        try
        {
            if (!CheckSeesion())
            {
                return Content("5");
            }
            int totalCounts = DBContext.User.Where(n => n.Role == Role.User).Count();
            var items = DBContext.User.Where(n => n.Role == Role.User).OrderBy(o => o.ID).Skip(pagesize * (currentPage - 1)).Take(pagesize).Select(s => s);
            MyPageModel myPageModel = new MyPageModel()
            {
                PageSize = pagesize,
                CurrentPage = currentPage,
                TotalCount = totalCounts,
                users = items.AsQueryable()
            };
            return Ok(myPageModel);        
        }
        catch (Exception ex)
        {
            logger.LogError("GetPagesUsers方法出错！" + ex.Message);
            throw;
        }
    }

    [HttpPost]
    public State CancleUser([FromBody] User user)
    {
        try
        {
            if (!CheckSeesion())
            {
                return Format(State.NOLOGIN);
            }
            var sameName = DBContext.User.Where(x => x.ID == user.ID).FirstOrDefault();
                if (sameName == null || sameName.ID <= 0)
                {
                    return State.UserNone;
                }
                else
                {
                    DBContext.User.Remove(sameName);
                    DBContext.SaveChanges();
                    return State.Success;
                }
        }
        catch (Exception ex)
        {
            logger.LogError("CancleUser方法出错！" + ex.Message);
            return State.Exception;
            throw;
        }
    }

    public State LogOut()
    {
        HttpContext.Session.SetString("SessionLogin","");
        return Format(State.Success);
    }
}
