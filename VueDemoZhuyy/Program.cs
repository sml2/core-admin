
using VueCliMiddleware;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SpaServices.Extensions.Vue;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Mvc.Formatters;
using VueDemoZhuyy.Models;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var vueSourceFolder = builder.Configuration.GetSection("Spa:SourcePath").Value;
var vueStaticFolder = builder.Configuration.GetSection("Spa:StaticPath").Value;
var port = builder.Configuration.GetSection("Spa:Port").Value;

builder.Services.AddLogging(log => {
    log.AddFilter("System", LogLevel.Warning);  // 忽略系统的其他日志
    log.AddFilter("Microsoft", LogLevel.Warning);
    log.AddLog4Net("Cfg/log4net.Config");
});


builder.Services.AddSpaStaticFiles(configuration =>
{ configuration.RootPath = vueStaticFolder; });


builder.Services.AddControllersWithViews();

builder.Services.AddSession(options =>
{
    options.Cookie.Name = ".Vue.Session";
    options.Cookie.HttpOnly = true;//设置在浏览器不能通过js获得该cookie的值
});

builder.Services.AddDbContext<UserDBContext>();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
});


var app = builder.Build();
#region AutoMigrate
//Update-Database -Context DBContext
var ApplicationBuilder = app as IApplicationBuilder;
var ServiceProvider = ApplicationBuilder.ApplicationServices;
var ServiceScopeFactory = ServiceProvider.GetService<IServiceScopeFactory>();
using var Scope = ServiceScopeFactory?.CreateScope();
Scope?.ServiceProvider.GetRequiredService<UserDBContext>().Database.Migrate();
#endregion 






// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();


app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
});

if (!app.Environment.IsDevelopment())
{
    app.UseSpaStaticFiles();
}

app.UseSession();//UseSession配置在UseMvc之前

app.UseEndpoints(endpoints =>
{
    //Microsoft.AspNetCore.Mvc.Core
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller}/{action=Index}/{id?}"
    );
});

//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");


app.UseSpa(spa =>
{
    // To learn more about options for serving an Angular SPA from ASP.NET Core,
    // see https://go.microsoft.com/fwlink/?linkid=864501

    spa.Options.SourcePath = vueSourceFolder;

    if (app.Environment.IsDevelopment())
    {
        //spa.UseVueCliServer(npmScript: "start");
        spa.UseProxyToSpaDevelopmentServer($"http://localhost:{port}"); // your Vue app port       
    }
});

app.Run();
