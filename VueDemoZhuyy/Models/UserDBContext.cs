﻿
using Microsoft.EntityFrameworkCore;

namespace VueDemoZhuyy.Models;

//Add-Migration XX
//Update-Database

public class UserDBContext : DbContext
{
    public DbSet<User> User { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=User.db");
        //optionsBuilder.LogTo(Console.WriteLine);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //添加种子数据
        modelBuilder.Entity<User>().HasData(
            new User()
            {
                ID = 1,
                NAME = "admin",
                PASSWORD = "123456",
                State = true,
                Role = Role.Admin ,
                Phone="",
                Mailbox="",
                Remark="管理员账户"
            });
    }
}
