﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VueDemoZhuyy.Models;
public class User
{   
    public int ID { get; set; }
    public string? NAME { get; set; }
    public string? PASSWORD { get; set; }
    [DefaultValue(Role.User)]
    public Role Role { get; set; }
    [DefaultValue(false)]
    public bool State { get; set; }
    public string? Phone { get; set; }
    public string? Mailbox { get; set; }
    public string? Remark { get; set; }
}


public enum Role
{
    User = 0,
    Admin= 1
}


public enum State
{
    [Description("异常")]
    Exception = -1,
    [Description("成功")]
    Success = 1,
    [Description("用户名不存在")]
    UserNone = 0,
    [Description("存在同名用户")]
    SameName = 2,
    /// <summary>
    /// 密码错误
    /// </summary>
    [Description("密码错误")]
    PasswordError = -2,
    /// <summary>
    /// 非管理员不能登录
    /// </summary>
    [Description("非管理员不能登录")]
    LoginNOTAdmin = 3,
    [Description("用户状态审核未通过")]
    LoginUserStatue = 4,
    [Description("未登陆")]
    NOLOGIN = 5,
}
