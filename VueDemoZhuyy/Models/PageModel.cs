﻿
using VueDemoZhuyy.Models;

public class MyPageModel
{
    public int TotalCount { get; set; }

    public int PageSize { get; set; }

    public int CurrentPage { get; set; }

    public int PageCount { get; set; }

    public IQueryable<User> users { get; set; }
}
