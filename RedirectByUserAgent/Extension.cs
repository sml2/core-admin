﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using RedirectByUserAgent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedirectByUserAgent
{
    public static class Extension
    {
        //public static Extension(ILogger<Extension> logger) { 
        //}
        public static void CoverCache(this List<Config> configs, IMemoryCache memoryCache)
        {
            // TODO: 不会清除旧的key
            MemoryCacheEntryOptions memoryCacheEntryOptions = new() {Priority = CacheItemPriority.NeverRemove};
            configs.ForEach(c => memoryCache.Set(c.Path.Trim('/'), c, memoryCacheEntryOptions));
        }
        public static void CoverCache(this List<Domain> domains, IMemoryCache memoryCache)
        {
            MemoryCacheEntryOptions memoryCacheEntryOptions = new() { Priority = CacheItemPriority.NeverRemove };
            domains.ForEach(c => memoryCache.Set($":{c.LowercaseName}", c, memoryCacheEntryOptions));
        }
    }
}