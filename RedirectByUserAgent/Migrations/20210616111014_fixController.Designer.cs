﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RedirectByUserAgent.EF;

namespace RedirectByUserAgent.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20210616111014_fixController")]
    partial class fixController
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("CoreAdmin.Mvc.Models.MenuModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Icon")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("icon");

                    b.Property<int>("Order")
                        .HasColumnType("INTEGER")
                        .HasColumnName("order");

                    b.Property<int>("ParentID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("parent_id");

                    b.Property<string>("Permission")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("permission");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("title");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("Uri")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("uri");

                    b.HasKey("Id");

                    b.ToTable("Menu");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 129, DateTimeKind.Local).AddTicks(2663),
                            Icon = "fa-bar-chart",
                            Order = 1,
                            ParentID = 0,
                            Title = "Dashboard",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(1244),
                            Uri = "Home/Index"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3744),
                            Icon = "fa-tasks",
                            Order = 2,
                            ParentID = 0,
                            Title = "Admin",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3750),
                            Uri = ""
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3805),
                            Icon = "fa-users",
                            Order = 3,
                            ParentID = 2,
                            Title = "Users",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3806),
                            Uri = "users"
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5527),
                            Icon = "fa-user",
                            Order = 4,
                            ParentID = 2,
                            Title = "Roles",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5556),
                            Uri = "roles"
                        },
                        new
                        {
                            Id = 5,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5611),
                            Icon = "fa-ban",
                            Order = 5,
                            ParentID = 2,
                            Title = "Permission",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5612),
                            Uri = "permissions"
                        },
                        new
                        {
                            Id = 6,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5617),
                            Icon = "fa-bars",
                            Order = 6,
                            ParentID = 2,
                            Title = "Menu",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5618),
                            Uri = "menu"
                        },
                        new
                        {
                            Id = 7,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5621),
                            Icon = "fa-history",
                            Order = 7,
                            ParentID = 2,
                            Title = "Operation Log",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5622),
                            Uri = "operationlog"
                        },
                        new
                        {
                            Id = 8,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2222),
                            Icon = "fa-link",
                            Order = 8,
                            ParentID = 0,
                            Title = "接入域名",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2227),
                            Uri = "/domain"
                        },
                        new
                        {
                            Id = 9,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2237),
                            Icon = "fa-500px",
                            Order = 9,
                            ParentID = 0,
                            Title = "跳转配置",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2238),
                            Uri = "/config"
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.OperationLog", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<string>("Body")
                        .HasColumnType("TEXT")
                        .HasColumnName("body")
                        .HasComment("可为空,表示没有请求体");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Host")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("host");

                    b.Property<string>("IP")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("ip");

                    b.Property<string>("Method")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("method");

                    b.Property<string>("Path")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("path");

                    b.Property<string>("Query")
                        .HasColumnType("TEXT")
                        .HasColumnName("query");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER")
                        .HasColumnName("user_id");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("username");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("OperationLog");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Permissions", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("HttpMethod")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("http_method");

                    b.Property<string>("HttpPath")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("http_path");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("slug");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("ID");

                    b.HasIndex("Slug", "Name")
                        .IsUnique();

                    b.ToTable("Permissions");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9121),
                            HttpMethod = "ANY",
                            HttpPath = "/*",
                            Name = "All permission",
                            Slug = "Dashboard",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9375)
                        },
                        new
                        {
                            ID = 2,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9635),
                            HttpMethod = "GET",
                            HttpPath = "/",
                            Name = "Dashboard",
                            Slug = "Admin",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9639)
                        },
                        new
                        {
                            ID = 3,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9644),
                            HttpMethod = "",
                            HttpPath = "/auth/login,/manage/auth/logout",
                            Name = "Login",
                            Slug = "Users",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9645)
                        },
                        new
                        {
                            ID = 4,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9647),
                            HttpMethod = "GET,PUT",
                            HttpPath = "/auth/setting",
                            Name = "User setting",
                            Slug = "Roles",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9648)
                        },
                        new
                        {
                            ID = 5,
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9649),
                            HttpMethod = "",
                            HttpPath = "/auth/roles,/auth/permissions,/auth/menu,/auth/logs",
                            Name = "Auth management",
                            Slug = "Permission",
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9650)
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Roles", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("slug");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex");

                    b.HasIndex("Slug", "Name")
                        .IsUnique();

                    b.ToTable("Roles");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            ConcurrencyStamp = "e958189d-96e8-49ca-b3ce-cec174107ea7",
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Admin",
                            NormalizedName = "ADMIN",
                            Slug = "Admin",
                            UpdatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Avatar")
                        .HasColumnType("TEXT")
                        .HasColumnName("avatar");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("TEXT");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("TEXT");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("INTEGER");

                    b.Property<string>("RememberToken")
                        .HasColumnType("TEXT")
                        .HasColumnName("remember_token");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("TEXT");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("username");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex");

                    b.HasIndex("UserName")
                        .IsUnique();

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AccessFailedCount = 0,
                            Avatar = "/_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg",
                            ConcurrencyStamp = "90e236c5-c4f8-4377-bf0f-e8fe00c7f23e",
                            CreatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(514),
                            Email = "admin@ca.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            Name = "Administrator",
                            PhoneNumberConfirmed = false,
                            TwoFactorEnabled = false,
                            UpdatedAt = new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(518),
                            UserName = "admin"
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserRole", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RoleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("MenuModelRoles", b =>
                {
                    b.Property<int>("MenuId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RolesId")
                        .HasColumnType("INTEGER");

                    b.HasKey("MenuId", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("MenuModelRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimType")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("TEXT");

                    b.Property<int>("RoleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimType")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Value")
                        .HasColumnType("TEXT");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("PermissionsRoles", b =>
                {
                    b.Property<int>("PermissionsID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RolesId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionsID", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("PermissionsRoles");
                });

            modelBuilder.Entity("PermissionsUserModel", b =>
                {
                    b.Property<int>("PermissionsID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("UserModelId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionsID", "UserModelId");

                    b.HasIndex("UserModelId");

                    b.ToTable("PermissionsUserModel");
                });

            modelBuilder.Entity("RedirectByUserAgent.Models.Config", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Android_Address")
                        .HasColumnType("TEXT");

                    b.Property<int>("Android_Count")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .HasColumnType("TEXT");

                    b.Property<string>("IOS_Address")
                        .HasColumnType("TEXT");

                    b.Property<int>("IOS_Count")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Invalid_Count")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<bool>("Open")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Path")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("Configs");
                });

            modelBuilder.Entity("RedirectByUserAgent.Models.Domain", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("Count")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Count_Android")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Count_IOS")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Count_Invalid")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("LastTime0")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime1")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime2")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime3")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime4")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime5")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime6")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime7")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime8")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastTime9")
                        .HasColumnType("TEXT");

                    b.Property<string>("LowercaseName")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT");

                    b.Property<int>("Rate")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("LowercaseName")
                        .IsUnique();

                    b.ToTable("Domains");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserRole", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Roles", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("User");
                });

            modelBuilder.Entity("MenuModelRoles", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.MenuModel", null)
                        .WithMany()
                        .HasForeignKey("MenuId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PermissionsRoles", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Permissions", null)
                        .WithMany()
                        .HasForeignKey("PermissionsID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PermissionsUserModel", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Permissions", null)
                        .WithMany()
                        .HasForeignKey("PermissionsID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Roles", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserModel", b =>
                {
                    b.Navigation("UserRoles");
                });
#pragma warning restore 612, 618
        }
    }
}
