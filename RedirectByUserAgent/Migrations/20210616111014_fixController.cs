﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RedirectByUserAgent.Migrations
{
    public partial class fixController : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 129, DateTimeKind.Local).AddTicks(2663), new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(1244) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3744), new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3750) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3805), new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3806), "users" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5527), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5556), "roles" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5611), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5612), "permissions" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5617), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5618), "menu" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5621), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5622) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2222), new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2227) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2237), new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2238) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9121), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9375) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9635), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9639) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9644), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9645) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9647), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9648) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9649), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9650) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "e958189d-96e8-49ca-b3ce-cec174107ea7");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "90e236c5-c4f8-4377-bf0f-e8fe00c7f23e", new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(514), new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(518) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 129, DateTimeKind.Local).AddTicks(6246), new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(4851) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7378), new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7384) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7392), new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7393), "userser" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(4922), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(4952), "roleser" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5008), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5009), "permissionser" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5015), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5016), "menuer" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5019), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5020) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1059), new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1063) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1073), new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1074) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8043), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8285) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8539), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8543) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8546), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8547) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8549), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8550) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8552), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8553) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3437df14-89e7-4ac2-bef6-59597fb73943");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "13d43edf-9478-4f86-89ab-6801d89e6151", new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(9384), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(9389) });
        }
    }
}
