﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RedirectByUserAgent.Migrations
{
    public partial class RateUI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Rate",
                table: "Domains",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 384, DateTimeKind.Local).AddTicks(3295), new DateTime(2021, 6, 17, 4, 51, 51, 385, DateTimeKind.Local).AddTicks(606) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 385, DateTimeKind.Local).AddTicks(3923), new DateTime(2021, 6, 17, 4, 51, 51, 385, DateTimeKind.Local).AddTicks(3929) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 385, DateTimeKind.Local).AddTicks(3935), new DateTime(2021, 6, 17, 4, 51, 51, 385, DateTimeKind.Local).AddTicks(3936) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5312), new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5331) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5371), new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5371) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5375), new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5375) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5378), new DateTime(2021, 6, 17, 4, 51, 51, 488, DateTimeKind.Local).AddTicks(5379) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 490, DateTimeKind.Local).AddTicks(3159), new DateTime(2021, 6, 17, 4, 51, 51, 490, DateTimeKind.Local).AddTicks(3164) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 490, DateTimeKind.Local).AddTicks(3169), new DateTime(2021, 6, 17, 4, 51, 51, 490, DateTimeKind.Local).AddTicks(3169) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9305), new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9651) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9977), new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9980) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9983), new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9983) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9985), new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9985) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9987), new DateTime(2021, 6, 17, 4, 51, 51, 489, DateTimeKind.Local).AddTicks(9987) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "15d2b410-aa95-4b50-9f20-bead8ac3df04");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "cdfea8e0-046d-414b-9edd-3b72ccc0c4a2", new DateTime(2021, 6, 17, 4, 51, 51, 490, DateTimeKind.Local).AddTicks(956), new DateTime(2021, 6, 17, 4, 51, 51, 490, DateTimeKind.Local).AddTicks(960) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastTime",
                table: "Domains");

            migrationBuilder.AlterColumn<int>(
                name: "Rate",
                table: "Domains",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL");

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 129, DateTimeKind.Local).AddTicks(2663), new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(1244) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3744), new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3750) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3805), new DateTime(2021, 6, 16, 19, 10, 14, 130, DateTimeKind.Local).AddTicks(3806) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5527), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5556) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5611), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5612) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5617), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5618) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5621), new DateTime(2021, 6, 16, 19, 10, 14, 256, DateTimeKind.Local).AddTicks(5622) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2222), new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2227) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2237), new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(2238) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9121), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9375) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9635), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9639) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9644), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9645) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9647), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9648) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9649), new DateTime(2021, 6, 16, 19, 10, 14, 257, DateTimeKind.Local).AddTicks(9650) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "e958189d-96e8-49ca-b3ce-cec174107ea7");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "90e236c5-c4f8-4377-bf0f-e8fe00c7f23e", new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(514), new DateTime(2021, 6, 16, 19, 10, 14, 258, DateTimeKind.Local).AddTicks(518) });
        }
    }
}
