﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RedirectByUserAgent.Migrations
{
    public partial class domainCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Count_Android",
                table: "Domains",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Count_IOS",
                table: "Domains",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Count_Invalid",
                table: "Domains",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime0",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime1",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime2",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime3",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime4",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime5",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime6",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime7",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime8",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTime9",
                table: "Domains",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Rate",
                table: "Domains",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 129, DateTimeKind.Local).AddTicks(6246), new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(4851) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7378), new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7384) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7392), new DateTime(2021, 6, 16, 19, 1, 34, 130, DateTimeKind.Local).AddTicks(7393), "userser" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(4922), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(4952), "roleser" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5008), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5009), "permissionser" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5015), new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5016), "menuer" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "title", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5019), "Operation Log", new DateTime(2021, 6, 16, 19, 1, 34, 256, DateTimeKind.Local).AddTicks(5020), "operationlog" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "icon", "title", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1059), "fa-link", "接入域名", new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1063), "/domain" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9,
                columns: new[] { "created_at", "icon", "title", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1073), "fa-500px", "跳转配置", new DateTime(2021, 6, 16, 19, 1, 34, 258, DateTimeKind.Local).AddTicks(1074), "/config" });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8043), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8285) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8539), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8543) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8546), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8547) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8549), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8550) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8552), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(8553) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3437df14-89e7-4ac2-bef6-59597fb73943");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "13d43edf-9478-4f86-89ab-6801d89e6151", new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(9384), new DateTime(2021, 6, 16, 19, 1, 34, 257, DateTimeKind.Local).AddTicks(9389) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count_Android",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "Count_IOS",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "Count_Invalid",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime0",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime1",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime2",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime3",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime4",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime5",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime6",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime7",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime8",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "LastTime9",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "Rate",
                table: "Domains");

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 967, DateTimeKind.Local).AddTicks(5226), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(4111) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6623), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6629) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6637), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6638), "users" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6640), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6640), "roles" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6642), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6643), "permissions" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6645), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6646), "menu" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "title", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6648), "Operation log", new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6649), "logs" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "icon", "title", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(972), "fa-500px", "跳转配置", new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(976), "/config" });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9,
                columns: new[] { "created_at", "icon", "title", "updated_at", "uri" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(984), "fa-link", "代理域名", new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(985), "/domain" });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(7984), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8229) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8446), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8450) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8452), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8453) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8454), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8455) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8458), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8459) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "f7253f61-5030-422a-909c-6163d02fe532");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "34c9ce1d-62cc-47d3-9eb9-0a760791ca54", new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(9243), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(9248) });
        }
    }
}
