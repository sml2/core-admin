﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RedirectByUserAgent.Migrations
{
    public partial class @int : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Invalid_Count",
                table: "Configs",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IOS_Count",
                table: "Configs",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 727, DateTimeKind.Local).AddTicks(9048), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(6469) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9830), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9835) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9840), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9841) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9843), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9844) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9909), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9910) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9912), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9912) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9914), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9914) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(6255), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(6260) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2289), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2634) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2961), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2965) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2967), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2968) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2969), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2970) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2971), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2972) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "5d8dead2-c3e9-495a-b96f-d060501aeb11");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "be297a24-cc41-418a-bf11-cc20555f5cf5", new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(3778), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(3781) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Invalid_Count",
                table: "Configs",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "IOS_Count",
                table: "Configs",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 477, DateTimeKind.Local).AddTicks(8681), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(5700) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9166), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9171) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9178), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9178) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9180), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9181) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9182), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9183) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9184), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9185) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9186), new DateTime(2021, 6, 10, 3, 6, 33, 478, DateTimeKind.Local).AddTicks(9187) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(5222), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(5226) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(1421), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(1749) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2063), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2067) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2069), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2070) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2071), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2072) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2073), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2074) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "e0f16a22-be12-44d1-8dd7-8934f15ec242");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "e7d9e3af-384b-4fb1-8823-d32e829ccd85", new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2848), new DateTime(2021, 6, 10, 3, 6, 33, 480, DateTimeKind.Local).AddTicks(2852) });
        }
    }
}
