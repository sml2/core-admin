﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RedirectByUserAgent.Migrations
{
    public partial class domain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Domains",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    LowercaseName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Count = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domains", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 967, DateTimeKind.Local).AddTicks(5226), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(4111) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6623), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6629) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6637), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6638) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6640), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6640) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6642), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6643) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6645), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6646) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6648), new DateTime(2021, 6, 16, 11, 53, 12, 968, DateTimeKind.Local).AddTicks(6649) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(972), new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(976) });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 9, new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(984), "fa-link", 9, 0, null, "代理域名", new DateTime(2021, 6, 16, 11, 53, 12, 970, DateTimeKind.Local).AddTicks(985), "/domain" });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(7984), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8229) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8446), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8450) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8452), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8453) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8454), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8455) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8458), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(8459) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "f7253f61-5030-422a-909c-6163d02fe532");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "avatar", "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "/_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg", "34c9ce1d-62cc-47d3-9eb9-0a760791ca54", new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(9243), new DateTime(2021, 6, 16, 11, 53, 12, 969, DateTimeKind.Local).AddTicks(9248) });

            migrationBuilder.CreateIndex(
                name: "IX_Domains_LowercaseName",
                table: "Domains",
                column: "LowercaseName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Domains");

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 9);

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 727, DateTimeKind.Local).AddTicks(9048), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(6469) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9830), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9835) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9840), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9841) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9843), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9844) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9909), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9910) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9912), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9912) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9914), new DateTime(2021, 6, 10, 3, 25, 14, 728, DateTimeKind.Local).AddTicks(9914) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 8,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(6255), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(6260) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2289), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2634) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2961), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2965) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2967), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2968) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2969), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2970) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2971), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(2972) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "5d8dead2-c3e9-495a-b96f-d060501aeb11");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "avatar", "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg", "be297a24-cc41-418a-bf11-cc20555f5cf5", new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(3778), new DateTime(2021, 6, 10, 3, 25, 14, 730, DateTimeKind.Local).AddTicks(3781) });
        }
    }
}
