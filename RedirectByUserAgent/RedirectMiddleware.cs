﻿using System.Collections.Generic;
using System.Linq;
using CoreAdmin.Mvc.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Memory;
using RedirectByUserAgent.Models;
using Microsoft.Extensions.Logging;
using CoreAdmin.EF;
using RedirectByUserAgent.EF;

namespace RedirectByUserAgent.Middleware
{
    internal class RedirectMiddleware
    {
        private readonly RequestDelegate _next;

        public RedirectMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public class DomainDic : Dictionary<string, int>
        {
            
        }
        public async Task InvokeAsync(HttpContext httpContext, IMemoryCache memoryCache,ILogger<RedirectMiddleware> logger, AdminDbContext adminDb)
        {
            var context = adminDb as Context;
            //System.Diagnostics.Debug.WriteLine(context.Configs.Find(1)?.GetHashCode());
            var Host = httpContext.Request.Host.Host;
            var DomainKey = $":{Host.ToLower()}";
            var Domain =  memoryCache.GetOrCreate(DomainKey, (e) =>
            {
                e.Priority = CacheItemPriority.NeverRemove;
                var newDomain = new Domain()
                {
                    Name = Host,
                    Count = 1
                };
                context.Add(newDomain);
                context.SaveChangesAsync().Wait(); 
                return newDomain;
            });
            var State = context.Attach(Domain);
            State.State = Microsoft.EntityFrameworkCore.EntityState.Unchanged;
            var reason = "NotFindConfig";
            var Path = httpContext.Request.Path.ToString().Trim('/');
            if (memoryCache.TryGetValue(Path, out Config config))
            {
                context.Attach(config);
                var ua = httpContext.Request.Headers["User-Agent"];
                if (ua.ToString().ToLower().Contains("android"))
                {
                    httpContext.Response.Redirect(config.Android_Address);
                    logger.LogInformation($"Jump To Android[{config.ID} - {config.Name}]:{System.Environment.NewLine}{Path}");
                    config.Android_Count += 1;
                    Domain.Visit(Domain.TYPES.ANDROID,State);
                    await context.SaveChangesAsync();
                    return;
                }
                else if (ua.ToString().ToLower().ContainsOne("ios", "iphone", "ipad", "mac"))
                {
                    httpContext.Response.Redirect(config.IOS_Address);
                    logger.LogInformation($"Jump To IOS[{config.ID} - {config.Name}]:{System.Environment.NewLine}{Path}");
                    config.IOS_Count += 1;
                    Domain.Visit(Domain.TYPES.IOS,State);
                    await context.SaveChangesAsync();
                    return;
                }
                else
                {
                    reason = $"UserAgent({ua}) Can Not Analysis Platform";
                    config.Invalid_Count += 1;
                }
            }
            Domain.Visit(Domain.TYPES.INVALID,State);
            await context.SaveChangesAsync();
            logger.LogInformation($"Next[{reason}]:{System.Environment.NewLine}{Path}");
            await _next(httpContext);
        }

    }
    public static class StringExtensions
    {
        public static bool ContainsOne(this string str,params string[] vs)
        {
            return vs.Any(str.Contains);
        }
    }
    public static class AuthenticateMiddlewareExtensions
    {
        public static IApplicationBuilder UseRedirect(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RedirectMiddleware>();
        }
    }
}
