﻿
using System;
using CoreAdmin.EF;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using RedirectByUserAgent.Models;

namespace RedirectByUserAgent.EF
{
    public class Context : AdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -OutputDir Migrations [name]
        //回滚迁移
        //Remove-Migration
        //执行迁移 没有数据库会自动创建
        //Update-Database
        //删库
        //Drop-Database
        //生成SQL脚本，来手动迁移
        //Script-Migration
        //获取帮助
        //get-help entityframework


        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source=CoreAdmin.db");

        public DbSet<Config> Configs { get; set; }
        public DbSet<Domain> Domains { get; set; }
        public Context(DbContextOptions<Context> options) : base(options)
        {
            Initial();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MenuModel>().HasData(
                    new MenuModel { Id = 8, ParentID = 0, Title = "接入域名", Uri = "/domain", Icon = "fa-link", Order = 8},
                    new MenuModel { Id = 9, ParentID = 0, Title = "跳转配置", Uri = "/config", Icon = "fa-500px", Order = 9 }
            );
        }
    }
}
