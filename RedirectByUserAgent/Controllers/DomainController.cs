﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.Extensions.Caching.Memory;
using RedirectByUserAgent.EF;
using RedirectByUserAgent.Middleware;
using RedirectByUserAgent.Models;

namespace RedirectByUserAgent.Controllers
{
    public class DomainController : AdminController<Domain>
    {
        private Context Context => DbContext as Context;

        public DomainController()
        {
            Title = "已有域名";
        }
        
        [HttpPost]
        public override Task<IActionResult> Post(Domain model)
        {
            return Form().Store(model);
        }

        protected override BGrid Grid()
        {
            var model = Context.Domains;
            var grid = new BGrid(model, HttpContext);
            grid.Column(nameof(Domain.Id)).Sortable();
            grid.Column(nameof(Domain.LowercaseName)).Copyable();
            grid.Column(nameof(Domain.Count_Android));
            grid.Column(nameof(Domain.Count_IOS));
            grid.Column(nameof(Domain.Count_Invalid));
            grid.Column(nameof(Domain.RateUI));

            grid.Actions(action =>
            {
                action.DisableEdit();
                action.DisableView();
                action.DisableDelete();
            });
            grid.DisableExport();
            grid.DisableCreateButton();
            return grid;
        }
        
        protected override BForm Form()
        {
            var model = Context.Domains;
            var form = new BForm(model).WithController(this);
            return form;
        }
        
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, Domain model)
        {
            var test = await Context.Domains.SingleAsync(m => m.Id == id);
            test.Name = model.Name;
            return await Form().Update(test);
        }
        
        protected override BShow Detail(int id)
        {
            var model = Context.Domains;
            var show = new BShow(model.Single(m => m.Id == id));

            return show;
        }
    }
}