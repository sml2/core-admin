﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.Extensions.Caching.Memory;
using RedirectByUserAgent.EF;
using ConfigModel = RedirectByUserAgent.Models.Config;
using System;

namespace RedirectByUserAgent.Controllers
{
    public class ConfigController : AdminController<ConfigModel>
    {
        private readonly IMemoryCache _memoryCache;
        private Context Context => DbContext as Context;

        public ConfigController(IMemoryCache memoryCache) : base()
        {
            _memoryCache = memoryCache;
            Title = "配置跳转规则";
        }

        /*  列表入口 */
        [HttpPost]
        public override Task<IActionResult> Post(ConfigModel model)
        {
            return Form().Store(model);
        }

        class Btn : CoreAdmin.Mvc.Widgets.IRenderable
        {
            public string Render()
            {
                return "<button>应用当前配置<button>";
            }
        }

        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Configs;
            var grid = new BGrid(model, HttpContext);
            var a = Utils;

            grid.Column(nameof(ConfigModel.ID)).Sortable();
            grid.Column(nameof(ConfigModel.Name));
            grid.Column(nameof(ConfigModel.Path)).Copyable();
            grid.Column(nameof(ConfigModel.Open));//.Switch();
            grid.Column(nameof(ConfigModel.Android_Address)).Editable().Copyable();
            grid.Column(nameof(ConfigModel.Android_Count));
            grid.Column(nameof(ConfigModel.IOS_Address)).Editable().Copyable();
            grid.Column(nameof(ConfigModel.IOS_Count));
            grid.Column(nameof(ConfigModel.Invalid_Count));

            //grid.Tools((tools) => tools.());
            grid.Actions(t => t.DisableView());
            grid.DisableExport();
            return grid;
        }

        /*  创建和编辑UI */
        protected override BForm Form()
        {
            var model = Context.Configs;
            var form = new BForm(model).WithController(this);
            var Rand = new Random(GetHashCode());
            string r() => "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz".Substring(Rand.Next(62), 1);
            form.Text(nameof(ConfigModel.Name));
            form.Text(nameof(ConfigModel.Path)).Default($"{r()}{r()}{r()}{r()}{r()}{r()}{r()}{r()}");
            form.Switch(nameof(ConfigModel.Open)).DefaultOn();
            form.Furl(nameof(ConfigModel.Android_Address)).Default("http://");
            form.Furl(nameof(ConfigModel.IOS_Address)).Default("http://");
            form.Textarea(nameof(ConfigModel.Description));
            form.Tools(t => t.DisableView());
            form.Saved(() =>
            {
                Context.Configs.AsNoTracking().ToList().CoverCache(_memoryCache);
                return null;
            });

            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, ConfigModel model)
        {
            var test = await Context.Configs.SingleAsync(m => m.ID == id);
            test.Name = model.Name;
            test.Path = model.Path;
            test.Open = model.Open;
            test.Android_Address = model.Android_Address;
            test.IOS_Address = model.IOS_Address;
            test.Description = model.Description;
            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Configs;
            var show = new BShow(model.Single(m => m.ID == id));

            show.Field(nameof(ConfigModel.ID));

            show.Field(nameof(ConfigModel.Name));
            show.Field(nameof(ConfigModel.Path));
            show.Field(nameof(ConfigModel.Open));
            show.Field(nameof(ConfigModel.Android_Address));
            show.Field(nameof(ConfigModel.Android_Count));
            show.Field(nameof(ConfigModel.IOS_Address));
            show.Field(nameof(ConfigModel.IOS_Count));
            show.Field(nameof(ConfigModel.Description));
            show.Field(nameof(ConfigModel.Invalid_Count));
            show.Field(nameof(ConfigModel.CreatedAt));
            show.Field(nameof(ConfigModel.UpdatedAt));

            return show;
        }
    }
}