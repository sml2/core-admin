﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace RedirectByUserAgent.Models
{
    [Index(nameof(LowercaseName), IsUnique = true)]
    public class Domain : BaseModel
    {
        public int Id { get; set; }

        private string _name;


        [Display(Name = "域名")]
        [MaxLength(byte.MaxValue)]
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                LowercaseName = value.ToLower();
            }
        }
        [Display(Name = "域名")]
        [MaxLength(byte.MaxValue)] public string LowercaseName { get; private set; }
        public DateTime LastTime { get; set; }
        public DateTime LastTime0 { get; set; }
        public DateTime LastTime1 { get; set; }
        public DateTime LastTime2 { get; set; }
        public DateTime LastTime3 { get; set; }
        public DateTime LastTime4 { get; set; }
        public DateTime LastTime5 { get; set; }
        public DateTime LastTime6 { get; set; }
        public DateTime LastTime7 { get; set; }
        public DateTime LastTime8 { get; set; }
        public DateTime LastTime9 { get; set; }
        [NotMapped]
        public int Pos => Count % 10;
        [NotMapped]
        public DateTime LastTimeLooper
        {
            get => Pos switch
            {
                0 => LastTime0,
                1 => LastTime1,
                2 => LastTime2,
                3 => LastTime3,
                4 => LastTime4,
                5 => LastTime5,
                6 => LastTime6,
                7 => LastTime7,
                8 => LastTime8,
                9 => LastTime9,
                _ => throw new ArgumentOutOfRangeException(nameof(Pos))
            };
            set
            {
                switch (Pos)
                {
                    case 0: LastTime0 = value; break;
                    case 1: LastTime1 = value; break;
                    case 2: LastTime2 = value; break;
                    case 3: LastTime3 = value; break;
                    case 4: LastTime4 = value; break;
                    case 5: LastTime5 = value; break;
                    case 6: LastTime6 = value; break;
                    case 7: LastTime7 = value; break;
                    case 8: LastTime8 = value; break;
                    case 9: LastTime9 = value; break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(LastTimeLooper));
                };
            }
        }
        public enum TYPES
        {
            ANDROID, IOS, INVALID
        }
        public void Visit(TYPES type, EntityEntry<Domain> state)
        {
            switch (type)
            {
                case TYPES.ANDROID: Count_Android++; state.Property(nameof(Count_Android)).IsModified = true; break;
                case TYPES.IOS: Count_IOS++; state.Property(nameof(Count_IOS)).IsModified = true; break;
                case TYPES.INVALID: Count_Invalid++; state.Property(nameof(Count_Invalid)).IsModified = true; break;
                default: throw new ArgumentOutOfRangeException(nameof(type));
            }
            if (type != TYPES.INVALID)
            {
                var now = DateTime.Now;
                LastTimeLooper = now;
                state.Property($"{nameof(LastTime)}{Pos}").IsModified = true;
                Count++;
                state.Property(nameof(Count)).IsModified = true;
                if (Count > 10)
                {
                    var interval = (now - LastTimeLooper).TotalMilliseconds / 10;
                    Rate = interval == 0 ? double.PositiveInfinity : 60000 / interval;
                    state.Property(nameof(Rate)).IsModified = true;
                }
                LastTime = now;
                state.Property(nameof(LastTime)).IsModified = true;
            }
        }
        [Display(Name = "Android访问量")]
        public int Count_Android { get; set; }
        [Display(Name = "IOS访问量")]
        public int Count_IOS { get; set; }
        [Display(Name = "无效访问量")]
        public int Count_Invalid { get; set; }
        [Display(Name = "总有效访问量")]
        public int Count { get; set; }
        /// <summary>
        /// 每分钟的访问量(根据最近十次访问平均值求得)
        /// </summary>
        [Display(Name = "每分钟访问量")]
        public double Rate { get; set; }
        [Display(Name = "每分钟访问量")]
        public string RateUI
        {
            get
            {
                if ((DateTime.Now - LastTime).TotalMinutes > 1)
                {
                    return $"一分钟内无访问";
                }
                else if (Count < 10)
                {
                    return $"小于{Count}次";
                }
                else
                {
                    return $"约{Rate:00.00}次";
                }
            }
        }
    }
}