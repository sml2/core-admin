using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using test_xqj.Data;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<test_xqjContext>(options =>

    options.UseSqlite("Data Source=Data/test_xqj.db"));


// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
