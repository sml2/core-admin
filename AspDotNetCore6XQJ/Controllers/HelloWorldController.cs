﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace AspDotNetCore6XQJ.Controllers;
[Route("")]
[Route("xxx")]
[Route("yyy")]
[ApiController]
public class HelloWorldController : Controller
{
    private readonly EndpointDataSource _endpointDataSource;
    public HelloWorldController(EndpointDataSource endpointDataSource) {
        _endpointDataSource = endpointDataSource;
    }
    // 
    // GET: /HelloWorld/
    [Route("I")]
    public string Index()
    {
        return "This is my default action...";
    }
    [HttpGet("[action]")]
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IActionResult TestView() {
        return View();
    }
    [HttpGet("[action]")]
    public async void TestRoute() {
        foreach (var item in _endpointDataSource.Endpoints)
        {
            await Response.WriteAsync($"{item.DisplayName}\n");
        }
    }
    // 
    // GET: /HelloWorld/Welcome/ 
    // Requires using System.Text.Encodings.Web;
    public string Welcome(string name, int ID = 1)
    {
        return HtmlEncoder.Default.Encode($"Hello {name}, ID: {ID}");
    }
}
