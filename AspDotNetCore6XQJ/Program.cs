
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews(o => o.EnableEndpointRouting = false);//

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}

app.UseStaticFiles();

app.UseRouting();

//app.UseMvcWithDefaultRoute();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
RequestDelegate TestFun = async c => await c.Response.BodyWriter.WriteAsync(System.Text.Encoding.UTF8.GetBytes("HelloWorld"));
app.UseEndpoints(IERB =>
{
    IERB.MapGet("TestUseEndpoints", TestFun);
    IERB.MapPost("TestUseEndpointsPost", TestFun);
});
app.MapGet("Route", async (c) => {
    var v = c.RequestServices.GetService<EndpointDataSource>();
    foreach (var item in v.Endpoints)
    {
        if (item is RouteEndpoint one)
        {
            await c.Response.WriteAsync($"{one.RoutePattern.RawText} {item.DisplayName}\n");
        }
    }
});
app.MapGet("TestMapGet", TestFun);
app.MapGet("TestMapGetTask", (c) =>
{
    c.Response.BodyWriter.WriteAsync(System.Text.Encoding.UTF8.GetBytes("HelloWorld"));
    return Task.CompletedTask;
});
app.Run();
