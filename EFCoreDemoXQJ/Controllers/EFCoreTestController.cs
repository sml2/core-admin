﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFCoreDemoXQJ.Data;
using EFCoreDemoXQJ.Models;

namespace EFCoreDemoXQJ.Controllers
{
    public class EFCoreTestController : Controller
    {
        private readonly EFCoreDemoXQJContext _context;

        public EFCoreTestController(EFCoreDemoXQJContext context)
        {
            _context = context;
        }

        // GET: EFCoreTest
        public async Task<IActionResult> Index()
        {
            return View(await _context.EFCoreTestModel.ToListAsync());
        }

        // GET: EFCoreTest/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eFCoreTestModel = await _context.EFCoreTestModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (eFCoreTestModel == null)
            {
                return NotFound();
            }

            return View(eFCoreTestModel);
        }

        // GET: EFCoreTest/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EFCoreTest/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,ReleaseDate,Genre,Price")] EFCoreTestModel eFCoreTestModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(eFCoreTestModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(eFCoreTestModel);
        }

        // GET: EFCoreTest/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eFCoreTestModel = await _context.EFCoreTestModel.FindAsync(id);
            if (eFCoreTestModel == null)
            {
                return NotFound();
            }
            return View(eFCoreTestModel);
        }

        // POST: EFCoreTest/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,ReleaseDate,Genre,Price")] EFCoreTestModel eFCoreTestModel)
        {
            if (id != eFCoreTestModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(eFCoreTestModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EFCoreTestModelExists(eFCoreTestModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(eFCoreTestModel);
        }

        // GET: EFCoreTest/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eFCoreTestModel = await _context.EFCoreTestModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (eFCoreTestModel == null)
            {
                return NotFound();
            }

            return View(eFCoreTestModel);
        }

        // POST: EFCoreTest/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var eFCoreTestModel = await _context.EFCoreTestModel.FindAsync(id);
            _context.EFCoreTestModel.Remove(eFCoreTestModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EFCoreTestModelExists(int id)
        {
            return _context.EFCoreTestModel.Any(e => e.Id == id);
        }
    }
}
