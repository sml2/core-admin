﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EFCoreDemoXQJ.Models;

namespace EFCoreDemoXQJ.Data
{
    public class EFCoreDemoXQJContext : DbContext
    {
        public EFCoreDemoXQJContext (DbContextOptions<EFCoreDemoXQJContext> options)
            : base(options)
        {
        }

        public DbSet<EFCoreDemoXQJ.Models.EFCoreTestModel> EFCoreTestModel { get; set; }
    }
}
