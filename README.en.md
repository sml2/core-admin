# CoreAdmin

#### Description
LaravelAdmin In Core(Net6)

#### Software Architecture
- [AspCore MVC](https://docs.microsoft.com/aspnet/core)
- [AdminLTE](https://almsaeedstudio.com/)
- [bootstrap](https://getbootstrap.com/)
- [bootstrap-colorpicker](https://itsjavi.com/bootstrap-colorpicker/)
- [bootstrap-fileinput](https://github.com/kartik-v/bootstrap-fileinput)
- [bootstrap-iconpicker](https://github.com/victor-valencia/bootstrap-iconpicker)
- [bootstrap-input-spinner](https://github.com/shaack/bootstrap-input-spinner)
- [bootstrap-datetimepicker](https://github.com/Eonasdan/bootstrap-datetimepicker/)
- [bootstrap-duallistbox](http://www.virtuosoft.eu/code/bootstrap-duallistbox/)
- [bootstrap4-toggle](https://gitbrent.github.io/bootstrap4-toggle/)
- [Font awesome](https://fontawesome.com/)
- [icheck-bootstrap](https://github.com/bantikyan/icheck-bootstrap)
- [Inputmask](https://github.com/RobinHerbots/Inputmask)
- [ion.rangeSlider](http://ionden.com/a/plugins/ion.rangeSlider)
- [jQuery](https://jquery.com/)
- [Pjax](https://github.com/defunkt/jquery-pjax)
- [jQuery initialize](https://github.com/pie6k/jquery.initialize)
- [moment](http://momentjs.com/)
- [Nestable](http://dbushell.github.io/Nestable/)
- [Nprogress](http://ricostacruz.com/nprogress)
- [Select2](https://github.com/select2/select2)
- [Sweetalert2](https://sweetalert2.github.io/)
#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
