﻿//// See https://aka.ms/new-console-template for more information
//using System.Runtime.InteropServices;
//using System.Threading.Channels;

using Client;
using Grpc.Core;
using Grpc.Net.Client;
using System.Diagnostics;

class Program
{
    static async Task Main(string[] arg)
    {
        try
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);
            
            do
            {

                //var mailboxName = GetMailboxName(arg);
                bool complete = false;
                var cts = new CancellationTokenSource();
                using var call=client.StreamSayHello();

                HelloReply reply = null;
                var readTask = Task.Run(async () =>
                {
                    await foreach (var message in call.ResponseStream.ReadAllAsync())
                    {
                        reply = message;
                        Console.WriteLine($"Read Message:{message}");
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.ResetColor();
                       // call.RequestStream.WriteAsync(new HelloRequest { Name = $"Name" });
                    }
                });
                var sw = Stopwatch.StartNew();
                var sent = 0;

                var reportTask = Task.Run(async () =>
                {
                    while (!readTask.IsCompleted)
                    {
                        //CancellationToken cancellationToken = CancellationToken;
                        await call.RequestStream.WriteAsync(new HelloRequest { Name = $"Name{sent}" });
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"Writ Message: {sent}");
                        Console.ResetColor();
                        if (!complete)
                        {
                            await Task.Delay(TimeSpan.FromSeconds(1));
                            Console.SetCursorPosition(0, Console.CursorTop - 2);
                        }
                        else
                        {
                            break;
                        }
                    }
                });


                // Finish call and report results
                
                await readTask;

                complete = true;
                await reportTask;
                await call.RequestStream.CompleteAsync();
                //var response = await client.SayHelloAsync(new HelloRequest { Name = "World" });
                //Console.WriteLine("send over");
                //Console.ForegroundColor = ConsoleColor.Yellow;
                //Console.Write("Rev:");
                //Console.ForegroundColor = ConsoleColor.Green;
                //Console.WriteLine(response.Message);
                //Console.ResetColor();
                //Console.ReadKey();
            } while (true);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            Console.WriteLine(e.Message);
        }
    }

    private static string GetMailboxName(string[] args)
    {
        if (args.Length < 1)
        {
            Console.WriteLine("No mailbox name provided. Using default name. Usage: dotnet run <name>.");
            return "DefaultMailbox";
        }

        return args[0];
    }
}
