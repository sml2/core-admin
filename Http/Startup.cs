using Http.DynamicRoute;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
//using Newtonsoft.Json.Converters;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace Http
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.


        [Obsolete]
        public void ConfigureServices(IServiceCollection services)
        {
          // services.AddControllersWithViews()
          //     .AddNewtonsoftJson(options =>
          //     {
          //         //不使用驼峰样式的key
          //         //options.SerializerSettings.ContractResolver = new DefaultContractResolver();
          //        
          //     });
   
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddRazorPages();
            services.AddSingleton<TranslationTransformer>();
            services.AddSingleton<TranslationDatabase>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDynamicControllerRoute<TranslationTransformer>("{language}/{controller}/{action}");

                endpoints.MapGet("TestGet", (c) => {
                    var w = new System.IO.StreamWriter(c.Response.Body);
                    return w.WriteAsync("HelloWorld");
                });
                // endpoints.MapGet("TestGet", async (c) => await c.Response.BodyWriter.WriteAsync(System.Text.Encoding.UTF8.GetBytes("HelloWorld")));
                // endpoints.MapControllerRoute(
                //     name: "admin",
                //     pattern: !string.IsNullOrEmpty(options.Value.Route.Prefix) ? options.Value.Route.Prefix + "/" : string.Empty + "{controller=Home}/{action=Index}/{id?}");
                //endpoints.MapDynamicControllerRoute<>();
                 //endpoints.MapPost("",new Controllers.Report.ApiController().OnPostAsync());
                 endpoints.MapControllerRoute(
                     name: "default",
                     pattern: "{controller=Home}/{action=Privacy}/{id?}");

            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });

        }
    }
}
