﻿using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Http.DynamicRoute
{
    public class TranslationDatabase
    {
   

        private static Dictionary<string, Dictionary<string, string>> Translations = new Dictionary<string, Dictionary<string, string>>
    {
        {
            "en", new Dictionary<string, string>
            {
                { "orders", "HttpRoute" },
                { "list", "ActionIndex" }
            }
        },
        {
            "de", new Dictionary<string, string>
            {
                { "bestellUngen", "HttpRoute" },
                { "lisTe", "ActionIndex" }
            }
        },
        {
            "pl", new Dictionary<string, string>
            {
                { "zamowienia", "HttpRoute" },
                { "lista", "ActionIndex" }
            }
        },
    };

        public async Task<string> Resolve(string lang, string value)
        {
            if (items == null)
            {
                items = new Dictionary<string, Dictionary<string, string>>();
                foreach (var item in Translations)
                {

                    var key = item.Key.ToLowerInvariant();

                    Dictionary<string, string> values = new Dictionary<string, string>();
                    foreach (var itemvalue in item.Value)
                    {
                        var newkey = itemvalue.Key.ToLowerInvariant();
                        values.Add(newkey, itemvalue.Value);
                    }
                    items.Add(key, values);
                }
            }
            var normalizedLang = lang.ToLowerInvariant();
            var normalizedValue = value.ToLowerInvariant();
            
           if (items.TryGetValue(normalizedLang,out var lange) && lange.TryGetValue(normalizedValue,out var dicvalue))
            {
                return dicvalue;
            }

            return null;
        }

        private static Dictionary<string, Dictionary<string, string>> items = null;
    }
}
