# CoreAdmin

#### 介绍
LaravelAdmin In Core(Net6)

#### 软件架构

- [AspCore MVC](https://docs.microsoft.com/zh-cn/aspnet/core/)
- [AdminLTE](https://almsaeedstudio.com/)
- [bootstrap](https://getbootstrap.com/)
- [bootstrap-colorpicker](https://itsjavi.com/bootstrap-colorpicker/)
- [bootstrap-fileinput](https://github.com/kartik-v/bootstrap-fileinput)
- [bootstrap-iconpicker](https://github.com/victor-valencia/bootstrap-iconpicker)
- [bootstrap-input-spinner](https://github.com/shaack/bootstrap-input-spinner)
- [bootstrap-datetimepicker](https://github.com/Eonasdan/bootstrap-datetimepicker/)
- [bootstrap-duallistbox](http://www.virtuosoft.eu/code/bootstrap-duallistbox/)
- [bootstrap4-toggle](https://gitbrent.github.io/bootstrap4-toggle/)
- [Font awesome](https://fontawesome.com/)
- [icheck-bootstrap](https://github.com/bantikyan/icheck-bootstrap)
- [Inputmask](https://github.com/RobinHerbots/Inputmask)
- [ion.rangeSlider](http://ionden.com/a/plugins/ion.rangeSlider)
- [jQuery](https://jquery.com/)
- [Pjax](https://github.com/defunkt/jquery-pjax)
- [jQuery initialize](https://github.com/pie6k/jquery.initialize)
- [moment](http://momentjs.com/)
- [Nestable](http://dbushell.github.io/Nestable/)
- [Nprogress](http://ricostacruz.com/nprogress)
- [Select2](https://github.com/select2/select2)
- [Sweetalert2](https://sweetalert2.github.io/)

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
