import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import login from './views/Home/'

export default new VueRouter({
    // 配置路由信息
    routes: [
        {
            path: '/',        //如果只写 / 则说明默认打开一个页面
            redirect: '/Login'//默认指向页面
        },
        {
            path: '/Register',
            component: Home
        }
    ]
})