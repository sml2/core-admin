﻿$(function () {
	var bar = $('.bar');
	var percent = $('.percent');
	var showimg = $('#showimg');
	var progress = $(".progress");
	var files = $(".files");
	var btn = $(".adbtn span");
	$("#startsubmit").click(function(){
		var reg = /^[\u4E00-\u9FA5]$/; 
		if(!reg.test($("#myfont").attr("value"))){   alert('请输入有且只有一个汉字' );  showimg.html('请输入有且只有一个汉字'); return; }
		if($("#fileupload").attr("value")==""){ alert('请选择要上传的图片文件' );  showimg.html('请选择要上传的图片文件'); return; }
		$("#myupload").ajaxSubmit({
			dataType:  'json',
			beforeSend: function() {
        		showimg.empty();
				progress.show();
        		var percentVal = '0%';
        		bar.width(percentVal);
        		percent.html(percentVal);
				btn.html("上传中...");
    		},
    		uploadProgress: function(event, position, total, percentComplete) {
        		var percentVal = percentComplete + '%';
        		bar.width(percentVal)
        		percent.html(percentVal);
    		},
			/*complete: function(xhr) {
				$(".files").html(xhr.responseText);
			},*/
			success: function(data) {
				files.html("<b>"+data.name+"("+data.size+"k)</b> <span class='delimg' rel='"+data.font+"/"+data.pic+"'>删除</span>");
				var img = "files/"+data.font+"/"+data.pic+"?"+Math.random();
				var showhtml = "<img src='"+img+"'>";
				showhtml += "<br/>" ;
				showhtml += "文字：" + data.font ;				
				showimg.html(showhtml);
				btn.html("添加附件");
			},
			error:function(xhr){
				btn.html("上传失败");
				bar.width('0')
				files.html(xhr.responseText);
			},
			clearForm: true   
		});
	});
	
	$(".delimg").live('click',function(){
		var pic = $(this).attr("rel");
		$.post("action.php?act=delimg",{imagename:pic},function(msg){
			if(msg==1){
				files.html("删除成功.");
				showimg.empty();
				progress.hide();
			}else{
				files.html("删除失败.");
				alert(msg);
			}
		});
	});
	
	
});