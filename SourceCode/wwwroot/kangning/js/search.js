﻿$(function () {
	$("#search").live('click',function(){
		var reg = /^[\u4E00-\u9FA5]$/; 
		var font = $("#text").attr("value");		
		if(!reg.test(font)){   alert('请输入有且只有一个汉字' );  $("#screen").html('请输入有且只有一个汉字'); return; }
		$("#screen").html('查询中...');
		$.post("action.php?act=searchimg&font="+encodeURI(font),"",function(msg){
			var rowhtml="";
			var json = eval(msg);
			for(var i=0;i<json.length;i++){
				var img = "files/"+font+"/"+json[i]+"?"+Math.random();
				rowhtml+='<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">';
				rowhtml+='<div class="thumbnail">';
				rowhtml+='<a href="javascript:void(0)"><img class="imgshow" src="'+img+'" data-toggle="tooltip" data-placement="top" title="" data-original-title="某"></a>';
				rowhtml+='	<div class="caption" rel="'+ font+'/'+json[i]+'">';
				rowhtml+='		<p class="text-center"><a class="btn btn-success AA" role="button">删除字体...</a></p>';									
				rowhtml+='	</div>';						
				rowhtml+='</div>';
				rowhtml+='</div>';
			}
			if(rowhtml==""){rowhtml="未发现该字，请上传...";}
			$("#screen").html(rowhtml);
		});
	});
	$(".AA").live('click',function(){
		var cap = $(this);
		if(cap[0].innerHTML=="删除字体..."){
			cap[0].innerHTML="删除字体..";
		}else if(cap[0].innerHTML=="删除字体.."){
			cap[0].innerHTML="删除字体.";
		}else if(cap[0].innerHTML=="删除字体."){
			cap[0].innerHTML="删除字体";
		}else if(cap[0].innerHTML=="删除字体"){
			var pic = cap.parent().parent().attr("rel");
			$.post("action.php?act=delimg",{imagename:pic},function(msg){
				if(msg==1){
					cap.parent().parent().parent().parent().remove();				
					//cap.empty();
				}else{
					alert(msg);
				}
			});
		}
	});
	$(".imgshow").live('click',function(){	
		$('#showimg').html("<img src='"+$(this).attr("src")+"' style='margin:0 10% 50px 10%;width: 80%;' >");
	});
});