﻿"use strict";
let state = 0;//0 username 1 password 2normal
var term = null;
$.terminal.prism_formatters.prompt = true;
jQuery(function ($, undefined) {
    // ref: https://stackoverflow.com/q/67322922/387194
    var __EVAL = (s) => eval(`void (__EVAL = ${__EVAL}); ${s}`);
    // something is making blur on terminal on click
    $(document).on('click', '.terminal', function (e) {
        e.stopPropagation();
    });
    // global to access from js terminal
    function js_formatter(string, options) {
        return $.terminal.prism("javascript", string, options);
    }
    // hack that should be fixed by https://github.com/jcubic/jquery.terminal/issues/552
    // it work because terminal is never resized. It will break (highlight comments
    // if you type into both terminals, click js terminal and make window very narrow,
    // then it will apply javascript formatting to comments), but it work most of the time.
    function onFocus() {
        var defaults = $.terminal.defaults;
        if (this.settings().name === 'term') {
            if (defaults.formatters.indexOf(js_formatter) === -1) {
                defaults.formatters.unshift(js_formatter);
            }
        } else {
            var pos = defaults.formatters.indexOf(js_formatter);
            if (pos !== -1) {
                defaults.formatters.splice(pos, 1);
            }
        }
    }
    term = $('#term').terminal(function (command, term) {
        switch (state) {
            case 0:
                if (command !== '') {
                    connection.invoke("LoginCheckUser", command).catch((err) => term.error(err));
                } else {
                    term.echo("[[;yellow;]Username can not be none,please enter again...]");
                }
                break;
            case 1:
                if (command !== '') {
                    connection.invoke("LoginCheckPassword", command).catch((err) => term.error(err));
                } else {
                    term.echo("[[;yellow;]Password can not be none,please enter again...]");
                }
                break;
            default:
                if (command !== '') {
                    try {
                        connection.invoke("Grpc", command).catch((err) => term.error(err));

                        //var result = __EVAL(command);
                        //if (result !== undefined) {
                        //    if (result instanceof $.fn.init) {
                        //        this.echo('#<jQuery>', { formatters: false });
                        //    } else {
                        //        this.echo(new String(result), { formatters: false });
                        //    }
                        //}
                    } catch (e) {
                        term.error(new String(e));
                    }
                }
        }
    }, {
        greetings: greetings,
        name: 'term',
        onFocus: onFocus,
        enabled: false,
        prompt: 'wait conetting >>>'
    });
});