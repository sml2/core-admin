﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/cmd").build();

connection.on("Password", (message) => { term.echo(message); term.set_prompt("Password>"); state = 1 });
connection.on("LoginSuccess", (message, username) => { term.echo(`[[;Green;]${message}]`); term.set_prompt(`[[;Yellow;]${username}>]`); state = 2 });
connection.on("Message", (message) => term.echo(message));
connection.on("Error", (message) => term.error(message));
connection.on("ReceiveMessage", function (user, message) {
    //var li = document.createElement("li");
    //document.getElementById("messagesList").appendChild(li);
    // We can assign user-supplied strings to an element's textContent because it
    // is not interpreted as markup. If you're assigning in any other way, you 
    // should be aware of possible script injection concerns.
    //li.textContent = `${user} says ${message}`;
});
connection.on("setPrompt", (prompt) => term.set_prompt(prompt));
connection.start().then(function () {
    connection.invoke("GetUserInputPrompt").catch((err) => term.error(err));
    term.echo("[[;green;]Connected Success!]\nPlease enter your username...");
    term.enable()
    //document.getElementById("sendButton").disabled = false;
}).catch((err) => term.error(err));

//document.getElementById("sendButton").addEventListener("click", function (event) {
//    //var user = document.getElementById("userInput").value;
//    //var message = document.getElementById("messageInput").value;
//    //connection.invoke("SendMessage", user, message).catch(function (err) {
//    //    return console.error(err.toString());
//    //});
//    //event.preventDefault();
//});