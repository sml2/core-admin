﻿using Google.Protobuf;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace SML.Hubs;

public class CMD : Hub
{
    public async Task GetUserInputPrompt() => await Clients.Caller.SendAsync("setPrompt", "Username>");
    public async Task LoginCheckUser(string username)
    {
        if (username.Equals("admin"))
        {
            await Clients.Caller.SendAsync("Password", $"Please Enter password of {username}");
        }
        else
        {
            await Clients.Caller.SendAsync("Message","User Not Exist");

        }
    }
    public async Task LoginCheckPassword(string password)
    {
        var u = "admin";
        var p = "admin";
        if (password.Equals(p))
        {
            await Clients.Caller.SendAsync("LoginSuccess", $"Super administrator {u} has logged in successfully", u);
        }
        else
        {
            await Clients.Caller.SendAsync("Error", "The password is wrong");

        }
    }
    public async Task<string> Grpc(string message)
    {
        await Clients.All.SendAsync("Message", message);
        Console.WriteLine(message);
         return  message;
    }



    public async Task SendMessage(string user, string message)
    {
        await Clients.All.SendAsync("ReceiveMessage", user, message);
    }
    public async Task SendAllMessage(string message)
    {
        await Clients.All.SendAsync("Message",  message);
    }

    public async IAsyncEnumerable<int> Counter(
       int count,
       int delay,
       [EnumeratorCancellation]
        CancellationToken cancellationToken)
    {
        for (var i = 0; i < count; i++)
        {
            // Check the cancellation token regularly so that the server will stop
            // producing items if the client disconnects.
            cancellationToken.ThrowIfCancellationRequested();

            yield return i;

            // Use the cancellationToken in other APIs that accept cancellation
            // tokens so the cancellation can flow down to them.
            await Task.Delay(delay, cancellationToken);
        }
    }
}