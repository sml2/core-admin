﻿using Microsoft.EntityFrameworkCore;
using System;
using CoreAdmin.Mvc;
using CoreAdmin.EF;
using ModelLabel=SML.Models.Label;
using ModelRelation = SML.Models.Relation;
namespace SML.EF
{
    public class Label : BaseDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Label -OutputDir Context\Migrations_Label [name]
        //回滚迁移
        //Remove-Migration  -Context Label
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Label
        //删库
        //Drop-Database -Context Label
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Label
        //获取帮助
        //get-help entityframework

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source=CoreAdmin.db");
        public DbSet<ModelLabel> Labels { get; set; }
        public DbSet<ModelRelation> LabelRelations { get; set; }


        public Label(DbContextOptions<Label> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ModelRelation>(entity =>
            {

                entity.HasOne(d => d.FromLabelsNavigation)
                    .WithMany(p => p.LabelRelationsFromLabelsNavigation)
                    .HasForeignKey(d => d.FromLabels);

                entity.HasOne(d => d.ToLabelsNavigation)
                    .WithMany(p => p.LabelRelationsToLabelsNavigation)
                    .HasForeignKey(d => d.ToLabels);
            });
            modelBuilder.Entity<ModelLabel>().HasData(
              new ModelLabel { ID = 1, Name = "Label_1_没得", RefCount = 2, Remark = "没得", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, },
              new ModelLabel { ID = 2, Name = "Label_2_小眼睛", RefCount = 4, Remark = "小眼睛", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, },
              new ModelLabel { ID = 3, Name = "Label_3_小苹果", RefCount = 54, Remark = "小苹果", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, },
              new ModelLabel { ID = 4, Name = "Label_4_丑橘子", RefCount = 1, Remark = "丑橘子", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, },
              new ModelLabel { ID = 5, Name = "Label_5_大西瓜", RefCount = 0, Remark = "大西瓜", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, }
  );
        }
    }
}
