﻿using SML.Models.Tool;
using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc;

namespace CoreAdmin.EF
{
    public class Tool : BaseDbContext, IAdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Tool -OutputDir Context\Migrations_Tool [name]
        //回滚迁移
        //Remove-Migration  -Context Tool
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Tool
        //删库
        //Drop-Database  -Context Tool
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Tool
        //获取帮助
        //get-help entityframework
        
        public DbSet<Password> Passwords { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Code> Codes { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<RichText> RichTexts { get; set; }


        public Tool(DbContextOptions<Tool> options) : base(options)
        {
        }
    }
}
