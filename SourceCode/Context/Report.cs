﻿using SML.Models.Report;
using ReportException=SML.Models.Report.Exception;
using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc;
using CoreAdmin.EF;

namespace SML.EF
{
    public class Report : BaseDbContext, IAdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Report -OutputDir Context\Migrations_Report [name]
        //回滚迁移
        //Remove-Migration  -Context Report
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Report
        //删库
        //Drop-Database  -Context Report
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Report
        //获取帮助
        //get-help entityframework
        public DbSet<ReportException> ReportExceptions { get; set; }
        public DbSet<GroupHash> GroupHashes { get; set; }
        public DbSet<IP> IPs { get; set; }
        public DbSet<RequestLog> RequestLogs { get; set; }
        public DbSet<Filter> Filters { get; set; }


        public Report(DbContextOptions<Report> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //TypeName = "Integer REFERENCES IP(Id) ON DELETE CASCADE"
            //CREATE TABLE t_Imp_Amazon_Detail(
            //    CollectTime      VARCHAR,
            //    Attributes       VARCHAR,
            //    Asin             VARCHAR,
            //    Name             VARCHAR,
            //    Type             VARCHAR,
            //    Shopname         VARCHAR,
            //    Sketch           VARCHAR,
            //    CMMTCount        VARCHAR,
            //    CMMTStar         VARCHAR,
            //    Rank             VARCHAR,
            //    Task             INTEGER NOT NULL
            //                             REFERENCES t_Model_Task(ObjectDBID) ON DELETE CASCADE   
            //                                                                 ON UPDATE CASCADE,
            //    PageImages       VARCHAR,
            //    Products         VARCHAR,
            //    PriceValue       VARCHAR,
            //    Unit             INTEGER,
            //    Keyword          VARCHAR,
            //    Description_Text VARCHAR,
            //    Category         VARCHAR,
            //    CategoryDetail   VARCHAR,
            //    UploadResult     INTEGER,
            //    ObjectDBID       INTEGER PRIMARY KEY AUTOINCREMENT
            //);


            //表约束
            //当前默认建表操作就是代码所示
            //modelBuilder.Entity<RequestLog>().HasOne(r => r.IP).WithMany().OnDelete(DeleteBehavior.Cascade);

            //CREATE TABLE RequestLog(
            //    id         INTEGER CONSTRAINT 'PK_RequestLog' PRIMARY KEY AUTOINCREMENT

            //                       NOT NULL,
            //    iid        INTEGER NOT NULL,
            //    datas      TEXT    NOT NULL,
            //    Content    BLOB,
            //    created_at TEXT    NOT NULL,
            //    updated_at TEXT    NOT NULL,
            //    FOREIGN KEY(iid) REFERENCES IP(id) ON DELETE CASCADE
            //);
            //modelBuilder.Entity<ReportException>().Property(nameof(ReportException.))

        }
    }
}
