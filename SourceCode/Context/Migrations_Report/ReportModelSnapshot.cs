﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SML.EF;

namespace SML.Context.Migrations_Report
{
    [DbContext(typeof(Report))]
    partial class ReportModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.7");

            modelBuilder.Entity("CoreAdmin.Mvc.Models.MenuModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Icon")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("icon");

                    b.Property<int>("Order")
                        .HasColumnType("INTEGER")
                        .HasColumnName("order");

                    b.Property<int>("ParentID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("parent_id");

                    b.Property<string>("Permission")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("permission");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("title");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("Uri")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("uri");

                    b.HasKey("Id");

                    b.ToTable("MenuModel");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Permissions", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("HttpMethod")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("http_method");

                    b.Property<string>("HttpPath")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("http_path");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("slug");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("ID");

                    b.HasIndex("Slug", "Name")
                        .IsUnique();

                    b.ToTable("Permissions");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Roles", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("slug");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex");

                    b.HasIndex("Slug", "Name")
                        .IsUnique();

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Avatar")
                        .HasColumnType("TEXT")
                        .HasColumnName("avatar");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("TEXT");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("TEXT");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("INTEGER");

                    b.Property<string>("RememberToken")
                        .HasColumnType("TEXT")
                        .HasColumnName("remember_token");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("TEXT");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("username");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex");

                    b.HasIndex("UserName")
                        .IsUnique();

                    b.ToTable("Users");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserRole", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RoleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("MenuModelRoles", b =>
                {
                    b.Property<int>("MenuId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RolesId")
                        .HasColumnType("INTEGER");

                    b.HasKey("MenuId", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("MenuModelRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimType")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("TEXT");

                    b.Property<int>("RoleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimType")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Value")
                        .HasColumnType("TEXT");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("PermissionsRoles", b =>
                {
                    b.Property<int>("PermissionsID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RolesId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionsID", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("PermissionsRoles");
                });

            modelBuilder.Entity("PermissionsUserModel", b =>
                {
                    b.Property<int>("PermissionsID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("UserModelId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionsID", "UserModelId");

                    b.HasIndex("UserModelId");

                    b.ToTable("PermissionsUserModel");
                });

            modelBuilder.Entity("SML.Models.Report.Exception", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<string>("Assemblies")
                        .HasColumnType("TEXT")
                        .HasColumnName("assemblies");

                    b.Property<int>("AssemblyCount")
                        .HasColumnType("INTEGER")
                        .HasColumnName("assembly_count");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("From")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("from");

                    b.Property<int?>("GroupHashID")
                        .HasColumnType("INTEGER");

                    b.Property<string>("HResult")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("h_result");

                    b.Property<string>("Hash")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("hash");

                    b.Property<int>("IPID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("iip");

                    b.Property<string>("IPValue")
                        .HasColumnType("TEXT")
                        .HasColumnName("ip_value");

                    b.Property<string>("Message")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("message");

                    b.Property<int>("ModuleCount")
                        .HasColumnType("INTEGER")
                        .HasColumnName("module_count");

                    b.Property<string>("Modules")
                        .HasColumnType("TEXT")
                        .HasColumnName("modules");

                    b.Property<string>("NetFramework")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("net_framework");

                    b.Property<string>("OSVersion")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("os_version");

                    b.Property<string>("Path")
                        .HasMaxLength(500)
                        .HasColumnType("TEXT")
                        .HasColumnName("path");

                    b.Property<string>("RawText")
                        .HasColumnType("TEXT")
                        .HasColumnName("raw_text");

                    b.Property<int>("RequestLogId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("RunTime")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("runtime");

                    b.Property<string>("Sender")
                        .HasColumnType("TEXT")
                        .HasColumnName("sender");

                    b.Property<string>("Software")
                        .HasColumnType("TEXT")
                        .HasColumnName("software");

                    b.Property<string>("Source")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("source");

                    b.Property<string>("StackTrace")
                        .HasColumnType("TEXT")
                        .HasColumnName("stack_trace");

                    b.Property<int>("State")
                        .HasColumnType("INTEGER");

                    b.Property<string>("TargetSite")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("target_site");

                    b.Property<int>("ThreadCount")
                        .HasColumnType("INTEGER")
                        .HasColumnName("thread_count");

                    b.Property<string>("Threads")
                        .HasColumnType("TEXT")
                        .HasColumnName("threads");

                    b.Property<string>("Time")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("time");

                    b.Property<string>("Type")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("type");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("UserName")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("username");

                    b.Property<string>("Version")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT")
                        .HasColumnName("version");

                    b.HasKey("ID");

                    b.HasIndex("GroupHashID");

                    b.HasIndex("IPID");

                    b.HasIndex("RequestLogId");

                    b.ToTable("ReportExceptions");
                });

            modelBuilder.Entity("SML.Models.Report.Filter", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<int>("Type")
                        .HasColumnType("INTEGER")
                        .HasColumnName("Type");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("Value")
                        .HasColumnType("TEXT")
                        .HasColumnName("Value");

                    b.HasKey("ID");

                    b.ToTable("Filters");
                });

            modelBuilder.Entity("SML.Models.Report.GroupHash", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<int>("Count")
                        .HasColumnType("INTEGER")
                        .HasColumnName("count");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("description");

                    b.Property<string>("FilterUI")
                        .HasColumnType("TEXT");

                    b.Property<long>("LastFileLength")
                        .HasColumnType("INTEGER")
                        .HasColumnName("file_length");

                    b.Property<int?>("LastIPID")
                        .HasColumnType("INTEGER");

                    b.Property<string>("LastIPValue")
                        .HasColumnType("TEXT")
                        .HasColumnName("ip_value");

                    b.Property<int?>("MatchFilterID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("fid");

                    b.Property<string>("RawText")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("raw_text");

                    b.Property<string>("Software")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("software");

                    b.Property<int>("State")
                        .HasColumnType("INTEGER")
                        .HasColumnName("state");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("hash_value");

                    b.HasKey("ID");

                    b.HasIndex("LastIPID");

                    b.HasIndex("MatchFilterID");

                    b.ToTable("GroupHashes");
                });

            modelBuilder.Entity("SML.Models.Report.IP", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<int>("ExceptionCount")
                        .HasColumnType("INTEGER")
                        .HasColumnName("exception_count")
                        .HasComment("提交Exception数量");

                    b.Property<int>("PushCount")
                        .HasColumnType("INTEGER")
                        .HasColumnName("push_count")
                        .HasComment("总请求次数");

                    b.Property<int>("PushCountToday")
                        .HasColumnType("INTEGER")
                        .HasColumnName("push_count_today")
                        .HasComment("当日请求次数");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("TEXT")
                        .HasColumnName("value");

                    b.HasKey("ID");

                    b.ToTable("IPs");
                });

            modelBuilder.Entity("SML.Models.Report.RequestLog", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<byte[]>("Content")
                        .HasColumnType("BLOB");

                    b.Property<int>("Count")
                        .HasColumnType("INTEGER")
                        .HasColumnName("count");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<int>("IPID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("iid");

                    b.Property<string>("IPValue")
                        .HasColumnType("TEXT")
                        .HasColumnName("ip_value");

                    b.Property<long>("Size")
                        .HasColumnType("INTEGER")
                        .HasColumnName("size");

                    b.Property<int>("State")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("ID");

                    b.HasIndex("IPID");

                    b.ToTable("RequestLogs");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserRole", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Roles", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("User");
                });

            modelBuilder.Entity("MenuModelRoles", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.MenuModel", null)
                        .WithMany()
                        .HasForeignKey("MenuId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PermissionsRoles", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Permissions", null)
                        .WithMany()
                        .HasForeignKey("PermissionsID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PermissionsUserModel", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Permissions", null)
                        .WithMany()
                        .HasForeignKey("PermissionsID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("SML.Models.Report.Exception", b =>
                {
                    b.HasOne("SML.Models.Report.GroupHash", "GroupHash")
                        .WithMany()
                        .HasForeignKey("GroupHashID");

                    b.HasOne("SML.Models.Report.IP", "IP")
                        .WithMany()
                        .HasForeignKey("IPID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("SML.Models.Report.RequestLog", "RequestLog")
                        .WithMany()
                        .HasForeignKey("RequestLogId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("GroupHash");

                    b.Navigation("IP");

                    b.Navigation("RequestLog");
                });

            modelBuilder.Entity("SML.Models.Report.GroupHash", b =>
                {
                    b.HasOne("SML.Models.Report.IP", "LastIP")
                        .WithMany()
                        .HasForeignKey("LastIPID");

                    b.HasOne("SML.Models.Report.Filter", "MatchFilter")
                        .WithMany()
                        .HasForeignKey("MatchFilterID");

                    b.Navigation("LastIP");

                    b.Navigation("MatchFilter");
                });

            modelBuilder.Entity("SML.Models.Report.RequestLog", b =>
                {
                    b.HasOne("SML.Models.Report.IP", "IP")
                        .WithMany()
                        .HasForeignKey("IPID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("IP");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Roles", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserModel", b =>
                {
                    b.Navigation("UserRoles");
                });
#pragma warning restore 612, 618
        }
    }
}
