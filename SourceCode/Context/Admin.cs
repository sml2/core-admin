﻿using CoreAdmin.EF;
using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc.Models;

namespace SML.EF
{
    public class Admin : AdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Admin -OutputDir Context\Migrations_Admin [name]
        //回滚迁移
        //Remove-Migration  -Context Admin
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Admin
        //删库
        //Drop-Database -Context Admin
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Admin
        //获取帮助
        //get-help entityframework


        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source=CoreAdmin.db");


        public Admin(DbContextOptions<Admin> options) : base(options)
        {
            Initial();
        }

        public DbSet<Models.SimpleTemplateModel> SimpleTemplateModel { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var Report = new MenuModel { Id = 8, ParentID = 0, Title = "错误上报", Uri = "/report", Icon = "fa-500px", Order = 8 };
            var Label = new MenuModel { Id = 14, ParentID = 0, Title = "标签", Uri = "/labels", Icon = "fa-bars", Order = 14 };
            var Tool = new MenuModel { Id = 18, ParentID = 0, Title = "工具", Uri = "", Icon = "fa-book", Order = 18 };
            var Money = new MenuModel { Id = 24, ParentID = 0, Title = "账本", Uri = "", Icon = "fa-book", Order = 24 };
            var Demands = new MenuModel { Id = 28, ParentID = 0, Title = "用户反馈", Uri = "/DemandsNeed", Icon = "fa-text-width", Order = 28 };
            var Template = new MenuModel { Id = 27, ParentID = 0, Title = "组件模版", Uri = "/SimpleTemplate", Icon = "fa-text-width", Order = 27 };
            
            modelBuilder.Entity<MenuModel>().HasData(
                    Report,
                    new MenuModel { Id = Report.Id + 1, ParentID = Report.Id, Title = "汇总", Uri = "/groupHash", Icon = "fa-bars", Order = Report.Id + 1 },
                    new MenuModel { Id = Report.Id + 2, ParentID = Report.Id, Title = "过滤规则", Uri = "/report", Icon = "fa-bars", Order = Report.Id + 2 },
                    new MenuModel { Id = Report.Id + 3, ParentID = Report.Id, Title = "异常列表", Uri = "/report", Icon = "fa-bars", Order = Report.Id + 3 },
                    new MenuModel { Id = Report.Id + 4, ParentID = Report.Id, Title = "IP", Uri = "/report/ip", Icon = "fa-bars", Order = Report.Id + 4 },
                    new MenuModel { Id = Report.Id + 5, ParentID = Report.Id, Title = "请求日志", Uri = "/requestlog", Icon = "fa-bars", Order = Report.Id + 5 },
                    //Last=13

                    Label,
                    new MenuModel { Id = Label.Id + 1, ParentID = Label.Id, Title = "列表", Uri = "/labels", Icon = "fa-bars", Order = Label.Id + 1 },
                    new MenuModel { Id = Label.Id + 2, ParentID = Label.Id, Title = "关联关系", Uri = "/labelRelations", Icon = "fa-bars", Order = Label.Id + 2 },
                    new MenuModel { Id = Label.Id + 3, ParentID = Label.Id, Title = "3维关系图", Uri = "Labels/D3Relations/", Icon = "fa-chain", Order = Label.Id + 3 },
                    //Last=17

                    Tool,
                    new MenuModel { Id = Tool.Id + 1, ParentID = Tool.Id, Title = "密码本", Uri = "/Password", Icon = "fa-cc", Order = Label.Id + 1 },
                    new MenuModel { Id = Tool.Id + 2, ParentID = Tool.Id, Title = "简易记事本", Uri = "/Note", Icon = "fa-info", Order = Label.Id + 2 },
                    new MenuModel { Id = Tool.Id + 3, ParentID = Tool.Id, Title = "富文本记事本", Uri = "/RichText", Icon = "fa-area-chart", Order = Label.Id + 3 },
                    new MenuModel { Id = Tool.Id + 4, ParentID = Tool.Id, Title = "文档记事本", Uri = "/Document", Icon = "fa-align-left", Order = Label.Id + 4 },
                    new MenuModel { Id = Tool.Id + 5, ParentID = Tool.Id, Title = "代码片段", Uri = "/Code", Icon = "fa-clipboard", Order = Label.Id + 5 },
                    //Last=23

                    Money,
                    new MenuModel { Id = Money.Id + 1, ParentID = Money.Id, Title = "债务人", Uri = "/People", Icon = "fa-bars", Order = Label.Id + 1 },
                    new MenuModel { Id = Money.Id + 2, ParentID = Money.Id, Title = "明细", Uri = "/Detail", Icon = "fa-bars", Order = Label.Id + 2 },
                    //Last=26
                    Demands,
                    Template
            //Last=17
            );
        }
    }
}
