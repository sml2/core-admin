﻿using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc;
using SML.Models.Demands;
using CoreAdmin.EF;

namespace SML.EF
{
    public class Demandss : BaseDbContext, IAdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Demandss -OutputDir Context\Migrations_Demandss [name]
        //回滚迁移
        //Remove-Migration  -Context Demandss
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Tool
        //删库
        //Drop-Database  -Context Tool
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Tool
        //获取帮助
        //get-help entityframework

        public DbSet<Demands> Demand {get;set;}
        public Demandss(DbContextOptions<Demandss> options) : base(options)
        {
        }
    }
}
