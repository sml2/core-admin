﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SML.Context.Migrations_Label
{
    public partial class First : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Labels",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    remark = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    ref_count = table.Column<int>(type: "INTEGER", nullable: false),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Labels", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "LabelRelations",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    from_labels = table.Column<int>(type: "INTEGER", nullable: false),
                    to_labels = table.Column<int>(type: "INTEGER", nullable: false),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LabelRelations", x => x.id);
                    table.ForeignKey(
                        name: "FK_LabelRelations_Labels_from_labels",
                        column: x => x.from_labels,
                        principalTable: "Labels",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LabelRelations_Labels_to_labels",
                        column: x => x.to_labels,
                        principalTable: "Labels",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Labels",
                columns: new[] { "id", "created_at", "name", "ref_count", "remark", "updated_at" },
                values: new object[] { 1, new DateTime(2021, 1, 9, 11, 38, 22, 987, DateTimeKind.Local).AddTicks(2711), "Label_1_没得", 2, "没得", new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(3511) });

            migrationBuilder.InsertData(
                table: "Labels",
                columns: new[] { "id", "created_at", "name", "ref_count", "remark", "updated_at" },
                values: new object[] { 2, new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4022), "Label_2_小眼睛", 4, "小眼睛", new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4030) });

            migrationBuilder.InsertData(
                table: "Labels",
                columns: new[] { "id", "created_at", "name", "ref_count", "remark", "updated_at" },
                values: new object[] { 3, new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4033), "Label_3_小苹果", 54, "小苹果", new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4035) });

            migrationBuilder.InsertData(
                table: "Labels",
                columns: new[] { "id", "created_at", "name", "ref_count", "remark", "updated_at" },
                values: new object[] { 4, new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4038), "Label_4_丑橘子", 1, "丑橘子", new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4039) });

            migrationBuilder.InsertData(
                table: "Labels",
                columns: new[] { "id", "created_at", "name", "ref_count", "remark", "updated_at" },
                values: new object[] { 5, new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4042), "Label_5_大西瓜", 0, "大西瓜", new DateTime(2021, 1, 9, 11, 38, 22, 989, DateTimeKind.Local).AddTicks(4043) });

            migrationBuilder.CreateIndex(
                name: "IX_LabelRelations_from_labels",
                table: "LabelRelations",
                column: "from_labels");

            migrationBuilder.CreateIndex(
                name: "IX_LabelRelations_to_labels",
                table: "LabelRelations",
                column: "to_labels");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LabelRelations");

            migrationBuilder.DropTable(
                name: "Labels");
        }
    }
}
