﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SML.Context.Migrations_Money
{
    public partial class Detail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(331), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(339) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(348), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(348) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(350), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(350) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(363), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(363) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(366), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(366) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(369), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(369) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(372), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(373) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(562), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(562) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(564), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(564) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(566), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(566) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(567), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(567) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(569), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(569) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "5046e0a5-eeea-4702-9442-0797939eb795");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "b7d3bd5a-2eae-4ac8-b6e5-df89d009bc73", new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(581), new DateTime(2021, 8, 18, 13, 21, 31, 549, DateTimeKind.Local).AddTicks(582) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9751), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9758) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9769), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9769) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9771), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9771) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9787), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9788) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9791), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9791) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 6,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9795), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9795) });

            migrationBuilder.UpdateData(
                table: "Menu",
                keyColumn: "id",
                keyValue: 7,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9797), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9798) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9982), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9982) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9984), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9984) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9985), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9986) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 4,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9987), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9987) });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9988), new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9989) });

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "88c3f72c-8cd1-4c6b-bc56-98e2a4867e78");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "created_at", "updated_at" },
                values: new object[] { "e24326fe-5c8b-45d9-9d01-0111ed8e0078", new DateTime(2021, 8, 18, 13, 20, 50, 29, DateTimeKind.Local).AddTicks(3), new DateTime(2021, 8, 18, 13, 20, 50, 29, DateTimeKind.Local).AddTicks(3) });
        }
    }
}
