﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SML.Context.Migrations_Money
{
    public partial class People : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    parent_id = table.Column<int>(type: "INTEGER", nullable: false),
                    title = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    icon = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    uri = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    permission = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    order = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "OperationLog",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    user_id = table.Column<int>(type: "INTEGER", nullable: false),
                    username = table.Column<string>(type: "TEXT", nullable: false),
                    path = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    method = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    ip = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    host = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    query = table.Column<string>(type: "TEXT", nullable: true),
                    body = table.Column<string>(type: "TEXT", nullable: true, comment: "可为空,表示没有请求体"),
                    ResponseCode = table.Column<int>(type: "INTEGER", nullable: false, comment: "响应码"),
                    ContentLength = table.Column<long>(type: "INTEGER", nullable: false, comment: "响应内容长度"),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationLog", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Peoples",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Job = table.Column<string>(type: "TEXT", nullable: true),
                    Total = table.Column<decimal>(type: "TEXT", nullable: false),
                    LastRepay = table.Column<DateTime>(type: "TEXT", nullable: false),
                    LastBorrow = table.Column<DateTime>(type: "TEXT", nullable: false),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Peoples", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    slug = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    http_method = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
                    http_path = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    slug = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    NormalizedName = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    username = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    name = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
                    avatar = table.Column<string>(type: "TEXT", nullable: true),
                    remember_token = table.Column<string>(type: "TEXT", nullable: true),
                    PasswordHash = table.Column<string>(type: "TEXT", nullable: true),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    NormalizedUserName = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "INTEGER", nullable: false),
                    SecurityStamp = table.Column<string>(type: "TEXT", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "TEXT", nullable: true),
                    PhoneNumber = table.Column<string>(type: "TEXT", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "INTEGER", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "TEXT", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Details",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    U_Name = table.Column<string>(type: "TEXT", nullable: true),
                    PeopleID = table.Column<int>(type: "INTEGER", nullable: false),
                    Amount = table.Column<decimal>(type: "TEXT", nullable: false),
                    created_at = table.Column<DateTime>(type: "TEXT", nullable: false),
                    updated_at = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Details", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Details_Peoples_PeopleID",
                        column: x => x.PeopleID,
                        principalTable: "Peoples",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<int>(type: "INTEGER", nullable: false),
                    ClaimType = table.Column<string>(type: "TEXT", nullable: true),
                    ClaimValue = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MenuModelRoles",
                columns: table => new
                {
                    MenuId = table.Column<int>(type: "INTEGER", nullable: false),
                    RolesId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuModelRoles", x => new { x.MenuId, x.RolesId });
                    table.ForeignKey(
                        name: "FK_MenuModelRoles_Menu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "Menu",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MenuModelRoles_Roles_RolesId",
                        column: x => x.RolesId,
                        principalTable: "Roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissionsRoles",
                columns: table => new
                {
                    PermissionsID = table.Column<int>(type: "INTEGER", nullable: false),
                    RolesId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionsRoles", x => new { x.PermissionsID, x.RolesId });
                    table.ForeignKey(
                        name: "FK_PermissionsRoles_Permissions_PermissionsID",
                        column: x => x.PermissionsID,
                        principalTable: "Permissions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionsRoles_Roles_RolesId",
                        column: x => x.RolesId,
                        principalTable: "Roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    ClaimType = table.Column<string>(type: "TEXT", nullable: true),
                    ClaimValue = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "TEXT", nullable: false),
                    ProviderKey = table.Column<string>(type: "TEXT", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "TEXT", nullable: true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    LoginProvider = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Value = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissionsUserModel",
                columns: table => new
                {
                    PermissionsID = table.Column<int>(type: "INTEGER", nullable: false),
                    UserModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionsUserModel", x => new { x.PermissionsID, x.UserModelId });
                    table.ForeignKey(
                        name: "FK_PermissionsUserModel_Permissions_PermissionsID",
                        column: x => x.PermissionsID,
                        principalTable: "Permissions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionsUserModel_Users_UserModelId",
                        column: x => x.UserModelId,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    RoleId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 1, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9751), "fa-bar-chart", 1, 0, null, "Dashboard", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9758), "Home/Index" });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 2, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9769), "fa-tasks", 2, 0, null, "Admin", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9769), "" });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 3, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9771), "fa-users", 3, 2, null, "Users", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9771), "users" });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 4, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9787), "fa-user", 4, 2, null, "Roles", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9788), "roles" });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 5, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9791), "fa-ban", 5, 2, null, "Permission", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9791), "permissions" });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 6, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9795), "fa-bars", 6, 2, null, "Menu", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9795), "menu" });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "id", "created_at", "icon", "order", "parent_id", "permission", "title", "updated_at", "uri" },
                values: new object[] { 7, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9797), "fa-history", 7, 2, null, "Operation Log", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9798), "operationlog" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "id", "created_at", "http_method", "http_path", "name", "slug", "updated_at" },
                values: new object[] { 1, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9982), "ANY", "/*", "All permission", "Dashboard", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9982) });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "id", "created_at", "http_method", "http_path", "name", "slug", "updated_at" },
                values: new object[] { 2, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9984), "GET", "/", "Dashboard", "Admin", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9984) });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "id", "created_at", "http_method", "http_path", "name", "slug", "updated_at" },
                values: new object[] { 3, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9985), "", "/auth/login,/manage/auth/logout", "Login", "Users", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9986) });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "id", "created_at", "http_method", "http_path", "name", "slug", "updated_at" },
                values: new object[] { 4, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9987), "GET,PUT", "/auth/setting", "User setting", "Roles", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9987) });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "id", "created_at", "http_method", "http_path", "name", "slug", "updated_at" },
                values: new object[] { 5, new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9988), "", "/auth/roles,/auth/permissions,/auth/menu,/auth/logs", "Auth management", "Permission", new DateTime(2021, 8, 18, 13, 20, 50, 28, DateTimeKind.Local).AddTicks(9989) });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "id", "ConcurrencyStamp", "created_at", "name", "NormalizedName", "slug", "updated_at" },
                values: new object[] { 1, "88c3f72c-8cd1-4c6b-bc56-98e2a4867e78", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Admin", "ADMIN", "Admin", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "id", "AccessFailedCount", "avatar", "ConcurrencyStamp", "created_at", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "name", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "remember_token", "SecurityStamp", "TwoFactorEnabled", "updated_at", "username" },
                values: new object[] { 1, 0, "/_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg", "e24326fe-5c8b-45d9-9d01-0111ed8e0078", new DateTime(2021, 8, 18, 13, 20, 50, 29, DateTimeKind.Local).AddTicks(3), "admin@ca.com", true, false, null, "Administrator", null, null, null, null, false, null, null, false, new DateTime(2021, 8, 18, 13, 20, 50, 29, DateTimeKind.Local).AddTicks(3), "admin" });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Details_PeopleID",
                table: "Details",
                column: "PeopleID");

            migrationBuilder.CreateIndex(
                name: "IX_MenuModelRoles_RolesId",
                table: "MenuModelRoles",
                column: "RolesId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationLog_user_id",
                table: "OperationLog",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_slug_name",
                table: "Permissions",
                columns: new[] { "slug", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsRoles_RolesId",
                table: "PermissionsRoles",
                column: "RolesId");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsUserModel_UserModelId",
                table: "PermissionsUserModel",
                column: "UserModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_slug_name",
                table: "Roles",
                columns: new[] { "slug", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_Users_username",
                table: "Users",
                column: "username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Details");

            migrationBuilder.DropTable(
                name: "MenuModelRoles");

            migrationBuilder.DropTable(
                name: "OperationLog");

            migrationBuilder.DropTable(
                name: "PermissionsRoles");

            migrationBuilder.DropTable(
                name: "PermissionsUserModel");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Peoples");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
