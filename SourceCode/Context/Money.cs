﻿using CoreAdmin.EF;
using CoreAdmin.Mvc;
using Microsoft.EntityFrameworkCore;
using SML.Models.Money;

namespace SML.EF
{
    public class Money : AdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context Money -OutputDir Context\Migrations_Money [name]
        //回滚迁移
        //Remove-Migration  -Context Money
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Money
        //删库
        //Drop-Database  -Context Money
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Money
        //获取帮助
        //get-help entityframework
       
        public DbSet<People> Peoples { get; set; }

        public DbSet<Detail> Details { get; set; }
        public Money(DbContextOptions<Money> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

