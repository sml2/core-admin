﻿using Microsoft.AspNetCore.Mvc;

namespace SML.Controllers
{
    public class WWW : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
