﻿using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DemandsContext = SML.EF.Demandss;
using Model = SML.Models.Demands.Demands;


namespace SML.Controllers.Tool
{
    /// <summary>
    /// 反馈
    /// </summary>
    //[Route("")]
    public class DemandsNeedController : AdminController<Model>
    {
        private new DemandsContext Context => DbContext as DemandsContext;


        public DemandsNeedController(DemandsContext Context) : base()
        {
            DbContext = Context;
            Title = "反馈片段";
        }


        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Demand;
            var grid = new BGrid(model, HttpContext);

            grid.Column(nameof(Model.Id)).Sortable();
            grid.Column(nameof(Model.Title)).Sortable();
            grid.Column(nameof(Model.Content)).Sortable();
            grid.Column(nameof(Model.Feedback)).Sortable();
            grid.Column(nameof(Model.Process)).EditableOptions(Model.Processes.DEVELOPED).Sortable();

            // grid.Column(nameof(Model.State), "状态").EditableOptions(Model.States.Unknown, Model.States.Solved).MinWidth(70);
            //Display<Processes>(m => m.GetDescriptionAttribute<DisplayAttribute>().Name);
            // EditableOptions(Demands.Processes.DEVELOPED, Demands.Processes.DEVELOPING, Demands.Processes.PUBLISHED, Demands.Processes.TESTED, Demands.Processes.SHELVED, Demands.Processes.WAIT_DEVELOP, Demands.Processes.WAIT_EVALUATION, Demands.Processes.WAIT_PUBLISH, Demands.Processes.WAIT_REPLY, Demands.Processes.WAIT_TEST, Demands.Processes.DELAY);
            //grid.Tools((tools) => { });

            return grid;
        }


        /*  创建和编辑UI */
        protected override BForm Form()
        {
            var model = Context.Demand;
            var form = new BForm(model).WithController(this);

            form.Textarea(nameof(Model.Title));
            form.RichText(nameof(Model.Content));
            form.Text(nameof(Model.Feedback));
            form.Select(nameof(Model.Process));

            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, Model model)
        {
            var test = await Context.Demand.SingleAsync(m => m.Id == id);
            test.Title = model.Title;
            test.Content = model.Content;
            test.Title = model.Feedback;
            test.Process = model.Process;

            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Demand;
            var show = new BShow(model.Single(m => m.Id == id), null, HttpContext);

            show.Field(nameof(Model.Id), "Id");
            show.Field(nameof(Model.Title));
            show.Field(nameof(Model.Content));
            show.Field(nameof(Model.Feedback));
            show.Field(nameof(Model.Process));


            return show;
        }
    }
}
