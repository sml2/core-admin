﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Threading.Tasks;

namespace SML.Controllers.Api
{
    public class Cmd : Controller
    {
        public IActionResult Index()
        {
            //标题
            ViewData["Title"] = "Command";
            //欢迎语
            ViewData["Greetings"] = "Welcome to CMD,connecting in progress...";
            return View();
        }
    }
}
