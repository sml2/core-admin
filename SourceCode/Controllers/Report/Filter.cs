﻿using System.Linq;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using ReporterContext = SML.EF.Report;
using Model = SML.Models.Report.Filter;
using Service = SML.Services.Report.Filter;
namespace SML.Controllers.Report
{
    public class Filter : AdminController<Model>
    {
        private ReporterContext ReporterContext => DbContext as ReporterContext;
        private readonly Service service;
        public Filter(ReporterContext reporterContext, Service service) : base()
        {
            DbContext = reporterContext;
            this.service = service;
            Title = "过滤规则";
        }
        protected override BGrid Grid()
        {
            var exceptionModel = ReporterContext.Filters.OrderByDescending(m => m.UpdatedAt);

            var grid = new BGrid<Model>(exceptionModel, HttpContext);
            var a = Utils;
            grid.Column(nameof(Model.ID)).Sortable().MinWidth(60);
            grid.Column(nameof(Model.Name)).Label().MinWidth(125);
            grid.Column(nameof(Model.Value));
            grid.Column(nameof(Model.CreatedAt)).MinWidth(145);
            grid.Column(nameof(Model.UpdatedAt)).MinWidth(145);

            grid.Actions((actions) =>
            {
                actions.DisableEdit();
                actions.DisableView();
            });
            grid.Tools((tools) =>
            {
            });

            return grid;
        }
        protected override BForm Form()
        {
            var Models = ReporterContext.Filters;

            var form = new BForm(Models).WithController(this);
            form.Text(nameof(Model.Name));
            form.Select(nameof(Model.Type));
            form.Text(nameof(Model.Value));
            form.Saved((f,m) =>
            {
                if(m is Model mm)
                {
                    ReporterContext.GroupHashes.Where(gh => gh.MatchFilterID == mm.ID).ToList().ForEach(gh=>gh.MatchFilterID = null);
                    ReporterContext.SaveChanges();
                    service.Refresh();
                    ReporterContext.GroupHashes.Where(gh => gh.MatchFilterID == null).ToList().ForEach((gh) => service.Match(gh));
                    ReporterContext.SaveChanges();
                }
                return null;
            });
            return form;
        }

        protected override BShow Detail(int id)
        {
            var exceptionModel = ReporterContext.ReportExceptions.Include(m => m.RequestLog);
            var row = exceptionModel.SingleOrDefault(m => m.ID == id);
            if (row == null)
            {
                return null;
            }
            var show = new BShow(exceptionModel.Single(m => m.ID == id), context: HttpContext);


            show.Field(nameof(Model.ID));
            show.Field(nameof(Model.Name));
            show.Field(nameof(Model.Type));
            show.Field(nameof(Model.Value));
            show.Field(nameof(Model.CreatedAt));
            show.Field(nameof(Model.UpdatedAt));

            return show;
        }

    }

}
