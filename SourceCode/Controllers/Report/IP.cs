﻿using System.Linq;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Model = SML.Models.Report.IP;
using ReporterContext = SML.EF.Report;

namespace SML.Controllers.Report
{
    [Route("report/ip/")]
    public class IP : AdminController
    {
        private ReporterContext ReporterContext => DbContext as ReporterContext;

        public IP(ReporterContext reporterContext) : base()
        {
            DbContext = reporterContext;
            Title = "IP统计";
        }

        protected override BGrid Grid()
        {
            var ip = ReporterContext.IPs.OrderByDescending(o => o.UpdatedAt);

            var grid = new BGrid(ip, HttpContext);
            var a = Utils;
            grid.Column(nameof(Model.ID),"编号").Sortable().MinWidth(60);
            grid.Column(nameof(Model.Value),"IP").Label().MinWidth(125);
            grid.Column(nameof(Model.PushCount),"总请求次数");
            grid.Column(nameof(Model.ExceptionCount),"总异常总数");
            grid.Column(nameof(Model.CreatedAt), "首次时间").MinWidth(145);
            grid.Column(nameof(Model.UpdatedAt), "最后时间").MinWidth(145);

            grid.Actions((actions) =>
            {
                actions.DisableEdit();
                actions.DisableView();
            });

            grid.Tools((tools) => { });

            return grid;
        }


        protected override BForm Form()
        {
            var ip = ReporterContext.IPs;
            var form = new BForm(ip).WithController(this);

            return form;
        }

        protected override BShow Detail(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}