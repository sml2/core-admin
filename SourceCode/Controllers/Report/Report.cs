﻿using System.Linq;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Model = SML.Models.Report.Exception;
using ReporterContext = SML.EF.Report;
namespace SML.Controllers.Report
{
    public class Report : AdminController
    {
        private ReporterContext _reporterContext => DbContext as ReporterContext;

        public Report(ReporterContext reporterContext) : base()
        {
            DbContext = reporterContext;
            Title = "异常列表";
        }
        protected override BGrid Grid()
        {
            var exceptionModel = _reporterContext.ReportExceptions.OrderByDescending(m=>m.UpdatedAt);
            
            var grid = new BGrid<Model>(exceptionModel, HttpContext);
            var a = Utils;
            grid.Column(nameof(Model.ID)).Sortable().MinWidth(60);
            grid.Column(nameof(Model.IPValue)).Label().MinWidth(125);
            //grid.Column(nameof(ReportException.Hash),"Hash值");

            grid.Column("Name", "简要描述").Display((RE) => RE.ToString()).MaxWidth(500);
            grid.Column("Software", "软件").Display((RE) => $"{RE.Source} V{RE.Version}");

            //grid.Column("Name", "简要描述").Display<ReportException>((RE) => RE.ToString());
            //grid.Column("Software", "软件").Display<ReportException>((RE) => $"{RE.Source}{RE.Version}");

            //grid.Column("Name", "简要描述").Display<object>((col, RE) => (RE as ReportException)?.ToString());
            //grid.Column("Software", "软件").Display<object>((col, REM) => {
            //    var RE = REM as ReportException;
            //    if (RE == null) {
            //        Log
            //        return string.Empty;
            //    } else { 
            //        return $"{RE.Source} V{RE.Version}";
            //    }
            //});

            grid.Column(nameof(Model.CreatedAt)).MinWidth(145);
            grid.Column(nameof(Model.UpdatedAt)).MinWidth(145);
            grid.Column(nameof(Model.State)).Display<Model.States>(v=>v.ToString());

            grid.Actions((actions) =>
            {
            //    var values = Enum.GetValues<ReportException.States>();
            //    var currentState = actions.GetData<ReportException>().State;
            actions.DisableEdit();
            actions.DisableView();
            //    //foreach (var value in values)
            //    //{
            //    //    if (value != currentState)
            //    //    {
            //    //        actions.Append($@"<a href=""/Reporter/UpdateState/{(int)value}""><i class=""fa fa-edit""></i>{value}</a>");
            //    //    }
            //    //}
            });
            
            grid.Tools((tools) =>
            {
            });

            return grid;
        }



        protected override BForm Form()
        {
            var reporterModel = _reporterContext.ReportExceptions;

            var form = new BForm(reporterModel).WithController(this);

            return form;
        }

        protected override BShow Detail(int id)
        {
            var exceptionModel = _reporterContext.ReportExceptions.Include(m => m.RequestLog);
            var row = exceptionModel.SingleOrDefault(m => m.ID == id);
            if (row == null)
            {
                return null;
            }
            var show = new BShow(exceptionModel.Single(m => m.ID == id), context: HttpContext);


            show.Field(nameof(Model.ID));
            //show.Field(nameof(ReportException.RequestLog), Admin.Trans("text")).As<RequestLog>((log) => log.Datas).Label();
            show.Field(nameof(Model.CreatedAt));
            show.Field(nameof(Model.UpdatedAt));

            return show;
        }
        
    }

}
