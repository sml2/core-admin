﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelException = SML.Models.Report.Exception;
using ModelGroupHash = SML.Models.Report.GroupHash;
using ModelRequestLog = SML.Models.Report.RequestLog;
using ModelIP = SML.Models.Report.IP;
using CoreAdmin.Mvc.Extensions;
using ReportContext = SML.EF.Report;
using Service = SML.Services.Report.Filter;
using Microsoft.EntityFrameworkCore;

namespace SML.Controllers.Report
{
    //[Route(nameof(Controllers.Report))]
    [Route("")]
    [ApiController]
    public class Receive : ControllerBase
    {
        private readonly static object lockobj = new();
        private readonly ReportContext _reporterContext;
        private readonly Service _filter;
        public Receive(ReportContext reporterContext, Service filter) : base()
        {
            _reporterContext = reporterContext;
            _filter = filter;
        }
        [HttpPost]
        public async Task<string> OnPostAsync(IFormFile log)
        {
            if (log == null) { return "NoLogData"; }
            using (var memoryStream = new MemoryStream())
            {
                await log.CopyToAsync(memoryStream);
                var remoteIp = HttpContext.GetIPv4();
                lock (lockobj)
                {
                    ModelIP ModelIP = _reporterContext.IPs.SingleOrDefault(ip => ip.Value == remoteIp) ?? _reporterContext.IPs.Add(new ModelIP(remoteIp)).Entity;
                    //ModelIP ModelIP = _reporterContext.IPs.SingleOrDefault(ip => ip.Value == remoteIp) ?? new ModelIP(remoteIp);
                    ModelIP.PushCount++;
                    if (DateTime.Now.Date.Equals(ModelIP.UpdatedAt.Date))
                    {
                        if (++ModelIP.PushCountToday > 10)
                        {
                            _reporterContext.SaveChanges();
                            return "Frequently";
                        }
                    }
                    else
                    {
                        ModelIP.PushCountToday = 1;
                    }
                    ModelIP.UpdatedAt = DateTime.Now;

                    //_reporterContext.SaveChanges();
                    // Upload the file if less than 2 MB
                    var Length = memoryStream.Length;
                    if (Length < 2097152)
                    {

                        //防止添加Exception空循环或格式错误时无法记录请求
                        var ModelRequestLog = _reporterContext.RequestLogs.Add(new ModelRequestLog(ModelIP)
                        {
                            //Datas = Encoding.UTF8.GetString(memoryStream.ToArray()),
                            Size = Length,
                            Content = memoryStream.ToArray(),
                            CreatedAt = DateTime.Now,
                        }).Entity;
                        //_reporterContext.SaveChanges();
                        Sy.String Text = null;
                        try
                        {
                            Text = Encoding.UTF8.GetString(ModelRequestLog.Content);
                        }
                        catch (ArgumentNullException)
                        {
                            ModelRequestLog.State = ModelRequestLog.States.DecoderArgumentNullException;
                        }
                        catch (DecoderFallbackException)
                        {
                            ModelRequestLog.State = ModelRequestLog.States.DecoderArgumentException;
                        }
                        catch (ArgumentException)
                        {
                            ModelRequestLog.State = ModelRequestLog.States.DecoderArgumentException;
                        }
                        var Spliter = new string('=', 4);
                        if (Text.IsNullOrEmpty() && ModelRequestLog.State == ModelRequestLog.States.Unknow)
                        {
                            ModelRequestLog.State = ModelRequestLog.States.DecoderNullOrEmpty;
                        }
                        else if (Text.Contains(Spliter))
                        { //正经数据
                            Dictionary<string, ModelGroupHash> GroupHashTemp = new();
                            var Recodes = Text.Split(Spliter, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var Recode in Recodes.Select(s => s.Trim('=', '\r', '\n')).Where(s => s.Length > 0))
                            {
                                var ModelException = new ModelException(ModelRequestLog, ModelIP);
                                if (ModelException.Analysis(Recode) == ModelException.States.AnalysisSuccess)
                                {
                                    var Hash = ModelException.Hash;
                                    ModelGroupHash ModelGroupHash;
                                    if (GroupHashTemp.ContainsKey(Hash))
                                    {
                                        ModelGroupHash = GroupHashTemp[Hash];
                                    }
                                    else
                                    {
                                        ModelGroupHash = _reporterContext.GroupHashes.SingleOrDefault(h => h.Value == Hash) ?? new (Hash, Recode);
                                        GroupHashTemp.Add(Hash, ModelGroupHash);
                                    }
                                    ModelGroupHash.Count++;
                                    ModelGroupHash.Description = ModelException.ToString();
                                    _filter.Match(ModelGroupHash);
                                    ModelGroupHash.Software = $"{ModelException.Software} V{ModelException.Version}";
                                    ModelIP.ExceptionCount++;
                                    ModelGroupHash.SetLastIP(ModelIP);
                                    ModelGroupHash.LastFileLength = memoryStream.Length;
                                    switch (ModelGroupHash.State)
                                    {
                                        case ModelGroupHash.States.Resolved:
                                            ModelGroupHash.State = ModelGroupHash.States.Solved;
                                            break;
                                        case ModelGroupHash.States.Shelve or ModelGroupHash.States.Solved:
                                            break;
                                        case ModelGroupHash.States.Unknown or ModelGroupHash.States.Unresolved:
                                            ModelGroupHash.State = ModelGroupHash.States.Unresolved;
                                            break;
                                        default:
                                            throw new ArgumentException(nameof(ModelGroupHash.State));
                                    }
                                    //Ghash.State = Ghash.State switch
                                    //{
                                    //    GroupHash.States.Resolved => GroupHash.States.Solved,
                                    //    GroupHash.States.Shelve => GroupHash.States.Shelve,
                                    //    GroupHash.States.Solved => GroupHash.States.Solved,
                                    //    GroupHash.States.Unknown or GroupHash.States.Unresolved => GroupHash.States.Unresolved,
                                    //    _ => throw new ArgumentException(nameof(Ghash.State)),
                                    //};
                                    ModelException.GroupHash = ModelGroupHash;
                                    //ModelGroupHash.MatchFilter = new();
                                    ModelGroupHash.UpdatedAt = DateTime.Now;
                                    ModelRequestLog.Count++;
                                }
                                else
                                {
                                    ModelRequestLog.State = ModelRequestLog.States.Partial;
                                    Console.WriteLine($"Exception Analysis Error:[{ModelException.State} {ModelException.State:D}]");
                                };
                                _reporterContext.ReportExceptions.Add(ModelException);
                            }
                            if (ModelRequestLog.State == ModelRequestLog.States.Unknow)
                            {
                                ModelRequestLog.State = ModelRequestLog.States.Complete;
                            };
                        }
                        else
                        {//不正经数据，不处理
                            return "The file format is invalid.";
                        }

                    }
                    else
                    {
                        _reporterContext.SaveChanges();
                        return "The file is too large.";
                    }
                    _reporterContext.SaveChanges();
                }
            }
            return "ReportSuccess";
        }
    }
}
