﻿using System.Linq;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Model = SML.Models.Report.RequestLog;
using ReporterContext = SML.EF.Report;
namespace SML.Controllers.Report
{
    public class RequestLog : AdminController
    {
        private ReporterContext ReporterContext => DbContext as ReporterContext;

        public RequestLog(ReporterContext reporterContext) : base()
        {
            DbContext = reporterContext;
            Title = "请求记录列表";
        }
        protected override BGrid Grid()
        {
            var requestLog = ReporterContext.RequestLogs.OrderByDescending(o=> o.ID);
            
            var grid = new BGrid(requestLog, HttpContext);
            var a = Utils;
            grid.Column(nameof(Model.ID),"编号").Sortable();
            grid.Column(nameof(Model.IPValue), "IP").Label().MinWidth(125);
            grid.Column(nameof(Model.UI_Size), "大小");
            grid.Column(nameof(Model.State), "处理结果").Display<int>((data,model) => (model as Model)?.State.GetDescription()).MinWidth(60);
            grid.Column(nameof(Model.CreatedAt), "提交时间").MinWidth(145);
            grid.Actions((actions) =>                      
            {
            //    var values = Enum.GetValues<ReportException.States>();
            //    var currentState = actions.GetData<ReportException>().State;
            actions.DisableEdit();
            //    //foreach (var value in values)
            //    //{
            //    //    if (value != currentState)
            //    //    {
            //    //        actions.Append($@"<a href=""/Reporter/UpdateState/{(int)value}""><i class=""fa fa-edit""></i>{value}</a>");
            //    //    }
            //    //}
            });
            
            grid.Tools((tools) =>
            {
            });

            return grid;
        }



        protected override BForm Form()
        {
            var requestLog = ReporterContext.RequestLogs;

            var form = new BForm(requestLog).WithController(this);

            return form;
        }

        protected override BShow Detail(int id)
        {
            var requestLog =  ReporterContext.RequestLogs;
            var row = requestLog.SingleOrDefault(m => m.ID == id);
            if (row == null)
            {
                return null;
            }
            var show = new BShow(requestLog.Single(m => m.ID == id));

            show.Field(nameof(Model.ID), "编号");
            //show.Field(nameof(RequestLog.Content), Admin.Trans("text"));
            show.Field(nameof(Model.UI_Content), "数据");

            show.Field(nameof(Model.CreatedAt), "录入时间");
            //show.Field(nameof(RequestLog.UpdatedAt));
            show.Panel.Tools(tools =>
            {
                tools.DisableEdit();
            });
            return show;
        }
    }

}
