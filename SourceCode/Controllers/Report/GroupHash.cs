﻿using System;
using System.Linq;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model = SML.Models.Report.GroupHash;
using ReporterContext = SML.EF.Report;

namespace SML.Controllers.Report
{
    public class GroupHash : AdminController<Model>
    {
        private ReporterContext ReporterContext => DbContext as ReporterContext;
        private ILogger<GroupHash> Log;
        public GroupHash(ReporterContext reporterContext, ILogger<GroupHash> Log) : base()
        {
            DbContext = reporterContext;
            this.Log = Log;
            Title = "上报错误列表";
        }

        protected override BGrid Grid()
        {
            var hash = ReporterContext.GroupHashes.OrderByDescending(o => o.UpdatedAt);

            var grid = new BGrid<Model>(hash, HttpContext);
            var a = Utils;
            grid.Column(nameof(Model.ID), "编号").Sortable().MinWidth(60);
            grid.Column(nameof(Model.Description), "简要描述").MaxWidth(500);

            grid.Column(nameof(Model.Software), "软件").Display((m) => $"<span style=\"color:{m.State switch { Model.States.Unresolved => "red", Model.States.Resolved => "green", Model.States.Shelve => "#85139c", _ => "black" }}\">{m.Software}</span>").MinWidth(300);

            grid.Column(nameof(Model.Count), "次数").MinWidth(60);

            grid.Column(nameof(Model.State), "状态").EditableOptions(Model.States.Unknown, Model.States.Solved).MinWidth(70);

            //grid.Column(nameof(GroupHash.State), "状态").Editable.Select("{'1':‘xxx’,‘2’，‘3’}");

            //var items = Sy.Enum.GetItems<GroupHash.States>();
            //var xxx = items.ToDictionary(x => x.GetDescription(), x => (object)(int)x.Enum);
            //var options = Enum.GetValues(typeof(GroupHash.States));
            //grid.Column(nameof(GroupHash.State), "状态").Editable.Select(xxx);

            //grid.Column(nameof(GroupHash.State), "状态").Editable.Select<GroupHash.States>();//enum => x.ToString()

            //grid.Column(nameof(GroupHash.State), "状态").Editable.Select<GroupHash.States>(enum => x.GetDescription());

            grid.Column(nameof(Model.UILastFileLength), "所属文件长度").MinWidth(100).Center();
            grid.Column(nameof(Model.LastIPValue), "所属IP").Label().MinWidth(125);
            grid.Column(nameof(Model.CreatedAt), "首次出现时间").MinWidth(145);
            grid.Column(nameof(Model.UpdatedAt), "最近更新时间").MinWidth(145);

            grid.Actions((actions) =>
            {
                actions.DisableEdit().DisableView().GetColumn().MinWidth(70);
                actions.Prepend($"<a href=\"/GroupHash/Details/{actions.GetKey()}\" target=\"_blank\"><i class=\"fa fa-eye\"></i></a>");
            });

            grid.Tools((tools) => { });

            return grid;
        }


        protected override BForm Form()
        {
            var hash = ReporterContext.GroupHashes;
            var form = new BForm(hash).WithController(this);
            return form;
        }

        [HttpGet("{id:int}")]
        [HttpGet("[action]/{id:int}")]
        public IActionResult Details(int id)
        {
            var groupHash = ReporterContext.GroupHashes.SingleOrDefault(m => m.ID == id);
            if (groupHash == null)
            {
                return NotFound();
            }
            else
            {
                return View(groupHash);
            }
        }

        [Route("/[controller]/{id:int}/[action]/{state}")]
        public IActionResult ChangeState(int id, Model.States state)
        {
            var groupHash = ReporterContext.GroupHashes.SingleOrDefault(m => m.ID == id);
            if (groupHash == null)
            {
                return NotFound();
            }

            groupHash.State = state;
            ReporterContext.SaveChanges();
            return Redirect("/GroupHash/Details/" + id);
        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }
    }
}