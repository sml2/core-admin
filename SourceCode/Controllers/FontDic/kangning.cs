﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SML.Controllers.FontDic
{
    [Route("{Controller}")]
    [ApiController]
    public class Kangning : ControllerBase
    {
        private const int MaxSize = 8388608;
        private readonly IWebHostEnvironment env;
        public Kangning(IWebHostEnvironment env) {
            this.env = env;
        }
        [Route("action.php")]
        public object Index([FromQuery] string act, [FromQuery] string font)
        {
            var contentRootFileProvider = env.WebRootFileProvider;
            
            switch (act)
            {
                case "delimg":
                    if (Request.HasFormContentType) {
                        var filename = Request.Form["imagename"].ToString().Replace(@"..","");
                        Console.WriteLine($"delimg:{filename}");
                        var FilePath = Path.Combine(nameof(Kangning).ToLower(), @"files", filename);
                        var FileInfo = contentRootFileProvider.GetFileInfo(FilePath);
                        if (FileInfo.Exists && !FileInfo.IsDirectory) {
                            try
                            {
                                //System.IO.File.Delete(FileInfo.PhysicalPath);
                                var Dic_Del = $"{Path.GetDirectoryName(FileInfo.PhysicalPath)}_del";
                                var Target = Path.Combine(Dic_Del, Path.GetFileName(FileInfo.PhysicalPath));
                                Directory.CreateDirectory(Dic_Del);
                                System.IO.File.Move(FileInfo.PhysicalPath, Target);
                                return "1";
                            }
                            catch (Exception e)
                            {
                                return e.Message;
                            }
                        }
                    }
                    return @"删除失败.";
                case "searchimg":
                    Console.WriteLine($"searchimg:{font}");
                    List<string> ArrayList = new();
                    var Dic = Path.Combine(nameof(Kangning).ToLower(), @"files", font);
                    var DirectoryContents = contentRootFileProvider.GetDirectoryContents(Dic);
                    if (DirectoryContents.Exists) {
                        foreach (var Filse in DirectoryContents)
                        {
                            ArrayList.Add(Filse.Name);
                        }
                    }
                    return ArrayList;
                case "upload":
                    if (Request.HasFormContentType)
                    {
                        font = Request.Form["myfont"].ToString().Replace(@"..", "");
                        Console.WriteLine($"upload:{font}");
                        var File = Request.Form.Files["mypic"];
                        if (null != File) {
                            if (File.Length > MaxSize) {
                                return "图片大小不能超过8M";
                            }
                            string[] AllowExts= { "png", "jpg", "jpeg", "bmp","gif" };
                            var Ext = Path.GetExtension(File.FileName).ToLower();
                            if (AllowExts.Contains(Ext.Trim('.')))
                            {
                                var Name = $"{Sy.DataTimeFormat.Fromat_yyyyMMddHHmmssffff()}{Ext}";
                                var SubPath = Path.Combine(font, Name);
                                var FilePath = Path.Combine(env.WebRootPath, nameof(Kangning).ToLower(), @"files", SubPath);
                                var DirPath = Path.GetDirectoryName(FilePath);
                                Directory.CreateDirectory(DirPath);
                                if (!System.IO.File.Exists(FilePath))
                                {
                                    try
                                    {
                                        using (var stream = System.IO.File.OpenWrite(FilePath))
                                        {
                                            File.CopyTo(stream);
                                        }
                                        return new {name = File.FileName, pic = Name, size = File.Length, font };
                                    }
                                    catch (Exception e)
                                    {
                                        return e.Message;
                                    }
                                }
                                else {
                                    return $"文件[{FilePath}]已存在！";
                                }
                            }
                            else {
                                return $"错误的图片格式[{Ext}]！";
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            return "[]";
        }
    }
}
