﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SML.Models;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using SML.Context.Migrations_Tool;
using SML.Models.Tool;

namespace SML.Controllers.Tool
{ 
    /// <summary>
    /// 简易记事本
    /// </summary>
    public class NoteController : AdminController<Note>
    {
        private CoreAdmin.EF.Tool Context => DbContext as CoreAdmin.EF.Tool;


        public NoteController(CoreAdmin.EF.Tool Context) : base()
        {
            DbContext = Context;
            Title = "记事本";
        }


        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Notes;
            var grid = new BGrid(model, HttpContext);

            grid.Column(nameof(Note.ID)).Sortable();
            grid.Column(nameof(Note.Title)).Editable();
            grid.Column(nameof(Note.Content));

            //grid.Tools((tools) => { });

            return grid;
        }


        /*  创建和编辑UI */
        [HttpPost]
        protected override BForm Form()
        {
            var model = Context.Notes;
            var form = new BForm(model).WithController(this);
            form.Text(nameof(Note.Title));
            form.Textarea(nameof(Note.Content));

            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, Note model)
        {
            var test = await Context.Notes.SingleAsync(m => m.ID == id);
            test.Title = model.Title;
            test.Content = model.Content;

            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Notes;
            var show = new BShow(model.Single(m => m.ID == id), null, HttpContext);

            show.Field(nameof(Note.ID), "Id");
            show.Field(nameof(Note.Title));
            show.Field(nameof(Note.Content));

            return show;
        }
    }
}