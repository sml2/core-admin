﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SML.Models;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using PasswordModel = SML.Models.Tool.Password;

namespace SML.Controllers.Tool
{
    /// <summary>
    /// 密码本
    /// </summary>
    public class PasswordController : AdminController<PasswordModel>
    {
        private CoreAdmin.EF.Tool Context => DbContext as CoreAdmin.EF.Tool;

        public PasswordController(CoreAdmin.EF.Tool Context) : base()
        {
            DbContext = Context;
            Title = "密码本";
        }


        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Passwords;
            var grid = new BGrid(model, HttpContext);
            
            grid.Column(nameof(PasswordModel.ID)).Sortable().MinWidth(60);
            grid.Column(nameof(PasswordModel.Name)).Editable().MinWidth(60);
            grid.Column(nameof(PasswordModel.WebSite)).Editable().MinWidth(150);//.url() BBBBBBBBBug
            grid.Column(nameof(PasswordModel.WebSite_Backup)).Editable().MinWidth(150);
            grid.Column(nameof(PasswordModel.UserName)).Copyable().MinWidth(60);
            grid.Column(nameof(PasswordModel.Account)).Copyable().MinWidth(60);
            grid.Column(nameof(PasswordModel.PassWord)).Copyable().MinWidth(60);
            grid.Column(nameof(PasswordModel.Phone)).MinWidth(60); 
            grid.Column(nameof(PasswordModel.Email)).MinWidth(60); 
            grid.Column(nameof(PasswordModel.EmailVerify)).MinWidth(120);//.Switch() BBBBBBBBBug
            grid.Column(nameof(PasswordModel.LoginTime)).MinWidth(90); 
            grid.Column(nameof(PasswordModel.RegistTime)).MinWidth(90);
            grid.Column(nameof(PasswordModel.Description)).MinWidth(90);
            grid.Column(nameof(PasswordModel.CreatedAt)).MinWidth(90); 
            grid.Column(nameof(PasswordModel.UpdatedAt)).MinWidth(90); 

            //grid.Tools((tools) => { });

            return grid;
        }



        /// <summary>
        /// 创建和编辑UI 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        protected override BForm Form()
        {
            var model = Context.Passwords;
            var form = new BForm(model).WithController(this);
            form.Text(nameof(PasswordModel.Name));
            form.Furl(nameof(PasswordModel.WebSite));
            form.Furl(nameof(PasswordModel.WebSite_Backup));
            form.Text(nameof(PasswordModel.UserName));
            form.Text(nameof(PasswordModel.Account));
            form.Password(nameof(PasswordModel.PassWord));
            form.Text(nameof(PasswordModel.Phone));
            form.Text(nameof(PasswordModel.Email));
            form.Switch(nameof(PasswordModel.EmailVerify));
            form.Date(nameof(PasswordModel.LoginTime)).format("YYYY-MM-DD HH:mm:ss");
            form.Date(nameof(PasswordModel.RegistTime)).format("YYYY-MM-DD HH:mm:ss");
            form.Textarea(nameof(PasswordModel.Description));
            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, PasswordModel model)
        {

            //无法更新  原因未知
            //model.ID = id;
            //Context.Attach(model);
            //return await Form().Update(model);

            var test = await Context.Passwords.SingleAsync(m => m.ID == id);
            test.Name = model.Name;
            test.WebSite = model.WebSite;
            test.WebSite_Backup = model.WebSite_Backup;
            test.UserName = model.UserName;
            test.Account = model.Account;
            test.PassWord = model.PassWord;
            test.Phone = model.Phone;
            test.Email = model.Email;
            test.EmailVerify = model.EmailVerify;
            test.LoginTime = model.LoginTime;
            test.RegistTime = model.RegistTime;
            test.Description = model.Description;
            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Passwords;
            var show = new BShow(model.Single(m => m.ID == id),null,HttpContext);

            show.Field(nameof(PasswordModel.ID), "Id");
            show.Field(nameof(PasswordModel.Name));
            show.Field(nameof(PasswordModel.WebSite));
            show.Field(nameof(PasswordModel.WebSite_Backup));
            show.Field(nameof(PasswordModel.UserName));
            show.Field(nameof(PasswordModel.Account));
            show.Field(nameof(PasswordModel.PassWord));
            show.Field(nameof(PasswordModel.Phone));
            show.Field(nameof(PasswordModel.Email));
            show.Field(nameof(PasswordModel.EmailVerify));
            show.Field(nameof(PasswordModel.LoginTime));
            show.Field(nameof(PasswordModel.RegistTime));
            show.Field(nameof(PasswordModel.Description));
            show.Field(nameof(PasswordModel.CreatedAt));
            show.Field(nameof(PasswordModel.UpdatedAt));

            return show;
        }

    }
}