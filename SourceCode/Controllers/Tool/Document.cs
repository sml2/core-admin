﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using SML.Models.Tool;

namespace SML.Controllers.Tool
{
    /// <summary>
    /// 文档记事本[editorMd]
    /// </summary>
    public class DocumentController : AdminController<Document>
    {
        private CoreAdmin.EF.Tool Context => DbContext as CoreAdmin.EF.Tool;


        public DocumentController(CoreAdmin.EF.Tool Context) : base()
        {
            DbContext = Context;
            Title = "文档记事本";
        }


        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Documents;
            var grid = new BGrid(model, HttpContext);

            grid.Column(nameof(Document.ID)).Sortable();
            grid.Column(nameof(Document.Title)).Editable();
            grid.Column(nameof(Document.Content));

            //grid.Tools((tools) => { });

            return grid;
        }


        /*  创建和编辑UI */
        [HttpPost]
        protected override BForm Form()
        {
            var model = Context.Documents;
            var form = new BForm(model).WithController(this);

            form.Text(nameof(Document.Title));
            form.EditorMd(nameof(Document.Content));
            
            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, Note model)
        {
            var test = await Context.Documents.SingleAsync(m => m.ID == id);
            test.Title = model.Title;
            test.Content = model.Content;

            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Documents;
            var show = new BShow(model.Single(m => m.ID == id), null, HttpContext);

            show.Field(nameof(Document.ID), "Id");
            show.Field(nameof(Document.Title));
            show.Field(nameof(Document.Content));

            return show;
        }
    }
}