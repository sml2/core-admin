﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SML.Models;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using SML.Models.Tool;

namespace SML.Controllers.Tool
{
    /// <summary>
    /// 代码片段[Ace]
    /// </summary>
    public class CodeController : AdminController<Code>
    {
        private CoreAdmin.EF.Tool Context => DbContext as CoreAdmin.EF.Tool;


        public CodeController(CoreAdmin.EF.Tool Context) : base()
        {
            DbContext = Context;
            Title = "代码片段";
        }


        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Codes;
            var grid = new BGrid(model, HttpContext);

            grid.Column(nameof(Code.ID)).Sortable();
            grid.Column(nameof(Code.Title)).Editable();
            grid.Column(nameof(Code.Content));

            //grid.Tools((tools) => { });

            return grid;
        }


        /*  创建和编辑UI */
        [HttpPost]
        protected override BForm Form()
        {
            var model = Context.Codes;
            var form = new BForm(model).WithController(this);

            form.Text(nameof(Code.Title));
            form.Ace(nameof(Code.Content));

            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, Code model)
        {
            var test = await Context.Codes.SingleAsync(m => m.ID == id);
            test.Title = model.Title;
            test.Content = model.Content;

            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Codes;
            var show = new BShow(model.Single(m => m.ID == id), null, HttpContext);

            show.Field(nameof(Code.ID), "Id");
            show.Field(nameof(Code.Title));
            show.Field(nameof(Code.Content));

            return show;
        }
    }
}