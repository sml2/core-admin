﻿using System.Linq;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Model = CoreAdmin.Mvc.Models.UserModel;

namespace SML.Controllers.Demo
{
    [Route("demo/grid")]
    public class GridDemo : AdminController
    {
        private EF.Admin Context => DbContext as EF.Admin;

        public GridDemo()
        {
            Title = "IP统计";
        }

        protected override BGrid Grid()
        {
            var ip = Context.Users.OrderByDescending(o => o.UpdatedAt);

            var grid = new BGrid(ip, HttpContext);
            // 进度条
            grid.Column(nameof(Model.Id)).ProgressBar().MinWidth(60);
            // 方块标签
            grid.Column(nameof(Model.Name)).Label().MinWidth(125);
            // 圆角标签
            grid.Column(nameof(Model.Roles)).Badge();
            // 链接
            grid.Column(nameof(Model.RolesList)).Link("http://localhost:5000");
            // 图片
            grid.Column(nameof(Model.Avatar)).Image("http://localhost:5000", 50, 50);
            // 可复制
            grid.Column(nameof(Model.CreatedAt)).Copyable().MinWidth(145);
            grid.Column(nameof(Model.UpdatedAt)).MinWidth(145);

            grid.Actions((actions) =>
            {
                actions.DisableEdit();
                actions.DisableView();
            });

            grid.Tools((tools) => { });

            return grid;
        }


        protected override BForm Form()
        {
            var model = Context.Users;
            var form = new BForm(model).WithController(this);

            return form;
        }

        protected override BShow Detail(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}