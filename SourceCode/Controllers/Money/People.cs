﻿using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DBContext = SML.EF.Money;
using PeopleModel = SML.Models.Money.People;

namespace SML.Controllers.Money
{
    public class PeopleController : AdminController<PeopleModel>
    {
        private DBContext Context => DbContext as DBContext;

        public PeopleController(DBContext Context) : base()
        {
            DbContext = Context;
            Title = "债务人";
        }

        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Peoples;
            var grid = new BGrid(model, HttpContext);
            var a = Utils;

            grid.Column(nameof(PeopleModel.ID)).Sortable();
            grid.Column(nameof(PeopleModel.Name)).Copyable();
            grid.Column(nameof(PeopleModel.Job)).Editable().Copyable().MaxWidth(400);
            //BBBBBBBBBug
            //grid.Column(nameof(PeopleModel.Job)).Copyable().Editable().MaxWidth(400); 
            //数据会乱码<a href="javascript:void(0);" class="grid-column-copyable text-muted" data-content="姓名A" title="Copied!" data-placement="bottom"> <i class="fa fa-copy"></i> </a>&nbsp;姓名A
            grid.Column(nameof(PeopleModel.UTotal));
            grid.Column(nameof(PeopleModel.LastBorrow)).MinWidth(150);
            grid.Column(nameof(PeopleModel.LastRepay)).MinWidth(150);
            grid.Column(nameof(PeopleModel.CreatedAt)).MinWidth(150);
            grid.Column(nameof(PeopleModel.UpdatedAt)).MinWidth(150);

            grid.Actions(t => t.DisableView());
            grid.DisableExport();
            return grid;
        }

        /*  创建和编辑UI */
        protected override BForm Form()
        {
            var model = Context.Peoples;
            var form = new BForm(model).WithController(this);
            form.Text(nameof(PeopleModel.Name));
            form.Text(nameof(PeopleModel.Job));
            form.Tools(t => t.DisableView());
            form.Saved(() =>
            {
                //Context.Configs.AsNoTracking().ToList().CoverCache(_memoryCache);
                return null;
            });

            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, PeopleModel model)
        {
            var test = await Context.Peoples.SingleAsync(m => m.ID == id);
            test.Job = model.Job;
            test.Name = model.Name;
            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Peoples;
            var show = new BShow(model.Single(p => p.ID == id));
            show.Field(nameof(PeopleModel.ID));
            show.Field(nameof(PeopleModel.Name));
            show.Field(nameof(PeopleModel.Job));
            show.Field(nameof(PeopleModel.Total));
            show.Field(nameof(PeopleModel.LastBorrow));
            show.Field(nameof(PeopleModel.LastRepay));
            show.Field(nameof(PeopleModel.CreatedAt));
            show.Field(nameof(PeopleModel.UpdatedAt));
            return show;
        }
    }
}