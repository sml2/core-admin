﻿using CoreAdmin.Mvc;
using System.Collections.Generic;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DBContext = SML.EF.Money;
using DetailModel = SML.Models.Money.Detail;
using System;

namespace SML.Controllers.Money
{
    public class DetailController : AdminController<DetailModel>
    {
        private DBContext Context => DbContext as DBContext;

        public DetailController(DBContext Context) : base()
        {
            DbContext = Context;
            Title = "账单明细";
        }


        /*  列表数据 */

        protected override BGrid Grid()
        {
            var model = Context.Details;
            var grid = new BGrid(model, HttpContext);
            var a = Utils;

            grid.Column(nameof(DetailModel.ID)).Sortable();
            grid.Column(nameof(DetailModel.U_Name)).Copyable();
            grid.Column(nameof(DetailModel.UAmount)).Copyable();
            grid.Column(nameof(DetailModel.CreatedAt));
            grid.Column(nameof(DetailModel.UpdatedAt));

            grid.Actions(t => t.DisableView());
            grid.DisableExport();
            return grid;
        }

        /*  创建和编辑UI */
        protected override BForm Form()
        {
            var model = Context.Details;
            var form = new BForm(model).WithController(this);
            form.Editing(f =>
            {
                var d = f.Original as DetailModel;
                d.U_Name = d.PeopleID.ToString();
            });

            form.Select(nameof(DetailModel.U_Name)).Options(Context.Peoples.ToDictionary(p => p.ID.ToString(), p => p.Name));
            form.Switch(nameof(DetailModel.Amount));
            form.Number(nameof(DetailModel.Amount));
            form.Tools(t => t.DisableView());
            form.Saving(o =>
            {
                var d = o.Original as DetailModel;
                if (d != null && int.TryParse(d.U_Name, out int id))
                {
                    //应该判断NULL
                    d.People = Context.Peoples.Single(p => p.ID == id);
                    d.People.Total += d.Amount;
                }
                else
                {
                    throw new ArgumentNullException("ID");
                }
                return null;
            });
            //form.Saved(() =>
            //{
            //    //Context.Configs.AsNoTracking().ToList().CoverCache(_memoryCache);
            //    return null;
            //});
            form.Deleting(ids =>
            {
                foreach (var id in ids)
                {
                    var Detail = Context.Details.Include(m => m.People).Single(m => m.ID == id);
                    Detail.People.Total -= Detail.Amount;
                }
                return null;
            });
            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, DetailModel input)
        {
            var test = await Context.Details.Include(m => m.People).SingleAsync(m => m.ID == id);
            if (int.TryParse(input.U_Name, out int pid))
            {
                test.People.Total -= test.Amount;
                if (test.People.ID != pid)
                {
                    test.People = Context.Peoples.Single(p => p.ID == pid);
                }
                test.People.Total += input.Amount;
                test.Amount = input.Amount;
            }
            else
            {
                throw new ArgumentException(nameof(input.ID));
            }
            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Details;
            var show = new BShow(model.Single(p => p.ID == id));
            show.Field(nameof(DetailModel.ID));
            show.Field(nameof(DetailModel.U_Name));
            show.Field(nameof(DetailModel.UAmount));
            show.Field(nameof(DetailModel.Amount));
            show.Field(nameof(DetailModel.CreatedAt));
            show.Field(nameof(DetailModel.UpdatedAt));
            return show;
        }
    }
}