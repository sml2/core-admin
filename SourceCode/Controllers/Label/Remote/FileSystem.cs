﻿using CoreAdmin.Mvc.Layout;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model = SML.Controllers.Label.Remote.FileInfo;
using SML.Models;
using Newtonsoft.Json;
using SML.Controllers.LabelRelations;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.FileProviders;
using SML.Controllers.Label.Remote;

namespace SML.Controllers.Label
{
    public class FileSystem : AdminController
    {
        private FileProvider Fp { get; }
        public FileSystem(FileProvider fileProvider)
        {
            Fp = fileProvider;
            var _ = Fp;
            Title = "RemoteFileSystem";
        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }

        //[HttpPost]
        //[HttpPut]
        //protected override BForm Form()
        //{
        //    var labelsModels = _labelsContext.Labels;
        //    var form = new BForm(labelsModels).WithController(this);
        //    //form.SetAction("/labels/store");
        //    form.Text(nameof(Model.Name), "名称").Required();
        //    form.Text(nameof(Model.Remark), "备注");
        //    return form;
        //}

        //        [HttpPost]
        //        protected override BGrid Grid()
        //        {
        //            content.FullScreenSwitch();
        //            var model = _labelsContext.Labels;
        //            //var TData = model.Where(m => m.ID > 10);

        //            var Grid = new BGrid(model, HttpContext);

        //            Grid.Column(nameof(Model.ID), "ID");
        //            Grid.Column(nameof(Model.Name),"名称");
        //            Grid.Column(nameof(Model.RefCount), "引用次数");
        //            Grid.Column(nameof(Model.Remark), "备注");
        //            Grid.Column(nameof(Model.CreatedAt), "创建时间");
        //            Grid.Column(nameof(Model.UpdatedAt), "更新时间");
        //            Grid.Actions(actions =>
        //            {
        //                actions.DisableView();
        //                actions.DisableEdit();
        //                actions.Append($@"　<a href=""Labels/edit/{actions.GetKey()}""><i class=""fa fa-edit""></i></a>　");
        //                actions.Append($@"<a href=""Labels/relations/{actions.GetKey()}/create""><i class=""fa fa-plus""></i></a>　");
        //                actions.Append($@"<a href=""Labels/relations/{actions.GetKey()}""><i class=""fa fa-binoculars""></i></a>　");
        //                actions.Append($@"<a href=""Labels/D3relations/{actions.GetKey()}""><i class=""fa fa-transgender-alt""></i></a>　");
        //            });
        //            return Grid;
        //        }
        //        /// <summary>
        //        /// 新增
        //        /// </summary>
        //        /// <param name="model"></param>
        //        /// <returns></returns>
        //        [HttpPost]
        //        public IActionResult Index([Bind("Name,Remark")] Models.Label model)
        //        {
        //            return Form().Store(model);
        //        }

        //        [HttpPost("{id:int}")]
        //        public async Task<IActionResult> Update(int id, [Bind("Name,Remark")] Models.Label model)
        //        {
        //            var labels = await _labelsContext.Labels.SingleAsync(m => m.ID == id);
        //            labels.UpdatedAt = DateTime.Now;
        //            labels.Name = model.Name;
        //            labels.Remark = model.Remark;
        //            return Form().Update(labels);
        //        }

        //        [HttpPost]
        //        [Route("labels/[action]")]
        //        public dynamic Store([Bind("Name,Remark")] Models.Label model)
        //        {;
        //            return Form().Store(model);
        //            //return Redirect("/labels");
        //        }

        //        [HttpDelete("{id}")]
        //        public async override Task<IActionResult> Destroy(string id)
        //        {
        //            var relationModel = _labelsContext.LabelRelations;
        //            var labelsModel = _labelsContext.Labels;
        //            //清除删除数据引用数,查询引用id,进行减1操作
        //            var a = id.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k));
        //            foreach (var b in a) {
        //                var list = new List<int>();
        //                var Tdata = relationModel.Where(m => m.FromLabels == b || m.ToLabels == b).ToList();
        //                Tdata.ForEach(m =>
        //                {
        //                    list.Add(m.FromLabels);
        //                    list.Add(m.ToLabels);

        //                });
        //                list = list.Distinct().ToList();
        //                list.Remove(b);
        //                foreach (var c in list) {
        //                    var labelsModelsData = labelsModel.Where(m => m.ID == c).ToList();
        //                    foreach (var data in labelsModelsData)
        //                    {
        //                        data.RefCount--;
        //                    }
        //                }
        //            }
        //            return await Form().Destroy(id.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k)));
        //        }

        //        [Route("[action]/{id}")]
        //        public async Task<IActionResult> Relations(int id, [FromServices] Content content)
        //        {

        //            var model = _labelsContext.Labels;
        //            Title = "【" + model.Where(m => m.ID == id).FirstOrDefault().Name + "】的关联标签";
        //            var relationModel = _labelsContext.LabelRelations;
        ///**
        //            //var m1 = new Labels()
        //            //{
        //            //    Name = "test",
        //            //    Remark = "Test",
        //            //    //LabelRelationsTid1Navigation = new List<Labels>() {
        //            //    //    new () {Name = "test1", Remark="test1"},
        //            //    //    new () {Name = "test2", Remark="test2"},
        //            //    //}
        //            //};
        //            ////model.Add(m1);
        //            //var m2 = new Labels() { Name = "test2", Remark = "test2" };
        //            ////model.Add(m2);
        //            //((Context)DbContext).Add(new LabelRelations() { FromLabelsNavigation = m1, ToLabelsNavigation = m2 });
        //            //DbContext.SaveChanges();

        //*/
        //            var list = new List<int>();
        //            var Tdata = relationModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            Tdata.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);

        //            });
        //            list = list.Distinct().ToList();
        //            list.Remove(id);

        //            var RData =
        //                model.Where(m => list.Contains(m.ID));
        //            var Grid = new BGrid(RData, HttpContext);

        //            /** 
        //             * var data = model.Include(m => m.LabelRelationsFromLabelsNavigation).Include(m => m.LabelRelationsToLabelsNavigation).Where(m => m.ID == 6).FirstOrDefault();
        //             * var data = model.Include(m => m.LabelRelationsFromLabelsNavigation).Include(m => m.LabelRelationsToLabelsNavigation).Where(m => m.ID == id);
        //             * var Grid = new BGrid(data);
        //            */

        //            Grid.Column(nameof(Model.Name), "关联标签");
        //            Grid.Actions(actions =>
        //            {
        //                actions.DisableView();
        //                actions.DisableEdit();
        //            });

        //                await content.WithController(this)
        //                .Title(Title)
        //                .Description(Description.ContainsKey("index") ? Description["index"] : CoreAdmin.Mvc.Admin.Trans("list"))
        //                .Body(Grid);


        //            return content.Render();
        //        }

        //        [Route("[action]/{id?}")]
        //        public async Task<IActionResult> D3Relations(int? id=null)
        //        {
        //            var LabelsModel = _labelsContext.Labels;
        //            var LabelRelationsModel = _labelsContext.LabelRelations;

        //            var list = new List<int>();
        //            var list_two = new List<int>();
        //            List<Relation> LabelRelationsModelData = default;
        //            List<Relation> LabelRelationsModelDataTwo = default;
        //            if (null == id)
        //            {
        //                LabelRelationsModelData = LabelRelationsModel.ToList();
        //            }
        //            else {
        //                LabelRelationsModelData = LabelRelationsModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            }

        //            LabelRelationsModelData.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);
        //            });

        //            list = list.Distinct().ToList();

        //            IQueryable<Models.Label> LabelsData = default;
        //            if (null == id) {
        //                LabelsData = LabelsModel;
        //                LabelRelationsModelDataTwo = LabelRelationsModelData;
        //            }
        //            else {
        //                LabelRelationsModelDataTwo = LabelRelationsModel.Where(m => list.Contains(m.FromLabels) && list.Contains(m.ToLabels)).ToList();
        //                LabelsData = LabelsModel.Where(m => list.Contains(m.ID));
        //            }

        //            //  { source : 0 , target: 1 } ,
        //            var UiLabelRelationsModelData = LabelRelationsModelDataTwo.Select(m => new { source = m.FromLabels, target = m.ToLabels }).ToList();
        //            var UiLabelRelationsModelDataStr = JsonConvert.SerializeObject(UiLabelRelationsModelData);

        //            var UiLabelData = LabelsData.Select(m => new { name = m.Name, id = m.ID,count = m.RefCount}).ToList();
        //            //[
        //            //   { name: "桂林" , id: 0 }
        //            //]
        //            var UiLabelDataStr = JsonConvert.SerializeObject(UiLabelData);

        //            if (UiLabelDataStr == null || UiLabelRelationsModelDataStr == null)
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                ViewData["UiLabelDataStr"] = UiLabelDataStr;
        //                ViewData["UiLabelRelationsModelDataStr"] = UiLabelRelationsModelDataStr;
        //                if (null == id)
        //                {
        //                    ViewData["Title"] = "关 系 图　　　　";
        //                }else {
        //                    ViewData["Title"] = "【" + LabelsModel.Where(m => m.ID == id).FirstOrDefault().Name + "】的 关 系 图　　";
        //                }
        //                return View("D3LabelToLableRelations");
        //            }
        //        }

        //        [Route("[action]/{id?}")]
        //        public async Task<IActionResult> D3Relations1(int? id = null)
        //        {
        //            var LabelsModel = _labelsContext.Labels;
        //            var LabelRelationsModel = _labelsContext.LabelRelations;

        //            var list = new List<int>();
        //            var list_two = new List<int>();
        //            List<Relation> LabelRelationsModelData = default;
        //            List<Relation> LabelRelationsModelDataTwo = default;
        //            if (null == id)
        //            {
        //                LabelRelationsModelData = LabelRelationsModel.ToList();
        //            }
        //            else
        //            {
        //                LabelRelationsModelData = LabelRelationsModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            }

        //            LabelRelationsModelData.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);
        //            });

        //            list = list.Distinct().ToList();

        //            IQueryable<Models.Label> LabelsData = default;
        //            if (null == id)
        //            {
        //                LabelsData = LabelsModel;
        //                LabelRelationsModelDataTwo = LabelRelationsModelData;
        //            }
        //            else
        //            {
        //                LabelRelationsModelDataTwo = LabelRelationsModel.Where(m => list.Contains(m.FromLabels) && list.Contains(m.ToLabels)).ToList();
        //                LabelsData = LabelsModel.Where(m => list.Contains(m.ID));
        //            }

        //            //  { source : 0 , target: 1 } ,
        //            var UiLabelRelationsModelData = LabelRelationsModelDataTwo.Select(m => new { source =  m.FromLabels.ToString(), target = m.ToLabels.ToString() }).ToList();
        //            var UiLabelRelationsModelDataStr = JsonConvert.SerializeObject(UiLabelRelationsModelData);

        //            var UiLabelData = LabelsData.Select(m => new { name = m.Name, id = m.ID.ToString(), count = m.RefCount,scale = 1, image =  "https://dss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=3775999286,734565944&fm=58" }).ToList();
        //            //[
        //            //   { name: "桂林" , id: 0 }
        //            //]
        //            var UiLabelDataStr = JsonConvert.SerializeObject(UiLabelData);

        //            if (UiLabelDataStr == null || UiLabelRelationsModelDataStr == null)
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                ViewData["UiLabelDataStr"] = UiLabelDataStr;
        //                ViewData["UiLabelRelationsModelDataStr"] = UiLabelRelationsModelDataStr;
        //                if (null == id)
        //                {
        //                    ViewData["Title"] = "关 系 图　　　　";
        //                }
        //                else
        //                {
        //                    ViewData["Title"] = "【" + LabelsModel.Where(m => m.ID == id).FirstOrDefault().Name + "】的 关 系 图　　";
        //                }
        //                return View("D3LabelToLableRelations1");
        //            }
        //        }

        //        [Route("[action]/{id?}")]
        //        public async Task<IActionResult> D3Relations2(int? id = null)
        //        {
        //            var LabelsModel = _labelsContext.Labels;
        //            var LabelRelationsModel = _labelsContext.LabelRelations;

        //            var list = new List<int>();
        //            var list_two = new List<int>();
        //            List<Relation> LabelRelationsModelData = default;
        //            List<Relation> LabelRelationsModelDataTwo = default;
        //            if (null == id)
        //            {
        //                LabelRelationsModelData = LabelRelationsModel.ToList();
        //            }
        //            else
        //            {
        //                LabelRelationsModelData = LabelRelationsModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            }

        //            LabelRelationsModelData.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);
        //            });

        //            list = list.Distinct().ToList();

        //            IQueryable<Models.Label> LabelsData = default;
        //            if (null == id)
        //            {
        //                LabelsData = LabelsModel;
        //                LabelRelationsModelDataTwo = LabelRelationsModelData;
        //            }
        //            else
        //            {
        //                LabelRelationsModelDataTwo = LabelRelationsModel.Where(m => list.Contains(m.FromLabels) && list.Contains(m.ToLabels)).ToList();
        //                LabelsData = LabelsModel.Where(m => list.Contains(m.ID));
        //            }

        //            //  { source : 0 , target: 1 } ,
        //            var UiLabelRelationsModelData = LabelRelationsModelDataTwo.Select(m => new { source = m.FromLabels.ToString(), target = m.ToLabels.ToString() }).ToList();
        //            var UiLabelRelationsModelDataStr = JsonConvert.SerializeObject(UiLabelRelationsModelData);

        //            var UiLabelData = LabelsData.Select(m => new { name = m.Name, id = m.ID.ToString(), count = m.RefCount, scale = 1, image = "https://dss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=3775999286,734565944&fm=58" }).ToList();
        //            //[
        //            //   { name: "桂林" , id: 0 }
        //            //]
        //            var UiLabelDataStr = JsonConvert.SerializeObject(UiLabelData);

        //            if (UiLabelDataStr == null || UiLabelRelationsModelDataStr == null)
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                ViewData["UiLabelDataStr"] = UiLabelDataStr;
        //                ViewData["UiLabelRelationsModelDataStr"] = UiLabelRelationsModelDataStr;
        //                if (null == id)
        //                {
        //                    ViewData["Title"] = "关 系 图　　　　";
        //                }
        //                else
        //                {
        //                    ViewData["Title"] = "【" + LabelsModel.Where(m => m.ID == id).FirstOrDefault().Name + "】的 关 系 图　　";
        //                }
        //                return View("D3LabelToLableRelations2");
        //            }
        //        }


        //        /// <summary>
        //        /// 关联标签的添加页面
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <param name="content"></param>
        //        /// <returns></returns>
        //        [Route("relations/{id}/[action]")]
        //        public async Task<IActionResult> Create(int id, [FromServices] Content content)
        //        {
        //            var model = _labelsContext.Labels;

        //            Title = "为【" + model.Where(m => m.ID == id).FirstOrDefault().Name + "】标签添加新的关联";

        //            content.WithController(this)
        //               .Title(Title)
        //               .Description(Description.ContainsKey("index") ? Description["index"] : CoreAdmin.Mvc.Admin.Trans("list"))
        //               .Body(LabelRelationForm(id));


        //            return content.Render();
        //        }

        //        /// <summary>
        //        /// Create 方法的表格部分内容
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <returns></returns>
        //        [NonAction]
        //        public BForm LabelRelationForm(int id)
        //        {
        //            var model = _labelsContext.Labels;
        //            var relationModel = _labelsContext.LabelRelations;
        //            var labelsModels = _labelsContext.Labels.Where(m => m.ID == id);
        //            var currentModel = labelsModels.SingleOrDefault();
        //            var form = new BForm(_labelsContext.LabelRelations).WithController(this);
        //            // 过滤已有关联
        //            var list = new List<int>();
        //            var Tdata = relationModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            Tdata.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);

        //            });
        //            list = list.Distinct().ToList();

        //            var options = model.Where(m => !list.Contains(m.ID)).Where(m => m.ID != id).Select(r => KeyValuePair.Create(r.ID.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value);
        //            //var options = model.Where(m => !list.Contains(m.ID)).Select(r => KeyValuePair.Create(r.ID.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value);
        //            //var options = model.Where(m => m.ID != id).Select(r => KeyValuePair.Create(r.ID.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value);
        //            form.SetAction("/labels/relations/" + id + "/store");
        //            form.Text(nameof(Relation.FromLabels), "被关联标签").Default(currentModel.Name).Disable();
        //            form.MultipleSelect(nameof(Relation.UIToLabels), "添加新的关联标签").Options(options);
        //            return form;
        //        }

        //        /// <summary>
        //        /// 处理 新增关联标签 请求
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <param name="model"></param>
        //        /// <returns></returns>
        //        [HttpPost("relations/{id}/[action]")]
        //        public async Task<IActionResult> Store(int id, [Bind("UIToLabels")] Relation model)
        //        {
        //            var uiLabels = model.UIToLabels.Where(m => !string.IsNullOrEmpty(m)).Select(m => Convert.ToInt32(m)).ToList();
        //            var currentModel = _labelsContext.Labels.Where(m => m.ID == id).SingleOrDefault();
        //            var relationModels = _labelsContext.Labels.Where(m => uiLabels.Contains(m.ID)).ToList();
        //            //relationModels.ForEach(currentModel.LabelRelationsToLabelsNavigation.Add);
        //            var res = relationModels.Select(r => new Relation() { FromLabelsNavigation = currentModel, ToLabelsNavigation = r });
        //            var labelsModel = _labelsContext.Labels;
        //            //过滤已存在数据  TODO


        //            // 对引用次数进行计算 TODO
        //            var Tdata = new List<int>();
        //            Tdata.Add(id);
        //            Tdata.AddRange(uiLabels);
        //            var labelsData = labelsModel.Where(m => Tdata.Contains(m.ID)).ToList();
        //            // 这里注意引用次数的计算
        //            foreach (var data in labelsData)
        //            {
        //                if (data.ID == id) {
        //                    data.RefCount += labelsData.Count() - 1;
        //                }
        //                else {
        //                    data.RefCount++;
        //                }
        //            }

        //            _labelsContext.LabelRelations.AddRange(res);
        //            _labelsContext.SaveChanges();

        //            CoreAdmin.Mvc.Admin.Toastr(CoreAdmin.Mvc.Admin.Trans("save_succeeded"));
        //            return Redirect("/labels/relations/" + id);

        //        }

        //        /// <summary>
        //        /// 删除/批量删除关联标签
        //        /// </summary>
        //        /// <param name="LabelsA"></param>
        //        /// <param name="LabelsB"></param>
        //        /// <returns></returns>
        //        [HttpDelete("relations/{LabelsA}/{LabelsB}")]
        //        public IActionResult DeleteLabelRelations(int LabelsA, string LabelsB)
        //        {
        //            var relationModel = _labelsContext.LabelRelations;
        //            var labelsModel = _labelsContext.Labels;
        //            var a = LabelsB.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k));

        //            foreach (var b in a)
        //            {
        //                var ids = relationModel
        //                    .Where(m => (m.FromLabels == LabelsA && m.ToLabels == b) || (m.FromLabels == b && m.ToLabels == LabelsA)).Distinct(m => m.ID).ToList();
        //                // 删除引用次数
        //                var labelsModelsData = labelsModel.Where(m => m.ID == LabelsA || m.ID == b).ToList();
        //                foreach (var data in labelsModelsData)
        //                {
        //                    data.RefCount--;
        //                }
        //                relationModel.RemoveRange(ids);
        //                _labelsContext.SaveChanges();
        //            }
        //            return Redirect("/labels/relations/" + LabelsA);
        //        }
    }
}
