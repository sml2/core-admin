﻿using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SML.Controllers.Label.Remote
{
    public class FileProvider : IFileProvider
    {
        public IDirectoryContents GetDirectoryContents(string subpath)
        {
            return new DirectoryContents();
        }

        public IFileInfo GetFileInfo(string subpath)
        {
            return new FileInfo(subpath);
        }

        public IChangeToken Watch(string filter)
        {
            throw new NotImplementedException();
        }
    }
    public class DirectoryContents : IDirectoryContents
    {
        public bool Exists => true;

        public IEnumerator<IFileInfo> GetEnumerator()
        {
            var lst = new List<FileInfo>();
            lst.Add(new FileInfo("DirectoryRRNNDD36.cs"));
            return lst.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    public class FileInfo : IFileInfo
    {
        private string subpath;

        public FileInfo(string subpath)
        {
            this.subpath = subpath;
        }

        public bool Exists => true;

        public bool IsDirectory => false;

        public DateTimeOffset LastModified => DateTimeOffset.Now;

        public long Length => 22222;

        public string Name => $"Sml2's file Name {subpath}";

        public string PhysicalPath => $"Remote PhysicalPath {subpath}";

        public Stream CreateReadStream()
        {
            throw new NotImplementedException();
        }
    }

}
