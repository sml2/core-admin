﻿using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Model = SML.Models.Relation;
using Microsoft.EntityFrameworkCore;
using CoreAdmin.Mvc.Grid.Tools;

namespace SML.Controllers.LabelRelations
{
    public class LabelRelations : AdminController
    {
        private EF.Label LabelsContext { get => DbContext as EF.Label; }
        public LabelRelations(EF.Label labelsContext)
        {
            Title = "LabelRelations";

            DbContext = labelsContext;
        }

        [HttpPost]
        protected override BForm Form()
        {
            var labelRelationsModels = LabelsContext.LabelRelations;
            var form = new BForm(labelRelationsModels).WithController(this);
            //form.SetAction("/labels/store");
            form.Text(nameof(Model.FromLabels), "名称");
            form.Text(nameof(Model.ToLabels), "备注");
            return form;
        }

        [HttpPost]
        protected override BGrid Grid()
        {
            var model = LabelsContext.LabelRelations;

            var Grid = new BGrid(model.Include(m => m.FromLabelsNavigation).Include(m => m.ToLabelsNavigation), HttpContext);

            Grid.Tools((Fullscreens) => {
                Fullscreens.Append(new BatchActions());
            } );

            Grid.Column(nameof(Model.ID), "Id").MinWidth(60);
            Grid.Column(nameof(Model.FromLabels), "标签1").MinWidth(60).Display<int>((v, m) => $"【{(m as Model).FromLabelsNavigation.Name}】（{v}）");
            Grid.Column(nameof(Model.ToLabels), "标签2").MinWidth(60).Display<int>((v, m) => $"【{(m as Model).ToLabelsNavigation.Name}】（{v}）");
            Grid.Column(nameof(Model.CreatedAt), "时间").MinWidth(60);
            Grid.Actions(actions =>
            {
            actions.DisableView();
            actions.DisableEdit();
            actions.DisableDelete();
            //actions.Append($@"<a href=""Labels/relations/{actions.GetKey()}""><i class=""fa fa-plus""></i></a>");
            });
            return Grid;
        }

        protected string GetLabelsName(int id ) {
            var labelsModel = LabelsContext.Labels;

             string name = labelsModel.Where(m => m.ID == id).First().Name;
            return name;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<IActionResult> Index([Bind("Name,Remark")] Models.Label model)
        {
            //model.RefCount = 0;
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            return Form().Store(model);
        }

        protected override BShow Detail(int id)
        {
            throw new NotImplementedException();
        }

        //        [HttpPost]
        //        [Route("labels/[action]")]
        //        public dynamic Store([Bind("Name,Remark")] Models.Label model)
        //        {
        //            model.RefCount = 0;
        //            model.CreatedAt = DateTime.Now;
        //            model.UpdatedAt = DateTime.Now;
        //            return Form().Store(model);
        //            //return Redirect("/labels");
        //        }

        //        [HttpDelete("{id}")]
        //        public async override Task<IActionResult> Destroy(string id)
        //        {
        //            var relationModel = _labelsContext.LabelRelations;
        //            var labelsModel = _labelsContext.Labels;
        //            //清除删除数据引用数,查询引用id,进行减1操作
        //            var a = id.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k));
        //            foreach (var b in a) {
        //                var list = new List<int>();
        //                var Tdata = relationModel.Where(m => m.FromLabels == b || m.ToLabels == b).ToList();
        //                Tdata.ForEach(m =>
        //                {
        //                    list.Add(m.FromLabels);
        //                    list.Add(m.ToLabels);

        //                });
        //                list = list.Distinct().ToList();
        //                list.Remove(b);
        //                foreach (var c in list) {
        //                    var labelsModelsData = labelsModel.Where(m => m.Id == c).ToList();
        //                    foreach (var data in labelsModelsData)
        //                    {
        //                        data.RefCount--;
        //                    }
        //                }
        //            }
        //            return await Form().Destroy(id.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k)));
        //        }

        //        [Route("[action]/{id}")]
        //        public async Task<IActionResult> Relations(int id, [FromServices] Content content)
        //        {

        //            var model = _labelsContext.Labels;
        //            Title = "【" + model.Where(m => m.Id == id).FirstOrDefault().Name + "】的关联标签";
        //            var relationModel = _labelsContext.LabelRelations;
        ///**
        //            //var m1 = new Labels()
        //            //{
        //            //    Name = "test",
        //            //    Remark = "Test",
        //            //    //LabelRelationsTid1Navigation = new List<Labels>() {
        //            //    //    new () {Name = "test1", Remark="test1"},
        //            //    //    new () {Name = "test2", Remark="test2"},
        //            //    //}
        //            //};
        //            ////model.Add(m1);
        //            //var m2 = new Labels() { Name = "test2", Remark = "test2" };
        //            ////model.Add(m2);
        //            //((Context)DbContext).Add(new LabelRelations() { FromLabelsNavigation = m1, ToLabelsNavigation = m2 });
        //            //DbContext.SaveChanges();

        //*/
        //            var list = new List<int>();
        //            var Tdata = relationModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            Tdata.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);

        //            });
        //            list = list.Distinct().ToList();
        //            list.Remove(id);

        //            var RData =
        //                model.Where(m => list.Contains(m.Id));
        //            var Grid = new BGrid(RData);

        //            /** 
        //             * var data = model.Include(m => m.LabelRelationsFromLabelsNavigation).Include(m => m.LabelRelationsToLabelsNavigation).Where(m => m.Id == 6).FirstOrDefault();
        //             * var data = model.Include(m => m.LabelRelationsFromLabelsNavigation).Include(m => m.LabelRelationsToLabelsNavigation).Where(m => m.Id == id);
        //             * var Grid = new BGrid(data);
        //            */

        //            Grid.Column(nameof(Model.Name), "关联标签");
        //            Grid.Actions(actions =>
        //            {
        //                actions.DisableView();
        //                actions.DisableEdit();
        //            });

        //                await content.WithController(this)
        //                .Title(Title)
        //                .Description(Description.ContainsKey("index") ? Description["index"] : CoreAdmin.Mvc.Admin.Trans("list"))
        //                .Body(Grid);


        //            return content.Render();
        //        }

        //        /// <summary>
        //        /// 关联标签的添加页面
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <param name="content"></param>
        //        /// <returns></returns>
        //        [Route("relations/{id}/[action]")]
        //        public async Task<IActionResult> Create(int id, [FromServices] Content content)
        //        {
        //            var model = _labelsContext.Labels;

        //            Title = "为【" + model.Where(m => m.Id == id).FirstOrDefault().Name + "】标签添加新的关联";

        //            content.WithController(this)
        //               .Title(Title)
        //               .Description(Description.ContainsKey("index") ? Description["index"] : CoreAdmin.Mvc.Admin.Trans("list"))
        //               .Body(LabelRelationForm(id));


        //            return content.Render();
        //        }

        //        /// <summary>
        //        /// Create 方法的表格部分内容
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <returns></returns>
        //        [NonAction]
        //        public BForm LabelRelationForm(int id)
        //        {
        //            var model = _labelsContext.Labels;
        //            var relationModel = _labelsContext.LabelRelations;
        //            var labelsModels = _labelsContext.Labels.Where(m => m.Id == id);
        //            var currentModel = labelsModels.SingleOrDefault();
        //            var form = new BForm(_labelsContext.LabelRelations).WithController(this);
        //            // 过滤已有关联
        //            var list = new List<int>();
        //            var Tdata = relationModel.Where(m => m.FromLabels == id || m.ToLabels == id).ToList();
        //            Tdata.ForEach(m =>
        //            {
        //                list.Add(m.FromLabels);
        //                list.Add(m.ToLabels);

        //            });
        //            list = list.Distinct().ToList();

        //            var options = model.Where(m => !list.Contains(m.Id)).Where(m => m.Id != id).Select(r => KeyValuePair.Create(r.Id.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value);
        //            //var options = model.Where(m => !list.Contains(m.Id)).Select(r => KeyValuePair.Create(r.Id.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value);
        //            //var options = model.Where(m => m.Id != id).Select(r => KeyValuePair.Create(r.Id.ToString(), r.Name)).ToDictionary(x => x.Key, x => x.Value);
        //            form.SetAction("/labels/relations/" + id + "/store");
        //            form.Text(nameof(Relation.FromLabels), "被关联标签").Default(currentModel.Name).Disable();
        //            form.MultipleSelect(nameof(Relation.UIToLabels), "添加新的关联标签").Options(options);
        //            return form;
        //        }

        //        /// <summary>
        //        /// 处理 新增关联标签 请求
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <param name="model"></param>
        //        /// <returns></returns>
        //        [HttpPost("relations/{id}/[action]")]
        //        public async Task<IActionResult> Store(int id, [Bind("UIToLabels")] Relation model)
        //        {
        //            var uiLabels = model.UIToLabels.Where(m => !string.IsNullOrEmpty(m)).Select(m => Convert.ToInt32(m)).ToList();
        //            var currentModel = _labelsContext.Labels.Where(m => m.Id == id).SingleOrDefault();
        //            var relationModels = _labelsContext.Labels.Where(m => uiLabels.Contains(m.Id)).ToList();
        //            //relationModels.ForEach(currentModel.LabelRelationsToLabelsNavigation.Add);
        //            var res = relationModels.Select(r => new Relation() { FromLabelsNavigation = currentModel, ToLabelsNavigation = r });
        //            var labelsModel = _labelsContext.Labels;
        //            //过滤已存在数据  TODO


        //            // 对引用次数进行计算 TODO
        //            var Tdata = new List<int>();
        //            Tdata.Add(id);
        //            Tdata.AddRange(uiLabels);
        //            var labelsData = labelsModel.Where(m => Tdata.Contains(m.Id)).ToList();
        //            // 这里注意引用次数的计算
        //            foreach (var data in labelsData)
        //            {
        //                if (data.Id == id) {
        //                    data.RefCount += labelsData.Count() - 1;
        //                }
        //                else {
        //                    data.RefCount++;
        //                }
        //            }

        //            _labelsContext.LabelRelations.AddRange(res);
        //            _labelsContext.SaveChanges();

        //            CoreAdmin.Mvc.Admin.Toastr(CoreAdmin.Mvc.Admin.Trans("save_succeeded"));
        //            return Redirect("/labels/relations/" + id);

        //        }

        //        /// <summary>
        //        /// 删除/批量删除关联标签
        //        /// </summary>
        //        /// <param name="LabelsA"></param>
        //        /// <param name="LabelsB"></param>
        //        /// <returns></returns>
        //        [HttpDelete("relations/{LabelsA}/{LabelsB}")]
        //        public IActionResult DeleteLabelRelations(int LabelsA, string LabelsB)
        //        {
        //            var relationModel = _labelsContext.LabelRelations;
        //            var labelsModel = _labelsContext.Labels;
        //            var a = LabelsB.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(k => Convert.ToInt32(k));

        //            foreach (var b in a)
        //            {
        //                var ids = relationModel
        //                    .Where(m => (m.FromLabels == LabelsA && m.ToLabels == b) || (m.FromLabels == b && m.ToLabels == LabelsA)).Distinct(m => m.Id).ToList();
        //                // 删除引用次数
        //                var labelsModelsData = labelsModel.Where(m => m.Id == LabelsA || m.Id == b).ToList();
        //                foreach (var data in labelsModelsData)
        //                {
        //                    data.RefCount--;
        //                }
        //                relationModel.RemoveRange(ids);
        //                _labelsContext.SaveChanges();
        //            }
        //            return Redirect("/labels/relations/" + LabelsA);
        //        }
    }
}
