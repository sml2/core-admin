﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SML.Models;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;

namespace SML.Controllers
{
    public class SimpleTemplateController : AdminController
    {
        private EF.Admin Context => DbContext as  EF.Admin;

        public SimpleTemplateController() : base()
        {
            Title = "模版";
        }

        /*  列表入口 */
        [HttpPost]
        public Task<IActionResult> Index(SimpleTemplateModel model)
        {
            return Form().Store(model);
        }

        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.SimpleTemplateModel;
            var grid = new BGrid(model, HttpContext);
            var a = Utils;

            grid.Column(nameof(SimpleTemplateModel.ID), "编号").Sortable();
            grid.Column(nameof(SimpleTemplateModel.Str), "文字信息");
            grid.Column(nameof(SimpleTemplateModel.Number), "数字");
            grid.Column(nameof(SimpleTemplateModel.Date), "日期信息");
            grid.Column(nameof(SimpleTemplateModel.DateTime), "时间信息");
            grid.Column(nameof(SimpleTemplateModel.Year), "年");
            grid.Column(nameof(SimpleTemplateModel.Month), "月");
            grid.Column(nameof(SimpleTemplateModel.Time), "时间");
            grid.Column(nameof(SimpleTemplateModel.Url), "网址");

            grid.Tools((tools) => { });

            return grid;
        }
        
        
        /*  创建和编辑UI */
        [HttpPost]
        protected override BForm Form()
        {
            var model = Context.SimpleTemplateModel;
            var form = new BForm(model).WithController(this);

            form.Text(nameof(SimpleTemplateModel.Str));
            form.Furl(nameof(SimpleTemplateModel.Url));
            form.Number(nameof(SimpleTemplateModel.Number)).min(1).max(10).SetWidth("200px").Value("5");
            form.Date(nameof(SimpleTemplateModel.Date)).SetWidth("200px");
            form.Date(nameof(SimpleTemplateModel.DateTime)).format("YYYY-MM-DD HH:mm:ss").SetWidth("200px");
            form.Datetime(nameof(SimpleTemplateModel.DateTime1)).SetWidth("200px");
            form.Year(nameof(SimpleTemplateModel.Year)).SetWidth("200px").Value("1950");
            form.Month(nameof(SimpleTemplateModel.Month)).SetWidth("200px");
            form.Time(nameof(SimpleTemplateModel.Time)).SetWidth("200px");
            form.Divider(nameof(SimpleTemplateModel.Divider)).SetText("name");
            
            
            //form.Text(nameof(SimpleTemplateModel.Select));         
            
            // form.Textarea(nameof(SimpleTemplateModel.Textarea));
            // form.Ace(nameof(SimpleTemplateModel.Ace));
            form.EditorMd(nameof(SimpleTemplateModel.EditorMd));
            // form.Monaco(nameof(SimpleTemplateModel.Monaco));
            
            
            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, [Bind("Str")] SimpleTemplateModel model)
        {
            var test = await Context.SimpleTemplateModel.SingleAsync(m => m.ID == id);
            test.Str = model.Str;
            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.SimpleTemplateModel;
            var show = new BShow(model.Single(m => m.ID == id));

            show.Field(nameof(SimpleTemplateModel.ID), "Id");

            show.Field(nameof(SimpleTemplateModel.Str), "单行输入框");
            show.Field(nameof(SimpleTemplateModel.Url), "网址");
            show.Field(nameof(SimpleTemplateModel.Number), "数字输入框");
            show.Field(nameof(SimpleTemplateModel.Date), "日期");
            show.Field(nameof(SimpleTemplateModel.DateTime), "日期时间");
            show.Field(nameof(SimpleTemplateModel.DateTime1), "日期时间");
            show.Field(nameof(SimpleTemplateModel.Year), "年");
            show.Field(nameof(SimpleTemplateModel.Month), "月份");
            show.Field(nameof(SimpleTemplateModel.Time), "时间");
            show.Field(nameof(SimpleTemplateModel.Divider), "线");
            show.Field(nameof(SimpleTemplateModel.Textarea), "文本域");

            return show;
        }

    }
}