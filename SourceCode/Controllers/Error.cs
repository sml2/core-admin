﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace SML.Controllers
{
    public class Error : Controller
    {

        private readonly ILogger<Error> logger;

        public Error(ILogger<Error> logger)
        {
            this.logger = logger;
        }

        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            if (statusCodeResult == null)
            {
                ViewBag.ErrorMessage = "抱歉，访问的页面不存在";
                ViewBag.Path = "111";
                ViewBag.QS = "w";
                return View("NotFound");
            }
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "哎呦~您找的页面失踪了!";
                    logger.LogWarning($"发生了一个404错误。路径:" + Request.Host +  statusCodeResult.OriginalPath + statusCodeResult.OriginalQueryString + "时间:"+ DateTime.Now.ToString());
                    break;
                default:
                    ViewBag.ErrorMessage = "页面异常";
                    ViewBag.Path = statusCodeResult.OriginalPath;
                    ViewBag.QS = statusCodeResult.OriginalQueryString;
                    break;
            }
            return View("NotFound");
        }

        [Route("Error")]
        public IActionResult ExceptionHandler()
        {
            //获取异常细节
            //获取异常详情信息
            var exceptionHandlerPathFeature =
                    HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            //ViewBag.ExceptionPath = statusCodeResult.OriginalPathBase;
            //ViewBag.ExceptionPath = exceptionHandlerPathFeature.ToString();
            //ViewBag.ExceptionMessage = exceptionHandlerPathFeature.Error.Message;
            //ViewBag.StackTrace = exceptionHandlerPathFeature.Error.StackTrace;
            //存储日志
            logger.LogError($"路径 {Request.Host + exceptionHandlerPathFeature.Path} " +
                $"产生了一个错误{exceptionHandlerPathFeature.Error}");
            return View("Error");
        }
    }
}
