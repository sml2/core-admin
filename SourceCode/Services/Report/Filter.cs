﻿using Microsoft.Extensions.Caching.Memory;
using SML.Models.Report;
using System.Collections.Generic;
using System.Linq;
using Model = SML.Models.Report.Filter;

namespace SML.Services.Report
{
    public class Filter
    {
        private const string MemoryCacheKey = nameof(Filter);
        private readonly EF.Report Context;
        private readonly IMemoryCache memoryCache;
        public Filter(EF.Report Context, IMemoryCache memoryCache)
        {
            this.Context = Context;
            this.memoryCache = memoryCache;
        }
        public List<Model> Refresh()
        {
            MemoryCacheEntryOptions o = new() { Priority = CacheItemPriority.NeverRemove };
            return memoryCache.Set(MemoryCacheKey, Context.Filters.ToList(), o);
        }
        public void Match(GroupHash groupHash)
        {
            if (!groupHash.HasFilter()) {
                if (!memoryCache.TryGetValue(MemoryCacheKey, out List<Model> ls))
                {
                    ls = Refresh();
                }
                ls.ForEach((f) =>
                {
                    if (f.Match(groupHash))
                    {
                        groupHash.MatchFilter = f;
                        return true;
                    }
                    return false;
                });
            }
        }

    }
}
