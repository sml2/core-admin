using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Server;
using SML.Controllers.Api;
using System;
using System.Threading.Tasks;

namespace SML.Services.Grpcs
{
   
    public class HelloGreeter : Greeter.GreeterBase
    {
        private readonly ILogger<HelloGreeter> _logger;
        private readonly IHubContext<Hubs.CMD> _hubContext;
        public HelloGreeter(ILogger<HelloGreeter> logger, IHubContext<Hubs.CMD> hubContext)
        {
            _logger = logger;
            _hubContext = hubContext;
        }
        
        public override async Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {

            var GetContext = context.GetHttpContext();//Http2
            
            Console.WriteLine(request.Name);
            await _hubContext.Clients.All.SendAsync("Message", request.Name);

            return await Task.FromResult(new HelloReply
            {
                Message = $"Hello {request.Name}!"
            });
        }



        public override async Task<HelloReply> StreamSayHello(IAsyncStreamReader<HelloRequest> requestStream,
          IServerStreamWriter<HelloReply> responseStream, ServerCallContext context)
        {

            // Read requests in a background task.
            var readTask = Task.Run(async () =>
            {
                
                await foreach (var message in requestStream.ReadAllAsync())
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"ServerRead::{ message.Name}");
                    Console.ResetColor();
                    await _hubContext.Clients.All.SendAsync("Message",message.Name);
                    // Process request.
                }
            });
            // Send responses until the client signals that it is complete.
            var WritTask = Task.Run(async () =>
            {
                while (!readTask.IsCompleted)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"ServerWirt::{readTask.Id}");
                    Console.ResetColor();
                   
                    // await _hubContext.Clients.All.SendAsync("Message",new HelloRequest { Name=""});
                    var Wirt = new HelloReply { Message = $"{readTask.Id}" };
                    await responseStream.WriteAsync(Wirt);
                    await Task.Delay(TimeSpan.FromSeconds(1), context.CancellationToken);
                    
                }
            });

            await WritTask;
            await readTask;
            return new();
        }
    }
}