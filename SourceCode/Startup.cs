using CoreAdmin.EF;
using CoreAdmin.Mvc.Configure;
using CoreAdmin.Mvc.Middleware;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SML.EF;
using SML.Middleware;
using SML.Services.Report;
using System;
using System.Threading.Tasks;
namespace SML
{
    public class Startup
    {
        private IApplicationBuilder _applicationBuilder;


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
         
        public IConfiguration Configuration { get; }
        private string DatabaseDriver => Configuration["DatabaseDriver"].ToUpper() ?? "SQLITE";
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region 数据库
            var connectionString = Configuration.GetConnectionString($"{DatabaseDriver}_admin");
            var reportConnectionString = Configuration.GetConnectionString($"{DatabaseDriver}_report");
            var labelsConnectionString = Configuration.GetConnectionString($"{DatabaseDriver}_labels");
            var toolsConnectionString = Configuration.GetConnectionString($"{DatabaseDriver}_tools");
            var moneysConnectionString = Configuration.GetConnectionString($"{DatabaseDriver}_moneys");
            var DemandssConnectionString = Configuration.GetConnectionString($"{DatabaseDriver}_Demandss");

            //services.AddDbContext<Context>(optionsAction => _ = DatabaseDriver switch
            //{
            //    "SQLITE" => optionsAction.UseSqlite(ConnectionString),
            //    "MYSQL" => ((dynamic)optionsAction).UseMySQL(ConnectionString),
            //    "SQLSERVER" => ((dynamic)optionsAction).UseSqlServer(ConnectionString),
            //    _ => throw new System.ArgumentException($"Unsupported DatabaseDriver: {DatabaseDriver}")
            //});
            //使用ef core sqlite 连接
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new EFLoggerProvider());
            //需要指定AdminDbContext,要不会注入失败 
            services.AddDbContext<AdminDbContext,Admin>(r => r.UseSqlite(connectionString).UseLoggerFactory(loggerFactory));
            services.AddDbContext<Report>(r => r.UseSqlite(reportConnectionString).UseLoggerFactory(loggerFactory));
            services.AddDbContext<Label>(r => r.UseSqlite(labelsConnectionString).UseLoggerFactory(loggerFactory));        
            services.AddDbContext<Money>(r => r.UseSqlite(moneysConnectionString).UseLoggerFactory(loggerFactory));
            services.AddDbContext<Tool>(r => r.UseSqlite(toolsConnectionString).UseLoggerFactory(loggerFactory));
            services.AddDbContext<Demandss>(r => r.UseSqlite(DemandssConnectionString).UseLoggerFactory(loggerFactory));
            #endregion

            services.AddSignalR().AddHubOptions<Hubs.CMD>(o=>o.KeepAliveInterval = TimeSpan.FromSeconds(10));
            services.AddGrpc();
            
            /* .AddInterceptor<>()*/
            services.AddMvc().ConfigureApiBehaviorOptions(options => { options.SuppressConsumesConstraintForFormFileParameters = true; });
            //services.AddEndpointsApiExplorer();//AddMvc()时会调用，不需要手动添加
            services.AddIdentity<UserModel, Roles>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<AdminDbContext>();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequiredLength = 8;
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(300);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(300);
                options.LoginPath = "/Auth/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });
            services.UseCoreAdmin(Configuration);
            services.AddSession();
            services.AddTransient<Filter>();
        }
        private void SetupDatabase(IApplicationBuilder app)
        {
            //Microsoft.Extensions.Logging.Logger<>
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var ServiceProvider = serviceScope.ServiceProvider;
            //Migate any pending changes: 

            ServiceProvider.GetRequiredService<AdminDbContext>().Database.Migrate();

            ServiceProvider.GetRequiredService<Report>().Database.Migrate();
            //ServiceProvider.GetRequiredService<Report>().Database.ExecuteSqlRaw("PRAGMA foreign_keys = ON;");
            //尝试关闭索引检查
            //ServiceProvider.GetRequiredService<Report>().Database.ExecuteSqlRaw("PRAGMA foreign_keys = OFF;PRAGMA ignore_check_constraints=true;");
            //ServiceProvider.GetRequiredService<Report>().Database.Log = (s) => Console.WriteLine(s);
            ServiceProvider.GetRequiredService<Money>().Database.Migrate();
            ServiceProvider.GetRequiredService<Demandss>().Database.Migrate();
            ServiceProvider.GetRequiredService<Tool>().Database.Migrate();
            ServiceProvider.GetRequiredService<Demandss>().Database.Migrate();
            ServiceProvider.GetRequiredService<Money>().Database.Migrate();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<CoreAdminConfigure> options, RoleManager<Roles> roleManager, UserManager<UserModel> userManager)
        {
            SetupDatabase(app);

            CreateRolesAndUsers(roleManager, userManager).Wait();

            if (env.IsDevelopment())
            {
                //app.UseExceptionHandler("/Error");

                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                //app.UseExceptionHandler("/Home/Error");
            }
            //app.UseStatusCodePagesWithReExecute("/Error/{0}");

            //app.UseStatusCodePages(appBuilder =>  appBuilder.UseMiddleware<SubDomain>());
            app.UseMiddleware<SubDomainSpecial>();

            app.UseCoreAdminMiddleware(env, Configuration);

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("TestGet", (c) => {
                //    var w = new System.IO.StreamWriter(c.Response.Body);
                //    return w.WriteAsync("HelloWorld");
                //});
                endpoints.MapGet("TestGet",async (c) =>await c.Response.BodyWriter.WriteAsync(System.Text.Encoding.UTF8.GetBytes("HelloWorld")));
                // endpoints.MapControllerRoute(
                //     name: "admin",
                //     pattern: !string.IsNullOrEmpty(options.Value.Route.Prefix) ? options.Value.Route.Prefix + "/" : string.Empty + "{controller=Home}/{action=Index}/{id?}");
                //endpoints.MapDynamicControllerRoute<>();
                //endpoints.MapPost("",new Controllers.Report.ApiController().OnPostAsync());
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Privacy}/{id?}");
                endpoints.MapFallbackToController(
                    nameof(CoreAdmin.Mvc.Controllers.HomeController.Privacy),
                    nameof(CoreAdmin.Mvc.Controllers.HomeController).Replace("Controller",string.Empty));
                endpoints.MapHub<Hubs.CMD>("/cmd");
                endpoints.MapGrpcService<Services.Grpcs.HelloGreeter>();
                // Configure the HTTP request pipeline.
                //endpoints.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
            });
        }

        // 创建管理员角色和超级用户    
        private async Task CreateRolesAndUsers(RoleManager<Roles> roleManager, UserManager<UserModel> userManager)
        {

            // 判断管理员角色是否存在     
            var res = await roleManager.RoleExistsAsync("Admin");
            if (res)
            {
                var user = await userManager.Users.FirstAsync(u => u.Id == 1);
                if (string.IsNullOrEmpty(user.PasswordHash))
                {
                    userManager.Options.Password.RequireLowercase = false;
                    userManager.Options.Password.RequiredLength = 6;

                    const string userPwd = "123456";
                    var a = await userManager.AddPasswordAsync(user, userPwd);
                    await userManager.AddToRoleAsync(user, "Admin");

                    userManager.Options.Password.RequireLowercase = true;
                    userManager.Options.Password.RequiredLength = 8;
                }
            }
        }
    }
}
