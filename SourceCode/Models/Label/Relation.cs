﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;

namespace SML.Models
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class Relation:BaseModel, IHasTimestamps
    {

        [Column("id")]
        public int ID { get; set; }

        [Column("from_labels"),Display(Name = "tid1")]
        public int FromLabels { get; set; }

        [Column("to_labels"), Display(Name = "tid2")]
        public int ToLabels { get; set; }

        [NotMapped]
        public List<string> UIToLabels { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }


        public virtual Label FromLabelsNavigation { get; set; }
  
        public virtual Label ToLabelsNavigation { get; set; }
    }
}
