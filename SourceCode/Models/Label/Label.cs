﻿using CoreAdmin.Mvc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SML.Models
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class Label : BaseModel
    {
        [Column("id")]
        public int ID { get; set; }

        [StringLength(50),Column("name"), Required]
        public string Name { get; set; }
        [StringLength(50),Column("remark")]
        public string Remark { get; set; }

        [Column("ref_count")]
        public int RefCount { get; set; }

        //[NotMapped]
        //public List<string> HttpMethodList { 
        //    get => !string.IsNullOrEmpty(HttpMethod) ? HttpMethod.Split(",").ToList() : new();
        //    set {
        //        HttpMethod = string.Join(",", value);
        //    } }

        //[StringLength(255),Column("http_path")]
        //public string HttpPath { get; set; }



        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public virtual ICollection<Relation> LabelRelationsFromLabelsNavigation { get; set; } = new HashSet<Relation>();
        public virtual ICollection<Relation> LabelRelationsToLabelsNavigation { get; set; } = new HashSet<Relation>();

    }
}
