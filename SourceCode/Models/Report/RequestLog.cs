﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Models;
using System.ComponentModel;

namespace SML.Models.Report
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class RequestLog : BaseModel
    {
        public RequestLog() { }
        public RequestLog(IP modelIP)
        {
            SetLastIP(modelIP);
        }

        [Column("id")]
        public int ID { get; set; }

     

        [Column("ip_value")]
        public string IPValue{get; private set; }
        [Column("iid")]
        public int IPID { get; private set; }
        public IP IP{get; private set; }
        public void SetLastIP(IP Value)
        {
            IP = Value;
            IPValue = Value.Value;
        }
        [Column("size")]
        public long Size { get; set; }
        [Column("count")]
        public int Count { get; set; }
        public States State { get; set; }
        public enum States
        {
            [Description("未知")]
            Unknow,
            [Description("解码器参数为空异常")]
            DecoderArgumentNullException,
            [Description("解码器参数异常")]
            DecoderArgumentException,
            [Description("解码器结果为空")]
            DecoderNullOrEmpty,
            [Description("部分解析出错")]
            Partial,
            [Description("解析完成")]
            Complete
        }
        public string UI_Size { get => Mod_GlobalFunction.GetByteSize2String(Size); }

        //[StringLength(2147483647), Column("datas"), Required]
        //public string Datas { get; set; }
        
        public string UI_Content
        {
            get {
                if (Content == null) return "None";
                try
                {
                    return System.Text.Encoding.UTF8.GetString(Content);
                }
                catch (System.Exception)
                {
                    return Sy.Text.Encode.ToHex(Content);
                }
            }
        }
        public byte[] Content { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
