﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;

namespace SML.Models.Report
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class IP : BaseModel
    {

        public IP(string Value)
        {
            this.Value = Value;
        }

        [Column("id")]
        public int ID { get; set; }

        [StringLength(64), Column("value"), Required]
        public string Value { get{ Console.WriteLine("x"); return _Value; } set =>_Value=value; }
        private string _Value;
        [Column("push_count_today"), Comment("当日请求次数")]
        public int PushCountToday { get; set; } = 0;
        
        [Column("push_count"), Comment("总请求次数")]
        public int PushCount { get; set; } = 0;

        [Column("exception_count"), Comment("提交Exception数量")]
        public int ExceptionCount { get; set; } = 0;

        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        //public List<RequestLog> RequestLogs { get; set; }

    }
}
