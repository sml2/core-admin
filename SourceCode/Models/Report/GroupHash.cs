﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Interface;
using CoreAdmin.Mvc.Models;

namespace SML.Models.Report
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class GroupHash : BaseModel, IHasTimestamps
    {
        public GroupHash()
        {
        }
        public GroupHash(string value, string rawText)
        {
            Value = value;
            RawText = rawText;
        }

        [Column("id")]
        public int ID { get; set; }

        [Column("hash_value"), Required]
        public string Value { get; set; }

        [Column("raw_text"), Required]
        public string RawText { get; set; }

        [Column("description"), Required]
        public string Description { get; set; }

        [Column("software"), Required]
        public string Software { get; set; }

        [Column("iid"), Display(Name = "最近上报IP")]
        public IP LastIP { get; private set; }
        public void SetLastIP(IP Value) {
            LastIP = Value;
            LastIPValue = Value.Value;
        }
        [Column("ip_value")]
        public string LastIPValue { get;private set;}
        [Column("file_length")]
        public long LastFileLength { get; set; }
        public string UILastFileLength { get => Mod_GlobalFunction.GetByteSize2String(LastFileLength); }
        public enum States{
            /// <summary>
            /// 未  知
            /// </summary>
            [System.ComponentModel.Description("未  知")]
            Unknown,
            /// <summary>
            /// 已解决
            /// </summary>
            [System.ComponentModel.Description("已解决")]
            Resolved,
            /// <summary>
            /// 解决过
            /// </summary>
            [System.ComponentModel.Description("解决过")]
            Solved,
            /// <summary>
            /// 未解决
            /// </summary>
            [System.ComponentModel.Description("未解决")]
            Unresolved,
            /// <summary>
            /// 已搁置
            /// </summary>
            [System.ComponentModel.Description("已搁置")]
            //[System.ComponentModel.Description] 不确定是不是WinForm控件开发特定使用的
            //[System.ComponentModel.Display]
            Shelve,
        }
        [Column("state"), Display(Name = "state")]
        public States State { get; set; } = States.Unknown;

        [Column("count"), Display(Name = "次数")]
        public int Count { get; set; } = 0;
        internal bool HasFilter()
        {
            return MatchFilterID == null;
        }

        [Column("fid"), Display(Name = "最近上报IP")]
        public int? MatchFilterID { get; set; }  
        private Filter filter;
        [Display(Name = "匹配的过滤规则")]
        public Filter MatchFilter { get => filter; 
            set{
                filter = value;
                FilterUI = value.Name;
            }
        }

        [Display(Name = "过滤原因")]
        public string FilterUI { get; set; } = "无匹配的过滤规则";

        [Column("created_at"), Display(Name = "created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Column("updated_at"), Display(Name = "updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}
