﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Interface;

namespace SML.Models.Report
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public partial class Filter : BaseModel, IHasTimestamps
    {
        [Column("id")]
        public int ID { get; set; }

        public string Name { get; set; }
        public enum MatchType
        {
            ContainsString_Description,
            ContainsString_Software
        }

        [Column("Type")]
        public MatchType Type { get; set; }


        [Column("Value")]
        public string Value { get; set; }

        public bool Match(GroupHash groupHash) {
            switch (Type) { 
                case MatchType.ContainsString_Description:
                    return (groupHash?.Description?.Contains(Value, StringComparison.OrdinalIgnoreCase)).GetValueOrDefault();
                case MatchType.ContainsString_Software:
                    return (groupHash?.Software?.Contains(Value, StringComparison.OrdinalIgnoreCase)).GetValueOrDefault();
                default:
                    return false;
            }
        }
        [Column("created_at"), Display(Name = "created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at"), Display(Name = "updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
