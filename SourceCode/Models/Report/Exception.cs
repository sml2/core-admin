﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Interface;
using Newtonsoft.Json.Linq;

namespace SML.Models.Report
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public partial class Exception : BaseModel, IHasTimestamps
    {
        public Exception(){}
        public Exception(RequestLog requestLog,IP ip)
        {
            RequestLog = requestLog;
            IP = ip;
            IPValue = ip.Value;
        }

        [Column("id"), Display(Name = "编号")]
        public int ID { get; set; }

        public int RequestLogId { get; init; }
        public RequestLog RequestLog { get; init; }
        [Column("iip")]
        public int IPID { get; init; }
        public IP IP { get; init; }
        [Column("ip_value"), Display(Name = "上报IP")]
        public string IPValue { get; init; }


        public int? GroupHashID { get; set; }
        public GroupHash GroupHash { get; set; }

        [StringLength(256), Column("hash")]
        public string Hash { get; set; }

        [ Column("raw_text")]
        public string RawText { get; set; }

        #region "直接触发源"
        /// <summary>
        /// 报错来源 Not Null
        /// </summary>
        [Column("from"), Required(AllowEmptyStrings = false)]
        public string From { get; set; } = "OLD EXE";//最初报错模块没有FROM字段
        /// <summary>
        /// 触发对象
        /// </summary>
        [Column("sender")]
        public string Sender { get; set; }
        /// <summary>
        /// 软件名称
        /// </summary>
        [Column("software")]
        public string Software { get; set; }
        #endregion
        #region "源信息"
        [StringLength(100), Column("version")]
        public string Version { get; set; }

        [StringLength(100), Column("type")]
        public string Type { get; set; }

        [StringLength(100), Column("message")]
        public string Message { get; set; }

        [StringLength(50), Column("h_result")]
        public string HResult { get; set; }

        [StringLength(100), Column("source")]
        public string Source { get; set; }

        [StringLength(100), Column("target_site")]
        public string TargetSite { get; set; }

        [Column("stack_trace")]
        public string StackTrace { get; set; }

        #endregion
        #region "作用域加载项"

        [Column("assembly_count")]
        public int AssemblyCount { get; set; }
        [Column("assemblies")]
        public string Assemblies { get; set; }

        [Column("thread_count")]
        public int ThreadCount { get; set; }
        [Column("threads")]
        public string Threads { get; set; }

        [Column("module_count")]
        public int ModuleCount { get; set; }
        [Column("modules")]
        public string Modules { get; set; }

        #endregion
        #region "客户端环境"

        [StringLength(50), Column("time")]
        public string Time { get; set; }
        [StringLength(100), Column("net_framework")]
        public string NetFramework { get; set; }

        [StringLength(100), Column("os_version")]
        public string OSVersion { get; set; }

        [StringLength(50), Column("username")]
        public string UserName { get; set; }

        [StringLength(100), Column("runtime")]
        public string RunTime { get; set; }

        [StringLength(500), Column("path")]
        public string Path { get; set; }
        #endregion
        [Column("created_at"), Display(Name = "首次上报时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at"), Display(Name = "最近更新时间")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        /// <summary>
        /// 当前数据填充状态，正常应为<see cref="States.AnalysisSuccess"/>
        /// </summary>
        [Display(Name = "状态")]
        public States State { get; set; }
        public enum States { 
            None,
            Analysising,
            AnalysisFail_Null,
            AnalysisFail_Empty,
            AnalysisFail_Format,
            AnalysisFail_Invalid,
            AnalysisFail_NotFound,
            CalcHashing,
            AnalysisSuccess
        }

        public States Analysis(string RawReocdeText) {
            if (null != RawReocdeText)
            {
                if (RawReocdeText.Length != 0) {
                    RawText = RawReocdeText;
                    //检测换行符
                    string LineBreak, LbWindows = "\r\n"; char LbUnixOrIOS = '\r',LbLinux = '\n';
                    if (RawReocdeText.Contains(LbWindows)) {
                        LineBreak = LbWindows;
                    } else if (RawReocdeText.Contains(LbUnixOrIOS)) {
                        LineBreak = LbUnixOrIOS.ToString();
                    } else if (RawReocdeText.Contains(LbLinux)) {
                        LineBreak = LbLinux.ToString();
                    } else { State = States.AnalysisFail_Format;return State; }
                    //转换为易处理字符串
                    Sy.String ReocdeText = RawReocdeText;
                    //拆分行，过滤空行
                    var Lines = ReocdeText.Split(LineBreak).Where((s)=>s.Length>0);
                    if (Lines.IsNotEmpty()) { 
                        State = States.Analysising;
                        var Datas = new Queue<Sy.String>(Lines);
                        TextField.Init();
                        BitArray bits = TextField.CreateFlag();
                        do
                        {
                            var Line = Datas.Dequeue();
                            foreach (var Token in TextField.Fields)
                            {
                                System.Diagnostics.Debug.Assert(bits.Length > 0);
                                if (Token.Value.Analysis(Line, Datas, this,bits)) break;
                            }
                        } while (Datas.Count > 0);
                        if (string.IsNullOrEmpty(Software)) {
                            //报错模块中没有Software字段的情况
                            Sy.String Modules = this.Modules;
                            var NameMid = new Midor("Name[", ".exe]");
                            if ((Modules?.Contains(NameMid)).GetValueOrDefault())
                            {
                                Software = Modules.Mid(NameMid);
                            }
                            else if ((Sender?.Contains(".exe")).GetValueOrDefault())
                            {
                                Software = Sender.Replace(".exe", "").Replace("名称:", "").Replace("Name:", "");
                            }
                            else {
                                Software = Source;
                            }
                        }
                        //检验必须字段填充情况抛异常
                        foreach (var Token in TextField.Fields)
                        {
                            if (!Token.Value.Check(bits)) {
                                State = States.AnalysisFail_NotFound;
                                Console.WriteLine($"Not Found {Token.Value}");
                                return State;
                            }
                        }
                        State = States.CalcHashing;
                        Hash = Sy.Security.Sha256.Encrypt($"{Version}{Type}{Message}{HResult}{Source}{TargetSite}");
                        State = States.AnalysisSuccess;
                    }
                    else { State = States.AnalysisFail_Invalid; }
                } else { State = States.AnalysisFail_Empty; }
            } else { State = States.AnalysisFail_Null; }
            return State;
        }
        public override string ToString()
        {
            return Type??Message??HResult??"None";
        }
    }
}
