﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SML.Models.Report
{
    public class TextField
    {
        public static void Init()
        {
            Mod_GlobalFunction.Nop(Fields);
        }
        private static volatile int RequiredRef = 0;
        //去除字段两边的可读性界定符
        private static Func<string, string> FilterFormat = (string data) => data.Trim('[', ']', ':', ' ');
        private static readonly Lazy<Dictionary<string, TextField>> Fields_ = new(() => new()
        {
            {
                "----",
                new(CheckModes.None,
                      l => l.StartsWith("----"),
                      (l, s, m) => { return true; }
                  )
            },
            {
                nameof(Exception.From),
                new(CheckModes.Required,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Error From:", "From:"),
                      (l, s, m) => { var t = FilterFormat(l.After("From:")); var r = t.Length > 0; if (r) m.From = t; return r; }
                  )
            },
            {
                nameof(Exception.Sender),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "sender"),
                      (l, s, m) => { var t = FilterFormat(s.Dequeue()); var r = t.Length > 0; if (r) m.Sender = t; return r; }
                  )
            },
            {
                nameof(Exception.Software),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Software"),
                      (l, s, m) => { var t = FilterFormat(s.Dequeue()); var r = t.Length > 0; if (r) m.Sender = t; return r; }
                  )
            },
            {
                nameof(Exception.Version),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Version:"),
                      (l, s, m) => { var t = FilterFormat(l.After("Version:")); var r = t.Length > 0; if (r) m.Version = t; return r; }
                  )
            },
            {
                nameof(Exception.Type),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Type:"),
                      (l, s, m) => { var t = FilterFormat(l.After("Type:")); var r = t.Length > 0; if (r) m.Type = t; return r; }
                  )
            },
            {
                nameof(Exception.Message),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Message:"),
                      (l, s, m) => { var t = FilterFormat(l.After("Message:")); var r = t.Length > 0; if (r) m.Message = t; return r; }
                  )
            },
            {
                nameof(Exception.HResult),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "HResult:"),
                      (l, s, m) => { var t = FilterFormat(l.After("HResult:")); var r = t.Length > 0; if (r) m.HResult = t; return r; }
                  )
            },
            {
                nameof(Exception.Source),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Source:"),
                      (l, s, m) => { var t = FilterFormat(l.After("Source:")); var r = t.Length > 0; if (r) m.Source = t; return r; }
                  )
            },
            {
                nameof(Exception.TargetSite),
                new(CheckModes.Required,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "TargetSite:"),
                      (l, s, m) => { var t = FilterFormat(l.After("TargetSite:")); var r = t.Length > 0; if (r) m.TargetSite = t; return r; }
                  )
            },
            {
                nameof(Exception.Time),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Time:"),
                      (l, s, m) => { var t = FilterFormat(l.After("Time:")); var r = t.Length > 0; if (r) m.Time = t; return r; }
                  )
            },
            {
                nameof(Exception.NetFramework),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "NetFramework:"),
                      (l, s, m) => { var t = FilterFormat(l.After("NetFramework:")); var r = t.Length > 0; if (r) m.NetFramework = t; return r; }
                  )
            },
            {
                nameof(Exception.OSVersion),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "OSVersion:"),
                      (l, s, m) => { var t = FilterFormat(l.After("OSVersion:")); var r = t.Length > 0; if (r) m.OSVersion = t; return r; }
                  )
            },
            {
                nameof(Exception.UserName),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "UserName:"),
                      (l, s, m) => { var t = FilterFormat(l.After("UserName:")); var r = t.Length > 0; if (r) m.UserName = t; return r; }
                  )
            },
            {
                nameof(Exception.RunTime),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "RunTime:"),
                      (l, s, m) => { var t = FilterFormat(l.After("RunTime:")); var r = t.Length > 0; if (r) m.RunTime = t; return r; }
                  )
            },
            {
                nameof(Exception.Path),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Path:"),
                      (l, s, m) => { var t = FilterFormat(l.After("Path:")); var r = t.Length > 0; if (r) m.Path = t; return r; }
                  )
            },
            {
                nameof(Exception.StackTrace),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "StackTrace:"),
                      (l, s, m) =>
                      {
                          var sb = new System.Text.StringBuilder();
                          while (s.Count > 0 && !s.Peek().StartsWith("----"))
                          {
                              sb.AppendLine(s.Dequeue());
                          }
                          if (sb.Length > 0)
                          {
                              m.StackTrace = sb.ToString();
                              return true;
                          }
                          return false;
                      }
                  )
            },
            {
                nameof(Exception.Assemblies),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Assemblies"),
                      (l, s, m) =>
                      {
                          var t = FilterFormat(l.After("Assemblies"));
                          if (t.Length > 0 && int.TryParse(t, out int i))
                          {
                              m.AssemblyCount = i;
                              var sb = new System.Text.StringBuilder();
                              while (s.Count > 0 && !s.Peek().StartsWith("----"))
                              {
                                  sb.AppendLine(s.Dequeue());
                              }
                              if (sb.Length > 0)
                              {
                                  m.Assemblies = sb.ToString();
                                  return true;
                              }
                          };
                          return false;
                      }
                  )
            },
            {
                nameof(Exception.Threads),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Threads"),
                      (l, s, m) =>
                      {
                          var t = FilterFormat(l.After("Threads"));
                          if (t.Length > 0 && int.TryParse(t, out int i))
                          {
                              m.ThreadCount = i;
                              var sb = new System.Text.StringBuilder();
                              while (s.Count > 0 && !s.Peek().StartsWith("----"))
                              {
                                  sb.AppendLine(s.Dequeue());
                              }
                              if (sb.Length > 0)
                              {
                                  m.Threads = sb.ToString();
                                  return true;
                              }
                          };
                          return false;
                      }
                  )
            },
            {
                nameof(Exception.Modules),
                new(CheckModes.None,
                      l => l.StartsWith(StringComparison.OrdinalIgnoreCase, "Modules"),
                      (l, s, m) =>
                      {
                          var t = FilterFormat(l.After("Modules"));
                          if (t.Length > 0 && int.TryParse(t, out int i))
                          {
                              m.ModuleCount = i;
                              var sb = new System.Text.StringBuilder();
                              while (s.Count > 0 && !s.Peek().StartsWith("----"))
                              {
                                  sb.AppendLine(s.Dequeue());
                              }
                              if (sb.Length > 0)
                              {
                                  m.Modules = sb.ToString();
                                  return true;
                              }
                          };
                          return false;
                      }
                  )
            }
        });
        public static BitArray CreateFlag() => new(RequiredRef, false);
        public static Dictionary<string, TextField> Fields { get => Fields_.Value; }
        private delegate bool DetectionHandle(Sy.String v);
        private readonly DetectionHandle Detection_;
        private delegate bool AnalysisHandle(Sy.String CurLine, Queue<Sy.String> Queue, Exception Model);
        private readonly AnalysisHandle Analysis_;
        public enum CheckModes { None, Required }
        public readonly CheckModes CheckMode;
        private int RequiredIndex = -1;
        TextField(CheckModes CheckMode_, DetectionHandle detection_, AnalysisHandle analysis_)
        {
            if (CheckModes.Required == (CheckMode = CheckMode_)) { RequiredIndex = RequiredRef++; }
            Detection_ = detection_;
            Analysis_ = analysis_;
        }
        public bool Analysis(Sy.String CurLine, Queue<Sy.String> Queue, Exception Model, BitArray bitArray)
        {
            if (Detection_(CurLine))
            {
                var r = Analysis_(CurLine, Queue, Model);
                if (r && CheckMode == CheckModes.Required)
                {
                    bitArray.Set(RequiredIndex, true);
                }
                return r;
            }
            else { return false; }
        }
        public bool Check(BitArray Bits)
        {
            if (CheckMode == CheckModes.Required)
            {
                return Bits.Get(RequiredIndex);
            }
            else { return true; }
        }
        public override string ToString()
        {
            return CheckMode == CheckModes.Required ? $"Required[{RequiredIndex}]" : "NonRequired";
        }
    }
}