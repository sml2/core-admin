﻿using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SML.Models.Tool
{
    /// <summary>
    /// 富文本类
    /// </summary>
    public class RichText : BaseModel
    {
        [Column("id"), Display(Name = "编号")]
        public int ID { get; set; }

        [Display(Name = "名称")]
        [Comment("名称")]
        public string Title { get; set; }

        [Comment("富文本内容"), Display(Name = "富文本内容")]
        public string Content { get; set; }

    }
}
