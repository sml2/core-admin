﻿using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SML.Models.Tool
{
    /// <summary>
    /// 代码记事本实体类
    /// </summary>
    public class Code : BaseModel
    {
        [Column("id"), Display(Name = "编号")]
        public int ID { get; set; }

        [Display(Name = "名称")]
        [Comment("名称")]
        public string Title { get; set; }

        [Comment("代码内容"), Display(Name = "代码内容")]
        public string Content { get; set; }

    }
}
