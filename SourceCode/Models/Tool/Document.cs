﻿using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SML.Models.Tool
{
    /// <summary>
    ///文档记事本类
    /// </summary>
    public class Document : BaseModel
    {
        [Column("id"), Display(Name = "编号")]
        public int ID { get; set; }

        [Display(Name = "名称")]
        [Comment("名称")]
        public string Title { get; set; }

        [Comment("文档内容"), Display(Name = "文档内容")]
        public string Content { get; set; }

    }
}
