﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;

namespace SML.Models.Tool
{
    //[Index(new string[] { nameof(Slug), nameof(Name) }, IsUnique = true)]
    public class Password : BaseModel
    {
        [Column("id"), Display(Name = "编号")]
        public int ID { get; set; }
        [Display(Name = "名称")]
        [Comment("名称")]
        public string Name { get; set; }

        [Comment("网址"), Display(Name = "网址")]
        public string WebSite { get; set; }

        [Comment("备用网址"), Display(Name = "备用网址")]
        public string WebSite_Backup { get; set; }

        [Comment("用户名"), Display(Name = "用户名")]
        public string UserName { get; set; }

        [Comment("账号"), Display(Name = "账号")]
        public string Account { get; set; }

        [Comment("密码"), Display(Name = "密码")]
        public string PassWord { get; set; }

        [Comment("手机号"), Display(Name = "手机号")]
        public string Phone { get; set; }

        [Comment("邮箱"), Display(Name = "邮箱")]
        public string Email { get; set; }

        [Comment("邮箱认证状态"), Display(Name = "邮箱认证状态")]
        public bool EmailVerify { get; set; }

        [Comment("登录时间"), Display(Name = "登录时间")]
        public DateTime LoginTime { get; set; } = DateTime.Now;

        [Comment("注册时间"), Display(Name = "注册时间")]
        public DateTime RegistTime { get; set; } = DateTime.Now;

        [Comment("备注"), Display(Name = "备注")]
        public string Description { get; set; }


        [Column("created_at"), Display(Name = "创建时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at"), Display(Name = "最后修改")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        //public List<RequestLog> RequestLogs { get; set; }

    }
}
