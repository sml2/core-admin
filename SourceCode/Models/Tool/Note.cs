﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Models;
using Microsoft.EntityFrameworkCore;


namespace SML.Models.Tool
{
    /// <summary>
    /// 记事本实体类
    /// </summary>
    public class Note : BaseModel
        {
            [Column("id"), Display(Name = "编号")]
            public int ID { get; set; }

            [Display(Name = "名称")]
            [Comment("名称")]
            public string Title { get; set; }

            [Comment("内容"), Display(Name = "内容")]
            public string Content { get; set; }
        
    }
}
