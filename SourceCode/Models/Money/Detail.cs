﻿using System;
using System.ComponentModel.DataAnnotations;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Interface;
using System.ComponentModel.DataAnnotations.Schema;

namespace SML.Models.Money
{
    /// <summary>
    /// 账单明细
    /// </summary>
    public class Detail : BaseModel, IHasTimestamps
    {
        [Display(Name = "编号")]
        public int ID { get; set; }
        [Display(Name = "所属人")]
        public string U_Name { get; set; }
        public int PeopleID { get; set; }
        private People _People;

        [Display(Name = "所属人")]
        public People People
        {
            get => _People;

            set
            {
                U_Name = value.Name;
                _People = value;
            }
        }
        [Display(Name = "发生额")]
        [NotMapped]
        public string UAmount { get => Amount > 0 ? $"借 {Amount:0.00}" : $"还 {Math.Abs(Amount):0.00}"; }

        //public string U2Amount { get => $"{ Amount:00.00}";}

        [Display(Name = "发生额")]
        public decimal Amount { get; set; }

        [Column("created_at"), Display(Name = "创建时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at"), Display(Name = "更新时间")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
