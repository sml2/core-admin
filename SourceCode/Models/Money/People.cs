﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;
using CoreAdmin.Mvc.Interface;
using System.ComponentModel.DataAnnotations.Schema;

namespace SML.Models.Money
{
    /// <summary>
    /// 人员
    /// </summary>
    public class People : BaseModel, IHasTimestamps
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "姓名")]

        public string Name { get; set; }

        [Display(Name = "工作")]

        public string Job { get; set; }

        [Display(Name = "总金额")]

        public decimal Total { get; set; }

        [Display(Name = "总金额")]
        [NotMapped]
        public string UTotal { get => Total > 0 ? $"借 {Total:0.00}" : $"还 {Math.Abs(Total):0.00}"; }

        [Display(Name = "最后还款")]
        public DateTime LastRepay { get; set; } = DateTime.Now;

        [Display(Name = "最后借款")]
        public DateTime LastBorrow { get; set; } = DateTime.Now;


        [Column("created_at"), Display(Name = "创建时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("updated_at"), Display(Name = "更新时间")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

    }
}
