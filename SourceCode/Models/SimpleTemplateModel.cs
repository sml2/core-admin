﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreAdmin.Mvc.Models;

namespace SML.Models
{
    [Table("SimpleTemplate")]
    public class SimpleTemplateModel : BaseModel
    {
        public int ID { get; set; }

        [StringLength(50), Column("str"), Display(Name = "单行输入框")]
        public string Str { get; set; }

        [Column("number"), Display(Name = "数字输入框")]
        public string Number { get; set; }

        [Column("radio"), Display(Name = "单选框")]
        public string Radio { get; set; }

        [Column("checkbox"), Display(Name = "复选框")]
        public string Checkbox { get; set; }

        [Column("select"), Display(Name = "选择框")]
        public string Select { get; set; }

        [Column("date"), Display(Name = "日期")] 
        public string Date { get; set; }

        [Column("dateTime"), Display(Name = "日期时间")]
        public string DateTime { get; set; }

        [Column("dateTime1"), Display(Name = "日期时间1")]
        public string DateTime1 { get; set; }

        [Column("year"), Display(Name = "年")]
        public string Year { get; set; }

        [Column("month"), Display(Name = "月份")]
        public string Month { get; set; }

        [Column("time"), Display(Name = "时间")]
        public string Time { get; set; }

        [Column("dateRange"), Display(Name = "日期范围")]
        public string DateRange { get; set; }

        [Column("textarea"), Display(Name = "文本域")]
        public string Textarea { get; set; }
        
        // [Column("monaco"), Display(Name = "代码")]
        // public string Monaco { get; set; }
        
        [Column("ace"), Display(Name = "ace代码")]
        public string Ace { get; set; }
        
        [Display(Name = "md编辑器")]
        public string EditorMd { get; set; }
        
        [Column("url"), Display(Name = "网址")]
        public string Url { get; set; }


        [Column("divider"), Display(Name = "线")]
        public string Divider { get; }
    }
}