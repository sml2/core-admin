﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SML.Middleware
{
    /// <summary>
    /// 子域名(多级域名解析)
    /// </summary>
    public class SubDomain
    {

        private readonly RequestDelegate _next;
        public SubDomain(RequestDelegate next, ILogger<SubDomain> Log)
        {
            Log.LogInformation("Middleware[SubDomain] Has Beed Created");
            _next = next;
        }
        public Task Invoke(HttpContext context, ILogger<SubDomain> Log)
        {
            //if (context.Request.Path.Equals("/") || context.Request.Path.Equals(""))
            //{
            //    string host = context.Request.Host.Value;
            //    if (host.Equals(_domain, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        context.Request.Path = _path;
            //    }
            //}
            var Request = context.Request;
            var Host = Request.Host.Host;
            var Path = Request.Path;
            if (!Path.StartsWithSegments("/_content"))
            {
                var domains = Host.Split('.');//var domains = Host.Contains('.') ? Host.Split('.') : new[]{Host};
                switch (domains.Length)
                {

                    case > 2:
                        if (domains.Length == 4 && domains.All(d => int.TryParse(d, out int i) && i <= 255 && i >= 0))
                        {//IPv4 127.0.0.1

                        }
                        else
                        { //aaa.domain.com ccc.ddd.domain.com
                            var Paths = domains.Take(domains.Length - 2);
                            var PathTemp = string.Concat(Paths.Reverse().Select(s => $"/{s}"));//For Example:/d1/d2
                            if (Path.StartsWithSegments(PathTemp))
                            {
                                Log.LogWarning("应该使用相对路径避免渲染输出主域[Relative paths should be used to avoid rendering the output main domain]");
                            } else { 
                                Request.Path = $"{PathTemp}{Path}";
                            }
                        }
                        break;
                    case 2://domain.com
                        Request.Path = $"/www{Path}";
                        break;
                    case 1://localhost

                        break;
                    default:
                        var name = nameof(domains.Length);
                        throw new ArgumentOutOfRangeException(name);
                }
            }
            return _next(context);

        }
        //错误写法
        //private Task Invoke(HttpContext context, RequestDelegate next)
        //{
        //}

        ////IMiddleware 要注册两次
        //services.AddSingleton<MyMiddleware>();
        //builder.UseMiddleware<MyMiddleware>();
        //private Task InvokeAsync(HttpContext context, RequestDelegate next)
        //{
        //}
    }
}
