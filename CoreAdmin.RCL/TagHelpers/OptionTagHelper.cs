﻿using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Struct.Attributes;
namespace CoreAdmin.RCL.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("option")]
    public class OptionTagHelper : TagHelper
    {
        public bool CaSelected { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (CaSelected)
            {
                output.Attributes.Add("selected", "selected");
            }
        }
    }
}
