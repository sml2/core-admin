﻿using Microsoft.AspNetCore.Razor.TagHelpers;
namespace CoreAdmin.RCL.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("ca-button")]
    public class ButtonTagHelper : TagHelper
    {
        public int CaId { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var className = output.Attributes;
            output.PostElement.AppendHtml($@"
<script>
    $("".delete"").on(""click"", document, () => {{
        
    }})
</script>");
            
        }
    }
}
