﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Text.Encodings.Web;
using CoreAdmin.Mvc.Struct.Attributes;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.Extensions.Logging;

namespace CoreAdmin.RCL.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("*")]
    public class AttributesTagHelper : TagHelper
    {
        public HtmlAttributes CaAttributes { get; set; }
        public string CaCheck { get; set; }
        private static bool logFlag = true;

        public AttributesTagHelper(ILogger<AttributesTagHelper> logger) : base()
        {
            //ILogger logger = Admin.LoggerFactory.CreateLogger(nameof(AttributesTagHelper));
            if (logFlag)
            {
                logger.LogWarning("foreach -> TagBuilder ?");
                logFlag = false;
            }
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (null != CaAttributes)
            {
                foreach (var (key, value) in CaAttributes)
                {
                    //switch (key)
                    //{
                    //    case CSS.Name:
                    //        (value as CSS)?.ForEach(x => output.AddClass(x, HtmlEncoder.Default));
                    //        break;
                    //    case Style.Name:
                    //    default:
                    //        output.Attributes.SetAttribute(key, value);
                    //        break;
                    //}
                    output.Attributes.SetAttribute(key, value);
                }

                output.Attributes.SetAttribute(Style.Name, CaAttributes.StyleDic);
                CaAttributes.ClassList.ForEach(x => output.AddClass(x, HtmlEncoder.Default));
            }


            if (CaCheck is "true" or "on" or "True")
            {
                output.Attributes.SetAttribute("checked", "checked");
            }

            base.Process(context, output);
        }
    }
}