﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Struct.Attributes;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using CoreAdmin.Mvc;
using Microsoft.Extensions.Logging;

namespace CoreAdmin.RCL.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("script")]
    public class ScriptTagHelper : TagHelper
    {
        private readonly Admin _admin;
        public bool Append { get; set; }

        public ScriptTagHelper(Admin admin)
        {
            _admin = admin;
        }
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (Append)
            {
                output.TagName = null;
                _admin.NewScript((await output.GetChildContentAsync()).GetContent());
                output.Content.Clear();
            }
        }
    }
}
