﻿using System;
using System.Threading.Tasks;
using CoreAdmin.Lib;
using CoreAdmin.Mvc.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreAdmin.RCL.Extensions
{
    public static class HtmlHelperExtension
    {
        public static Task<IHtmlContent> AdminRenderAsync(
            this IHtmlHelper htmlHelper,
            HtmlContent htmlContent)
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            if (htmlContent == null)
            {
                return Task.FromResult<IHtmlContent>(new HtmlString(string.Empty));
            }

            if (htmlContent.Name == HtmlContentType.Str)
            {
                return new Task<IHtmlContent>(_ => htmlHelper.Raw(htmlContent.Value), null);
            }
            else
            {
                if (htmlContent.Form != null)
                {
                    return htmlHelper.PartialAsync(htmlContent.Value, htmlContent.Form, htmlHelper.ViewData);
                }
                else
                {
                    return htmlHelper.PartialAsync(htmlContent.Value, htmlHelper.ViewData.Model, Temp.AddToViewData(htmlContent.Data, htmlHelper.ViewData));
                }
            }
        }
    }
}