﻿using CoreAdmin.EF;
using CoreAdmin.Mvc;
using Microsoft.EntityFrameworkCore;
using NetAdminTestTDC.Models;

namespace NetAdminTestTDC.EF
{
    public class Context : AdminDbContext
    {
        //添加迁移文件/初始化
        //Add-Migration -Context People -OutputDir Context\Migrations_People [Peoples]
        //Add-Migration -Context Tool -OutputDir Context\Migrations_Tool[name]
        //回滚迁移
        //Remove-Migration  -Context Tool
        //执行迁移 没有数据库会自动创建
        //Update-Database -Context Tool
        //删库
        //Drop-Database  -Context Tool
        //生成SQL脚本，来手动迁移
        //Script-Migration -Context Tool
        //获取帮助
        //get-help entityframework
        public Context(DbContextOptions options) : base(options)
        {

        }

        public DbSet<People> Peoples { get; set; }

        public DbSet<Detail> Details { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

