﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NetAdminTestTDC.Controllers
{
    public class HelloController : ControllerBase
    {
        //可以使用依赖项注入 (DI) 来请求 IHttpClientFactory
        private readonly IHttpClientFactory _clientFactory;
        public HelloController(IHttpClientFactory _clientFactory)
        {
            this._clientFactory = _clientFactory;
        }

        //获取tenant_access_token临时存储token所需要的类
        class token
        {
            public int code { get; set; }
            public int expire { get; set; }
            public string msg { get; set; }
            public string tenant_access_token { get; set; }
        }

        //模拟和机器人聊天 飞书请求本地的地址时所要用到的请求体json对应的类
        public class Rootobject
        {
            public string schema { get; set; }
            public Header header { get; set; }
            public Event _event { get; set; }
        }

        public class Header
        {
            public string event_id { get; set; }
            public string event_type { get; set; }
            public string create_time { get; set; }
            public string token { get; set; }
            public string app_id { get; set; }
            public string tenant_key { get; set; }
        }

        public class Event
        {
            public Sender sender { get; set; }
            public Message message { get; set; }
        }

        public class Sender
        {
            public Sender_Id sender_id { get; set; }
            public string sender_type { get; set; }
        }

        public class Sender_Id
        {
            public string union_id { get; set; }
            public string user_id { get; set; }
            public string open_id { get; set; }
        }

        public class Message
        {
            public Message()
            {
                content_ = new Lazy<MContent>(() => System.Text.Json.JsonSerializer.Deserialize<MContent>(content));
            }
            public string message_id { get; set; }
            public string root_id { get; set; }
            public string parent_id { get; set; }
            public string create_time { get; set; }
            public string chat_id { get; set; }
            public string chat_type { get; set; }
            public string message_type { get; set; }
            public string content { get; set; }
            private Lazy<MContent> content_ { get; }
            public string GetContent
            {
                get
                {
                    return content_.Value.text;
                }
            }
            public class MContent
            {
                public string text { get; set; }
            }
        }

        //群聊
        public async Task<ActionResult> GroupChat()
        {
            //智障机器人的webhook
            string url = @"https://open.feishu.cn/open-apis/bot/v2/hook/92606bd7-2552-4fe4-ad5d-05a3b22abef7";

            //需要发送的文本
            string text = "这是一条测试消息";

            var response = await GroupChatHttpPost(url, text);

            //将群聊响应体写入日志
            WriteLog("群聊响应体", response, DateTime.Now);

            return Content(response);
        }

        //私聊
        public async Task<ActionResult> PrivateChat()
        {
            //请求url
            string url = @"https://open.feishu.cn/open-apis/im/v1/messages";

            //需要发送的文本
            string text = "这是一条测试消息";

            //消息接收者id类型
            string receive_id_type = "email";

            //依据receive_id_type的值，填写对应的消息接收者id
            string receive_id_str = "tangdc@nanjing.com";

            var response = await PrivateChatHttpPost(url, text, receive_id_type, receive_id_str);

            //将私聊响应体写入日志
            WriteLog("私聊响应体", response, DateTime.Now);

            return Content(response);
        }

        //和机器人聊天 mime 405
        public async Task<ActionResult> ChatWithBot(Rootobject rootobject)
        {
            //获取http/https请求的请求体的内容
            if ((rootobject._event ?? (object)rootobject.header ?? (object)rootobject.schema) != null)
            {
                //请求url
                string url = @"https://open.feishu.cn/open-apis/im/v1/messages";

                //消息接收者id类型
                string receive_id_type = "email";

                //依据receive_id_type的值，填写对应的消息接收者id
                string receive_id_str = "tangdc@nanjing.com";

                var response = await PrivateChatHttpPost(url, rootobject._event.message.GetContent, receive_id_type, receive_id_str);

                //将聊天响应体写入日志
                WriteLog("聊天响应体", response, DateTime.Now);

                return Content(response);
            }
            else
            {
                throw new Exception();
            }
        }

        //群聊调用方法
        public async Task<string> GroupChatHttpPost(string url, string textStr)
        {
            //请求消息体
            var param = new
            {
                msg_type = "text",

                content = new
                {
                    text = textStr
                }
            };

            string paramStr = JsonConvert.SerializeObject(param);

            //将请求体写入日志
            WriteLog("群聊请求体", paramStr, DateTime.Now);

            var client = _clientFactory.CreateClient();

            var postData = new StringContent(paramStr, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, postData);

            if (response.IsSuccessStatusCode)
            {
                //var responseStr = await response.Content.ReadAsStringAsync();

                //获取响应的content-length
                var responseStr = response.Content.Headers.ContentLength.ToString();

                return responseStr;
            }
            else
            {
                throw new Exception();
            }
        }

        //私聊调用方法
        public async Task<string> PrivateChatHttpPost(string url, string textStr, string receive_id_type, string receive_id_str)
        {
            //消息接收者id类型值
            string receive_id = receive_id_str;

            //请求消息体
            var content = new
            {
                text = textStr
            };

            var param = new
            {
                receive_id = receive_id,

                content = JsonConvert.SerializeObject(content),

                msg_type = "text"
            };

            string paramStr = System.Text.Json.JsonSerializer.Serialize(param);

            var request = new HttpRequestMessage(HttpMethod.Post, url + $"?receive_id_type={receive_id_type}");

            string tokenStr = await GetTenant_access_token();

            //添加请求头
            request.Headers.Add("Authorization", $"Bearer {tokenStr}");

            request.Headers.Add("ContentType", "application/json;charset=utf-8");

            //添加请求体
            StringContent stringContent = new StringContent(paramStr);

            //将请求体写入日志
            WriteLog("私聊请求体", paramStr, DateTime.Now);

            request.Content = stringContent;

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            //添加请求头
            //client.DefaultRequestHeaders.Clear();

            //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + GetTenant_access_token());

            //client.DefaultRequestHeaders.Add("ContentType", "application/json;charset=utf-8");

            //var postData = new StringContent(System.Text.Json.JsonSerializer.Serialize(param), Encoding.UTF8, "application/json");

            //postData.Headers.Add("Authorization", $"Bearer {GetTenant_access_token()}");

            //postData.Headers.Add("ContentType", "application/json;charset=utf-8");

            //postData.Headers.Add("Authorization", $"Bearer {GetTenant_access_token()}");

            //var response = await client.PostAsync(url + $"?receive_id_type={receive_id_type}", postData);

            if (response.IsSuccessStatusCode)
            {
                var responseStr = await response.Content.ReadAsStringAsync();

                //使用httpClient获取的HttpResponseMessage类型的response，直接对其toString()获取的是请求的响应头，并没有获取响应体的内容
                //var responseHeaders = response.ToString();

                //获取响应的headers
                //var responseHeaders = response.Content.Headers.ToString();

                //获取响应的ContentLength
                //var responseContentLength = response.Content.Headers.ContentLength.ToString();

                return responseStr;
            }
            else
            {
                throw new Exception();
            }
        }


        //私聊获取tenant_access_token（企业自建应用）
        private async Task<string> GetTenant_access_token()
        {
            //获取访问令牌的请求url
            string accessTokenUrl = "https://open.feishu.cn/open-apis/auth/v3/tenant_access_token/internal/";

            //企业自建应用的机器人的app_id和app_secret
            string app_id = "cli_a06415351b3ad00b";

            string app_secret = "lpuqhFbKCteA0nno6p82ybRiPlaHGpbO";

            //请求消息体
            var param = new
            {
                app_id = app_id,

                app_secret = app_secret
            };

            var client = _clientFactory.CreateClient();

            var postData = new StringContent(JsonConvert.SerializeObject(param), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(accessTokenUrl, postData);

            if (response.IsSuccessStatusCode)
            {
                //using var responseStream = await response.Content.ReadAsStreamAsync();
                //var Token = await System.Text.Json.JsonSerializer.DeserializeAsync
                //    <token>(responseStream);
                //return Token.tenant_access_token;

                using var responseStream = await response.Content.ReadAsStreamAsync();

                var Token = await System.Text.Json.JsonSerializer.DeserializeAsync
                    <token>(responseStream);

                return Token.tenant_access_token;
            }
            else
            {
                throw new Exception();
            }
        }

        //获取userid
        public string GetUserId()
        {
            return null;
        }

        /// <summary>  

        /// 写入日志到文本文件  

        /// </summary>  

        /// <param name="action">动作</param>  

        /// <param name="strMessage">日志内容</param>  

        /// <param name="time">时间</param>  

        public static void WriteLog(string action, string strMessage, DateTime time)
        {
            StringBuilder str = new StringBuilder();

            str.Append("Time:    " + time.ToString() + "\r\n");

            str.Append("Action:  " + action + "\r\n");

            str.Append("Message: " + strMessage + "\r\n");

            str.Append("-----------------------------------------------------------\r\n\r\n");

            FileStream fs = new FileStream("RequestAndResponse.log", FileMode.Append, FileAccess.Write);

            StreamWriter sw = new StreamWriter(fs);

            sw.WriteLine(str.ToString());

            sw.Close();

            fs.Close();
        }
    }
}