﻿using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetAdminTestTDC.EF;
using System;
using System.Linq;
using System.Threading.Tasks;
using PeopleModel = NetAdminTestTDC.Models.People;

namespace NetAdminTestTDC.Controllers
{
    public class PeopleController : AdminController<PeopleModel>
    {
        private Context Context => DbContext as Context;

        public PeopleController() : base()
        {
            Title = "债务人";
        }

        /*  列表入口 */
        [HttpPost]
        public override Task<IActionResult> Post(PeopleModel model)
        {
            return Form().Store(model);
        }

        /*  列表数据 */
        protected override BGrid Grid()
        {
            var model = Context.Peoples;
            var grid = new BGrid(model, HttpContext);
            var a = Utils;

            grid.Column(nameof(PeopleModel.ID)).Sortable();
            grid.Column(nameof(PeopleModel.Name));
            grid.Column(nameof(PeopleModel.Job)).Copyable();
            grid.Column(nameof(PeopleModel.Mcount));
            grid.Column(nameof(PeopleModel.LastBorrow));
            grid.Column(nameof(PeopleModel.LastRepay));
            grid.Column(nameof(PeopleModel.CreatedAt));
            grid.Column(nameof(PeopleModel.UpdatedAt));

            grid.Actions(t => t.DisableView());
            grid.DisableExport();
            return grid;
        }

        /*  创建和编辑UI */
        protected override BForm Form()
        {
            var model = Context.Peoples;
            var form = new BForm(model).WithController(this);
            form.Text(nameof(PeopleModel.ID));
            form.Text(nameof(PeopleModel.Name));
            form.Text(nameof(PeopleModel.Job));
            form.Tools(t => t.DisableView());
            form.Saved(() =>
            {
                //Context.Configs.AsNoTracking().ToList().CoverCache(_memoryCache);
                return null;
            });

            return form;
        }

        /*  编辑提交 */
        [HttpPost("{id:int}")]
        public async Task<IActionResult> Update(int id, PeopleModel model)
        {
            var test = await Context.Peoples.SingleAsync(m => m.ID == id);
            test.Job = model.Job;
            test.Name = model.Name;
            return await Form().Update(test);
        }

        /*  详情 */
        protected override BShow Detail(int id)
        {
            var model = Context.Peoples;
            var show = new BShow(model.Single(p => p.ID == id));
            show.Field(nameof(PeopleModel.ID));
            show.Field(nameof(PeopleModel.Name));
            show.Field(nameof(PeopleModel.Job));
            show.Field(nameof(PeopleModel.Mcount));
            show.Field(nameof(PeopleModel.LastBorrow));
            show.Field(nameof(PeopleModel.LastRepay));
            show.Field(nameof(PeopleModel.CreatedAt));
            show.Field(nameof(PeopleModel.UpdatedAt));
            return show;
        }
    }
}