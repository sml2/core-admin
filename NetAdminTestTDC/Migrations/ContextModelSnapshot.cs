﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NetAdminTestTDC.EF;

namespace NetAdminTestTDC.Migrations
{
    [DbContext(typeof(Context))]
    partial class ContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.7");

            modelBuilder.Entity("CoreAdmin.Mvc.Models.MenuModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Icon")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("icon");

                    b.Property<int>("Order")
                        .HasColumnType("INTEGER")
                        .HasColumnName("order");

                    b.Property<int>("ParentID")
                        .HasColumnType("INTEGER")
                        .HasColumnName("parent_id");

                    b.Property<string>("Permission")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("permission");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("title");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("Uri")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("uri");

                    b.HasKey("Id");

                    b.ToTable("Menu");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 230, DateTimeKind.Local).AddTicks(8304),
                            Icon = "fa-bar-chart",
                            Order = 1,
                            ParentID = 0,
                            Title = "Dashboard",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 232, DateTimeKind.Local).AddTicks(3638),
                            Uri = "Home/Index"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 232, DateTimeKind.Local).AddTicks(6356),
                            Icon = "fa-tasks",
                            Order = 2,
                            ParentID = 0,
                            Title = "Admin",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 232, DateTimeKind.Local).AddTicks(6359),
                            Uri = ""
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 232, DateTimeKind.Local).AddTicks(6366),
                            Icon = "fa-users",
                            Order = 3,
                            ParentID = 2,
                            Title = "Users",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 232, DateTimeKind.Local).AddTicks(6366),
                            Uri = "users"
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(3936),
                            Icon = "fa-user",
                            Order = 4,
                            ParentID = 2,
                            Title = "Roles",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(3946),
                            Uri = "roles"
                        },
                        new
                        {
                            Id = 5,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(4012),
                            Icon = "fa-ban",
                            Order = 5,
                            ParentID = 2,
                            Title = "Permission",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(4012),
                            Uri = "permissions"
                        },
                        new
                        {
                            Id = 6,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(4016),
                            Icon = "fa-bars",
                            Order = 6,
                            ParentID = 2,
                            Title = "Menu",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(4017),
                            Uri = "menu"
                        },
                        new
                        {
                            Id = 7,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(4022),
                            Icon = "fa-history",
                            Order = 7,
                            ParentID = 2,
                            Title = "Operation Log",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 366, DateTimeKind.Local).AddTicks(4022),
                            Uri = "operationlog"
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.OperationLog", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<string>("Body")
                        .HasColumnType("TEXT")
                        .HasColumnName("body")
                        .HasComment("可为空,表示没有请求体");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Host")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("host");

                    b.Property<string>("IP")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("ip");

                    b.Property<string>("Method")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("method");

                    b.Property<string>("Path")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("path");

                    b.Property<string>("Query")
                        .HasColumnType("TEXT")
                        .HasColumnName("query");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER")
                        .HasColumnName("user_id");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("username");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("OperationLog");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Permissions", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("HttpMethod")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("http_method");

                    b.Property<string>("HttpPath")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("http_path");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("slug");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("ID");

                    b.HasIndex("Slug", "Name")
                        .IsUnique();

                    b.ToTable("Permissions");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(7642),
                            HttpMethod = "ANY",
                            HttpPath = "/*",
                            Name = "All permission",
                            Slug = "Dashboard",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(7900)
                        },
                        new
                        {
                            ID = 2,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8129),
                            HttpMethod = "GET",
                            HttpPath = "/",
                            Name = "Dashboard",
                            Slug = "Admin",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8131)
                        },
                        new
                        {
                            ID = 3,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8133),
                            HttpMethod = "",
                            HttpPath = "/auth/login,/manage/auth/logout",
                            Name = "Login",
                            Slug = "Users",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8133)
                        },
                        new
                        {
                            ID = 4,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8134),
                            HttpMethod = "GET,PUT",
                            HttpPath = "/auth/setting",
                            Name = "User setting",
                            Slug = "Roles",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8134)
                        },
                        new
                        {
                            ID = 5,
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8135),
                            HttpMethod = "",
                            HttpPath = "/auth/roles,/auth/permissions,/auth/menu,/auth/logs",
                            Name = "Auth management",
                            Slug = "Permission",
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(8136)
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Roles", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("TEXT")
                        .HasColumnName("slug");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex");

                    b.HasIndex("Slug", "Name")
                        .IsUnique();

                    b.ToTable("Roles");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            ConcurrencyStamp = "12b1559f-65bf-45ef-b03c-9107344de450",
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Admin",
                            NormalizedName = "ADMIN",
                            Slug = "Admin",
                            UpdatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("id");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Avatar")
                        .HasColumnType("TEXT")
                        .HasColumnName("avatar");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("created_at");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT")
                        .HasColumnName("name");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("TEXT");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("TEXT");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("TEXT");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("INTEGER");

                    b.Property<string>("RememberToken")
                        .HasColumnType("TEXT")
                        .HasColumnName("remember_token");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("TEXT");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT")
                        .HasColumnName("updated_at");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("TEXT")
                        .HasColumnName("username");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex");

                    b.HasIndex("UserName")
                        .IsUnique();

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AccessFailedCount = 0,
                            Avatar = "/_content/CoreAdmin.RCL/assets/AdminLTE/dist/img/user2-160x160.jpg",
                            ConcurrencyStamp = "791297fa-9f53-4ccd-94f3-2b5265a0725b",
                            CreatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(9091),
                            Email = "admin@ca.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            Name = "Administrator",
                            PhoneNumberConfirmed = false,
                            TwoFactorEnabled = false,
                            UpdatedAt = new DateTime(2021, 7, 15, 18, 50, 35, 367, DateTimeKind.Local).AddTicks(9093),
                            UserName = "admin"
                        });
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserRole", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RoleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("MenuModelRoles", b =>
                {
                    b.Property<int>("MenuId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RolesId")
                        .HasColumnType("INTEGER");

                    b.HasKey("MenuId", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("MenuModelRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimType")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("TEXT");

                    b.Property<int>("RoleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimType")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Value")
                        .HasColumnType("TEXT");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("NetAdminTestTDC.Models.Detail", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<decimal>("Account")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT");

                    b.Property<int?>("PeopleID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Pid")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("PeopleID");

                    b.ToTable("Details");
                });

            modelBuilder.Entity("NetAdminTestTDC.Models.People", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("TEXT");

                    b.Property<string>("Job")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastBorrow")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastRepay")
                        .HasColumnType("TEXT");

                    b.Property<string>("Mcount")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("Peoples");
                });

            modelBuilder.Entity("PermissionsRoles", b =>
                {
                    b.Property<int>("PermissionsID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("RolesId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionsID", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("PermissionsRoles");
                });

            modelBuilder.Entity("PermissionsUserModel", b =>
                {
                    b.Property<int>("PermissionsID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("UserModelId")
                        .HasColumnType("INTEGER");

                    b.HasKey("PermissionsID", "UserModelId");

                    b.HasIndex("UserModelId");

                    b.ToTable("PermissionsUserModel");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserRole", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Roles", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("User");
                });

            modelBuilder.Entity("MenuModelRoles", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.MenuModel", null)
                        .WithMany()
                        .HasForeignKey("MenuId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("NetAdminTestTDC.Models.Detail", b =>
                {
                    b.HasOne("NetAdminTestTDC.Models.People", "People")
                        .WithMany("Details")
                        .HasForeignKey("PeopleID");

                    b.Navigation("People");
                });

            modelBuilder.Entity("PermissionsRoles", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Permissions", null)
                        .WithMany()
                        .HasForeignKey("PermissionsID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.Roles", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PermissionsUserModel", b =>
                {
                    b.HasOne("CoreAdmin.Mvc.Models.Permissions", null)
                        .WithMany()
                        .HasForeignKey("PermissionsID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CoreAdmin.Mvc.Models.UserModel", null)
                        .WithMany()
                        .HasForeignKey("UserModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.Roles", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("CoreAdmin.Mvc.Models.UserModel", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("NetAdminTestTDC.Models.People", b =>
                {
                    b.Navigation("Details");
                });
#pragma warning restore 612, 618
        }
    }
}
