﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetAdminTestTDC.Models
{
    /// <summary>
    /// 账单明细
    /// </summary>
    public class Detail
    {
        [Display(Name = "编号")]
        public int ID { get; set; }
        public int Pid { get; set; }
        [Display(Name = "所属人")]
        public People People { get; set; }

        public Decimal Account { get; set; }

        [Display(Name = "创建时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Display(Name = "更新时间")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
