﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAdmin.Mvc.Models;

namespace NetAdminTestTDC.Models
{
    /// <summary>
    /// 人员
    /// </summary>
    public class People : BaseModel
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "名称")]

        public string Name { get; set; }

        [Display(Name = "工作")]

        public string Job { get; set; }

        [Display(Name = "总金额")]

        public string Mcount { get; set; }

        [Display(Name = "最后还款")]
        public DateTime LastRepay { get; set; } = DateTime.Now;

        [Display(Name = "最后借款")]
        public DateTime LastBorrow { get; set; } = DateTime.Now;

        [Display(Name = "创建时间")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Display(Name = "更新时间")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;


        [Display(Name = "账单明细")]
        public List<Detail> Details { get; set; }
    }
}
