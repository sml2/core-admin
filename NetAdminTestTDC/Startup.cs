using CoreAdmin.EF;
using CoreAdmin.Mvc;
using CoreAdmin.Mvc.Configure;
using CoreAdmin.Mvc.Middleware;
using CoreAdmin.Mvc.Models;
using NetAdminTestTDC.EF;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Hosting.StaticWebAssets;

namespace NetAdminTestTDC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region 数据库

            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new EFLoggerProvider());
            //需要指定AdminDbContext,要不会注入失败 
            services.AddDbContext<AdminDbContext, Context>(r =>
                r.UseSqlite("Data Source=Data/Data.db").UseLoggerFactory(loggerFactory));

            #endregion
            services.AddMvc();
            services.AddControllersWithViews();

            #region 授权认证

            services.AddIdentity<UserModel, Roles>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<AdminDbContext>();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequiredLength = 8;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
                options.SignIn.RequireConfirmedAccount = false;
                // options.User.
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(5);

                options.LoginPath = "/login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            #endregion

            services.UseCoreAdmin(Configuration);
            services.AddMemoryCache();
            services.AddHttpClient();
        }

        private void SetupDatabase(IApplicationBuilder app)
        {
            //Microsoft.Extensions.Logging.Logger<>
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<AdminDbContext>() as Context;
                //Migate any pending changes: 
                context.Database.Migrate();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<CoreAdminConfigure> options,
            RoleManager<Roles> roleManager, UserManager<UserModel> userManager)
        {
            SetupDatabase(app);

            CreateRolesAndUsers(roleManager, userManager).Wait();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            if (!env.IsProduction()) StaticWebAssetsLoader.UseStaticWebAssets(env, Configuration);

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCoreAdminMiddleware(env, Configuration);

            app.UseSession();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("TestGet",
                    async (c) =>
                        await c.Response.BodyWriter.WriteAsync(System.Text.Encoding.UTF8.GetBytes("HelloWorld")));
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Privacy}/{id?}");

                endpoints.MapFallbackToController(
                    nameof(CoreAdmin.Mvc.Controllers.HomeController.Privacy),
                    nameof(CoreAdmin.Mvc.Controllers.HomeController).Replace("Controller", string.Empty));
            });
            //CoreAdmin.Mvc.Controllers.AuthController.DefaultLoginSuccessPath = "/config";
        }

        // 创建管理员角色和超级用户    
        private async Task CreateRolesAndUsers(RoleManager<Roles> roleManager, UserManager<UserModel> userManager)
        {
            // 判断管理员角色是否存在     
            var res = await roleManager.RoleExistsAsync("Admin");
            if (res)
            {
                var user = await userManager.Users.FirstAsync(u => u.Id == 1);
                if (string.IsNullOrEmpty(user.PasswordHash))
                {
                    userManager.Options.Password.RequireLowercase = false;
                    userManager.Options.Password.RequiredLength = 6;

                    const string userPwd = "123456";
                    var a = await userManager.AddPasswordAsync(user, userPwd);
                    await userManager.AddToRoleAsync(user, "Admin");

                    userManager.Options.Password.RequireLowercase = true;
                    userManager.Options.Password.RequiredLength = 8;
                }
            }
        }
    }
}